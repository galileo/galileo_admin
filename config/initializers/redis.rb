# frozen_string_literal: true

Redis.current = if Rails.env.test?
                  MockRedis.new
                else
                  Redis.new(url: Rails.application.credentials[:redis][:url][Rails.env.to_sym],
                            db: Rails.application.credentials[:redis][:ip_db][Rails.env.to_sym],
                            driver: :hiredis,
                            reconnect_attempts: 5,
                            reconnect_delay: 1.0,
                            reconnect_delay_max: 10.0)
                end
