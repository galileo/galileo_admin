# frozen_string_literal: true

require 'pagy/extras/bootstrap'
require 'pagy/extras/limit' # works without further configuration
PAGE_SIZES = [25, 50, 100, 250]

# set the global default per page to 25
Pagy::DEFAULT[:limit]  = 25
Pagy::DEFAULT[:limit_param] = :limit
Pagy::DEFAULT[:limit_max]   = 250      # default

# Better user experience handled automatically
Pagy::DEFAULT.freeze


