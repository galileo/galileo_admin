 module Sunspot
    module Query
      module Restriction
        class StringContains < Base
          def to_positive_boolean_phrase
            if @value.nil?
              "#{Util.escape(@field.indexed_name)}:*"
            else
              %Q(#{Util.escape(@field.indexed_name)}:*#{Util.escape @value}*)
            end
          end

        end
      end
    end
 end

 Sunspot::DSL::Restriction.define_method(:string_contains) do |*value|
   @scope.add_restriction(@negative, @field, Sunspot::Query::Restriction::StringContains, *value)
 end