# frozen_string_literal: true

Rails.application.routes.draw do # establish distinct root for authenticated users
    # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  authenticated do
    root to: 'redirect#redirect', as: :authenticated_root
  end

  # root for unauthenticated users
  root to: redirect('auth/sign_in')

  # scope devise routes under /auth to avoid conflict with User CRUD
  devise_for :users, path: 'auth', controllers: {
    invitations: 'invitations'
  }
  concern :versionable do
    member do
      get :diff
      get :rollback
    end
  end

  devise_scope :user do
    get 'auth/invitations', to: 'invitations#index'
  end

  namespace :api do
    namespace :v1 do
      constraints(format: :json) do
        get 'ok', to: 'base#ok'
        get 'inst', to: 'auth_support#inst'
      end
    end
  end

  get 'ez_proxy_update', to: 'ez_proxy_update#report'
  get 'ez_proxy_update/:inst_code', to: 'ez_proxy_update#report'

  get 'institutions_for_stats', to: 'institutions_for_stats#report'
  get 'resources_for_stats', to: 'resources_for_stats#report'
  get 'vendors_for_stats', to: 'vendors_for_stats#report'
  get 'vendor_statistics_identifier_for_stats/:resource_code', to: 'vendor_statistics_identifier_for_stats#report', as: 'vendor_statistics_identifier_for_stats'

  resources :institutions, concerns: :versionable do
    resources :resources, controller: 'institution_resources', only: :index, as: 'resources'
    resources :sites, controller: 'institution_sites', except: [:destroy, :new, :create], as: 'sites'
    resources :brandings
    resources :features
    resources :widgets
    resources :bento_configs do
      member do
        get :test_config
        get :eds_content_providers
      end
      collection do
        get '/deleted', to: 'bento_versions#deleted', defaults: {item_type: 'bento_config'}
      end
    end
    resources :configured_bentos do
      collection do
        post :restore_defaults
        get '/view/(:user_view)', to: 'configured_bentos#reorder', as: 'reorder'
        post '/view/:user_view', to: 'configured_bentos#save_reorder', as: 'save_reorder'
        post '/view/:user_view/restore_defaults', to: 'configured_bentos#restore_defaults_for_view', as: 'restore_defaults_for_view'
        get '/deleted', to: 'bento_versions#deleted', defaults: {item_type: 'configured_bento'}
      end
    end
    collection do
      get '/deleted', to: 'versions#deleted', defaults: {item_type: 'institution'}
    end
    member do
      get :new_new_password
      get :update_password_asap
      get :notify_current_password
      get :notify_new_password
      get :express_links_report
      get :resources_report
      get :glri_resources_report
      get :institution_serial_count_report
      get :edit_institutional
      patch :update_institutional
    end
  end

  resources :sites

  resources :resources, concerns: :versionable do
    collection do
      get '/deleted', to: 'versions#deleted', defaults: {item_type: 'resource'}
    end
    member do
      get :copy
    end
  end

  resources :contacts, concerns: :versionable do
    collection do
      get '/deleted', to: 'versions#deleted', defaults: {item_type: 'contact'}
      delete :bulk_destroy
      get :bulk_destroy_confirm
    end
    member do
      get :notify_current_password
    end
  end

  resources :users do
    collection do
      get '/deleted', to: 'users#deleted'
    end
    post :undelete, on: :member
  end

  resources :institution_groups do
    resources :features
  end

  resources :users, :vendors, :passwords, :notify_groups, :formats, :subjects, :user_views

  resources :allocations, only: %i[index show edit update]
  resources :versions, only: %i[index show] do
    collection do
      get '/:item_type/deleted', to: 'versions#deleted', as: 'deleted'
    end
    member do
      post :undelete
    end
  end

  resources :reports, only: %i[index] do
    collection do
      get :resources_with_html
      get :resources_url_check
      get :institution_group_ips
      get :institution_resources
      get :institution_glri_resources
      get :institution_express_links
      get :glri_contacts
      get :non_glri_institutions
      get :ez_proxy_update
      get :public_libraries_primary_contacts
      get :vendor_resources
      get :allocations_vendor
      get :allocations_institution
      get :allocations_resource
      get :vendors_for_stats
      get :resources_for_stats
      get :institutions_for_stats
      get :vendor_statistics_identifier_for_stats
      get :glri_allocations
      get :inst_user_emails
      get :all_rows_for
      get :overlapping_ip_addresses
      get :institution_features
      get :institution_group_features
      get :institution_group_contacts
      get :galileo_passwords
      get :resource_keywords
      get :wayfinder_keywords
      get :deleted_contacts
      get :stats_for_snapshots
    end
  end

  resources :tools, only: %i[index] do
    collection do
      get :csv_import, to: 'tools#csv_import_tool'
      put :csv_import, to: 'tools#csv_import'
      get :ip_cache_status, to: 'tools#ip_cache_status_tool'
      put :ip_cache_status, to: 'tools#ip_cache_status'
    end
  end

  get 'bentos', to: 'template_view_bentos#index'
  get 'bento_templates', to: 'template_view_bentos#templates'

  if FeatureFlags.enabled? :flexible_bento_templates
    get 'bento_template/:from/fork', to: 'template_view_bentos#fork_template_form', as: 'fork_bento_template_form'
    post 'bento_template/:from/fork', to: 'template_view_bentos#fork_template', as: 'fork_bento_template'
  end

  resources :template_view_bentos do
    collection do
      get '/deleted', to: 'bento_versions#deleted', defaults: {item_type: 'template_view_bento'}
    end
  end
  resources :predefined_bentos do
    collection do
      get '/deleted', to: 'bento_versions#deleted', defaults: {item_type: 'predefined_bento'}
    end
  end
  resources :configured_bentos, only:[:show] do
    collection do
      get '/deleted', to: 'bento_versions#deleted', defaults: {item_type: 'configured_bento'}
    end
  end

  resources :bento_configs, only:[:show] do
    collection do
      get '/deleted', to: 'bento_versions#deleted', defaults: {item_type: 'bento_config'}
    end
  end

  resources :bento_versions, only: %i[index show] do
    collection do
      get '/:item_type/deleted', to: 'bento_versions#deleted', as: 'deleted'
    end
    member do
      post :undelete
    end
  end

  resources :banners, concerns: :versionable do
    collection do
      get '/deleted', to: 'versions#deleted', defaults: {item_type: 'banner'}
    end
    member do
      post :undelete
    end
  end

  namespace :stats do
    get '/', to: 'stats#index', as: :index
    delete ':period/delete_all/:table_name', to: 'stats#delete_all', as: :delete_all

    get 'daily/index/:table_name', to: 'daily#index', as: :daily_index
    get 'daily/new_accumulate_range/:table_name', to: 'daily#new_accumulate_range', as: :daily_new_accumulate_range
    post 'daily/accumulate_range/:table_name', to: 'daily#accumulate_range', as: :daily_accumulate_range
    get 'daily/new_delete_range/:table_name', to: 'daily#new_delete_range', as: :daily_new_delete_range
    delete 'daily/delete_range/:table_name', to: 'daily#delete_range', as: :daily_delete_range

    get 'monthly/index/:table_name', to: 'monthly#index', as: :monthly_index
    get 'monthly/new_accumulate/:table_name', to: 'monthly#new_accumulate', as: :monthly_new_accumulate
    post 'monthly/accumulate/:table_name', to: 'monthly#accumulate', as: :monthly_accumulate
    get 'monthly/new_delete_range/:table_name', to: 'monthly#new_delete_range', as: :monthly_new_delete_range
    delete 'monthly/delete_range/:table_name', to: 'monthly#delete_range', as: :monthly_delete_range

    #Problems putting these routes in the raw namespace
    get 'raw/index/:table_name', to: 'raw#index', as: :raw_index
    get 'raw/new_import/:table_name', to: 'raw#new_import', as: :raw_new_import
    post 'raw/import/:table_name', to: 'raw#import', as: :raw_import
    get 'raw/new_delete_range/:table_name', to: 'raw#new_delete_range', as: :raw_new_delete_range
    delete 'raw/delete_range/:table_name', to: 'raw#delete_range', as: :raw_delete_range
  end
end
