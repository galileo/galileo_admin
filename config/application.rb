require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module GalileoAdmin
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.

    config.time_zone = 'Eastern Time (US & Canada)'
    config.eager_load_paths << Rails.root.join('lib')
  end
end

# use .webmanifest extension instead of default behavior of appending .txt extension
# referenced from https://github.com/RealFaviconGenerator/rails_real_favicon/issues/35
# https://github.com/RealFaviconGenerator/rails_real_favicon/issues/26
Rails.application.config.assets.configure do |env|
  env.register_mime_type('application/manifest+json', extensions: ['.webmanifest', '.webmanifest.erb'])
  # Needs to be preprocessed
  env.register_preprocessor('application/manifest+json', Sprockets::ERBProcessor)
end
