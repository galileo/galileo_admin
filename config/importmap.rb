# Pin npm packages by running ./bin/importmap

pin "application"
pin "@hotwired/stimulus", to: "stimulus.min.js"
pin "@hotwired/stimulus-loading", to: "stimulus-loading.js"
pin_all_from "app/javascript/controllers", under: "controllers"
pin "@hotwired/turbo-rails", to: "turbo.min.js"
pin "trix", to: "https://unpkg.com/trix@2.0.8/dist/trix.umd.min.js"
pin "@rails/actiontext", to: "actiontext.js"
