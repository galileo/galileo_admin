# Changelog
All notable changes to the GALILEO Admin project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [20250221] (Sprint D25)
### Changed
- Update net-imap (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1238)

## [20250207] (Sprint C25)
### Changed
- Fix for allocations index page (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1235)

## [20250116] (Sprint A25)
### Changed
- Don't allow inst users to edit bento service credentials (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1050)
- Stats: rename RawStat to Raw (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1039)

## [20241101] (Sprint V24)
### Changed
- Update Rails (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1223)
### Staging
- Add per page control to stats tables (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1027)
- Raw stats views (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1028)
- Fix stat import local directories (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1029)
- Stat uniqueness validators (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1030)
- Front-end stats loader (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1031)
- Raw Stats Delete Range (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1032)
- Daily stats aggrgator UI (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1033)
- Raw Stat Timezones (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1034)
- Bug with RawSearchStat bug (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1222)

## [20241018] (Sprint U24)
### Changed Staging
- Refactor existing stats code (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1023)
- Clean up stats routes (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1210)
- Add search stat views (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1025)

## [20241004] (Sprint T24)
### Changed
- Make Prod push step in gitlab a step process instead of 2 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1020)
- Add note for OpenAthens Link Generator Tool for GLRI records. (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1021)
- Allocation Logo Improvements (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1167)
- Feature flags for logo improvements (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1208)


## [20240906] (Sprint R24)
### Changed
- Add toggle to suppress openathens redirector (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1013)
- Add view constraint for banners (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1014)


## [20240726] (Sprint O24)
### Changed
- Version item_name updates (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1191)
- Turn off deprecation warnings (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1192)
- Remove deprecation gem setting (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1193)
- Turn on view switching in prod (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1010)

## [20240628] (Sprint M24)
### Changed
- Move Widgets to Portal subability from default inst user (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1003)

### Staging
- Add a switch at inst level to turn Max View on/off (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/999)
- limit_view_switching bug that kept real value from being selected on edit (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1183)

## [20240614] (Sprint L24)
### Changed
- Turn widgets on in production (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1002)
- Chat Widget - Allow for pasting full URL or ID (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/998)
- Help text for Widget credentials (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1173)
- Improve script for setting up development environment from scratch (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/1001)
- Allow inst users to setup widgets (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1177)
- Update Rails (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1174, https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1178)
- Add import maps, turbo, and stimulus (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1180)


## [20240517] (Sprint J24)
### Changed
- Allocations sort bug (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1169)
- Update nokogiri (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1170)

## [20240506] (Sprint Mid J24)
### Changed
- Turn flexible bento templates on in production (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1166)
- Remove Features from Solr Institutions (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/992)
- Fix seed data bug (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1164)

## [20240419] (Sprint H24)
### Changed
- Solr Table for Features (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/987)
- Assigned subroles when inviting institution user are now carried over after they register. (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/994)
- Merge prod back to master after hot fix: Assigned subroles when inviting institution user are now carried over after they register (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1160)
- More flexibility for Default Bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/912)
- Allow forking a bento template without assigning any institutions (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1162)

## [20240405] (Sprint G24)
### Changed
- Rails 7.0 upgrade (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/985)
- Fix Data Importer (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1145)
- Add "Native EDS Profile" option to bento service credentials (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1149)
- Allow creation of one-off bentos with non-default EDS profile (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1151)
### Added Staging
- Add Display Name field for Chat widget (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/982)

## [20240223] (Sprint D24)
### Changed
- Banner for Inst Groups (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/979)
- Add "OpenAthens Enabled?" field to allocations CSV (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/981)
- Update devise_invitable (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1140)

## [20240209] (Sprint C24)
### Changed
- Hide contact multi-delete from helpdesk (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/977)

## [20240126] (Sprint B24)
### Added Staging
- Add ability for institutions to add a librarian chat widget (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/836)

### Changed
- Bento versions index fails when filtering by date (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1116)
- Allow Inst users to create Banners (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/966)
- Sprint Y23 Prod push (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/971)
- Update IP report to include active selector (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/973)
- Bulk destroy contacts (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1121)
- Update puma (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1122)
- Contact Delete Confirm page (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1123)
- Banner help message (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1125)
- Fix checkbox autofill bug (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1126)
- Add banner message note (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/974)

## [20231215] (Sprint Y23)
### Changed
- Bento versions index fails when filtering by date (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1116)
- Allow Inst users to create Banners (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/966)

## [20231115] (Sprint Mid-W23)
### Changed
- Link users to contacts (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1095)
- Generalized report utility (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1098)
- Add "Portal" contact type (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1099)
- Add gen type to adv Contacts (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1100)
- Update to contact types (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1101)
- Fix version bug (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1102)
- Enhance "Custom EDS Resource" bento to show available EDS databases in a dropdown within GALILEO Admin instead of having find database names in EDS manually. (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/962)
- Update postgres version in development environment (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/965)
- Add banner to GALILEO Admin (prod) for maintenance on 11/18 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/967)
- Add outage banner to sign in page (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1110)

### Staging
- Allow portal users to update Inst/Sites (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1108)
- Subroles Bug (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1104)
- Subroles (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1105)

## [20231103] (Sprint V23)
### Changed
- Add created_by to Banners (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1091)
- Update sites with DOE school codes (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/963)

## [20230908] (Sprint R23)
### Changed
- Add more conditions to valid_date_range in Banner (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1087)

## [20230825] (Sprint Q23)
### Changed
- Banner Prototype (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1071)
- Give GALILEO Banners better values in UI (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1081)
- Fix GALILEO spelling (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1082)
- Filteriffic Banners (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1083)
- Add Versionable to Banners (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1084)

## [20230811] (Sprint P23)
### Changed
- Extend advanced filter to support filtering by allocation (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1077)

## [20230714] (Sprint N23)
### Added
- Advanced institution filtering for several related classes (Contacts, GLRI Resources, etc) (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1072)

### Changed
- Update Rails to 6.1.7.4 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1073)

### Fixed
- "We're sorry, but something went wrong." error when editing institution record as institutional user (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/949)

## [20230602] (Sprint K23)
### Changed
- Add "Previous Password" field to institution edit form (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/947)

## [20230519] (Sprint J23)
### Changed
- Add Download CSV button to Resources (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/943)
- CSV button for Contacts and Institutions (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1065)
- Add missing pagy bootstrap nav (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1066)
- Uniform index pages (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1067)

## [20230505] (Sprint I23)
### Changed
- Expand error message for password updates (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/942)
- Allow hiding "Peer Reviewed" and "Library Catalog" limiter on eds_resource and eds_custom bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/938)


## [20230407] (Sprint G23)
### Added
- Add Institutional Branding  Option for core GALILEO resource allocations (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/394)
- Allow Institutions to change the display name of core resources (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/896)

### Changed
- Remove "scope" option from Primo bento service credentials (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/935)
- Encode EDS search query in URL when encoding link for OA Proxy Workaround (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/934)
- Fix too new js (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1046)



## [20230324] (Sprint F23)
### Staging
- Allow Institutions to change the display name of core resources (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/896)

### Changed
- Minor updates to rails and rack (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1038)

## [20230224] (Sprint D23)
### Added
- Options to suppress EDS limiters at the Bento Type level (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/928)

### Changed
- Update EDS Limiter options text. (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1035)

### Fixed
- Receiving "something went wrong" error when trying to run Resources Report for an institution in prod/staging. (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/929)
- Contacts report cannot fails on contacts without an institution (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/930)


## [20230131] (Sprint B23)
### Changed
- Upgrade Rails to latest 6.1.x patch version (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/926)

## [20230113] (Sprint A23)
### Changed
- Bento Version Undelete (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/924)
- Update Sassc (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1025)
- Update HTTParty (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1028)
- Accommodate new response format for EDS auth token API (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1029)

## [20221216] (Sprint Y22)
### Added
- Include bento customizations in history (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/834)

### Changed
- Add support for CRDL API (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/921)
- Update nokogiri to 1.13.10 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1021)
- Update html sanitizer gem (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1022)
- Make DLG and CRDL bentos generally available (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/922)

## [20221202] (Sprint X22)
### Changed
- Powernotes - different link for academic insts (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/917)
- Add OpenAthens Org ID to Institutions IPs/OA and Institution Group IPs/OA Reports (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/918)

### Fixed
- Error when notifying individual contact of current GALILEO password for Georgia Christian School (psgc) (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/905)


## [20221118] (Sprint W22)
### Added
- Add `vendor_statistics_identifier` to allocation (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/914)
- Let admins create instances of the forthcoming DLG bento (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1015)

### Changed
- Try to auto-fire up the webserver on CI/CD deploys (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/915)
- Small Sentry changes (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1014)

## [20221021] (Sprint U22)
### Added
- Implement stats accumulation from GALILEO Search (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/897)
- Add sentry (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1004)

### Changed
- Sentry Environments (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1006)
- Update Sentry environment keys (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/1007)

## [20221007] (Sprint T22)
### Added
- Implement stats accumulation from GALILEO Search (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/897)
- Sites Report/CSV loader (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/873)
- Ability to search or  limit deleted contacts (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/624)

### Changed
- A way to reindex rows at a given id (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/899)
- Add search to Allocations (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/901)
- Add Search to institution resources (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/902)
- Add support for DLG bento (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/900)
- Site index page fix (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/996)
- Add missing menu highlights (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/903)


## [20220923] (Sprint S22)
### Added
- Implement stats accumulation from GALILEO Search (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/897)

### Changed
- Give users with the Helpdesk role the ability to send password notification emails to individual contacts (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/887)


## [20220909] (Sprint R22)
### Added
- Create  Stats Database (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/882)

### Changed
- 20220906 sn allocations csv (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/974)
- Remove community costshare from subscription types (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/976)
- Institution Resources CSV button (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/977)
- Create  Stats Database (In Progress) (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/882)


## [20220907] (Mid-Sprint R22)
### Added
- Allocations Tab (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/891)

### Changed
- Add OpenAthens status to institution overview page (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/888)
- Change staging log_level to info (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/968)
- Add Display status to Resource overview page (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/890)
- Reduce padding in sidebar (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/892)

### Fixed
- Allocation filter fixes (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/972)
- Receiving "Something went wrong" error when accessing an institution's bento area (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/893)

# [20220826] (Sprint Q22)
### Added
- Link to  OpenAthens test Link Generation Tool (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/263)
- Identifying Subscription Type for Allocations (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/88)

### Changed
- Rails upgrade to 6.1 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/883)
- Improve some filterrific sorting (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/868)
- Remove unsupported values from populate_subscription_types (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/964)

### Fixed
- Institution sites overview page is showing two edit buttons. (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/876)

## [20220803] (Mid-Sprint P22)
### Added
- Add Open Athens API Name field to institution (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/880)

## [20220727] (Sprint O22)
### Changed
- update tzinfo (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/945)
- Fix inst ip issue when institution becomes active (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/946)
- Hardcode api name for AUC (emergency) (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/879)

### Fixed
- Cut down on unnecessary data transfer from Postgres when loading resource edit page (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/875)
- When validating IPs, don't compare to inactive institutions (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/863)

## [20220715] (Sprint N22)
### Added
- Ability to search sites (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/616)

### Changed
- Allow individual password notifications from Contacts page (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/833)
- Update diffy to 3.4.2 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/941)
- Update rails to 6.0.5 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/942)

## [20220701] (Sprint M22)
### Added
- Ability to add contacts to vendor records (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/869)

### Changed
- Update jmespath (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/871)


## [20220506] (Sprint I22)
- Update rails to 6.0.4.8 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/866)

## [20220422] (Sprint H22)
### Added
- Create a mechanism to have a Bento Type specify the name of the Service Credentials it should be paired with (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/860)

### Changed
- Make open_athens_subscope an array in the insitution model (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/859)
- Update nokogiri to 1.13.4 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/862)
- Data import - add Migration step (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/865)

## [20220408] (Sprint G22)
### Changed
- Allow multiple IPs in credentials for stats reports (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/858)
- Update puma to ~> 4.3.12 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/861)

## [20220325] (Sprint F22)
### Added
- Option to specify service credentials as "default for view" (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/845)
- Option to make custom EDS bentos behave like "Encyclopedia" (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/855)
### Changed
- Make CSV importer function with multiple EDS service credentials for same inst/view (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/846)

## [20220308] (Mid-Sprint E22)
### Changed
- Docker environments for development (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/841, https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/847, https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/849, https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/850, https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/851)
- Update image_processing gem (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/848)
- Primo service credentials: replace "Record Link Override" with "Primo Subdomain" (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/853)
- Update rails and nokogumbo (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/854)

## [20220225] (Sprint D22)
### Changed
- Update rails, puma, nokogiri (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/844)

## [20220211] (Sprint C22)
### Changed
- Bugfix: Testing EDS credentials fails for newly-created Service Credentials (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/839)
- Change logging level in staging and prod to warn (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/840)
- upgrade to ruby 2.7.5 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/832)

## [20220128] (Sprint B22)
### Changed
- Fix Firefox-specific errors in drag-and-drop view that resulted in a message for the format "Unable to connect: An error occurred during a connection to 0.0.x.x.". (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/837)
- Firefox-compatible styles for placeholder in bento drag-and-drop view (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/901)
- Workaround for Safari-specific behavior that doesn't hide unavailable bento type option from new bento form (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/900)
- Fix issue that prevented creation of new bentos that don't require service credentials (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/900)

## [20220114] (Sprint A22)
### Added
- Custom "No results..." message (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/828)

## [20220107] (Mid-Sprint A22)
### Added
- Add EDS gem (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/821)
- Allow "eds_resource" bentos to use multiple DBs (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/829)

### Changed
- Add research_starters to the "special bento" list (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/823)
- Be more consistent about validating bento "service field prerequisites" (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/824)
- Update specs to use solr 8.11.1 (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/825)
- Auto-populate description on new bento form; don't default to the first bento type. (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/826)
- ConfiguredBento specs (56% coverage) (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/812)
- Code cleanup for service and "service field prerequisites" validations (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/827)

## [20211220] (Sprint Z21)
### Added
- Institution-user management of bentos (from staging, https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/801)
- PredefinedBento specs (85% coverage) (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/814)
- BentoConfig specs (97% coverage) (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/811)

### Changed
- Suppress warning when custom catalog bento is skipped when restoring defaults (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/819)

## [20211216] (Sprint Y21)
### Added
- Add Service credential field to the edit page for Galileo created Bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/792)
- Give access to allocation K-12 view management to institutional users (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/803)
- Drag and drop view: Show confirmation when saving changes that include deletions. (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/810)
- Add Bento testing tasks to testing issue template (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/815)

### Changed
- Error when downloading BentoConfigs/Service Credentials. (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/802)
- Update Inst user Bento experience (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/801)
- Speed up restore of default configured bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/862)
- Fixes and improvements to BentoConfigs (aka Service Credentials) CSV tools (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/804)
- New Bento Form does not carry over view (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/805)
- Make Database Name a required field for EDS Custom Resource Bento (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/806)
- Update rails to 6.0.4.3 (from 6.0.4.1) (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/816)
- Updated New/Edit Bento Field Notes (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/808)
- Update helper text fot Bento form (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/873)
- Improvements to drag and drop view (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/809)

## [20211203] (Sprint X21)
### Added
- Predefined Bento: Publication finder API profile field (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/766)
- Add Template_id to ConfiguredBentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/782)
- Add help text to default_order and remove delete button from configured_bentos... (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/845)

### Changed
- Update Bento name in UI (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/784)

## [20211119] (Sprint W21)

### Added
- Add Breadcrumbs (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/763)
- Add breadcrumbs for InstitutionGroups (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/830)
- Support for institution-customizable bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/768)
- Support for entering Primo credentials (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/765)

### Changed
- Add ConfiguredBentos to reindex_all (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/824)
- Change 'elem' to 'elementary' (etc.) in Site type (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/626)
- Fix ConfiguredBentos filter issues (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/826)
- Handle deleting predefined bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/771)
- 20211110 sn deleted formats (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/832)
- Hide the admin side of bento config from inst users (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/770)
- Fixed: When a subject is changed/deleted, all its resources' allocations need to be reindexed (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/767)
- Display default display name and description of predefined bento when adding new configured bento (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/777)
- Alphabetize Predefined Bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/772)
- Hide slug for institutional users and generate based on Display Name (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/773)

## [20211104] (Sprint V21)

### Added
- Bento menu item (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/741)
- create configured bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/733)
- Seed predefined_bentos table (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/751)
- Fix PredefinedBento service menu (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/804)
- Add JSON credentials to bento_configs (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/739)
- Seed template_view_bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/753)
- Add filterriffic to template/predefined bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/752)
- Configured Bentos UI refinements (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/755)
- Predefined bentos changes (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/754)
- Restore default bentos for institution (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/750)
- Configured_Bentos add to solr (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/747)
- Allow for institutional customization of Bento boxes (configs, positions, etc.) (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/756)
- Make description and display_name in configured_bento overwrite versions in predefined bento only if not nil (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/757)
- Speed up "Restore Defaults" and reindex configured bentos when modifying predefined bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/815)
- ConfiguredBentos - add bento_configs, preconfigured_bentos to filteriffic (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/759)
- Add Sort to config_bentos index (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/761)
- Fix restore_defauts rake task (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/762)
- Fix "Restore Defaults" error when template is empty (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/760)
- Filter dropdowns (e.g., for configured_bento.bento_config) (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/758)

## [20211021] (Sprint U21)

### Added
- Update bento_configs (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/731)
- Change 'elem' to 'elementary' (etc.) in Site type (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/626)
- Create predefined_bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/732)
- Create configured_bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/733)
- Update bento_config name by default (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/744)
- Create institution_view_bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/734)
- Create template_view_bentos (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/735)

## [20211012] (Sprint Mid-U21)

### Changed
 - Allocation/Institution Solr Tab (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/742)

## [20211008] (Sprint T21)

### Changed
 - Data import - drop all tables right before import data (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/736)


## [20210924] (Sprint S21)

### Changed
  - Remove extra spaces for text string fields for Resources (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/671)
  - Rename Bento to BentoConfigs (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/724)

## [20210910] (Sprint R21)

### Added
  - Add Serial Count Report (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/720)
### Staging
  - Implement user view limiters (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/621)
  - Remove user view "subjects" and add user view fields to resources (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/721)

## [20210827] (Sprint Q21)

### Changed
- Allocations not appearing on subject show page for allocation subjects (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/719)
- Remove user view “subjects” and add user view fields to resources (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/712) 

## [20210813] (Sprint P21)

### Changed
- Add product_suite to Institution Allocation report (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/714)
- Remove INDB table and route (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/692)

## [20210730] (Sprint O21)

### Changed
Add solr subject sort field to allocations (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/753)

## [20210723] (Sprint N21 Hotfix)

### Fixed

- Keywords added by institution not searchable (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/710)

### Changed

- Update Resource Long Description Note (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/704)
- Update jquery gem (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/708)

## [20210716] (Sprint N21)

### Fixed

- Update addressable
  (https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/705)

## [20210702] (Sprint M21)

### Added
- Report of deleted contacts
 https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/700

### Changed
- Properly handle inactive institutions with respect to institutional users https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/694
- Allocation marked as having override URL when notes https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/582
- Remove default from view type menus for public libraries https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/698

## [20210621] (Sprint L21)

### Added

- Testing Template https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/693

- Allow institutional users edit powernotes/zoterobib booleans.  https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/695

### Changed

- wayfinder_keywords to array https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/696

## [20210603] (Sprint K21)

### Added

- Add Zotero to Tools https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/687
- In Tools section, allow institutions to exclude links https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/656
- Ability to add additional keywords to resource allocations for GALILEO central resources.  https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/669
- Add Fields to Resources https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/679

### Changed

- Password report missing test institutions https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/689
- Problem removing IPs from redis https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/686
- Nightly password change not indexed https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/685
- Update keyword field note for resources https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/688
- Remove contact name hyperlinks for institutional users.  https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/690

## [20210520] (Sprint J21)

### Added
- Ability for GA institutional users to update institutional logo and links. https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/599
- Ability for GA Institutions users to add keywords to Wayfinder for institution and sites. https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/663
- GALILEO Passwords Report. https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/681
- Hide Sites button for institutions who have no sites. https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/682

## [20210507] (Sprint I21)

### Added
- Ability to add additional existing subject categories to resource allocations for GALILEO central resources. https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/667

### Changed
- Encode url parameter for oa_proxy prefix https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/668
- update staging to use db300 solr https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/670
- Rails update https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/merge_requests/710

## [20210423] (Sprint H21)

### Added
- Create a password contacts report that includes a field for “institution group” https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/662
- Create a primary contacts report that includes a field for “institution group” https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/661

### Changed
- Add Remote Access and OpenAthens URL to the Vendor Resources reports https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/664


## [20210408] (Sprint G21)

### Changed
- Automatically Identify and remove extra spaces after URLs https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/658
- Update rails - removes mimemagic dependency https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/657

## [20210326] (Sprint F21)

### Changed
- Add Wayfinder Keywords field in GALILEO Admin https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/651
- Change Patron view to "Full View" (GALILEO Admin) https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/644

## [20210312] (Sprint E21)

### Fixed
- Still IP authenticated even when IP authentication is disabled https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/646
- Proxy remote being applied to override URLs https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/649


## [20210226] (Sprint D21)

### Changed
- Email Notification Emails from December 10, 2020, show password change will happen in 2020 instead of 2021 https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/627


## [20210212] (Sprint C21)

### Changed
- Add more eds profiles to institution show page https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/635
- Request to add contact group for EDS contacts https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/637

## [20210204] (Sprint B21)

### Changed
- EZproxy workaround and express links https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/634
- Restrict file types that can be uploaded for images https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/641
- Bento import: Better Bento Type/ View type Error messages https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/640
- Update CSV Importer report for Contacts to use inst code instead of inst id https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/642


## [20210129] (Sprint B21)

### Added
  - add contact group for OpenAthens IdP Contacts. [632](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/632)
  - Add Bento to CSV importer. [633](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/633)

## [20210115] (Sprint A21)

### Added
  - Ability to turn OpenAthens off at allocation level for Central Resources. [617](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/617)
  - New API/Bento table [625](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/625)
  - Index glri boolean for institution [622](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/622)

### Changed
  - Need to add "New" button to add (default) features to Institution Groups [618](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/618)
  - Add new password field to Institution edit page [487](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/487)
  - Don't allow double spaces in Institution name (Validation) [533](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/533)

### Fixed
  - Fix "Back to Features" navigation [628](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/628)

## [20201218] (Sprint Y)

### Added
  - Add ability to select multiple Institutions, vendors, resources for respective allocation reports [526](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/526)

### Changed
  - CSV Importer Bug resulting from moving to tools [614](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/614)

## [20201204] (Sprint X)

### Added
  - IP cache look up tool [612](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/612)

### Changed
  - Add `pines_codes` to Institution Edit form [610](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/610)


## [20201120] (Sprint W)
### Added
- Add validators for inst code and resource code
 [589](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/589)
- Add favicons and manifest file [606](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/606)

### Changed
- URLs copy when institution is copying one of their own records or when admin users are copying records [577](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/577)
- Redis indexing optimization
 [603](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/603)



## [20201106] (Sprint V)
### Added
- Add boolean flags for OAproxy to institutions and resources [601](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/601)
- Scheme (http/https) validation for Resource URLs [434](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/434)

### Changed
- Add OpenAthens State to GLRI Resource Report [550](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/550)
- Removed door import process [597](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/597)
- Removed institution_list and system_list columns from resources table [291](https://gitlab.galileo.usg.edu/galileo/galileo_admin/-/issues/291)
