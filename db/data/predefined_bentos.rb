# frozen_string_literal: true

all_views = UserView.all.pluck :code

PredefinedBento.create!(
  [
    { code: 'databases', display_name: 'Databases', service: 'galileo' },
    { code: 'journals', display_name: 'Journals', service: 'eds_publications' },

    # EDS source-type bentos
    { code: 'academic_journals', display_name: 'Academic Journals', service: 'eds_api',
      definition: {
        'Source Type Facet' => ['Academic Journals'],
        'Equivalent Limiter Clause' => '(PT (Periodical OR Review OR Trade Publication) AND RV Y) OR PT Academic Journal',
        'Has Parent Publication' => true
      }},
    { code: 'audio', display_name: 'Audio', service: 'eds_api', definition: {
      'Source Type Facet' => ['Audio'],
      'Equivalent Limiter Clause' => 'PT (Audio OR Audio Book OR Audiobook OR Audiobooks OR Audiocassette OR Audiovisual OR Sound record* OR Soundrecording OR Music)'
    }},
    { code: 'biography', display_name: 'Biography', service: 'eds_api', definition: {
      'Source Type Facet' => ['Biography'],
      'Equivalent Limiter Clause' => 'PT Biography OR PT Author Biography OR PZ (Biography OR Preferred Biography OR Obituary OR collective biographies OR individual biographies)'
    }},
    { code: 'books', display_name: 'Books', service: 'eds_api', definition: {
      'Source Type Facet' => ['Books'],
      'Equivalent Limiter Clause' => 'PT (Book OR Almanac OR REFBK OR Encyclopedia OR ENCY OR Bibliography OR Collective Volume Article OR Book Article OR Book Collection OR Play) OR PZ (book OR book chapter OR Book Parts OR Play OR Book Parts)'
    }},
    { code: 'conference_materials', display_name: 'Conference Materials', service: 'eds_api',
      definition: {
        'Source Type Facet' => ['Conference Materials'],
        'Equivalent Limiter Clause' => 'PT (Conference Paper OR Conference Papers OR Conference Proceeding) OR PZ Conference paper OR PZ Proceeding'
      }},
    { code: 'dissertations', display_name: 'Dissertations', service: 'eds_api', definition: {
      'Source Type Facet' => ['Dissertations'],
      'Equivalent Limiter Clause' => 'PT Dissertation Abstract OR PT Dissertation OR PZ dissertation'
    }},
    { code: 'ebooks', display_name: 'eBooks', service: 'eds_api', definition: {
      'Source Type Facet' => ['eBooks'],
      'Equivalent Limiter Clause' => 'PT eBook'
    }},
    { code: 'electronic_resources', display_name: 'Electronic Resources', service: 'eds_api',
      definition: {
        'Source Type Facet' => ['Electronic Resources'],
        'Equivalent Limiter Clause' => 'PT (Computer File OR Computer* OR Electronic Resource OR Electronic* OR eBook OR Database OR website) OR PZ (Computer Program OR Updating Website OR Electronic*)'
      }},
    { code: 'magazines', display_name: 'Magazines', service: 'eds_api', definition: {
      'Source Type Facet' => ['Magazines'],
      'Additional Limiters' => 'NOT FC Y',
      'Equivalent Limiter Clause' => '((PT Periodical OR PT Journal Article) NOT RV Y) NOT FC Y',
      'Has Parent Publication' => true
    }},
    { code: 'maps', display_name: 'Maps', service: 'eds_api', definition: {
      'Source Type Facet' => ['Maps'],
      'Equivalent Limiter Clause' => 'PZ Maps'
    }},
    { code: 'music_scores', display_name: 'Music Scores', service: 'eds_api', definition: {
      'Source Type Facet' => ['Music Scores'],
      'Equivalent Limiter Clause' => 'PT (Music OR Music Score OR Printed Music) OR PZ (Music OR Music Score OR Multiple Score Formats OR Voice Score)'
    }},
    { code: 'news', display_name: 'News', service: 'eds_api', definition: {
      'Source Type Facet' => ['News'],
      'Additional Limiters' => 'NOT FC Y',
      'Equivalent Limiter Clause' => 'PT (Newspaper OR NEWSP OR Newswire OR WIRES OR TSRPT OR Transcript) NOT FC Y',
      'Has Parent Publication' => true
    }},
    { code: 'non_print', display_name: 'Non-Print Resources', service: 'eds_api', definition: {
      'Source Type Facet' => ['Non-Print Resources'],
      'Equivalent Limiter Clause' => 'PT (cd-rom OR Game OR Graphic OR flash card* OR Image OR Images OR Interactive Multimedia OR Kit OR Mixed Materials OR Multimedia OR Object OR Pictorial) OR PZ (Chart OR mixed forms OR model OR Picture OR Realia OR Technical Drawing OR Visual Material)'
    }},
    { code: 'patents', display_name: 'Patents', service: 'eds_api', definition: {
      'Source Type Facet' => ['Patents'],
      'Equivalent Limiter Clause' => 'PZ Related Patent OR PZ Patent'
    }},
    { code: 'primary_sources', display_name: 'Primary Sources', service: 'eds_api',
      definition: {
        'Source Type Facet' => ['Primary Source Documents'],
        'Equivalent Limiter Clause' => 'PT Primary Source Document'
      }},
    { code: 'reports', display_name: 'Reports', service: 'eds_api', definition: {
      'Source Type Facet' => ['Reports'],
      'Equivalent Limiter Clause' => 'PT (Country Report OR Report OR Educational Report OR Industry Report OR Industry Profile OR Market Research Report OR SWOT Analysis) OR PZ (industry overview OR industry report OR market research report OR report OR Country report OR Product Review)'
    }},
    { code: 'research_starters', display_name: 'Research Starters', service: 'eds_api', definition: {
      'Source Type Facet' => ['Research Starters'],
      'Equivalent Limiter Clause' => 'PT Research Starter'
    }},
    { code: 'reviews', display_name: 'Reviews', service: 'eds_api', definition: {
      'Source Type Facet' => ['Reviews'],
      'Equivalent Limiter Clause' => 'PT (Review OR Review Article) OR PZ (advertising review OR architecture review OR Book Review OR course review OR Dance Review OR entertainment review OR establishment review OR exhibition review OR festival review OR Film Review OR literature review OR music review OR music video review OR Musical Comedy Review OR Opera Review OR Operetta review OR Oratorio Review OR Performance Review OR poetry review OR product review OR Radio Program Review OR Review Article OR short story review OR television review OR video game review OR web site review) OR JN Magill Book Reviews'
    }},
    { code: 'standards', display_name: 'Standards', service: 'eds_api', definition: {
      'Source Type Facet' => ['Standards'],
      'Equivalent Limiter Clause' => 'PZ standard'
    }},
    { code: 'trade_publications', display_name: 'Trade Publications', service: 'eds_api',
      definition: {
        'Source Type Facet' => ['Trade Publications'],
        'Equivalent Limiter Clause' => 'PT Trade Publication',
        'Has Parent Publication' => true
      }},
    { code: 'videos', display_name: 'Videos', service: 'eds_api', definition: {
      'Source Type Facet' => ['Videos'],
      'Equivalent Limiter Clause' => 'PT (DVD* OR Motion Picture OR Video OR Videor*)'
    }},

    # Bentos with multiple source types
    { code: 'media', display_name: 'Audio/Video', service: 'eds_api', definition: {
      'Source Type Facet' => ['Audio', 'Videos'],
      'Equivalent Limiter Clause' => 'PT (Audio OR Audio Book OR Audiobook OR Audiobooks OR Audiocassette OR Audiovisual OR Sound record* OR Soundrecording OR Music OR DVD* OR Motion Picture OR Video OR Videor*)'
    }},
    { code: 'scholarly', display_name: 'Scholarly Articles', service: 'eds_api', definition: {
      'Source Type Facet' => ['Academic Journals', 'Conference Materials', 'Reports', 'Trade Publications'],
      'Additional Limiters' => 'NOT PT eBook',
      'Equivalent Limiter Clause' => '(((PT Periodical OR PT Review) AND RV Y) OR PT (Academic Journal OR Conference Paper OR Conference Papers OR Conference Proceeding OR Country Report OR Industry Profile OR Industry Report OR Market Research Report OR Report OR Educational Report OR SWOT Analysis OR Trade Publication) OR PZ (Conference paper OR Country report OR industry overview OR industry report OR market research report OR Proceeding OR Product Review OR report)) NOT PT eBook',
      'Has Parent Publication' => true
    }},

    # Encyclopedia with and without Britannica
    { code: 'encyclopedia', display_name: 'Encyclopedia', service: 'eds_api', definition: {
      'Content Provider Facet' => ['Funk & Wagnalls New World Encyclopedia'],
      'Equivalent Limiter Clause' => 'SO Funk & Wagnalls'
    }},
    { code: 'encyclopedia-b', display_name: 'Encyclopedia', service: 'eds_api', definition: {
      'Content Provider Facet' => ['Funk & Wagnalls New World Encyclopedia', 'Britannica Online'],
      'Equivalent Limiter Clause' => 'SO Funk & Wagnalls OR SO Britannica Online'
    }},

    # LearningExpress
    {
      code: 'learning_express', display_name: 'LearningExpress', service: 'eds_api',
      definition: {
        'Content Provider Facet' => ['LearningExpress Library'],
        'Equivalent Limiter Clause' => 'AU LearningExpress'
      }}
  ]
)