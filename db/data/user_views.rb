# frozen_string_literal: true

UserView.create!(
  [
    { code: 'elementary', name: 'Elementary School', inst_type: %w[k12 publiclib], full: false },
    { code: 'middle', name: 'Middle School', inst_type: %w[k12 publiclib], full: false },
    { code: 'highschool', name: 'High School', inst_type: %w[k12 publiclib], full: false },
    { code: 'educator', name: 'Educator', inst_type: %w[k12], full: true },
    { code: 'full', name: 'Full', inst_type: %w[publiclib], full: true },
    { code: 'default', name: 'Default', inst_type: [], full: true }
  ]
)
