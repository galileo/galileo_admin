# frozen_string_literal: true

def lookup_bentos(predefined_bento_codes)
  (predefined_bento_codes.map {|pb_code| PredefinedBento.find_by(code: pb_code)}).filter {|pb| !pb.nil?}
end

def create_template(template_name, user_view, predefined_bento_codes, hidden_by_default_bentos=[])
  i = 0
  TemplateViewBento.create!(
    lookup_bentos(predefined_bento_codes).map do |pb|
      {
        template_name: template_name,
        user_view: user_view,
        predefined_bento_id: pb.id,
        shown_by_default: true,
        default_order: i+=1
      }
    end
  )
  TemplateViewBento.create!(
    lookup_bentos(hidden_by_default_bentos).map do |pb|
      {
        template_name: template_name,
        user_view: user_view,
        predefined_bento_id: pb.id,
        shown_by_default: false
      }
    end
  )
end

def default_bentos(britannica=false)
  encyclopedia = britannica ? 'encyclopedia-b' : 'encyclopedia'
  %w[databases journals scholarly] + [encyclopedia] + %w[ebooks news magazines primary_sources media]
end

def elementary_bentos(britannica=false)
  encyclopedia = britannica ? 'encyclopedia-b' : 'encyclopedia'
  %w[databases ebooks] + [encyclopedia] + %w[news magazines]
end

nondefault_eds_bentos = %w[academic_journals audio biography books conference_materials dissertations
                           electronic_resources maps music_scores non_print patents reports research_starters reviews
                           standards trade_publications videos]


create_template 'highered', 'default', default_bentos, nondefault_eds_bentos
create_template 'k12', 'elementary', elementary_bentos(true)
create_template 'k12', 'middle', default_bentos(true)
create_template 'k12', 'highschool', default_bentos(true)
create_template 'k12', 'educator', default_bentos(true), nondefault_eds_bentos
create_template 'publiclib', 'elementary', elementary_bentos
create_template 'publiclib', 'middle', default_bentos
create_template 'publiclib', 'highschool', default_bentos
create_template 'publiclib', 'full', default_bentos + ['learning_express'], nondefault_eds_bentos
create_template 'default', 'default', ['databases']