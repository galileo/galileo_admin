# frozen_string_literal: true

InstitutionGroup.create!(
  [
    { code: 'public', inst_type: 'default' },
    { code: 'univsystem', inst_type: 'highered' },
    { code: 'k12', inst_type: 'k12' },
    { code: 'techinst', inst_type: 'highered' },
    { code: 'publiclibs', inst_type: 'publiclib' },
    { code: 'univcenter', inst_type: 'highered' },
    { code: 'gpals', inst_type: 'highered' },
    { code: 'auc', inst_type: 'highered' },
    { code: 'privk12', inst_type: 'k12' },
    { code: 'k12ttc', inst_type: 'k12' },
    { code: 'uga', inst_type: 'highered' },
    { code: 'uga1', inst_type: 'highered' },
    { code: 'ampals', inst_type: 'highered' }
  ]
)
