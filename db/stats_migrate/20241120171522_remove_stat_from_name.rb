class RemoveStatFromName < ActiveRecord::Migration[7.0]
  def change
    rename_table :raw_link_stats, :raw_link
    rename_table :raw_login_stats, :raw_login

    rename_table :daily_link_stats, :daily_link
    rename_table :daily_login_stats, :daily_login

    # Rename galileo_search to galileo_search
    rename_table :raw_search_stats, :raw_galileo_search
    rename_table :daily_search_stats, :daily_galileo_search
    rename_table :monthly_search, :monthly_galileo_search
  end
end
