class ChangeStringsToDates < ActiveRecord::Migration[6.1]

# NOTE: no effort is made to convert existing data, so this migration should be run
# with completely empty tables.  One option is to rollback all migrations, e.g.
# `rake db:rollback:stats STEP=7` and then remigrate, e.g.,
# `rake db:migrate:stats`

  def change
    remove_column :raw_login_stats,  :begin_time, :string, default: "", null: false
    remove_column :raw_link_stats,   :begin_time, :string, default: "", null: false
    remove_column :raw_search_stats, :begin_time, :string, default: "", null: false
    remove_column :daily_stats,      :day,        :string, default: "", null: false
    remove_column :monthly_stats,    :month,      :string, default: "", null: false
    remove_column :stats_snapshots,  :month,      :string, default: "", null: false

    add_column :raw_login_stats,  :begin_time, :datetime, precision: 0, null: false
    add_column :raw_link_stats,   :begin_time, :datetime, precision: 0, null: false
    add_column :raw_search_stats, :begin_time, :datetime, precision: 0, null: false
    add_column :daily_stats,      :day,        :date, null: false
    add_column :monthly_stats,    :month,      :date, null: false  # use first day of the month
    add_column :stats_snapshots,  :month,      :date, null: false  # use first day of the month
  end
end
