class AddTimestampsQueryTypeToDailyStatMonthlyStat < ActiveRecord::Migration[6.1]
  def change
    add_column :daily_stats, :query_type, :string, null: false, default: ''
    add_column :daily_stats, :created_at, :datetime, null: false, precision: 6
    add_column :daily_stats, :updated_at, :datetime, null: false, precision: 6

    add_column :monthly_stats, :query_type, :string, null: false, default: ''
    add_column :monthly_stats, :created_at, :datetime, null: false, precision: 6
    add_column :monthly_stats, :updated_at, :datetime, null: false, precision: 6
  end
end
