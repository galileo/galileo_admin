class CreateRawLinkStats < ActiveRecord::Migration[6.1]
  # Session Id,Begin Time,Action,Database Name,Server,Institution Code
  def change
    create_table :raw_link_stats do |t|
      t.string :session_id, default: "", null: false
      t.string :begin_time, default: "", null: false
      t.string :action, default: "", null: false
      t.string :resource_code, default: "", null: false
      t.string :server, default: "", null: false
      t.string :institution_code, default: "", null: false
      t.timestamps
    end
  end
end
