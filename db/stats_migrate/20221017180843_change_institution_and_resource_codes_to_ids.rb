class ChangeInstitutionAndResourceCodesToIds < ActiveRecord::Migration[6.1]
  def up
    StatsSnapshot.delete_all
    remove_column :stats_snapshots, :institution_code
    remove_column :stats_snapshots, :resource_code

    add_reference :stats_snapshots, :institution
    add_reference :stats_snapshots, :resource
  end
  def down
    add_column :stats_snapshots, :institution_code, :string, null: false, default: ""
    add_column :stats_snapshots, :resource_code, :string, null: false, default: ""

    remove_column :stats_snapshots, :institution_id
    remove_column :stats_snapshots, :resource_id
  end
end
