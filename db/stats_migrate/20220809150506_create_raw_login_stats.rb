class CreateRawLoginStats < ActiveRecord::Migration[6.1]
  # Session Id,Begin Time,Action,IP,Server,Institution Code,Auth Method,Site
  def change
    create_table :raw_login_stats do |t|
      t.string :session_id, default: "", null: false
      t.string :begin_time, default: "", null: false
      t.string :action, default: "", null:false
      t.string :ip, default: "", null:false
      t.string :server, default: "", null:false
      t.string :institution_code, default: "", null:false
      t.string :auth_method, default: "", null:false
      t.string :site_code, default: "", null:false
      t.timestamps
    end
  end
end
