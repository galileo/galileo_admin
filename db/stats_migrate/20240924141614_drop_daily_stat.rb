class DropDailyStat < ActiveRecord::Migration[7.0]
  def change
    drop_table :daily_stats do |t|
      t.string :date, null: false
      t.string :stats_type, null: false
      t.string :institution_code, null: false
      t.string :resource_code, null: true
      t.integer :count, null: false, default: 0
    end
  end
end
