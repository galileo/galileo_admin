class CreateMonthlySearchStats < ActiveRecord::Migration[7.0]
  def change
    create_table :monthly_search_stats do |t|
      t.string :year_month, null: false
      t.string :institution_code, null: false
      t.string :query_type, null: true
      t.integer :count, null: false, default: 0
      t.timestamps
    end
  end
end
