class CreateMonthlyLoginStats < ActiveRecord::Migration[7.0]
  def change
    create_table :monthly_login_stats do |t|
      t.string :year_month, null: false
      t.string :institution_code, null: false
      t.integer :count, null: false, default: 0
      t.timestamps
    end
  end
end
