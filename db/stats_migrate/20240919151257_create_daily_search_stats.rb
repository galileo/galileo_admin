class CreateDailySearchStats < ActiveRecord::Migration[7.0]
  def change
    create_table :daily_search_stats do |t|
      t.string :date, null: false
      t.string :institution_code, null: false
      t.string :query_type, null: true
      t.integer :count, null: false, default: 0
      t.timestamps
    end
  end
end
