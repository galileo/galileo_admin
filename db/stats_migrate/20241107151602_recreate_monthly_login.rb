class RecreateMonthlyLogin < ActiveRecord::Migration[7.0]
  def up
    drop_table :monthly_login_stats

    create_table :monthly_login do |t|
      t.integer :year, null: false
      t.integer :month, null: false
      t.date :year_month, null: false
      t.string :institution_code, null: false
      t.integer :count, null: false, default: 0
      t.timestamps
    end
  end

  def down
    drop_table :monthly_login

    create_table :monthly_login_stats do |t|
      t.string :year_month, null: false
      t.string :institution_code, null: false
      t.integer :count, null: false, default: 0
      t.timestamps
    end
  end
end
