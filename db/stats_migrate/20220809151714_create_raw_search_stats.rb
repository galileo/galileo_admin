class CreateRawSearchStats < ActiveRecord::Migration[6.1]
  # Time,Institution Code,Site Code,Query Type,Query
  def change
    create_table :raw_search_stats do |t|
      t.string :begin_time, default: "", null: false
      t.string :server, default: "", null: false
      t.string :institution_code, default: "", null: false
      t.string :site_code, default: "", null: false
      t.string :query_type, default: "", null: false
      t.string :query, default: "", null: false
      t.timestamps
    end
  end
end
