class AddSnapshotsTable < ActiveRecord::Migration[6.1]
  def change
    create_table :stats_snapshots do |t|
      t.string :month, null: false
      t.string :institution_code, null: false
      t.string :resource_code, null: true
      t.timestamps
    end
  end
end
