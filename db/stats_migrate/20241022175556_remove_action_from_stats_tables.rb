class RemoveActionFromStatsTables < ActiveRecord::Migration[7.0]
  def change
    remove_column :raw_link_stats, :action, :string, default: "", null:false
    remove_column :raw_login_stats, :action, :string, default: "", null:false
  end
end
