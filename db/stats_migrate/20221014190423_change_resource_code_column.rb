class ChangeResourceCodeColumn < ActiveRecord::Migration[6.1]
  def change
    change_column_null(:daily_stats, :resource_code, false)
    change_column_null(:monthly_stats, :resource_code, false)
    change_column_null(:stats_snapshots, :resource_code, false)

    change_column_default(:daily_stats, :resource_code, from: nil, to: "" )
    change_column_default(:monthly_stats, :resource_code, from: nil, to: "" )
    change_column_default(:stats_snapshots, :resource_code, from: nil, to: "" )
  end
end
