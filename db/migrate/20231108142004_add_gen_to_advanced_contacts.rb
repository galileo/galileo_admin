class AddGenToAdvancedContacts < ActiveRecord::Migration[6.1]
  def up
    Contact.where("types && ?", "{adv}").where.not("types && ?", "{gen}").update_all(['types = array_append(types, ?)', 'gen'])
  end

  def down
    # Can't revert
  end
end
