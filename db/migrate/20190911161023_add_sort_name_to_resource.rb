class AddSortNameToResource < ActiveRecord::Migration[6.0]
  def change
    add_column :resources, :sort_name, :string
  end
end
