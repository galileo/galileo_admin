class AddNotesToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :note, :string, default: ''
    add_column :institutions, :special_information, :string, default: ''
  end
end
