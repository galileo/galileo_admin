class ChangeBentosToBentoConfigs < ActiveRecord::Migration[6.0]
  def change
    rename_table :bentos, :bento_configs
  end
end
