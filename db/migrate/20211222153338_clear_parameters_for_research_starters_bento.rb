class ClearParametersForResearchStartersBento < ActiveRecord::Migration[6.0]
  def up
    pb = PredefinedBento.find_by_code 'research_starters'
    pb.definition = {}
    pb.save
    Sunspot.index! pb.configured_bentos
  end
  def down
    pb = PredefinedBento.find_by_code 'research_starters'
    pb.definition = {
      'Source Type Facet' => ['Research Starters']
    }
    pb.save
    Sunspot.index! pb.configured_bentos
  end
end
