class AddSpecialToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :special, :boolean, default: false
  end
end
