class AddIpAuthenticationToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :ip_authentication, :boolean, default: true
  end
end
