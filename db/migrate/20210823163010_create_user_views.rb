class CreateUserViews < ActiveRecord::Migration[6.0]
  def up
    create_table :user_views do |t|
      t.string :code
      t.string :name
    end
    create_join_table :resources, :user_views
    create_join_table :allocations, :user_views
    load Rails.root.join 'db', 'data', 'user_views.rb'
  end
  def down
    drop_join_table :resources, :user_views
    drop_join_table :allocations, :user_views
    drop_table :user_views
  end
end
