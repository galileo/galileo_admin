class CreateAllocations < ActiveRecord::Migration[5.2]
  def change
    create_table :allocations do |t|
      t.belongs_to :institution, index: true
      t.belongs_to :resource, index: true
      t.timestamps
    end
  end
end
