# frozen_string_literal: true

# add fields display? and glri? to resource
class AddBooleanFieldsToResource < ActiveRecord::Migration[6.0]
  def change
    change_table :resources do |t|
      t.boolean :glri, default: false, index: true
      t.boolean :display, default: true, index: true
    end
  end
end
