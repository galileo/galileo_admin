class AddVendorNameToResource < ActiveRecord::Migration[6.0]
  def change
    add_column :resources, :vendor_name, :string, default: ''
  end
end
