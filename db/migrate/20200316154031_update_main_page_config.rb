class UpdateMainPageConfig < ActiveRecord::Migration[6.0]
  def change
    remove_reference :main_page_configurations, :featured_allocation_1
    remove_reference :main_page_configurations, :featured_allocation_2
    remove_reference :main_page_configurations, :featured_allocation_3
    remove_reference :main_page_configurations, :featured_allocation_4
    remove_reference :main_page_configurations, :featured_allocation_5
    remove_reference :main_page_configurations, :featured_allocation_6

    add_column :main_page_configurations, :featured_url_1, :string
    add_column :main_page_configurations, :featured_url_2, :string
    add_column :main_page_configurations, :featured_url_3, :string
    add_column :main_page_configurations, :featured_url_4, :string
    add_column :main_page_configurations, :featured_url_5, :string
    add_column :main_page_configurations, :featured_url_6, :string

    add_column :main_page_configurations, :featured_label_1, :string
    add_column :main_page_configurations, :featured_label_2, :string
    add_column :main_page_configurations, :featured_label_3, :string
    add_column :main_page_configurations, :featured_label_4, :string
    add_column :main_page_configurations, :featured_label_5, :string
    add_column :main_page_configurations, :featured_label_6, :string

    add_column :main_page_configurations, :featured_description_1, :string
    add_column :main_page_configurations, :featured_description_2, :string
    add_column :main_page_configurations, :featured_description_3, :string
    add_column :main_page_configurations, :featured_description_4, :string
    add_column :main_page_configurations, :featured_description_5, :string
    add_column :main_page_configurations, :featured_description_6, :string
  end
end
