class RemoveFieldsFromResources < ActiveRecord::Migration[6.0]
  def up
    remove_column :resources, :deep_linking_url
    remove_column :resources, :groups
    remove_column :resources, :last_updated_by
    remove_column :resources, :new_until
    remove_column :resources, :record_type
    remove_column :resources, :stats_group
    remove_column :resources, :status
    remove_column :resources, :update_history

    add_column :resources, :include_in_stats, :boolean, default: false
    Resource.reset_column_information # make the new column available to model methods
    Resource.where.not(stats_pulldown: nil).update_all(include_in_stats: true)
    remove_column :resources, :stats_pulldown

    add_column :institutions, :use_local_ez_proxy, :boolean, default: false
  end

  def down
    remove_column :institutions, :use_local_ez_proxy

    rename_column :resources, :include_in_stats, :stats_pulldown
    change_column :resources, :stats_pulldown, :string

    add_column :resources, :deep_linking_url, :string
    add_column :resources, :groups, :string, array: true
    add_column :resources, :last_updated_by, :string
    add_column :resources, :new_until, :timestamp
    add_column :resources, :record_type, :string
    add_column :resources, :stats_group, :string
    add_column :resources, :status, :string
    add_column :resources, :update_history, :string
  end
end
