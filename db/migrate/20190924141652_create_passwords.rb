class CreatePasswords < ActiveRecord::Migration[6.0]
  def change
    create_table :passwords do |t|
      t.string :password
      t.date :commit_date
      t.boolean :exclude
      t.string :user
      t.json :legacy_data

      t.timestamps
    end
  end
end
