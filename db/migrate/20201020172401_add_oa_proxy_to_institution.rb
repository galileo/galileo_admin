class AddOaProxyToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :oa_proxy, :boolean, default: false
    add_column :institutions, :oa_proxy_org, :string
  end
end
