class AddFieldsToResources < ActiveRecord::Migration[5.2]
  def change
    change_table :resources do |t|
      t.string :record_type
      t.string :system_list, array: true
      t.string :status
      t.string :institution_list, array: true
      t.string :groups, array: true
      t.string :paranthetical
      t.string :short_description
      t.boolean :openathens
      t.string :openathens_url
      t.string :proxy
      t.string :ip_access_url
      t.string :remote_access_url
      t.string :deep_linking_url
      t.string :vendor_code
      t.string :product_suite
      t.string :title_list_url
      t.string :vendor_user_id
      t.string :vendor_password
      t.string :long_description
      t.string :coverage_dates
      t.string :update_frequency
      t.string :audience
      t.string :language
      t.string :keywords
      t.string :stats_group
      t.string :stats_pulldown
      t.string :stats_code
      t.string :special_information
      t.string :note
      t.string :update_history
      t.string :last_updated_by
    end
  end
end
