class AddWayfinderKeywordsToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :wayfinder_keywords, :string, default: ''
  end
end
