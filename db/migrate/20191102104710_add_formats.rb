# frozen_string_literal: true

# create Format and relate to Resource
class AddFormats < ActiveRecord::Migration[6.0]
  def up
    create_table :formats do |t|
      t.string :name
      t.string :legacy_codes, array: true, default: []
    end
    create_join_table :resources, :formats

    # create formats from file
    load Rails.root.join 'db', 'data', 'formats.rb'
  end

  def down
    drop_join_table :resources, :formats
    drop_table :formats
  end
end
