class CreateBannerUserViews < ActiveRecord::Migration[7.0]
  def change
    create_join_table :banners, :user_views
  end
end
