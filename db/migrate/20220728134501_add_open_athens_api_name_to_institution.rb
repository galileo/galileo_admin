class AddOpenAthensApiNameToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :open_athens_api_name, :string, default: ''
    # Institution.find_by_code('auc1').update!(open_athens_api_name: 'aucenter.edu')
  end
end
