# frozen_string_literal: true

# :nodoc:
class AddInstitutionToResource < ActiveRecord::Migration[6.0]
  def up
    add_reference :resources, :institution, index: true

    Resource.all.each do |r|
      next unless r.glri?

      codes = r.code.split '-'
      inst_code = codes[1]
      institution = Institution.find_by code: inst_code
      r.institution = institution
      r.save
    end
  end

  def down
    remove_reference :resources, :institution
  end
end
