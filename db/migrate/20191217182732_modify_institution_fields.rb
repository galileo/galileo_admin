class ModifyInstitutionFields < ActiveRecord::Migration[6.0]
  def change
    change_table :institutions do |t|
      t.rename :open_athens_org_id, :open_athens_scope
      t.rename :open_athens_suborg_id, :open_athens_subscope
    end
  end
end
