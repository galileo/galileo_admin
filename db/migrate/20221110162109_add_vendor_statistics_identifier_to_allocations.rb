class AddVendorStatisticsIdentifierToAllocations < ActiveRecord::Migration[6.1]
  def change
    add_column :allocations, :vendor_statistics_identifier, :string, null: false, default: ''
  end
end
