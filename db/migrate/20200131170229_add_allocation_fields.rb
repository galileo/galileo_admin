# frozen_string_literal: true

# :nodoc:
class AddAllocationFields < ActiveRecord::Migration[6.0]
  def change
    change_table :allocations do |t|
      t.string :open_athens_url
      t.string :user_id
      t.string :password
      t.string :note
      t.string :special_info
    end
  end
end
