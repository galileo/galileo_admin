# frozen_string_literal: true

# adds a string value to Institution for the PINS library code
class AddPinesCodeToInst < ActiveRecord::Migration[6.0]
  def change
    change_table :institutions do |t|
      t.string :pines_library_code, index: true
    end
  end
end
