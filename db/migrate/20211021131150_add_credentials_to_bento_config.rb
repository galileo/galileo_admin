class AddCredentialsToBentoConfig < ActiveRecord::Migration[6.0]
  def change
    add_column :bento_configs, :credentials, :json

    reversible do |dir|
      dir.up do
        BentoConfig.all.each do |bento_config|
          bento_config.credentials = {} if bento_config.credentials.nil?
          if bento_config.service == 'eds_api'
            bento_config.credentials['User ID'] = bento_config.user_id
            bento_config.credentials['Password'] = bento_config.password
            bento_config.credentials['API Profile'] = bento_config.api_profile
            bento_config.save!
          end
        end
      end
    end
  end
end
