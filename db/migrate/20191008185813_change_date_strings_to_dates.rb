class ChangeDateStringsToDates < ActiveRecord::Migration[6.0]
  def up
    change_column :institutions, :password_last_changed, 'timestamp USING CAST(password_last_changed AS timestamp)'
    change_column :institutions, :password_last_notified, 'timestamp USING CAST(password_last_notified AS timestamp)'
  end

  def down
    change_column :institutions, :password_last_changed, :string
    change_column :institutions, :password_last_notified, :string
  end
end
