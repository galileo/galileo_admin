# frozen_string_literal: true

# :nodoc:
class RemoveBrandingCode < ActiveRecord::Migration[6.0]
  def change
    remove_column :brandings, :code
  end
end
