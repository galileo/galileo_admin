# frozen_string_literal: true

# Create Vendor model
class CreateVendors < ActiveRecord::Migration[6.0]
  def change
    create_table :vendors do |t|
      t.string :name
      t.string :code, index: true
      t.integer :resources_count
      t.timestamps
    end
  end
end
