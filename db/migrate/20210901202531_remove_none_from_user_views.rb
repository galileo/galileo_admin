class RemoveNoneFromUserViews < ActiveRecord::Migration[6.0]
  def up
    UserView.find_by_code('none').delete
  end
  def down
    UserView.create!(code: 'none', name: 'Only in full views', full: false, inst_type: ['k12','publiclib'])
  end
end
