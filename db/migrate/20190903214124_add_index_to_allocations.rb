class AddIndexToAllocations < ActiveRecord::Migration[6.0]
  def change
    add_index :allocations, [:institution_id, :resource_id], unique: true
  end
end
