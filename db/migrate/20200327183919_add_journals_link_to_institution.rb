# frozen_string_literal: true

# :nodoc:
class AddJournalsLinkToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :journals_link, :string, default: ''
  end
end
