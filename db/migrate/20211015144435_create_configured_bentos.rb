class CreateConfiguredBentos < ActiveRecord::Migration[6.0]
  def change
    create_table :configured_bentos do |t|
      t.belongs_to :bento_configs
      t.belongs_to :predefined_bentos
      t.string :code
      t.string :display_name
      t.string :user_views, array: true, null: false, default: []
      t.json :customizations
      t.timestamps
    end
  end
end
