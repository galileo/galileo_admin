# frozen_string_literal: true

# creates Contact table, Contact Type table, and the join table
class AddContactsAndContactTypes < ActiveRecord::Migration[6.0]
  def up
    create_table :contacts do |t|
      t.string :first_name, index: true
      t.string :last_name, index: true
      t.string :email, index: true
      t.string :phone
      t.text :notes
      t.references :institution
      t.timestamps
    end

    create_table :contact_types do |t|
      t.string :code
      t.string :name
      t.string :description
      t.boolean :system, default: false
      t.timestamps
    end

    create_join_table :contacts, :contact_types do |t|
      t.index :contact_id
      t.index :contact_type_id
    end

    # System-level Contact Types
    if Object.const_defined? 'ContactType'
      ContactType.create!(
        [
          { code: 'prim', name: 'Primary Contact', system: true },
          { code: 'glri', name: 'GLRI Contact', system: true },
          { code: 'pass', name: 'GALILEO Password Contact', system: true }
        ]
      )
    end
  end

  def down
    drop_join_table :contacts, :contact_types
    drop_table :contact_types
    drop_table :contacts
  end
end
