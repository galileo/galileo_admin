# frozen_string_literal: true

# make pines_library_code field on Institution multivalued
class AddPinesCodeArray < ActiveRecord::Migration[6.0]
  def change
    change_table :institutions do |t|
      t.string :pines_codes, array: true, default: []
    end
    Institution.all.each do |inst|
      next unless inst.pines_library_code

      inst.pines_library_code.split(',').each do |code|
        inst.pines_codes << code.strip
      end
    end
  end
end
