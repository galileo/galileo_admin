# frozen_string_literal: true

class RemoveOldRowsFromInstitutionGroups < ActiveRecord::Migration[6.0]
  def change
    InstitutionGroup.where(code: %w[glri_galileo sfx_local glri_locally]).destroy_all
  end
end
