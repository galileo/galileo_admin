class AddNoneToUserView < ActiveRecord::Migration[6.0]
  def up
    UserView.create!(code: 'none', name: 'Only in full views', full: false, inst_type: ['k12','publiclib'])
  end
  def down
    UserView.find_by_code('none').delete
  end
end
