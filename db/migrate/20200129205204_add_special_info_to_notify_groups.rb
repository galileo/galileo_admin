class AddSpecialInfoToNotifyGroups < ActiveRecord::Migration[6.0]
  def up
    remove_column :notify_groups, :update_history
    remove_column :notify_groups, :updated_by
    add_column :notify_groups, :special_information, :string, default: ''
    end

  def down
    remove_column :notify_groups, :special_information
    add_column :notify_groups, :updated_by, :string, default: ''
    add_column :notify_groups, :update_history, :string, array: true
  end
end
