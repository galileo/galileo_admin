class AddActiveToResources < ActiveRecord::Migration[6.0]
  def up
    add_column :resources, :active, :boolean
    Resource.all.each do |res|
      active = if res.system_list&.include?('prod')
                 true
               else
                 false
               end
      res.update(active: active)
    end
  end

  def down
    remove_column :resources, :active
  end
end
