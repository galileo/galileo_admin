class RemoveShowRemoteHelpFromResource < ActiveRecord::Migration[6.0]
  def change
    remove_column :resources, :show_remote_help, :boolean
    remove_column :resources, :always_show_remote_note, :boolean
  end
end
