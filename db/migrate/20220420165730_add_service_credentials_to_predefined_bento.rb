class AddServiceCredentialsToPredefinedBento < ActiveRecord::Migration[6.0]
  def change
    add_column :predefined_bentos, :service_credentials_name, :string
  end
end
