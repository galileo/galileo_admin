class UpdateBentoConfigs < ActiveRecord::Migration[6.0]
  def up
    add_column :bento_configs, :view_types, :string, array: true, null: false, default: []
    add_column :bento_configs, :name, :string, null: false, default: ''
    rename_column :bento_configs, :bento_type, :service

    BentoConfig.all.each do |bc|
      bc.view_types << bc.view_type
      bc.save!
    end
  end

  def down
    remove_column :bento_configs, :view_types, :string, array: true, null: false, default: []
    remove_column :bento_configs, :name, :string, null: false, default: ''
    rename_column :bento_configs, :service, :bento_type
  end
end
