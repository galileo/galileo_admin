class ChangeRemoteNoteToAccessNote < ActiveRecord::Migration[6.0]
  def change
    rename_column :resources, :remote_note, :access_note
  end
end
