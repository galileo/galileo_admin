class CreateMainPageConfigurations < ActiveRecord::Migration[6.0]
  def change
    create_table :main_page_configurations do |t|
      t.belongs_to :institution
      t.references :featured_allocation_1
      t.string  :featured_allocation_image_1, default: ''
      t.references :featured_allocation_2
      t.string  :featured_allocation_image_2, default: ''
      t.references :featured_allocation_3
      t.string  :featured_allocation_image_3, default: ''
      t.string  :institution_url_1, default: ''
      t.string  :institution_url_label_1, default: ''
      t.string  :institution_url_2, default: ''
      t.string  :institution_url_label_2, default: ''
      t.string  :institution_url_3, default: ''
      t.string  :institution_url_label_3, default: ''
      t.string  :institution_url_4, default: ''
      t.string  :institution_url_label_4, default: ''
      t.string  :institution_url_5, default: ''
      t.string  :institution_url_label_5, default: ''
      t.references :kids_allocation_1
      t.string  :kids_allocation_image_1, default: ''
      t.references :kids_allocation_2
      t.string  :kids_allocation_image_2, default: ''
      t.references :kids_allocation_3
      t.string  :kids_allocation_image_3, default: ''
      t.string  :kids_institution_url_1, default: ''
      t.string  :kids_institution_url_label_1, default: ''
      t.string  :kids_institution_url_2, default: ''
      t.string  :kids_institution_url_label_2, default: ''
      t.string  :kids_institution_url_3, default: ''
      t.string  :kids_institution_url_label_3, default: ''
      t.string  :kids_institution_url_4, default: ''
      t.string  :kids_institution_url_label_4, default: ''
      t.string  :kids_institution_url_5, default: ''
      t.string  :kids_institution_url_label_5, default: ''
      t.timestamps
    end
  end
end
