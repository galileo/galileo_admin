# frozen_string_literal: true

# :nodoc:
class AdjustProxyOnResource < ActiveRecord::Migration[6.0]
  def change
    change_table :resources do |t|
      t.remove :proxy
      t.boolean :proxy_remote, default: false
    end
  end
end
