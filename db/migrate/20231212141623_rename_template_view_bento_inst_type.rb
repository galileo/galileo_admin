class RenameTemplateViewBentoInstType < ActiveRecord::Migration[6.1]
  def change
    rename_column :template_view_bentos, :inst_type, :template_name
  end
end
