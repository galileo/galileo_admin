# frozen_string_literal: true

# Add "Kids" subjects unless they already exists (from seeds)
class AddKidSubjects < ActiveRecord::Migration[6.0]
  def change
    unless Subject.exists?(name: 'Elementary School')
      Subject.create!(name: 'Elementary School', legacy_codes: %w[k_all])
    end
    unless Subject.exists?(name: 'Middle School')
      Subject.create!(name: 'Middle School', legacy_codes: %w[t_all])
    end
    unless Subject.exists?(name: 'High School')
      Subject.create!(name: 'High School', legacy_codes: %w[s_all])
    end
  end
end
