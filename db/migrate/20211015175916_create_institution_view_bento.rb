class CreateInstitutionViewBento < ActiveRecord::Migration[6.0]
  def change
    create_table :institution_view_bentos do |t|
      t.belongs_to :institution
      t.belongs_to :configured_bentos
      t.float :order
      t.string :user_view
      t.boolean :shown_by_default, default: true
      t.timestamps
    end
  end
end
