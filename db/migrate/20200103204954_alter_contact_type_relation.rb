# frozen_string_literal: true

# remove relation that represents contact types
class AlterContactTypeRelation < ActiveRecord::Migration[6.0]
  def up
    change_table :contacts do |t|
      t.string :types, array: true, default: [], null: false, index: true
    end
    drop_join_table :contact_types, :contacts
    drop_table :contact_types
  end

  def down
    change_table :contacts do |t|
      t.remove :types
    end
    create_table :contact_types do |t|
      t.string 'code'
      t.string 'name'
      t.string 'description'
      t.boolean 'system', default: false
      t.timestamps
    end
    create_join_table :contact_types, :contacts
  end
end
