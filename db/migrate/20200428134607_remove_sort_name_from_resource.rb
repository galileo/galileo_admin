class RemoveSortNameFromResource < ActiveRecord::Migration[6.0]
  def change
    remove_column :resources, :sort_name, :string
  end
end
