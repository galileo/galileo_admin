class AddPowerNotesZoteroBibToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :power_notes, :boolean, default: true, null: false
    add_column :institutions, :zotero_bib, :boolean, default: true, null: false
  end
end
