# frozen_string_literal: true

# create vendor - institution join table
class CreateAccountConfigurations < ActiveRecord::Migration[6.0]
  def change
    create_table :account_configurations do |t|
      t.belongs_to :institution, index: true
      t.belongs_to :vendor, index: true
      t.json :credentials
      t.timestamps
    end
  end
end
