class SetDefaultValues < ActiveRecord::Migration[6.0]
  def up
    change_column :institutions, :open_athens, :boolean, default: false
    change_column :institutions, :active, :boolean, default: false
    change_column :institutions, :glri_participant, :string, default: 'none'
    change_column :passwords, :exclude, :boolean, default: false
    change_column :resources, :open_athens, :boolean, default: false
    change_column :resources, :active, :boolean, default: false
    change_column :resources, :show_institutional_branding, :boolean, default: false
    change_column :resources, :freely_available, :boolean, default: false
    change_column :resources, :feature, :boolean, default: false
    change_column :resources, :boost, :integer, default: 0
  end

  def down
    # Can't reverse setting a default value
  end
end
