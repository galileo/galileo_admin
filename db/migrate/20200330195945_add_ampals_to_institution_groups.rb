# frozen_string_literal: true

# Add AMPALS Institution Group if needed
class AddAmpalsToInstitutionGroups < ActiveRecord::Migration[6.0]
  def up
    return if InstitutionGroup.exists?(code: 'ampals')

    ampals = InstitutionGroup.new(code: 'ampals')
    ampals.save(validate: false)
  end

  def down
    return unless InstitutionGroup.exists?(code: 'ampals')

    InstitutionGroup.find_by_code('ampals').destroy
  end
end
