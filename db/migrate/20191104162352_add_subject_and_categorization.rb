# frozen_string_literal: true

# add Subject and Categorization join model
class AddSubjectAndCategorization < ActiveRecord::Migration[6.0]
  def up
    create_table :subjects do |t|
      t.string :name
      t.string :legacy_codes, array: true, default: []
    end

    create_table :categorizations do |t|
      t.belongs_to :subject
      t.belongs_to :resource
      t.timestamps
    end

    # Load Subjects
    load Rails.root.join 'db', 'data', 'subjects.rb'
  end

  def down
    drop_table :categorizations
    drop_table :subjects
  end
end
