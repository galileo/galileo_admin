class CreatePredefinedBentos < ActiveRecord::Migration[6.0]
  def change
    create_table :predefined_bentos do |t|
      t.string :service
      t.string :user_views, array: true, null: false, default: []
      t.string :code
      t.string :display_name
      t.json :customizations
      t.timestamps
    end
  end
end
