# frozen_string_literal: true

# add overriding URL fields to Allocation table
class AddUrlFieldsToAllocation < ActiveRecord::Migration[6.0]
  def change
    change_table :allocations do |t|
      t.string :ip_access_url
      t.string :remote_access_url
    end
  end
end
