# frozen_string_literal: true

# :nodoc:
class MakeSpotlightConfigPolymorphic < ActiveRecord::Migration[6.0]
  def change
    change_table :spotlight_configurations do |t|
      t.remove_references :institution

      t.bigint :spotlight_configable_id
      t.string :spotlight_configable_type
    end

    add_index :spotlight_configurations,
              %i[spotlight_configable_id spotlight_configable_type],
              name: 'index_spconfs_on_spconfigable_type_and_id'
  end
end
