class FixConfiguredBentos < ActiveRecord::Migration[6.0]
  def change
    remove_reference :configured_bentos, :predefined_bentos, index: true
    add_reference    :configured_bentos, :predefined_bento,  index: true
    remove_reference :configured_bentos, :bento_configs,     index: true
    add_reference    :configured_bentos, :bento_config,      index: true
  end
end
