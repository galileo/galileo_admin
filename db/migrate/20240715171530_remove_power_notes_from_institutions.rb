class RemovePowerNotesFromInstitutions < ActiveRecord::Migration[7.0]
  def change
    remove_column :institutions, :power_notes, :boolean
  end
end
