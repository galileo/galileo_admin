class AddVendorReferenceToContact < ActiveRecord::Migration[6.0]
  def change
    add_reference :contacts, :vendor
  end
end
