class AddTestSiteToInstitution < ActiveRecord::Migration[6.0]
  def change
      add_column :institutions, :test_site, :boolean, default: false
  end
end
