class DropSpotlightConfiguration < ActiveRecord::Migration[6.0]
  def change
    drop_table :spotlight_configurations
  end
end
