# frozen_string_literal: true

# :nodoc:
class RemoveExperimentsFromResource < ActiveRecord::Migration[6.0]
  def up
    change_table :resources do |t|
      t.remove :boost, :feature
    end
  end

  def down
    add_column :resources, :boost, :integer, default: 0
    add_column :resources, :feature, :boolean, default: false
  end
end
