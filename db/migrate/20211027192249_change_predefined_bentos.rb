class ChangePredefinedBentos < ActiveRecord::Migration[6.0]
  def change
    remove_column :predefined_bentos, :user_views, :string, array: true, null: false, default: []
    add_column :predefined_bentos, :description, :string, null: false, default: ''
    add_column :predefined_bentos, :admin_note, :string, null: false, default: ''
  end
end
