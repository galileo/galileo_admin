class ExplicitlySetNativeProfileForGkr < ActiveRecord::Migration[7.0]

  def gkr_bento_configs
    BentoConfig.where(%q[credentials #>> '{"API Profile"}' = 'gkr_api'])
  end

  def up
    gkr_bento_configs.update_all %q[credentials = credentials::jsonb||'{"Native EDS Profile": "gkr"}'::jsonb]
    Sunspot.index! gkr_bento_configs
  end

  def down
    gkr_bento_configs.update_all %q[credentials = credentials::jsonb - 'Native EDS Profile']
    Sunspot.index! gkr_bento_configs
  end

end
