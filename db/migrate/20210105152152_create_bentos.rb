class CreateBentos < ActiveRecord::Migration[6.0]
  def change
    create_table :bentos do |t|
      t.belongs_to :institution
      t.string :inst_code
      t.string :bento_type
      t.string :user_id
      t.string :password
      t.timestamps
    end
  end
end
