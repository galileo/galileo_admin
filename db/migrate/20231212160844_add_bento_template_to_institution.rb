class AddBentoTemplateToInstitution < ActiveRecord::Migration[6.1]
  def up
    add_column :institutions, :bento_template, :string
    Institution.includes(:institution_group).each do |inst|
      inst.update_columns bento_template: inst.inst_type
    end
  end

  def down
    remove_column :institutions, :bento_template
  end

end
