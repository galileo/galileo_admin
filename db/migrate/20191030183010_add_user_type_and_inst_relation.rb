# frozen_string_literal: true

# add field for user type, and set up join table for Users -> Institutions
class AddUserTypeAndInstRelation < ActiveRecord::Migration[6.0]
  def up
    change_table :users do |t|
      t.string :role
    end
    create_join_table :users, :institutions

    User.update_all role: 'admin'
  end

  def down
    remove_column :users, :role
    drop_join_table :users, :institutions
  end
end
