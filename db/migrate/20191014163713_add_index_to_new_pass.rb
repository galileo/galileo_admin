# frozen_string_literal: true

# add index to new_password field on Institution
class AddIndexToNewPass < ActiveRecord::Migration[6.0]
  def change
    add_index :institutions, :new_password
  end
end
