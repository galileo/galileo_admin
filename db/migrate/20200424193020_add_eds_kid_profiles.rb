# frozen_string_literal: true

# :nodoc:
class AddEdsKidProfiles < ActiveRecord::Migration[6.0]
  def change
    change_table :institutions do |t|
      t.string :eds_profile_teen, default: ''
      t.string :eds_profile_high_school, default: ''
    end
  end
end
