class AddNameToAllocations < ActiveRecord::Migration[6.1]
  def change
    add_column :allocations, :name, :string, null: false, default: ''
  end
end
