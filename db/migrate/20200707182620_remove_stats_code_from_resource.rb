class RemoveStatsCodeFromResource < ActiveRecord::Migration[6.0]
  def change
    remove_column :resources, :stats_code, :string
  end
end
