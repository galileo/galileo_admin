class RenameFreelyAvailableInInstitutions < ActiveRecord::Migration[6.0]
  def change
    rename_column :resources, :freely_available, 'bypass_galileo_authentication'
  end
end
