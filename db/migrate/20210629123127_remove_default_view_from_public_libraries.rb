class RemoveDefaultViewFromPublicLibraries < ActiveRecord::Migration[6.0]
  def up
    # features
    Institution.public_libraries.each do |publ|
      publ.features.destroy_by(view_type: 'default')
    end

    inst_group = InstitutionGroup.find_by_inst_type 'publiclib'
    inst_group.features.destroy_by(view_type: 'default')

    Bento.includes(institution: :institution_group).destroy_by(view_type: 'default', institution: { institution_groups: { code: 'publiclibs' }} )
  end

  def down
    # features
    inst_group = InstitutionGroup.find_by_inst_type 'publiclib'
    inst_group.features.create({
                                 position: 1, view_type: 'default', featuring_type: 'InstitutionGroup',
                                 link_url: 'express?link==zglf', link_label: 'Gale LegalForms',
                                 link_description: 'This resources helps patrons understand common legal procedures with access to authentic, legal documents and is useful for filing patents, developing leases, delegating power of attorney, and more.'
                               })
    inst_group.features.create({
                                 position: 2, view_type: 'default', featuring_type: 'InstitutionGroup',
                                 link_url: 'express?link=erkd', link_label: 'e-READ KIDS',
                                 link_description: 'A digital library giving young readers access to almost 15,000 electronic and audio books (both fiction and non-fiction titles) for children in pre-kindergarten to fourth grade.'
                               })
    inst_group.features.create({
                                 position: 3, view_type: 'default', featuring_type: 'InstitutionGroup',
                                 link_url: 'express?link=zual', link_label: 'Ancestry Library Edition',
                                 link_description: 'A collection of about 4,000 databases (continually updated) including collections such as: the U.S. Census, Map Center with more than 1,000 historical maps, and the Social Security Death Index.'
                               })
    inst_group.features.create({
                                 position: 4, view_type: 'default', featuring_type: 'InstitutionGroup',
                                 link_url: 'express?link=zbpl', link_label: 'EBSCO eBooks Public Library Collection',
                                 link_description: 'An e-book collection of titles chosen specifically for public libraries, featuring fiction titles for adults and juveniles, as well as best-selling and recommended titles from industry leading publishers.'
                               })
    inst_group.features.create({
                                 position: 5, view_type: 'default', featuring_type: 'InstitutionGroup',
                                 link_url: 'express?link=ango', link_label: 'Mángo Languages',
                                 link_description: 'An online language-learning system teaching conversational skills for a wide variety of languages, such as Spanish, French, Japanese, Brazilian Portuguese, German, Mandarin Chinese, Greek, Italian, Russian, and more.'
                               })
    inst_group.features.create({
                                 position: 6, view_type: 'default', featuring_type: 'InstitutionGroup',
                                 link_url: 'express?link=zkpl', link_label: 'NoveList Plus',
                                 link_description: 'An advisory tool that helps readers find new books and audiobooks based on books they’ve read or topics they are interested with access to book reviews and annotations.'
                               })

  end
end
