class CreateBrandings < ActiveRecord::Migration[6.0]
  def change
    create_table :brandings do |t|
      t.belongs_to :institution
      t.string :name, default: ''
      t.string :code, default: ''
      t.string :text, default: ''
      t.timestamps
    end
  end
end
