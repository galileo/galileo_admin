class AddNoResultsMessageToPredefinedBento < ActiveRecord::Migration[6.0]
  def change
    add_column :predefined_bentos, :no_results_message, :string, default: ''
  end
end
