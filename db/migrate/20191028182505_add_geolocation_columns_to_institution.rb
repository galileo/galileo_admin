# frozen_string_literal: true

class AddGeolocationColumnsToInstitution < ActiveRecord::Migration[6.0]
  def change
    change_table :institutions do |t|
      t.string :zip_codes, array: true, default: [], index: true
      t.string :counties, array: true, default: []
    end
  end
end
