class AddLtiFieldsToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :lti_consumer_key, :string, default: ''
    add_column :institutions, :lti_custom_inst_id, :string, default: ''
    add_column :institutions, :lti_secret, :string, default: ''
  end
end
