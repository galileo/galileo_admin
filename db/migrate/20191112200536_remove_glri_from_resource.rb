# frozen_string_literal: true

# :nodoc:
class RemoveGlriFromResource < ActiveRecord::Migration[6.0]
  def change
    remove_column :resources, :glri
  end
end
