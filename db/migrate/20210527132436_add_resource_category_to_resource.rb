class AddResourceCategoryToResource < ActiveRecord::Migration[6.0]
  def change
    add_column :resources, :serial_count, :integer, default: 0, null: false
    add_column :resources, :resource_category, :string, default: 'None', null: false
  end
end
