# frozen_string_literal: true

# :nodoc:
class AddEdsFieldsToInstitution < ActiveRecord::Migration[6.0]
  def change
    change_table :institutions do |t|
      t.string :eds_profile_default
      t.string :eds_profile_kids
      t.string :eds_customer_id
      t.string :eds_managed_by
    end
  end
end
