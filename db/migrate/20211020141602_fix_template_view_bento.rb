class FixTemplateViewBento < ActiveRecord::Migration[6.0]
  def change
    remove_reference :template_view_bentos, :predefined_bentos, index: true
    add_reference    :template_view_bentos, :predefined_bento, index: true
  end
end
