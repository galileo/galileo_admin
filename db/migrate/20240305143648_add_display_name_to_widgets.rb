class AddDisplayNameToWidgets < ActiveRecord::Migration[7.0]
  def change
    add_column :widgets, :display_name, :string
    Widget.all.update_all(display_name: Widget::DEFAULT_DISPLAY_NAME)
  end
end
