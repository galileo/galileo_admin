# frozen_string_literal: true

class ChangeNullArrayFieldsToEmptyArrays < ActiveRecord::Migration[6.0]
  def up
    # Need to change all array fields that are null to empty arrays before
    # changing the columns to not accep null and default to empty arrays
    null_to_empty(NotifyGroup, 'change_dates')
    null_to_empty(NotifyGroup, 'notify_dates')
    null_to_empty(NotifyGroup, 'update_history')
    null_to_empty(Resource, 'system_list')
    null_to_empty(Resource, 'institution_list')
    null_to_empty(Institution, 'ip_number')
    null_to_empty(Institution, 'remote_ips')
    null_to_empty(Institution, 'password_change_dates')
    null_to_empty(Institution, 'password_notify_dates')

    change_column :notify_groups, :change_dates, :string, array: true, null: false, default: []
    change_column :notify_groups, :notify_dates, :string, array: true, null: false, default: []
    change_column :notify_groups, :update_history, :string, array: true, null: false, default: []
    change_column :resources, :system_list, :string, array: true, null: false, default: []
    change_column :resources, :institution_list, :string, array: true, null: false, default: []
    change_column :institutions, :ip_number, :string, array: true, null: false, default: []
    change_column :institutions, :remote_ips, :string, array: true, null: false, default: []
    change_column :institutions, :password_change_dates, :string, array: true, null: false, default: []
    change_column :institutions, :password_notify_dates, :string, array: true, null: false, default: []
  end

  def down
    # Don't really need empty arrays to be turned back in to null values
    change_column :notify_groups, :change_dates, :string, array: true
    change_column :notify_groups, :notify_dates, :string, array: true
    change_column :notify_groups, :update_history, :string, array: true
    change_column :resources, :system_list, :string, array: true
    change_column :resources, :institution_list, :string, array: true
    change_column :institutions, :ip_number, :string, array: true
    change_column :institutions, :remote_ips, :string, array: true
    change_column :institutions, :password_change_dates, :string, array: true
    change_column :institutions, :password_notify_dates, :string, array: true
  end

  # Find all the null values in an array_field and set them to empty arrays
  # @param [ActiveRecord] model
  # @param [String] array_field
  def null_to_empty(model, array_field)
    model.where("#{array_field} is NULL").update_all(array_field => '{}')
  end
end
