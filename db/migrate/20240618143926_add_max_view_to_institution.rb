class AddMaxViewToInstitution < ActiveRecord::Migration[7.0]
  def change
    add_column :institutions, :limit_view_switching, :integer, default: 0, null: false
  end
end
