# frozen_string_literal: true

# adds a boolean field to Institution that will tell nightly jobs to update the
# password for this institution regardless of other logic
class AddChangePassAsapToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :change_password_asap, :boolean, default: false, null: false
  end
end
