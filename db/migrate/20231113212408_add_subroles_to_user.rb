class AddSubrolesToUser < ActiveRecord::Migration[6.1]
  def up
    add_column :users, :subroles, :string, array: true, nil: false, default: []
    User.with_role('institutional').joins(:contacts).merge(Contact.with_type('port')).update_all(['subroles = array_append(subroles, ?)', 'portal'])
    User.with_role('institutional').joins(:contacts).merge(Contact.with_type('adv')).update_all(['subroles = array_append(subroles, ?)', 'advanced'])
  end

  def down
    remove_column :users, :subroles, :string, array: true, nil: false, default: []
  end
end
