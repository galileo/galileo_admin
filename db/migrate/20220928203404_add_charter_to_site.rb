class AddCharterToSite < ActiveRecord::Migration[6.1]
  def change
    add_column :sites, :charter, :boolean, default: false
  end
end
