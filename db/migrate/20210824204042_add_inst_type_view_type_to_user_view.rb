# frozen_string_literal: true

class AddInstTypeViewTypeToUserView < ActiveRecord::Migration[6.0]
  def up
    add_column :user_views, :inst_type, :string, array: true, default: [], null: false
    add_column :user_views, :full, :boolean, default: false, null: false

    UserView.where(code: ['elementary', 'middle', 'highschool']).update_all(inst_type: ['k12','publiclib'])

    UserView.create!( code: 'educator', name: 'Educator', full: true, inst_type: ['k12'] )
    UserView.create!( code: 'full',     name: 'Full',     full: true, inst_type: ['publiclib'] )
    UserView.create!( code: 'default',  name: 'Default',  full: true )
  end

  def down
    remove_column :user_views, :inst_type
    remove_column :user_views, :full

    UserView.where(code: ['educator', 'full', 'default']).destroy_all
  end
end
