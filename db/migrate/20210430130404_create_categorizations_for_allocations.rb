class CreateCategorizationsForAllocations < ActiveRecord::Migration[6.0]
  def change
    create_table :allocation_categorizations do |t|
      t.belongs_to :subject
      t.belongs_to :allocation
      t.timestamps
    end
  end
end
