class AddOverrideUserViewToAllocations < ActiveRecord::Migration[6.0]
  def change
    add_column :allocations, :override_user_views, :boolean, default: false
  end
end
