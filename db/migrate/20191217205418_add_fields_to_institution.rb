class AddFieldsToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_column :institutions, :open_athens_org_id, :string
    add_column :institutions, :open_athens_entity_id, :string
    add_column :institutions, :open_athens_proxy_ip, :string
  end
end
