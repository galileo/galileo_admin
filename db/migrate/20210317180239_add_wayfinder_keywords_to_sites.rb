class AddWayfinderKeywordsToSites < ActiveRecord::Migration[6.0]
  def change
    add_column :sites, :wayfinder_keywords, :string, default: ''
  end
end
