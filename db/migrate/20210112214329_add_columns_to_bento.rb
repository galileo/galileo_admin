class AddColumnsToBento < ActiveRecord::Migration[6.0]
  def change
    add_column :bentos, :view_type, :string, default: ""
    add_column :bentos, :api_profile, :string, default: ""
  end
end
