class AddNotifyGroupToInstitution < ActiveRecord::Migration[6.0]
  def change
    add_reference :institutions, :notify_group, index: true
    remove_column :institutions, :password_notify_group, :string
  end
end
