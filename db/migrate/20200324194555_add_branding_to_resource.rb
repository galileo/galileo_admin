class AddBrandingToResource < ActiveRecord::Migration[6.0]
  def change
    add_reference :resources, :branding
  end
end
