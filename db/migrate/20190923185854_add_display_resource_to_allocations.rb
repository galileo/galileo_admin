class AddDisplayResourceToAllocations < ActiveRecord::Migration[6.0]
  def change
    add_column :allocations, :display, :boolean, default: true
  end
end
