class AddDisallowOpenAthensToAllocations < ActiveRecord::Migration[6.0]
  def change
    add_column :allocations, :disallow_open_athens, :boolean, default: false
  end
end
