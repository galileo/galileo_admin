class ChangePredefinedBentosField < ActiveRecord::Migration[6.0]
  def change
    rename_column :predefined_bentos, :customizations, :definition
  end
end
