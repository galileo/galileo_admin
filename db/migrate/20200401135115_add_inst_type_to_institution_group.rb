# frozen_string_literal: true

class AddInstTypeToInstitutionGroup < ActiveRecord::Migration[6.0]
  def change
    add_column :institution_groups, :inst_type, :string, default: 'default'

    InstitutionGroup.where(code: %w[ampals univsystem techinst univcenter gpals srec auc uga uga1])
                    .update_all(inst_type: 'highered')
    InstitutionGroup.where(code: %w[k12 k12ttc privk12])
                    .update_all(inst_type: 'k12')
    InstitutionGroup.where(code: %w[publiclibs])
                    .update_all(inst_type: 'publiclib')
    InstitutionGroup.where(code: %w[public])
                    .update_all(inst_type: 'default')
  end
end
