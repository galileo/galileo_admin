class SetDefaultPublicationFinderProfile < ActiveRecord::Migration[6.0]
  def up
    PredefinedBento.where(service: 'eds_publications').each {|pb| pb.update(definition: {'Profile Name' => 'pfi'})}
    Sunspot.commit
  end
  def down; end
end
