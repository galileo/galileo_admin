# frozen_string_literal: true

# :nodoc:
class AddCredentialDisplayField < ActiveRecord::Migration[6.0]
  def change
    change_table :resources do |t|
      t.string :credential_display, default: 'never'
    end
  end
end
