class UpdateViewSwitchingInInstitution < ActiveRecord::Migration[7.0]
  def change
    rename_column :institutions, :limit_view_switching, :view_switching
  end
end
