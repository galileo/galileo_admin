class AddKeywordsToAllocations < ActiveRecord::Migration[6.0]
  def change
    add_column :allocations, :keywords, :string, array: true, default: [], null: false
  end
end
