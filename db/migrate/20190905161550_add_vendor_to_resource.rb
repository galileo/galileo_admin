class AddVendorToResource < ActiveRecord::Migration[6.0]
  def up
    change_table :resources do |t|
      t.references :vendor, index: true
    end
    Resource.all.each do |res|
      next unless res.active? && !res.groups.include?('glri') && res.vendor_code

      vendor = Vendor.find_by(name: res.vendor_code)
      if vendor
        res.update vendor: vendor
      else
        res.update vendor: Vendor.create(name: res.vendor_code)
      end
    end
  end

  def down
    remove_reference :resources, :vendor
  end
end
