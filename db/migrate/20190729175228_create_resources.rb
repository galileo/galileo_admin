class CreateResources < ActiveRecord::Migration[5.2]
  def change
    create_table :resources do |t|
      t.string :resource_id
      t.string :name
      t.json :legacy_data

      t.timestamps
    end
  end
end
