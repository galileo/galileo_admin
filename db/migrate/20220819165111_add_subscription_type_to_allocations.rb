class AddSubscriptionTypeToAllocations < ActiveRecord::Migration[6.1]
  def up
    add_column :allocations, :subscription_type, :string, default: ''
  end

  def down
    remove_column :allocations, :subscription_type, :string, default: ''
  end
end
