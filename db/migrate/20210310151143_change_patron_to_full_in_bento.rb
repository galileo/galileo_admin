class ChangePatronToFullInBento < ActiveRecord::Migration[6.0]
  def up
    Bento.where( view_type: 'patron' ).update_all( view_type: 'full' )
  end
  def down
    Bento.where( view_type: 'full' ).update_all( view_type: 'patron' )
  end
end
