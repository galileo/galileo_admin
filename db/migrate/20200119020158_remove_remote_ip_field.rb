# frozen_string_literal: true

# :nodoc:
class RemoveRemoteIpField < ActiveRecord::Migration[6.0]
  def up
    remove_column :institutions, :remote_ips
  end

  def down
    change_table :institutions do |t|
      t.string :remote_ips, array: true, default: []
    end
  end
end
