class AddWidgets < ActiveRecord::Migration[6.1]
  def change
    create_table :widgets do |t|
      t.references :institution, null:false
      t.integer :service, null:false, default:0
      t.string :credentials
      t.boolean :active, null:false, default: false
    end
  end
end
