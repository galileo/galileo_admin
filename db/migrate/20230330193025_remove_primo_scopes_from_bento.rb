class RemovePrimoScopesFromBento < ActiveRecord::Migration[6.1]
  def up
    execute "UPDATE bento_configs
                 SET credentials = credentials::jsonb - 'Scope'
                 WHERE service = 'primo'"
    Sunspot.index! BentoConfig.where(service: 'primo')
  end

  def down
    execute "UPDATE bento_configs
                 SET credentials = credentials::jsonb||'{\"Scope\": \"default_scope\"}'::jsonb
                 WHERE service = 'primo'"
    Sunspot.index! BentoConfig.where(service: 'primo')
  end
end
