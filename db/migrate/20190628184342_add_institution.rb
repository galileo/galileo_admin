# frozen_string_literal: true

# create Institution table and add some fields
class AddInstitution < ActiveRecord::Migration[5.2]
  def change
    create_table :institutions do |t|
      t.string :institution_code
      t.string :public_code
      t.string :name
      t.json :legacy_data
    end
  end
end
