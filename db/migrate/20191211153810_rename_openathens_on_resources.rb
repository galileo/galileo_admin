# frozen_string_literal: true

# :nodoc:
class RenameOpenathensOnResources < ActiveRecord::Migration[6.0]
  def change
    change_table :resources do |t|
      t.rename :openathens, :open_athens
      t.rename :openathens_url, :open_athens_url
    end
  end
end
