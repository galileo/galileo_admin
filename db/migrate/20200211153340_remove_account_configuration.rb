# frozen_string_literal: true

# remove AccountConfiguration model - it didn't work with the legacy model
class RemoveAccountConfiguration < ActiveRecord::Migration[6.0]
  def up
    drop_table :account_configurations
  end

  def down
    create_table :account_configurations do |t|
      t.belongs_to :institution, index: true
      t.belongs_to :vendor, index: true
      t.json :credentials
      t.timestamps
    end
  end
end
