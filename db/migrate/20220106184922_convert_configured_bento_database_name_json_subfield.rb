class ConvertConfiguredBentoDatabaseNameJsonSubfield < ActiveRecord::Migration[6.0]
  def up
    custom_resource_bentos = PredefinedBento.find_by_code('eds_resource')&.configured_bentos || []
    custom_resource_bentos.each do |cb|
      if cb.customizations&.dig('Database Name').present?
        cb.customizations['Content Provider Facet'] = [cb.customizations.delete('Database Name')]
        cb.save
      end
    end
    # We don't expect the output to Solr to have changed, but still syncing anyway as a best-practice
    Sunspot.index! custom_resource_bentos
  end
  def down
    custom_resource_bentos = PredefinedBento.find_by_code('eds_resource')&.configured_bentos || []
    custom_resource_bentos.each do |cb|
      if cb.customizations&.dig('Content Provider Facet').present?
        databases = cb.customizations.delete('Content Provider Facet')
        cb.customizations['Database Name'] = databases[0] if databases.present? && databases.any?
        cb.save
      end
    end
    Sunspot.index! custom_resource_bentos
  end
end
