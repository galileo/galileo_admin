class CreateSites < ActiveRecord::Migration[6.0]
  def change
    create_table :sites do |t|
      t.belongs_to :institution
      t.string :name, default: ''
      t.string :code, default: ''
      t.string :site_type, default: ''
      t.json :legacy_data
      t.timestamps
    end
  end
end
