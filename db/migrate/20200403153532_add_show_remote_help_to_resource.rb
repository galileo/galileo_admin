class AddShowRemoteHelpToResource < ActiveRecord::Migration[6.0]
  def change
    add_column :resources, :show_remote_help, :boolean, default: false
  end
end
