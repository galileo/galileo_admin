# frozen_string_literal: true

class UpdatePasswordDatesInInstitutions < ActiveRecord::Migration[6.0]
  def change
    remove_column :institutions, :password_change_dates, :string
    remove_column :institutions, :password_notify_dates, :string
    remove_column :notify_groups, :change_dates, :string
    remove_column :notify_groups, :notify_dates, :string
    add_column :institutions, :password_change_dates, :string, array: true
    add_column :institutions, :password_notify_dates, :string, array: true
    add_column :notify_groups, :change_dates, :string, array: true
    add_column :notify_groups, :notify_dates, :string, array: true
  end
end
