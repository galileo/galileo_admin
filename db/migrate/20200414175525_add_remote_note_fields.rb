# frozen_string_literal: true

# Add fields for handling storage and display of "Remote Note" data
class AddRemoteNoteFields < ActiveRecord::Migration[6.0]
  def change
    change_table :resources do |t|
      t.boolean :always_show_remote_note, default: false
      t.string :remote_note, default: ''
    end
  end
end
