class ChangeWayfinderKeywordsIntoArray < ActiveRecord::Migration[6.0]
  def up
    add_column :institutions, :wayfinder_keyword_array, :string, array: true, default: [], null: false
    add_column :sites, :wayfinder_keyword_array, :string, array: true, default: [], null: false

    Institution.active.each do |institution|
      institution.update!(wayfinder_keyword_array: convert_keywords_to_array(institution.wayfinder_keywords))
    end
    Site.all.each do |site|
      site.update!(wayfinder_keyword_array: convert_keywords_to_array(site.wayfinder_keywords))
    end

    remove_column :institutions, :wayfinder_keywords
    rename_column :institutions, :wayfinder_keyword_array, :wayfinder_keywords

    remove_column :sites, :wayfinder_keywords
    rename_column :sites, :wayfinder_keyword_array, :wayfinder_keywords
  end

  def down
    add_column :institutions, :wayfinder_keyword_string, :string, default: ''
    add_column :sites, :wayfinder_keyword_string, :string, default: ''

    Institution.active.each do |institution|
      institution.update!(wayfinder_keyword_string: institution.wayfinder_keywords.join("\n")) if institution.wayfinder_keywords.any?
    end
    Site.all.each do |site|
      site.update!(wayfinder_keyword_string: site.wayfinder_keywords.join("\n")) if site.wayfinder_keywords.any?
    end

    remove_column :institutions, :wayfinder_keywords
    rename_column :institutions, :wayfinder_keyword_string, :wayfinder_keywords
    remove_column :sites, :wayfinder_keywords
    rename_column :sites, :wayfinder_keyword_string, :wayfinder_keywords
  end

  def convert_keywords_to_array(keywords_string)
    return [] if keywords_string.blank?

    keywords_string.split("\n").map(&:strip)
  end
end
