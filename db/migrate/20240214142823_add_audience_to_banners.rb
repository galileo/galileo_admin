class AddAudienceToBanners < ActiveRecord::Migration[6.1]
  def change
    add_column :banners, :audience, :integer, default: 0, null: false
    Banner.where(institution_id: [nil]).update_all(audience: Banner.audiences['everyone'])
    Banner.where.not(institution_id: [nil]).update_all(audience: Banner.audiences['institution'])

    add_reference :banners, :institution_group, null: true
  end
end
