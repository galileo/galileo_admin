# frozen_string_literal: true

# :nodoc:
class RemoveOldPinesCodeField < ActiveRecord::Migration[6.0]
  def change
    remove_column :institutions, :pines_library_code
  end
end
