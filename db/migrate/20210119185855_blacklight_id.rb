class BlacklightId < ActiveRecord::Migration[6.0]
  def change
    add_column :bentos, :blacklight_id, :string, default: ""
  end
end
