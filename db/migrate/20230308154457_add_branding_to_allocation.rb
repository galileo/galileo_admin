class AddBrandingToAllocation < ActiveRecord::Migration[6.1]
  def change
    add_reference :allocations, :branding, index: true
  end
end
