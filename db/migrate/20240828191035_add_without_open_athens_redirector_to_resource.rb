class AddWithoutOpenAthensRedirectorToResource < ActiveRecord::Migration[7.0]
  def change
    add_column :resources, :without_open_athens_redirector, :boolean, default: false
  end
end
