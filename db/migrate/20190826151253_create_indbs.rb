class CreateIndbs < ActiveRecord::Migration[6.0]
  def change
    create_table :indbs do |t|
      t.string :indb_id
      t.string :database_code
      t.string :institution_code
      t.string :local_url
      t.string :remote_url
      t.string :vendor_user_id
      t.string :vendor_password
      t.string :remote_note
      t.string :remote_help
      t.string :special_info
      t.string :note
      t.string :last_updated_by
      t.timestamps
    end
  end
end
