class RenameIpNumberInInstitution < ActiveRecord::Migration[6.0]
  def change
    rename_column :institutions, :ip_number, :ip_addresses
  end
end
