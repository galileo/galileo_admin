class AddOaProxyToResource < ActiveRecord::Migration[6.0]
  def change
    add_column :resources, :oa_proxy, :boolean, default: false
  end
end
