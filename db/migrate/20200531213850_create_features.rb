# frozen_string_literal: true

# Create Feature model to represent a featured thing in GALILEO Search
class CreateFeatures < ActiveRecord::Migration[6.0]
  def change
    create_table :features do |t|
      t.string :view_type
      t.integer :position
      t.string :link_url
      t.string :link_label
      t.string :link_description
      t.references :featuring, polymorphic: true
      t.timestamps
    end
  end
end
