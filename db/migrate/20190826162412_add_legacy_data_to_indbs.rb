class AddLegacyDataToIndbs < ActiveRecord::Migration[6.0]
  def change
    change_table :indbs do |t|
      t.json :legacy_data
    end
  end
end
