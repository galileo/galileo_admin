class AddDescriptionToConfiguredBento < ActiveRecord::Migration[6.0]
  def change
    add_column :configured_bentos, :description, :string, null: false, default: ''
    reversible do |dir|
      dir.up do
        change_column :configured_bentos, :display_name, :string, null: false, default: ''
      end
      dir.down do
        change_column :configured_bentos, :display_name, :string, null: true, default: nil
      end
    end
  end
end
