# frozen_string_literal: true

# Spotlight configs are added in an after_create hook on Institutions and IGs
# So we need this migration to add them for IGs that have already been created
class AddConfigsToGroups < ActiveRecord::Migration[6.0]
  def change
    InstitutionGroup.all.each do |ig|
      next unless ig.spotlight_configurations.any?
      
      ig.add_spotlight_configs
    end
  end
end
