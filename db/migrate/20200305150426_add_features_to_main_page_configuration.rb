class AddFeaturesToMainPageConfiguration < ActiveRecord::Migration[6.0]
  def change
    remove_column :main_page_configurations, :featured_allocation_image_1, :string
    remove_column :main_page_configurations, :featured_allocation_image_2, :string
    remove_column :main_page_configurations, :featured_allocation_image_3, :string
    remove_column :main_page_configurations, :kids_allocation_image_1, :string
    remove_column :main_page_configurations, :kids_allocation_image_2, :string
    remove_column :main_page_configurations, :kids_allocation_image_3, :string
    add_reference :main_page_configurations, :featured_allocation_4
    add_reference :main_page_configurations, :featured_allocation_5
    add_reference :main_page_configurations, :featured_allocation_6
    add_reference :main_page_configurations, :kids_allocation_4
    add_reference :main_page_configurations, :kids_allocation_5
    add_reference :main_page_configurations, :kids_allocation_6
  end
end
