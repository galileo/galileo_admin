class AddMultipleIndices < ActiveRecord::Migration[6.0]
  def change
    add_index :institutions, :current_password
    add_index :passwords, :password
    add_index :institution_groups, :code
    rename_column :institutions, :institution_code, :code
    rename_column :resources, :resource_id, :code
    add_index :resources, :code
    add_index :institutions, :code
  end
end
