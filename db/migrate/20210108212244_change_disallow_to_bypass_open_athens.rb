class ChangeDisallowToBypassOpenAthens < ActiveRecord::Migration[6.0]
  def change
    rename_column :allocations, :disallow_open_athens, :bypass_open_athens
  end
end
