class ConvertBannerMessageToActionText < ActiveRecord::Migration[7.0]
  include ActionView::Helpers::TextHelper
  def up
    add_column :banners, :content, :text
    Banner.all.each do |banner|
      banner.update_attribute(:content, simple_format(banner.message))
    end
    remove_column :banners, :message, :string

  end

  def down
    add_column :banners, :message, :string
    Banner.all.each do |banner|
      banner.update_attribute(:message, banner.message.to_plain_text)
    end
    remove_column :banners, :content, :text

  end
end
