class CreateTemplateViewBento < ActiveRecord::Migration[6.0]
  def change
    create_table :template_view_bentos do |t|
      t.string :inst_type, default: '', null: false
      t.belongs_to :predefined_bentos
      t.string :user_view, default: '', null: false
      t.float :default_order
      t.boolean :shown_by_default, default: false
      t.timestamps
    end
  end
end

