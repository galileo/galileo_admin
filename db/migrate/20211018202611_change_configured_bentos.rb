class ChangeConfiguredBentos < ActiveRecord::Migration[6.0]
  def change
    add_reference :configured_bentos, :institution, index: true
    remove_column :configured_bentos, :user_views, :string, array: true, null: false, default: []
    add_column    :configured_bentos, :user_view, :string, null: false, default: ''
    rename_column :configured_bentos, :code, :slug
    add_column    :configured_bentos, :order, :float
    add_column    :configured_bentos, :active, :boolean, default: true
    add_column    :configured_bentos, :shown_by_default, :boolean, default: false
  end
end
