class AddActiveColumnToInstitutions < ActiveRecord::Migration[6.0]
  def up
    add_column :institutions, :active, :boolean
    Institution.all.each do |inst|
      inst.update(active: inst.status == 'active')
    end
    remove_column :institutions, :status
  end

  def down
    add_column :institutions, :status, :string
    Institution.all.each do |inst|
      inst.update(status: ('active' if inst.active))
    end
    remove_column :institutions, :active
  end
end
