# frozen_string_literal: true

class CreateJoinTableInstitutionsInstitutionGroups < ActiveRecord::Migration[5.2]
  def change
    create_join_table :institutions, :institution_groups do |t|
      t.index(%i[institution_id institution_group_id],
              unique: true,
              name: 'idx_inst_inst_group')
      t.index(%i[institution_group_id institution_id],
              unique: true,
              name: 'idx_inst_group_inst')
    end
  end
end
