class ConsolidateActiveStorageServices < ActiveRecord::Migration[6.1]
  def up
    execute 'DELETE FROM active_storage_variant_records'
    execute "UPDATE active_storage_blobs SET service_name = 'uploads' " +
              "WHERE service_name in ('local', 'amazon_dev', 'amazon_staging', 'amazon_production')"
  end

  def down
    old_service = {
      development:  'local',
      dev:          'amazon_dev',
      staging:      'amazon_staging',
      production:   'amazon_production'
    }[Rails.env.to_sym]
    execute "UPDATE active_storage_blobs SET service_name = '#{old_service}' WHERE service_name = 'uploads'"
  end
end
