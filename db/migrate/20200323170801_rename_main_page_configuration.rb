class RenameMainPageConfiguration < ActiveRecord::Migration[6.0]
  def change
    rename_table :main_page_configurations, :spotlight_configurations

    remove_column :spotlight_configurations, :institution_url_1, :string
    remove_column :spotlight_configurations, :institution_url_2, :string
    remove_column :spotlight_configurations, :institution_url_3, :string
    remove_column :spotlight_configurations, :institution_url_4, :string
    remove_column :spotlight_configurations, :institution_url_5, :string

    remove_column :spotlight_configurations, :institution_url_label_1, :string
    remove_column :spotlight_configurations, :institution_url_label_2, :string
    remove_column :spotlight_configurations, :institution_url_label_3, :string
    remove_column :spotlight_configurations, :institution_url_label_4, :string
    remove_column :spotlight_configurations, :institution_url_label_5, :string

    remove_reference :spotlight_configurations, :kids_allocation_1
    remove_reference :spotlight_configurations, :kids_allocation_2
    remove_reference :spotlight_configurations, :kids_allocation_3
    remove_reference :spotlight_configurations, :kids_allocation_4
    remove_reference :spotlight_configurations, :kids_allocation_5
    remove_reference :spotlight_configurations, :kids_allocation_6

    remove_column :spotlight_configurations, :kids_institution_url_1, :string
    remove_column :spotlight_configurations, :kids_institution_url_2, :string
    remove_column :spotlight_configurations, :kids_institution_url_3, :string
    remove_column :spotlight_configurations, :kids_institution_url_4, :string
    remove_column :spotlight_configurations, :kids_institution_url_5, :string

    remove_column :spotlight_configurations, :kids_institution_url_label_1, :string
    remove_column :spotlight_configurations, :kids_institution_url_label_2, :string
    remove_column :spotlight_configurations, :kids_institution_url_label_3, :string
    remove_column :spotlight_configurations, :kids_institution_url_label_4, :string
    remove_column :spotlight_configurations, :kids_institution_url_label_5, :string

    add_column :spotlight_configurations, :view_type, :string, default: 'Default'


    add_column :institutions, :institution_url_1, :string, default: ''
    add_column :institutions, :institution_url_2, :string, default: ''
    add_column :institutions, :institution_url_3, :string, default: ''
    add_column :institutions, :institution_url_4, :string, default: ''
    add_column :institutions, :institution_url_5, :string, default: ''

    add_column :institutions, :institution_url_label_1, :string, default: ''
    add_column :institutions, :institution_url_label_2, :string, default: ''
    add_column :institutions, :institution_url_label_3, :string, default: ''
    add_column :institutions, :institution_url_label_4, :string, default: ''
    add_column :institutions, :institution_url_label_5, :string, default: ''

  end
end
