# frozen_string_literal: true

# update Institution model fields to match NotifyGroup fields
class UpdatePassFieldNames < ActiveRecord::Migration[6.0]
  def change
    change_table :institutions do |t|
      t.rename :password_change_dates, :change_dates
      t.rename :password_notify_dates, :notify_dates
    end
  end
end
