class RemoveVendorCodeFromResource < ActiveRecord::Migration[6.0]
  def change
    remove_column :resources, :vendor_code, :string
  end
end
