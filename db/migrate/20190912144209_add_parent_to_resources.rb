# frozen_string_literal: true
class AddParentToResources < ActiveRecord::Migration[6.0]
  def up
    add_reference :resources, :parent, index: true
    Resource.all.each do |res|
      parent = if res.resource_id.include? ':'
                 parent_resource_id = res.resource_id.split(':').first
                 Resource.find_by_resource_id(parent_resource_id)
               end
      res.update(parent_id: parent.id) if parent
    end
  end

  def down
    remove_reference :resources, :parent
  end
end
