class ChangeKeywordsToArray < ActiveRecord::Migration[6.0]
  def up
    add_column :resources, :keyword_array, :string, array: true, default: [], null: false
    Resource.all.each do |resource|
      resource.update!(keyword_array: convert_keywords_to_array(resource.keywords))
    end
    remove_column :resources, :keywords
    rename_column :resources, :keyword_array, :keywords
  end

  def down
    add_column :resources, :keyword_string, :string, default: ''

    Resource.all.each do |resource|
      resource.update!(keyword_string: resource.keywords.join("\n")) if resource.keywords.any?
    end
    remove_column :resources, :keywords
    rename_column :resources, :keyword_string, :keywords
  end

  def convert_keywords_to_array(keywords_string)
    return [] if keywords_string.blank?

    keywords_string.split("\n").map(&:strip)
    end
end
