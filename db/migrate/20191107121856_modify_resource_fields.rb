# frozen_string_literal: true

# :nodoc:
class ModifyResourceFields < ActiveRecord::Migration[6.0]
  def change
    change_table :resources do |t|
      t.rename :vendor_user_id, :user_id
      t.rename :vendor_password, :password

      t.datetime :new_until
      t.boolean :show_institutional_branding

      # experimental fields
      t.boolean :freely_available
      t.boolean :feature
      t.integer :boost
    end
  end
end
