class CreateBanners < ActiveRecord::Migration[6.1]
  def change
    create_table :banners do |t|
      t.string :message, null: false, default: ''
      t.datetime :start_time, null: false
      t.datetime :end_time, null: false
      t.integer :style, null: false, default: 0
      t.references :institution, null: true
      t.timestamps
    end
  end
end
