class CreateBibliographicInfoStyleJsonField < ActiveRecord::Migration[6.0]
  def change
    PredefinedBento.with_service('eds_api').each do |pb|
      bib_style = pb.definition&.dig 'Bibliographic Info Style'
      next if bib_style.present?

      bib_style = 'Standalone Work'
      parent_publication = pb.definition&.dig('Has Parent Publication')
      bib_style = 'Article' if parent_publication.present? && parent_publication != false
      bib_style = 'Encyclopedia' if pb.code =~ /^encyclopedia\b/i
      pb.definition ||= {}
      pb.definition['Bibliographic Info Style'] = bib_style
      pb.save
    end
    PredefinedBento.find_by_code('eds_custom').configured_bentos.each do |cb|
      bib_style = cb.customizations&.dig 'Bibliographic Info Style'
      next if bib_style.present?

      bib_style = 'Standalone Work'
      parent_publication = cb.customizations&.dig('Has Parent Publication')
      bib_style = 'Article' if parent_publication.present? && parent_publication != false
      cb.customizations ||= {}
      cb.customizations['Bibliographic Info Style'] = bib_style
      cb.save
    end
    SolrService.reindex ConfiguredBento
  end
end
