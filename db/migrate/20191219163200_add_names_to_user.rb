class AddNamesToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string

    User.all.each do |user|
      name = user.email.split('@').first
      first = name.split('.').first
      last = if name.split('.').size == 2
               name.split('.').second
             else
               first
             end
      user.update(first_name: first, last_name: last)
    end
  end
end
