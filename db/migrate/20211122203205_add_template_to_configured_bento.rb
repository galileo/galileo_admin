class AddTemplateToConfiguredBento < ActiveRecord::Migration[6.0]
  def change
    add_reference :configured_bentos, :template_view_bento
    change_column_default :configured_bentos, :shown_by_default, true
  end
end
