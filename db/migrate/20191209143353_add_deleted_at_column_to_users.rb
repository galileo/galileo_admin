# frozen_string_literal: true

# support 'soft delete' for users
class AddDeletedAtColumnToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :deleted_at, :datetime
  end
end
