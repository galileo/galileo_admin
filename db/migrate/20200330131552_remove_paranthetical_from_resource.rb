class RemoveParantheticalFromResource < ActiveRecord::Migration[6.0]
  def change
    remove_column :resources, :paranthetical, :string
  end
end
