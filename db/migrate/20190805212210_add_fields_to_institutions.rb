class AddFieldsToInstitutions < ActiveRecord::Migration[5.2]
  def change
    change_table :institutions do |t|
      t.string :status
      t.boolean :open_athens
      t.string :open_athens_org_id
      t.string :open_athens_suborg_id
      t.string :ip_number, array: true
      t.string :remote_ips, array: true
      t.string :address
      t.string :galileo_ez_proxy_ip
      t.string :galileo_ez_proxy_url
      t.string :local_ez_proxy_ip
      t.string :local_ez_proxy_url
      t.string :glri_participant
      t.string :current_password
      t.string :new_password
      t.string :prev_password
      t.string :password_last_notified
      t.string :password_last_changed
      t.string :password_notify_group
      t.string :password_notify_dates
      t.string :password_change_dates
      t.timestamps
    end

  end
end
