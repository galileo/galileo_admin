class ChangeOpenAthensSubscopeIntoArray < ActiveRecord::Migration[6.0]
  def up
    add_column :institutions, :open_athens_subscope_array, :string, array: true, default: [], null: false

    Institution.all.each do |institution|
      institution.update!(open_athens_subscope_array: convert_to_array(institution.open_athens_subscope))
    end
  end

  def down
    remove_column :institutions, :open_athens_subscope_array
  end

  def convert_to_array(subscope)
    return [] if subscope.blank?
    [subscope]
  end
end

