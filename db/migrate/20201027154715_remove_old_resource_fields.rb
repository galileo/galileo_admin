class RemoveOldResourceFields < ActiveRecord::Migration[6.0]
  def change
    remove_column :resources, :system_list, :string, array: true, default: []
    remove_column :resources, :institution_list, :string, array: true, default: []
  end
end
