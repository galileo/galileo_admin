class AddPortalContactType < ActiveRecord::Migration[6.1]
  def up
    Contact.where("types && ?", "{adv, gen}").update_all(['types = array_append(types, ?)', 'port'])
  end

  def down
    Contact.where("types && ?", "{port}").update_all(['types = array_remove(types, ?)', 'port'])
  end
end
