class CreateNotifyGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :notify_groups do |t|
      t.string :code, index: true
      t.string :name
      t.string :notify_dates
      t.string :change_dates
      t.text :note
      t.string :update_history, array: true
      t.string :updated_by
      t.json :legacy_data

      t.timestamps
    end
  end
end
