# frozen_string_literal: true

# :nodoc:
class AddGroupToInstitution < ActiveRecord::Migration[6.0]
  def change
    change_table :institutions do |t|
      t.references :institution_group
    end
  end
end
