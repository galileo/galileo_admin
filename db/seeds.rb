# frozen_string_literal: true

# Load Subjects
load Rails.root.join 'db', 'data', 'subjects.rb' unless Subject.any?

# Load Formats
load Rails.root.join 'db', 'data', 'formats.rb' unless Format.any?

# Load UserViews
load Rails.root.join 'db', 'data', 'user_views.rb' unless UserView.any?

# Load InstitutionGroups
load Rails.root.join 'db', 'data', 'institution_groups.rb' unless InstitutionGroup.any?

# Load PredefinedBentos
load Rails.root.join 'db', 'data', 'predefined_bentos.rb' unless PredefinedBento.any?

# Load PredefinedBentos
load Rails.root.join 'db', 'data', 'template_view_bentos.rb' unless TemplateViewBento.any?


# Initial Set of Users
User.create!(
  [
    { email: 'super@uga.edu', password: 'password', role: 'admin', first_name: 'An', last_name: 'Admin' },
    { email: 'inst@uga.edu', password: 'password', role: 'institutional', first_name: 'Inst', last_name: 'User' },
    { email: 'helpdesk@uga.edu', password: 'password', role: 'helpdesk', first_name: 'Help', last_name: 'Desk' }
  ]
) unless User.any?
