# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2024_11_20_171522) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "daily_galileo_search", force: :cascade do |t|
    t.string "date", null: false
    t.string "institution_code", null: false
    t.string "query_type"
    t.integer "count", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "daily_link", force: :cascade do |t|
    t.string "date", null: false
    t.string "institution_code", null: false
    t.string "resource_code"
    t.integer "count", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "daily_login", force: :cascade do |t|
    t.string "date", null: false
    t.string "institution_code", null: false
    t.integer "count", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "monthly_galileo_search", force: :cascade do |t|
    t.integer "year", null: false
    t.integer "month", null: false
    t.date "year_month", null: false
    t.string "institution_code", null: false
    t.string "query_type"
    t.integer "count", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "monthly_link", force: :cascade do |t|
    t.integer "year", null: false
    t.integer "month", null: false
    t.date "year_month", null: false
    t.string "institution_code", null: false
    t.string "resource_code"
    t.integer "count", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "monthly_login", force: :cascade do |t|
    t.integer "year", null: false
    t.integer "month", null: false
    t.date "year_month", null: false
    t.string "institution_code", null: false
    t.integer "count", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "raw_galileo_search", force: :cascade do |t|
    t.string "server", default: "", null: false
    t.string "institution_code", default: "", null: false
    t.string "site_code", default: "", null: false
    t.string "query_type", default: "", null: false
    t.string "query", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "begin_time", precision: 0, null: false
  end

  create_table "raw_link", force: :cascade do |t|
    t.string "session_id", default: "", null: false
    t.string "resource_code", default: "", null: false
    t.string "server", default: "", null: false
    t.string "institution_code", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "begin_time", precision: 0, null: false
  end

  create_table "raw_login", force: :cascade do |t|
    t.string "session_id", default: "", null: false
    t.string "ip", default: "", null: false
    t.string "server", default: "", null: false
    t.string "institution_code", default: "", null: false
    t.string "auth_method", default: "", null: false
    t.string "site_code", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "begin_time", precision: 0, null: false
  end

  create_table "stats_snapshots", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "month", null: false
    t.bigint "institution_id"
    t.bigint "resource_id"
    t.index ["institution_id"], name: "index_stats_snapshots_on_institution_id"
    t.index ["resource_id"], name: "index_stats_snapshots_on_resource_id"
  end

end
