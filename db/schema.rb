# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2024_08_28_191035) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", precision: nil, null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "allocation_categorizations", force: :cascade do |t|
    t.bigint "subject_id"
    t.bigint "allocation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["allocation_id"], name: "index_allocation_categorizations_on_allocation_id"
    t.index ["subject_id"], name: "index_allocation_categorizations_on_subject_id"
  end

  create_table "allocations", force: :cascade do |t|
    t.bigint "institution_id"
    t.bigint "resource_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.boolean "display", default: true
    t.string "ip_access_url"
    t.string "remote_access_url"
    t.string "open_athens_url"
    t.string "user_id"
    t.string "password"
    t.string "note"
    t.string "special_info"
    t.boolean "bypass_open_athens", default: false
    t.string "keywords", default: [], null: false, array: true
    t.boolean "override_user_views", default: false
    t.string "subscription_type", default: ""
    t.string "vendor_statistics_identifier", default: "", null: false
    t.string "name", default: "", null: false
    t.bigint "branding_id"
    t.index ["branding_id"], name: "index_allocations_on_branding_id"
    t.index ["institution_id", "resource_id"], name: "index_allocations_on_institution_id_and_resource_id", unique: true
    t.index ["institution_id"], name: "index_allocations_on_institution_id"
    t.index ["resource_id"], name: "index_allocations_on_resource_id"
  end

  create_table "allocations_user_views", id: false, force: :cascade do |t|
    t.bigint "allocation_id", null: false
    t.bigint "user_view_id", null: false
  end

  create_table "banners", force: :cascade do |t|
    t.datetime "start_time", precision: nil, null: false
    t.datetime "end_time", precision: nil, null: false
    t.integer "style", default: 0, null: false
    t.bigint "institution_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "audience", default: 0, null: false
    t.bigint "institution_group_id"
    t.text "content"
    t.index ["institution_group_id"], name: "index_banners_on_institution_group_id"
    t.index ["institution_id"], name: "index_banners_on_institution_id"
  end

  create_table "banners_user_views", id: false, force: :cascade do |t|
    t.bigint "banner_id", null: false
    t.bigint "user_view_id", null: false
  end

  create_table "bento_configs", force: :cascade do |t|
    t.bigint "institution_id"
    t.string "inst_code"
    t.string "service"
    t.string "user_id"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "view_type", default: ""
    t.string "api_profile", default: ""
    t.string "blacklight_id", default: ""
    t.string "view_types", default: [], null: false, array: true
    t.string "name", default: "", null: false
    t.json "credentials"
    t.index ["institution_id"], name: "index_bento_configs_on_institution_id"
  end

  create_table "bento_versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "item_name", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.text "object_changes"
    t.datetime "created_at", precision: nil
  end

  create_table "brandings", force: :cascade do |t|
    t.bigint "institution_id"
    t.string "name", default: ""
    t.string "text", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["institution_id"], name: "index_brandings_on_institution_id"
  end

  create_table "categorizations", force: :cascade do |t|
    t.bigint "subject_id"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["resource_id"], name: "index_categorizations_on_resource_id"
    t.index ["subject_id"], name: "index_categorizations_on_subject_id"
  end

  create_table "configured_bentos", force: :cascade do |t|
    t.string "slug"
    t.string "display_name", default: "", null: false
    t.json "customizations"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "institution_id"
    t.string "user_view", default: "", null: false
    t.float "order"
    t.boolean "active", default: true
    t.boolean "shown_by_default", default: true
    t.bigint "predefined_bento_id"
    t.bigint "bento_config_id"
    t.string "description", default: "", null: false
    t.bigint "template_view_bento_id"
    t.index ["bento_config_id"], name: "index_configured_bentos_on_bento_config_id"
    t.index ["institution_id"], name: "index_configured_bentos_on_institution_id"
    t.index ["predefined_bento_id"], name: "index_configured_bentos_on_predefined_bento_id"
    t.index ["template_view_bento_id"], name: "index_configured_bentos_on_template_view_bento_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "phone"
    t.text "notes"
    t.bigint "institution_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "types", default: [], null: false, array: true
    t.bigint "vendor_id"
    t.index ["email"], name: "index_contacts_on_email"
    t.index ["first_name"], name: "index_contacts_on_first_name"
    t.index ["institution_id"], name: "index_contacts_on_institution_id"
    t.index ["last_name"], name: "index_contacts_on_last_name"
    t.index ["types"], name: "index_contacts_on_types"
    t.index ["vendor_id"], name: "index_contacts_on_vendor_id"
  end

  create_table "features", force: :cascade do |t|
    t.string "view_type"
    t.integer "position"
    t.string "link_url"
    t.string "link_label"
    t.string "link_description"
    t.string "featuring_type"
    t.bigint "featuring_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["featuring_type", "featuring_id"], name: "index_features_on_featuring_type_and_featuring_id"
  end

  create_table "formats", force: :cascade do |t|
    t.string "name"
    t.string "legacy_codes", default: [], array: true
  end

  create_table "formats_resources", id: false, force: :cascade do |t|
    t.bigint "resource_id", null: false
    t.bigint "format_id", null: false
  end

  create_table "institution_groups", force: :cascade do |t|
    t.string "code"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "inst_type", default: "default"
    t.index ["code"], name: "index_institution_groups_on_code"
  end

  create_table "institution_groups_institutions", id: false, force: :cascade do |t|
    t.bigint "institution_id", null: false
    t.bigint "institution_group_id", null: false
    t.index ["institution_group_id", "institution_id"], name: "idx_inst_group_inst", unique: true
    t.index ["institution_id", "institution_group_id"], name: "idx_inst_inst_group", unique: true
  end

  create_table "institutions", force: :cascade do |t|
    t.string "code"
    t.string "public_code"
    t.string "name"
    t.json "legacy_data"
    t.boolean "open_athens", default: false
    t.string "open_athens_scope"
    t.string "open_athens_subscope"
    t.string "ip_addresses", default: [], null: false, array: true
    t.string "address"
    t.string "galileo_ez_proxy_ip"
    t.string "galileo_ez_proxy_url"
    t.string "local_ez_proxy_ip"
    t.string "local_ez_proxy_url"
    t.string "glri_participant", default: "none"
    t.string "current_password"
    t.string "new_password"
    t.string "prev_password"
    t.datetime "password_last_notified", precision: nil
    t.datetime "password_last_changed", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.boolean "active", default: false
    t.bigint "notify_group_id"
    t.string "change_dates", default: [], null: false, array: true
    t.string "notify_dates", default: [], null: false, array: true
    t.boolean "change_password_asap", default: false, null: false
    t.string "zip_codes", default: [], array: true
    t.string "counties", default: [], array: true
    t.boolean "ip_authentication", default: true
    t.string "open_athens_org_id"
    t.string "open_athens_entity_id"
    t.string "open_athens_proxy_ip"
    t.string "eds_profile_default"
    t.string "eds_profile_kids"
    t.string "eds_customer_id"
    t.string "eds_managed_by"
    t.boolean "special", default: false
    t.boolean "test_site", default: false
    t.boolean "use_local_ez_proxy", default: false
    t.string "note", default: ""
    t.string "special_information", default: ""
    t.string "pines_codes", default: [], array: true
    t.string "institution_url_1", default: ""
    t.string "institution_url_2", default: ""
    t.string "institution_url_3", default: ""
    t.string "institution_url_4", default: ""
    t.string "institution_url_5", default: ""
    t.string "institution_url_label_1", default: ""
    t.string "institution_url_label_2", default: ""
    t.string "institution_url_label_3", default: ""
    t.string "institution_url_label_4", default: ""
    t.string "institution_url_label_5", default: ""
    t.string "journals_link", default: ""
    t.bigint "institution_group_id"
    t.string "eds_profile_teen", default: ""
    t.string "eds_profile_high_school", default: ""
    t.string "lti_consumer_key", default: ""
    t.string "lti_custom_inst_id", default: ""
    t.string "lti_secret", default: ""
    t.boolean "oa_proxy", default: false
    t.string "oa_proxy_org"
    t.boolean "zotero_bib", default: true, null: false
    t.string "wayfinder_keywords", default: [], null: false, array: true
    t.string "open_athens_subscope_array", default: [], null: false, array: true
    t.string "open_athens_api_name", default: ""
    t.string "bento_template"
    t.integer "view_switching", default: 0, null: false
    t.index ["code"], name: "index_institutions_on_code"
    t.index ["current_password"], name: "index_institutions_on_current_password"
    t.index ["institution_group_id"], name: "index_institutions_on_institution_group_id"
    t.index ["new_password"], name: "index_institutions_on_new_password"
    t.index ["notify_group_id"], name: "index_institutions_on_notify_group_id"
    t.index ["zip_codes"], name: "index_institutions_on_zip_codes"
  end

  create_table "institutions_users", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "institution_id", null: false
  end

  create_table "notify_groups", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "note"
    t.json "legacy_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "change_dates", default: [], null: false, array: true
    t.string "notify_dates", default: [], null: false, array: true
    t.string "special_information", default: ""
    t.index ["code"], name: "index_notify_groups_on_code"
  end

  create_table "passwords", force: :cascade do |t|
    t.string "password"
    t.date "commit_date"
    t.boolean "exclude", default: false
    t.string "user"
    t.json "legacy_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["password"], name: "index_passwords_on_password"
  end

  create_table "predefined_bentos", force: :cascade do |t|
    t.string "service"
    t.string "code"
    t.string "display_name"
    t.json "definition"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description", default: "", null: false
    t.string "admin_note", default: "", null: false
    t.string "no_results_message", default: ""
    t.string "service_credentials_name"
  end

  create_table "resources", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.json "legacy_data"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "short_description"
    t.boolean "open_athens", default: false
    t.string "open_athens_url"
    t.string "ip_access_url"
    t.string "remote_access_url"
    t.string "product_suite"
    t.string "title_list_url"
    t.string "user_id"
    t.string "password"
    t.string "long_description"
    t.string "coverage_dates"
    t.string "update_frequency"
    t.string "audience"
    t.string "language"
    t.string "special_information"
    t.string "note"
    t.bigint "vendor_id"
    t.boolean "active", default: false
    t.bigint "parent_id"
    t.boolean "display", default: true
    t.boolean "show_institutional_branding", default: false
    t.boolean "bypass_galileo_authentication", default: false
    t.bigint "institution_id"
    t.boolean "proxy_remote", default: false
    t.boolean "include_in_stats", default: false
    t.string "vendor_name", default: ""
    t.bigint "branding_id"
    t.string "access_note", default: ""
    t.string "credential_display", default: "never"
    t.boolean "oa_proxy", default: false
    t.string "keywords", default: [], null: false, array: true
    t.integer "serial_count", default: 0, null: false
    t.string "resource_category", default: "None", null: false
    t.boolean "without_open_athens_redirector", default: false
    t.index ["branding_id"], name: "index_resources_on_branding_id"
    t.index ["code"], name: "index_resources_on_code"
    t.index ["display"], name: "index_resources_on_display"
    t.index ["institution_id"], name: "index_resources_on_institution_id"
    t.index ["parent_id"], name: "index_resources_on_parent_id"
    t.index ["vendor_id"], name: "index_resources_on_vendor_id"
  end

  create_table "resources_user_views", id: false, force: :cascade do |t|
    t.bigint "resource_id", null: false
    t.bigint "user_view_id", null: false
  end

  create_table "sites", force: :cascade do |t|
    t.bigint "institution_id"
    t.string "name", default: ""
    t.string "code", default: ""
    t.string "site_type", default: ""
    t.json "legacy_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "wayfinder_keywords", default: [], null: false, array: true
    t.boolean "charter", default: false
    t.index ["institution_id"], name: "index_sites_on_institution_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.string "legacy_codes", default: [], array: true
  end

  create_table "template_view_bentos", force: :cascade do |t|
    t.string "template_name", default: "", null: false
    t.string "user_view", default: "", null: false
    t.float "default_order"
    t.boolean "shown_by_default", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "predefined_bento_id"
    t.index ["predefined_bento_id"], name: "index_template_view_bentos_on_predefined_bento_id"
  end

  create_table "user_views", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "inst_type", default: [], null: false, array: true
    t.boolean "full", default: false, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "invitation_token"
    t.datetime "invitation_created_at", precision: nil
    t.datetime "invitation_sent_at", precision: nil
    t.datetime "invitation_accepted_at", precision: nil
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.string "role"
    t.datetime "deleted_at", precision: nil
    t.string "first_name"
    t.string "last_name"
    t.string "subroles", default: [], array: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vendors", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.integer "resources_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_vendors_on_code"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at", precision: nil
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "widgets", force: :cascade do |t|
    t.bigint "institution_id", null: false
    t.integer "service", default: 0, null: false
    t.string "credentials"
    t.boolean "active", default: false, null: false
    t.string "display_name"
    t.index ["institution_id"], name: "index_widgets_on_institution_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
end
