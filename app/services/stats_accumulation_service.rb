# frozen_string_literal: true

# Class to accumulate daily and monthly stats from the raw stats tables
class StatsAccumulationService

  include Stats

  ALLOWED_ENVIRONMENTS = %w[staging development dev].freeze
  ALLOWED_STATS_TYPES = %w[login link search].freeze

  #region public methods
  def self.accumulate_daily_stats_range(stats_type, start_date, end_date, notifier: NotificationService.new)
    (start_date..end_date).each do |current_date|
      accumulate_daily_stats(stats_type, current_date, notifier: notifier)
    end
  end
  def self.accumulate_daily_stats(stats_type, stat_date = DateTime.yesterday, notifier: NotificationService.new)
    process = "`Accumulating Daily Stats #{stats_type}`: #{stat_date}"

    notifier.start process

    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      notifier.finish process, additional_message: "ABORT: Not allowed to run in `#{Rails.env}`."
      return
    end

    unless ALLOWED_STATS_TYPES.include? stats_type
      notifier.finish process, additional_message: "ABORT: Invalid stats_type: `#{stats_type}`."
      return
    end

    if stat_date >= Date.today
      notifier.finish process, additional_message: "ABORT: this day, #{stat_date}, is not over yet."
      return
    end

    compute_daily_stats(stats_type, stat_date, notifier: notifier)

    notifier.finish process
  end

  def self.accumulate_monthly_stats(stats_type, yyyymm, notifier: NotificationService.new)
    process = "`Accumulating Monthly Stats #{stats_type}`: #{yyyymm}"

    notifier.start process

    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      notifier.finish process, additional_message: "ABORT: Not allowed to run in `#{Rails.env}`."
      return
    end

    unless ALLOWED_STATS_TYPES.include? stats_type
      notifier.finish process, additional_message: "ABORT: Invalid stats_type: `#{stats_type}`."
      return
    end

    if yyyymm.present?
      unless yyyymm.match(/^\d{6}$/)
        notifier.finish process, additional_message: "ABORT: yyyymm argument must 6 numbers"
        return
      end
    else
      yyyymm = Date.today.prev_month.strftime('%Y%m')
    end

    if yyyymm >= Date.today.strftime('%Y%m')
      notifier.finish process, additional_message: "ABORT: this month, #{yyyymm}, is not over yet."
      return
    end

    case stats_type
    when 'login'
      accumulate_monthly_login_stats(yyyymm, notifier: notifier)
    when 'link'
      accumulate_monthly_link_stats(yyyymm, notifier: notifier)
    when 'search'
      accumulate_monthly_search_stats(yyyymm, notifier: notifier)
    else
      notifier.notify "#{process}: Invalid stats_type: `#{stats_type}`."
    end

    notifier.finish process
  end

  # Note, we only add (create) snapshot records.  We never delete or update any.
  def self.update_stats_snapshots(yyyymm, notifier: NotificationService.new)
    process = "`Update Stats Snapshots`: #{yyyymm}"

    notifier.start process

    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      notifier.finish process, additional_message: "ABORT: Not allowed to run in `#{Rails.env}`."
      return
    end

    if yyyymm.present?
      unless yyyymm.match(/^\d{6}$/)
        notifier.finish process, additional_message: "ABORT: yyyymm argument must 6 numbers"
        return
      end
    else
      yyyymm = Date.today.strftime('%Y%m')
    end

    mdate = (yyyymm + "01").to_date
    snapshot_institutions = StatsSnapshot.where(month: mdate, resource_id: nil).pluck(:institution_id).uniq
    active_institutions = Institution.active

    notifier.notify "#{process}: adding snapshots for #{active_institutions.count} institutions."
    processed = 0
    records = []
    (active_institutions.pluck(:id) - snapshot_institutions).each do |institution_id|
      processed += 1
      records << stats_snapshots_record(mdate, institution_id)
    end
    StatsSnapshot.insert_all(records.compact) if records.compact.present?
    notifier.notify "#{process}: added snapshots for #{processed} institutions."

    processed = 0
    active_institutions.each do |institution|
      institution_resources = institution.resources.pluck(:id)
      snapshot_resources = StatsSnapshot.where(month: mdate, institution_id: institution.id).pluck(:resource_id)
      records = []
      (institution_resources - snapshot_resources).each do |resource_id|
        processed += 1
        records << stats_snapshots_record(mdate, institution.id, resource_id)
      end
      StatsSnapshot.insert_all(records.compact) if records.compact.present?
    end
    notifier.notify "#{process}: added snapshots for #{processed} institutions/resources."
    notifier.finish process
  end
  #endregion

  #region private methods
  # private

  def self.stats_snapshots_record(mdate, institution_id, resource_id = nil)
    record = {
      month: mdate,
      institution_id: institution_id,
      resource_id: resource_id,
    }
    record
  end
  #endregion
end
