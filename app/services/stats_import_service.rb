# frozen_string_literal: true

require 'net/ftp'
require 'aws-sdk-s3'
require 'open3'
require 'open-uri'

# Class to import Production Data
class StatsImportService
  include Stats

  ALLOWED_ENVIRONMENTS = %w[staging development dev].freeze
  ALLOWED_STATS_TYPES = %w[login link search].freeze
  LOCAL_BASE_PATH = File.join(Rails.root, 'tmp', 'stats_import').freeze
  REMOTE_BASE_PATH = 'production'
  REMOTE_GALSTATS_PATH = 'galstats/monthly'
  REMOTE_GALSTATS_TARBALL = 'galstats_monthly_202209.tar.gz'
  FTP_SETTINGS = {
    debug_mode: !Rails.env.production?,
    port: 990,
    ssl: { verify_mode: OpenSSL::SSL::VERIFY_NONE,
           min_version: OpenSSL::SSL::TLS1_2_VERSION,
           max_version: OpenSSL::SSL::TLS1_2_VERSION },
    username: Rails.application.credentials.dig(:galftp, :username),
    password: Rails.application.credentials.dig(:galftp, :password)
  }
  BATCH_LINES = 1000
  BATCH_FILES = 100
  AUTH_METHODS = %w[ip password ip2loc
    pinesid pines_remote pines_ip
    casid_remote cas_remote casid_ip cas_ip
    openathensid_remote oa_remote openathensid_ip oa_ip
    d2lid_remote d2l_remote d2lid_ip d2l_ip
    bbid_remote bbid_ip
    gilid_remote gilid_ip galid
    remote_ip].freeze
  INST_CODE_REGX = %r[\A[A-Z0-9]{4}\z]
  MONTHLY_STATS_TYPES = %w[citation fulltext link login search session index browse choose].freeze

  #region public methods
  # @param [String] stats_type
  # @param [Date] start_date
  # @param [Date] end_date
  # @param [NotificationService] notifier
  def self.import_raw_stats(stats_type, start_date, end_date, notifier: NotificationService.new)
    process = "`Importing Stats #{stats_type} #{start_date} - #{end_date}`"
    notifier.start process
    validate_raw_import stats_type, start_date, end_date

    stat_class = Stats.const_get("Raw#{stats_type.capitalize}Stat")
    prefix = stats_type == 'link' ? 'links' : stats_type  # to accommodate legacy naming
    local_file_path = File.join(LOCAL_BASE_PATH, stats_type)
    totals = { line_count: 0, prev_count: stat_class.count }

    (start_date..end_date).each do |current_date|
      yyyymmdd = current_date.strftime('%Y%m%d')
      stats_files = retrieve("#{prefix}.stats.D#{yyyymmdd}*", remote_path: REMOTE_BASE_PATH, local_path: local_file_path, notifier: notifier)
      counts = stat_class.load_stats(yyyymmdd, stats_files, notifier: notifier)
      notifier.notify "#{process}: #{load_status(counts[:prev_count], counts[:line_count], counts[:new_count])}"
      totals[:line_count] += counts[:line_count]
    end

    totals[:new_count] = stat_class.count
    totals[:total_count] = totals[:line_count] + totals[:prev_count]
    notifier.finish process
    totals
  end

  def self.validate_raw_import(stats_type, start_date, end_date)
    unless ALLOWED_STATS_TYPES.include? stats_type
      raise Exceptions::StatImportError.new, "Invalid stats_type: #{stats_type}."
    end

    unless start_date <= end_date
      raise Exceptions::StatImportError.new, "start_date must be less than or equal to end_date. start_date: #{start_date} end_date: #{end_date}."
    end

    unless end_date < Date.today
      raise Exceptions::StatImportError.new, "end_date must be yesterday or earlier. end_date: #{end_date}."
    end

    stat_class = Stats.const_get("Raw#{stats_type.capitalize}Stat")
    existing_dates = stat_class.with_date_range(start_date, end_date).order(:begin_time).limit(5)
    unless existing_dates.empty?
      raise Exceptions::StatImportError.new "stat_type #{stats_type} already has rows loaded starting at date #{existing_dates.first}."
    end
  end

  def self.valid_yyyymmdd?(yyyymmdd)
    return false unless yyyymmdd.match(/^\d{8}$/)
    begin
      Date.parse(yyyymmdd)
    rescue Date::Error
      return false
    end
    true
  end
  #endregion

  #region private methods
  # private

  def self.monthly_stats_record(stats_type, line)
    period = ''
    institution_code = ''
    resource_code = ''
    index = ''
    count = ''

    # login rows have 5 fields (we ignore server_code and site_code)
    if %w[login].include? stats_type # m202001 1KEL 18 L 1
      (period, institution_code, _server_code, _site_code, count) = line.split(/ /)

    # index and browse rows have 7 fields
    elsif %w[index browse].include? stats_type # m199510 ABR1 U I ABII pd:|yr: 4
      (period, institution_code, _server_code, _site_code, resource_code, index, count) = line.split(/ /)

    # other stats type rows have 6 fields
    else # m202001 ABR1 C A ZFOD 33
      (period, institution_code, _server_code, _site_code, resource_code, count) = line.split(/ /)
    end

    record = {
      month: (period[1..6] + "01").to_date,
      stats_type: stats_type,
      institution_code: institution_code.downcase,
      resource_code: resource_code.downcase,
      query_type: index,
      count: count
    }
    add_timestamp(record)
    record
  end

  def self.load_monthly_stats(notifier: NotificationService.new)
    process = "`Loading (legacy) Monthly Stats`"
    notifier.start process
    prev_count = Monthly.count
    if prev_count > 0
      notifier.notify "#{process}: ABORT: already loaded?"
      return
    end
    local_path = File.join(LOCAL_BASE_PATH, 'monthly')
    files_retrieved = retrieve_tarball(REMOTE_GALSTATS_TARBALL, remote_path: REMOTE_GALSTATS_PATH, local_path: local_path, notifier: notifier)
    line_count = 0
    total = files_retrieved.count
    processed = 0
    files_retrieved.each_slice(BATCH_FILES) do |group|
      group.each do |file|
        processed += 1
        (basename, month, stats_type) = monthly_file_info(file)
        next unless MONTHLY_STATS_TYPES.include? stats_type
        lines = File.readlines(file, chomp: true)
        line_count += lines.size
        lines.each_slice(BATCH_LINES) do |group|
          records = []
          group.each do |line|
            next unless line.present?
            record = monthly_stats_record(stats_type, line)
            if record.present?
              records << record
            else
              notifier.notify "#{process}: SKIP LINE (corrupt?): #{basename} `#{line}`"
            end
          end
          Monthly.insert_all(records.compact)
        end
        puts "#{process}: #{month}/#{basename} #{lines.size} lines #{notifier.progress(process, processed, total)}"
      end
      notifier.notify "#{process}: #{notifier.progress(process, processed, total)}"
    end
    notifier.notify "#{process}: #{line_count} total lines"
    new_count = Monthly.count
    notifier.notify "#{process}: #{load_status(prev_count, line_count, new_count)}"
    notifier.finish process
  end

  # https://stats.galileo.usg.edu/snapshots/months/all
  def self.snapshots_all_months
    months = []
    lines = URI.open('https://stats.galileo.usg.edu/snapshots/months/all') { |f| f.read }
    lines.split("\n").each do |line|
      if line.match(%r{ (\d{4})/(\d{2})$})
        months << "#{$1}#{$2}01".to_date
      end
    end
    months
  end

  # https://stats.galileo.usg.edu/snapshots/2022/08/institutions
  def self.snapshots_institutions(month)
    institution_codes = []
    month_path = month.strftime("%Y/%m")
    lines = URI.open("https://stats.galileo.usg.edu/snapshots/#{month_path}/institutions") { |f| f.read }
    lines.split("\n").each do |line|
      if line.match(%r{^(.{4}) })
        institution_codes << $1
      end
    end
    institution_codes
  end

  # https://stats.galileo.usg.edu/snapshots/2022/08/abr1/databases
  def self.snapshots_institutions_resources(month, institution_code)
    resource_codes = []
    month_path = month.strftime("%Y/%m")
    lines = URI.open("https://stats.galileo.usg.edu/snapshots/#{month_path}/#{institution_code}/databases") { |f| f.read }
    lines.split("\n").each do |line|
      if line.match(%r{^(.{4}) })
        resource_codes << $1
      end
    end
    resource_codes
  end

  def self.snapshots_record(month, institution_id, resource_id = "")
    record = {
      month: month,
      institution_id: institution_id,
      resource_id: resource_id
    }
    add_timestamp(record)
    record
  end

  def self.load_legacy_snapshots(notifier: NotificationService.new)
    process = "`Loading (legacy) Snapshots`"
    notifier.start process
    prev_count = StatsSnapshot.count
    if prev_count > 0
      notifier.notify "#{process}: ABORT: already loaded?"
      return
    end
    seen_institutions = {}
    seen_resources = {}
    record_count = 0
    month_count = 0
    all_months = snapshots_all_months
    total = all_months.count
    prev_year = ''
    all_months.reverse.each do |month|
      year = month.strftime("%Y");
      records = []
      snapshots_institutions(month).each do |institution_code|
        institution = Institution.find_by_code(institution_code)
        unless seen_institutions[institution_code]
          unless institution.present?
            notifier.notify "#{process}: #{month.strftime("%Y/%m")} Institution `#{institution_code}` not found."
            seen_institutions[institution_code] = true
          end
        end
        records << snapshots_record(month, institution.id)
        record_count += 1
        snapshots_institutions_resources(month, institution_code).each do |resource_code|
          resource = Resource.find_by_code(resource_code)
          unless seen_resources[resource_code]
            unless resource.present?
              notifier.notify "#{process}: #{month.strftime("%Y/%m")} #{institution_code} Resource `#{resource_code}` not found."
              seen_resources[resource_code] = true
            end
          end
          records << snapshots_record(month, institution.id, resource.id)
          record_count += 1
        end
      end
      StatsSnapshot.insert_all(records.compact)
      month_count += 1
      if year != prev_year
        notifier.notify "#{process}: #{month.strftime("%Y/%m")} #{notifier.progress(process, month_count, total)}"
        prev_year = year
      end
    end
    notifier.notify "#{process}: #{record_count} total records"
    new_count = StatsSnapshot.count
    notifier.notify "#{process}: #{load_status(prev_count, record_count, new_count)}"
    notifier.finish process
  end

  # Retrieves the raw stats data files from the ftp and puts it into the tmp/stats directory
  # @return [Array<String>] file paths
  def self.retrieve(file_glob, remote_path: REMOTE_BASE_PATH, local_path: LOCAL_BASE_PATH, notifier: NotificationService.new)
    process = "`Retrieving files`"
    notifier.start process
    notifier.notify "#{process}: #{file_glob}"
    initialize_path local_path

    local_file_paths = []
    Net::FTP.open('galftp.galib.uga.edu', FTP_SETTINGS) do |ftp|
      ftp.chdir(remote_path)
      files = []
      ftp.list(file_glob) do |line|
        if line.match(/(\S+)$/)
          files << $1
        end
      end
      files.each do |file|
        local_file_path = File.join(local_path, file)
        ftp.getbinaryfile(file, local_file_path.to_s, 1024)
        local_file_paths << local_file_path
      end
    end

    notifier.notify "#{process}: #{local_file_paths.count} files retrieved"
    notifier.finish process
    local_file_paths
  end

  # Retrieves the monthly stats tarball from galftp
  # @return [Array<String>] file paths
  def self.retrieve_tarball(tarball, remote_path: REMOTE_GALSTATS_PATH, local_path: LOCAL_BASE_PATH, notifier: NotificationService.new)
    process = "`Retrieving tarball`"
    notifier.start process
    notifier.notify "#{process}: #{tarball}"
    initialize_path local_path

    Net::FTP.open('galftp.galib.uga.edu', FTP_SETTINGS) do |ftp|
      ftp.chdir(remote_path)
      local_file_path = File.join(local_path, tarball)
      ftp.getbinaryfile(tarball, local_file_path.to_s, 1024)
    end

    Dir.chdir(local_path)
    command = "tar --gunzip -x -f #{tarball}"

    _stdout_str, error_str, status = Open3.capture3({}, command)
    unless status.success?
      message = "#{process} Unable to load tarball #{error_str}"
      notifier.notify message
      raise Exceptions::DataImportError.new, message
    end

    local_file_paths = Dir["#{local_path}/[1-2]*/**/*"].sort

    notifier.notify "#{process}: #{local_file_paths.count} files retrieved"
    notifier.finish process
    local_file_paths
  end

  def self.load_status(prev_count, line_count, new_count)
    if line_count == 0
      "BAD? load, no lines loaded"
    elsif prev_count + line_count == new_count
      "OKAY load (#{prev_count} + #{line_count} == #{new_count})"
    else
      diff = prev_count + line_count - new_count
      "BAD? load (#{prev_count} + #{line_count} != #{new_count} difference: #{diff})"
    end
  end

  # Empties path if it exists, otherwise creates path
  # @param [String] path Path to be initialized
  # @param [String] destructive When true, will remove all files in the given path
  def self.initialize_path(path, destructive = true)
    if File.exist?(path)
      FileUtils.rm_rf(Dir["#{path}/*"]) if destructive
    else
      FileUtils.mkdir_p(path)
    end
  end

  def self.file_info(file)
    basename = Pathname.new(file).basename.to_s
    server = basename.split(/[.]/).last
    date = basename.scan(/\d{8}/).first
    return basename, server, date
  end

  def self.monthly_file_info(file)
    (dir, base) = Pathname.new(file).split
    basename = base.to_s
    month = dir.to_s[-7..-1]  # '1995/10'
    stats_type = basename.sub(/s$/, '')  # links => link, sessions => session
    return basename, month, stats_type
  end

  def self.add_timestamp(record)
    time = Time.now.utc
    record['created_at'] = time
    record['updated_at'] = time
  end
  #endregion

end
