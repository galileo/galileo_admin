# frozen_string_literal: true

MAX_ATTEMPTS_EDS_ERROR_106 = 3

# Authenticate and search the EDS API
class EdsApi
  include HTTParty

  class EdsApiError < StandardError; end

  base_uri 'https://eds-api.ebscohost.com'

  attr_reader :auth_token_redis

  delegate :get_available_databases, to: :session

  # @param [BentoConfig] bento_config
  def initialize(bento_config, use_auth_token_cache: true)
    raise EdsApiError, 'Bento configuration is not set.' if bento_config.nil?
    unless bento_config.service == 'eds_api'
      raise EdsApiError, "Bento Configuration service is not set to 'eds_api'. service = #{bento_config.service}"
    end

    @bento_config = bento_config
    @use_auth_token_cache = use_auth_token_cache
    @auth_token_redis = Redis.new(url: Rails.application.credentials.redis[:url][Rails.env.to_sym],
                                  db: Rails.application.credentials.redis[:eds_auth_tokens][Rails.env.to_sym],
                                  driver: :hiredis)
  end

  def session
    @session = EBSCO::EDS::Session.new({ user: user_id, pass: password, profile: api_profile,
                                         guest: false, auth_token: auth_token, use_cache: false, timeout: 60,
                                         open_timeout: 60 })
  end

  # @param [Hash] search_params
  # @return [EBSCO::EDS::Results]
  def search(search_params)
    attempts = 0
    loop do
      return session.search(search_params)
    rescue EBSCO::EDS::BadRequest => e
      error_number = e.fault.dig :error_body, 'ErrorNumber'
      raise unless error_number == '106'

      attempts += 1
      raise unless attempts < MAX_ATTEMPTS_EDS_ERROR_106
    end
    nil
  end

  def retrieve(dbid, an)
    session.retrieve dbid: dbid, an: an
  end

  # Checks cache or retrieves a new eds auth_token
  def auth_token
    auth_token = (auth_token_redis.get @bento_config.inst_code if @use_auth_token_cache)

    return auth_token unless auth_token.blank?

    response = new_auth_token
    response = response['AuthResponseMessage'] if response['AuthResponseMessage']
    auth_token = response['AuthToken']
    if @use_auth_token_cache
      @auth_token_redis.set(@bento_config.inst_code, auth_token, ex: (response['AuthTimeout'].to_i - 60))
    end

    auth_token
  end

  def new_auth_token
    resp = HTTParty.post 'https://eds-api.ebscohost.com/authservice/rest/uidauth',
                         body: {
                           "UserId": user_id,
                           "Password": password
                         }.to_json,
                         headers: {
                           "Content-Type": 'application/json'
                         }
    unless resp.response.code == '200'
      raise EdsApiError, "Unable to retrieve new EDS auth token. Check the user id and password. Response from EDS: #{resp&.body}"
    end

    resp.parsed_response
  end

  def user_id
    @bento_config.credential 'User ID'
  end

  def password
    @bento_config.credential 'Password'
  end

  def api_profile
    @bento_config.credential 'API Profile'
  end

end
