# frozen_string_literal: true

class IpService
  CACHE_PREFIX = 'ip_'

  # Formats an ip address string into the key format for the
  # cache
  # @param [String] ip_address can be in integer or string format
  # @param [String] integer_format - ip_address is an integer
  def self.key_from(ip_address)
    ip_int = if ip_address.instance_of? String
               IPAddr.new(ip_address).to_i
             else
               ip_address
             end
    "#{CACHE_PREFIX}#{ip_int}"
  end

  # Returns an institution code for a given ip address if found. Returns nil otherwise
  # @param [String] ip_address
  # @return [String]
  def self.search(ip_address)
    key = key_from ip_address
    Redis.current.get(key)
  end

  # Returns an Array of ip_address, inst_code pairs for a given ip range.
  # @param [String] ip_range
  # @return [Array<String, String>]
  def self.search_range(ip_range)
    return [] if ip_range.blank?

    ip_integers = ips_in_range ip_range
    ip_keys = ip_integers.map { |ip| key_from ip }
    inst_codes = Redis.current.mget(ip_keys)
    ip_strings = ip_integers.map { |ip_int| string_representation ip_int }
    ip_strings.zip(inst_codes)
  end

  # Clears and builds a cache of ip address keys and values that represent
  # institution_ids and if ip_address is remote
  def self.clear_cache
    Redis.current.flushdb
  end

  # Clears and builds a cache of ip address keys and values that represent
  # institution_ids and if ip_address is remote
  def self.reset_cache
    clear_cache
    Institution.where(active: true, ip_authentication: true).pluck(:code, :ip_addresses).each do |code, ips|
      add_ip_ranges(ips, code)
    end
  end

  # Add multiple ip ranges to the cache for a given institution code
  # @param [Array<String>] ip_ranges
  # @param [String] inst_code
  def self.add_ip_ranges(ip_ranges, inst_code)
    return if ip_ranges.empty?

    redis = Redis.current
    ip_keys = []
    ip_ranges.each do |ip_notation|
      ips = ips_in_range ip_notation
      ip_keys += ips.map { |ip| key_from ip }
    end
    redis.pipelined do
      ip_keys.uniq.compact.each do |ip_key|
        redis.set(ip_key, inst_code)
      end
    end
  end

  # add ip address and institution code pair to the cache
  # @param [String] ip_address
  # @param [String] inst_code
  def self.add_ip(ip_address, inst_code)
    key = key_from ip_address
    Redis.current.set(key, inst_code)
  end

  # Remove multiple ranges of ip addresses from cache
  # @param [Array<String>] ip_ranges
  def self.delete_ip_ranges(ip_ranges)
    return unless ip_ranges.any?

    ip_keys = []
    ip_ranges.each do |ip_notation|
      ips = ips_in_range ip_notation
      ip_keys += ips.map { |ip| key_from ip }
    end
    ip_keys.in_groups_of(250_000, false) do |group|
      Redis.current.del group
    end
  end

  # Converts a string ip ip_range into an array of integers that represent ip
  # addresses
  # @param [String] ip_range
  # return [Array<Integer>] array of integer ip addresses
  def self.ips_in_range(ip_range)
    first, last = range_bookends(ip_range)
    Array(first..last).uniq
  end

  # Converts a string ip_range and return the start and end of the range as ip integers
  # @param [String] ip_range
  # return [Array<Integer, Integer>] first ip address, last ip address
  def self.range_bookends(ip_range)
    range = convert ip_range
    first = IPAddr.new(range.first).to_i
    last = IPAddr.new(range.last).to_i
    [first, last]
  end

  # Converts an array of ip_ranges and returns hash of ip_range and bookends value pairs
  # @param [Array<String>] range_array
  # return [Hash<String, Array<Integer>]
  def self.bookend_hash(range_array)
    range_array.map { |range| [range, range_bookends(range)] }.to_h
  end

  # Compares an ip_range with an array of ip_ranges and returns ip_addresses of
  # that are present in both. Used for uniqueness in the ip_address validator.
  # @param [Array<Integer>] ip_bookend [range_start, range_end]
  # @param [Array<String>] ip_range_array
  # @return [Boolean]
  def self.ranges_intersect?(ip_bookend1, ip_bookend2)
    range1_start, range1_end = ip_bookend1
    range2_start, range2_end = ip_bookend2

    return false if range1_end < range2_start ||
                    range1_start > range2_end

    true # Ranges Intersect
  end

  # Returns string representation of an ip range_value that is stored as an integer
  # @param [Integer] ip_int
  # @return [String]
  def self.string_representation(ip_int)
    IPAddr.new(ip_int, family = Socket::AF_INET).to_s
  end

  # Converts a string ip_range into it's beginning and ending ip addresses
  # @param [String] ip_range
  # @return [Array<String, String>]
  def self.convert(ip_range)
    first = []
    last = []

    ip = ip_range.split('.')
    if ip.size != 4
      raise(ArgumentError, "Can't split into four octets: '#{ip_range}''")
    end

    ip.each_with_index do |octet, index|
      octet = octet.strip

      if valid_octet? octet
        if index.between?(1, 3) && ip[0..index].include?('*')
          raise(ArgumentError, "a number octet can't follow '*'")
        end
        if index.between?(1, 3) && (ip[0..index].join.include? '-')
          raise(ArgumentError, "a number octet can't follow '-''")
        end


        first.append octet
        last.append octet
      elsif octet == '*'
        if index.between?(0, 1)
          raise(ArgumentError, "'*' only allowed in last 2 places")
        end

        first.append '0'
        last.append '255'
      elsif octet.include? '-'
        if index.between?(0, 1)
          raise(ArgumentError, "'-' only allowed in last 2 places")
        end
        if index.between?(1, 3) && ip[0..index].include?('*')
          raise(ArgumentError, "'-' can't follow '*''")
        end

        numbers = octet.split('-')

        raise(ArgumentError, "Not a valid range: '#{octet}'") unless numbers.size == 2
        raise(ArgumentError, "Not a valid number: '#{numbers.first}'") unless valid_octet?(numbers.first)
        raise(ArgumentError, "Not a valid number: '#{numbers.last}'") unless valid_octet?(numbers.last)
        raise(ArgumentError, "Not a valid range: '#{octet}'") unless Integer(numbers.first) <= Integer(numbers.last)

        first.append numbers.first
        last.append numbers.last
      else
        raise(ArgumentError, "Can't convert into octet: '#{octet}'")
        return
      end
    end

    [first.join('.'), last.join('.')]
  end

  # Returns true if string represents valid ip range_value octet
  def self.valid_octet?(string)
    Integer(string)&.between?(0, 255) ? true : false
  rescue ArgumentError
    false
  end

  def self.valid_ip_range?(ip_range, allow_private = false)
    ips = ips_in_range ip_range
    ips.each do |ip|
      return false unless valid_ip_int? ip, allow_private
    end
    true
  rescue ArgumentError
    false
  end

  def self.valid_ip_int?(ip_int, allow_private = false)
    ip = IPAddr.new(ip_int, family = Socket::AF_INET)

    # Check to make sure ip is not in the localhost range
    return false unless (ip <=> IPAddr.new('0.255.255.255')).positive?

    # May need to allow private ranges for locations where GALILEO servers are
    # stored
    return !ip.loopback? && !ip.link_local? if allow_private

    # Make sure not a loopback address or in private range
    !ip.loopback? && !ip.private? && !ip.link_local?
  rescue IPAddr::InvalidAddressError
    false
  end

  # Converts a Galileo ip string into an EZproxy string ip
  def self.ezproxy_format(ip_range)
    return if ip_range.blank?

    pre_string = 'E'
    start, finish = convert ip_range

    return "#{pre_string} #{start}" if start == finish

    "#{pre_string} #{start} - #{finish}"
  end
end

