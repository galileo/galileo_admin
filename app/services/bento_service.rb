# frozen_string_literal: true

class BentoService
  def initialize(code, display_name, config_fields=nil)
    @code = code
    @display_name = display_name
    @config_fields = config_fields
  end

  attr_reader :code, :display_name, :config_fields

  def requires_config?
    config_fields.present?
  end

  @all_services = [
    BentoService.new('galileo', 'GALILEO'),
    BentoService.new('eds_api', 'EDS API',
                     ['API Profile', 'User ID', 'Password', 'Custom Catalog Number', 'Native EDS Profile']),
    BentoService.new('eds_publications', 'EBSCO Publication Finder'),
    BentoService.new('primo', 'Primo API',
                     ['VID', 'API Key', 'Primo Subdomain']),
    BentoService.new('dlg', 'Digital Library of Georgia'),
    BentoService.new('crdl', 'Civil Rights Digital Library')
  ]
  @services_lookup = (@all_services.map { |s| [s.code, s]}).to_h

  # @return [Array<BentoService>]
  def self.all
    @all_services
  end

  # @return [Array<BentoService>]
  def self.all_configurable
    @all_services.filter { |s| s.requires_config? }
  end

  # @return [Array<BentoService>]
  def self.all_unconfigurable
    @all_services.filter { |s| !s.requires_config? }
  end

  # @param [String] code
  # @return [BentoService]
  def self.get(code)
    return @services_lookup[code.to_s]
  end

  def self.menu_values
    @all_services.map { |s| [s.display_name, s.code] }
  end

  def self.all_configurable_fields
    all_configurable.map { |s| s.config_fields }.flatten
  end
end