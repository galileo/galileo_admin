# frozen_string_literal: true

# Service that will search the index for Allocations
class SearchService

  ## Allocation methods

  # @param [String] id
  def self.allocation_find(id)
    Allocation.search do
      with :blacklight_id, id
    end.results
  end

  # @param [Hash] param
  def self.allocations_by(param)
    Allocation.search do
      with param.keys.first, param.values.first
    end.results
  end

  # @param [Resource] resource
  def self.allocations_for_resource(resource)
    Allocation.search do
      with :for_resource, resource.code
    end.results
  end

  # @param [Institution] institution
  def self.allocations_for_institution(institution)
    Allocation.search do
      with :for_institution, institution.code
    end.results
  end

  ## Institution methods

  # @param [String] code
  def self.institution_find(code)
    Institution.search do
      with :code, code
    end.results
  end

  # @param [Hash] param
  def self.institutions_by(param)
    Institution.search do
      with param.keys.first, param.values.first
    end.results
  end

  # @param [String] code
  # @return [Sunspot::Search::Hit]
  def self.inst_hit_for(code)
    Institution.search do
      with :code, code
    end.hits.first
  end

  # @param [String] blacklight_id
  # @return [Sunspot::Search::Hit]
  def self.alloc_hit_for(blacklight_id)
    Allocation.search do
      with :blacklight_id, blacklight_id
    end.hits.first
  end
end
