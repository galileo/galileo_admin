# frozen_string_literal: true

# Service that will generate a new, unique, and child-friendly resource code for a new Resource.
class ResourceCodeService

  # Create new unique code based on values in unsaved resource
  # @param [Resource] resource
  # @return [String] code
  def self.new_code_for(resource)
    seed = generate_seed(resource.name)
    code = generate_new_code(seed)
    institution_id = resource.institution_id
    if institution_id
      instcode = Institution.find(institution_id).code
      code += "-#{instcode}"
    end
    code # returned
  end

  def self.generate_seed(name)
    # e.g., (name) The New York Times
    # (seed) the new york times
    seed = name.downcase
    # ... new york times
    seed.sub! /^(a |an |the )/, ''
    # ... newyorktimes
    seed.gsub! /[^a-z0-9]/, ''
    # ... newyorktimesabcdefghijklmnopqrstuvwxyz0123456789
    seed + ('a'..'z').to_a.join('') + ('0'..'9').to_a.join('')
  end

  # Find a new unique 4-character code
  # @param [String] resource_name
  # @return [String] code
  def self.generate_new_code(seed)
    # e.g., (code) newy
    code = seed[0..3]
    # (a) ["o","r","k","t","i","m","e","s","a","b","c",...,"7","8","9"]
    a = seed[4..-1].split(//)
    exclude = exclude_list
    for pos0 in a
      for pos1 in a
        for pos2 in a
          for pos3 in a
            return code unless exclude.include? code
            code[3] = pos3
          end
          code[2] = pos2
        end
        code[1] = pos1
      end
      code[0] = pos0
    end
    nil # valid code could not be generated
  end

  # Get existing (4-character) codes and profanity
  # @return [Array<String>] codes
  def self.exclude_list
    codes = []
    Resource.find_each do |r|
      code = r.code
      # if glri, want resource portion
      if code =~ /^([a-z0-9]{4})-[a-z0-9]{4}$/
        codes << Regexp.last_match(1)
      else
        codes << code
      end
    end
    codes + profanity_list
  end

  # Seven dirty words
  # @return [Array<String>] words
  def self.profanity_list
    # rot13
    %w(
      nany
      nahf
      ones
      obbo
      pyvg
      penc
      phag
      qrnq
      qvpx
      qlxr
      sntf
      sneg
      shpx
      uryy
      ubzb
      xyna
      zbsb
      anmv
      cvff
      cbbc
      cbea
      encr
      fuvg
      fhpx
      gvgf
    ).map { |s| s.tr('A-Za-z', 'N-ZA-Mn-za-m') }
  end

end

