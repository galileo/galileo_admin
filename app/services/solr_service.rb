# frozen_string_literal: true

class SolrService
  SOLR_TABLES = %w[Allocation BentoConfig Institution Site ConfiguredBento Banner Feature].freeze
  REINDEX_BATCH_SIZE = 10_000

  def self.reindex_all(notifier = NotificationService.new)
    notifier.start 'reindex_all solr tables'
    SOLR_TABLES.each do |table_name|
      reindex(table_name, notifier: notifier)
    end
    notifier.start 'reindex_all solr tables'
  end

  def self.reindex(table_name, batch_size: REINDEX_BATCH_SIZE, starting_id: 0, notifier: NotificationService.new)
    batch_size = batch_size > 0 ? batch_size : REINDEX_BATCH_SIZE
    starting_id = starting_id >= 0 ? starting_id : 0

    raise ArgumentError, "#{table_name} not supported" unless SOLR_TABLES.include? table_name&.classify

    table = table_name.classify.constantize
    notifier.notify "Solr reindex #{table_name} initialized with batch_size of #{batch_size} starting at id: #{starting_id}."

    total_committed = 0
    start = Time.zone.now
    rows = table.where('id >= ?', starting_id).for_indexing.order(:id)

    rows.find_in_batches(batch_size: batch_size) do |batch|
      batch_start = Time.zone.now
      if table == Feature
        batch.each{|row| row.process_thumbnail}
      end
      Sunspot.index!(batch)
      batch_end = (Time.zone.now - batch_start).round(1)
      total_committed += batch.size
      percent_done = (total_committed.to_f / rows.all.size * 100.0).round(2)
      total_elapsed_minutes = ((Time.zone.now - start) / 60.0).round(1)
      notifier.notify "#{table_name} reindex: #{total_committed}/#{rows.all.size} rows (#{percent_done}%). last id: #{batch.last.id}. batch time: #{batch_end} seconds. total time: #{total_elapsed_minutes} minutes."
    end
    notifier.notify("Solr reindex #{table_name} finished in `#{((Time.zone.now - start)/60.0).round(1)}` minutes")
  end

  def self.delete_all(notifier = NotificationService.new)
    notifier.start 'Solr delete_all'
    SOLR_TABLES.each do |table_name|
      SolrService.delete_table(table_name, notifier: notifier)
    end
    notifier.finish 'Solr delete_all'
  end

  def self.delete_table(table_name, notifier: NotificationService.new)
    notifier.start "Solr delete_table #{table_name}"
    raise ArgumentError, "#{table_name} not supported" unless SOLR_TABLES.include? table_name&.classify

    table = table_name.classify.constantize

    Sunspot.remove_all! table
    notifier.finish "Solr delete_table #{table_name}"
  end
end
