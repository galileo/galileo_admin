# frozen_string_literal: true

# Service to generate links for resources based on Resource, Institution and
# Allocation values
class LinkService
  OA_REDIRECTOR_URL = 'https://go.openathens.net/redirector/'
  OA_PROXY_URL = 'https://proxy.openathens.net/login'
  URLENCODED_VARIABLES = %w[url_dbspwd url_dbsuid url_instname].freeze
  SUPPORTED_VARIABLES =
    URLENCODED_VARIABLES + %w[instcode uc_instcode timestamp].freeze

  attr_reader :variables

  # Represent a URL
  class ResourceUrl
    attr_accessor :type, :source, :template, :templated, :link

    def initialize(type)
      @type = type
    end

    def to_s
      "Final URL: #{link} \nTemplate: #{template} \nSource: #{source}"
    end
  end

  # Represent a Variable
  class UrlVariable
    attr_accessor :name, :value, :source, :error, :encoded

    def initialize(name)
      @name = name
    end

    def to_s
      "Name: #{name} \n Value: #{value} \n Source: #{source} \n Encoded: #{encoded} \n Error: #{error} \n"
    end
  end

  def self.default_logger
    @logger ||= Logger.new('log/link_service.log')
  end

  # @param [Allocation] allocation
  # @param [Logger] logger
  def initialize(allocation, logger = nil)
    if Rails.env.development?
      @logger = logger || self.class.default_logger
    end
    @allocation = allocation
    # set all potential variable values on init
    @variables = extract_vars_from_urls
  end

  def link_data
    { ip: ip, remote: remote, open_athens: open_athens, variables: variables }
  end

  # Return a Resource URL object for the @allocation's IP Link
  # @return [ResourceUrl]
  def ip
    # Initialize URL Object
    url = ResourceUrl.new(:ip)
    # Pull URL data from source
    source_url_data = ip_url_data
    # Add source URL data to URL Object
    url.source = source_url_data[:source]
    url.template = source_url_data[:value]
    # perform templating
    url.templated = interpolate url.template
    # in the IP case, no proxy or redirect required
    url.link = url.templated
    url
  end

  # Return a Resource URL object for the @allocation's Remote Link
  # @return [ResourceUrl]
  def remote
    # Initialize URL Object
    url = ResourceUrl.new(:remote)
    # Pull URL data from source
    source_url_data = remote_url_data
    # Add source URL data to URL Object
    url.source = source_url_data[:source]
    url.template = source_url_data[:value]
    # perform templating
    url.templated = interpolate url.template
    # in the remote case, use OAProxy or EZproxy if configured
    # unless the remote url is overridden in the allocation
    if url.source == :allocation
      url.link = url.templated
    elsif (link = oa_proxy(url.templated))
      url.link = link
    else
      url.link = ezproxy url.templated
    end
    url
  end

  # Return a Resource URL object for the @allocation's Open Athens Link
  # @return [ResourceUrl]
  def open_athens
    # Initialize URL Object
    url = ResourceUrl.new(:open_athens)
    # Pull URL data from source
    source_url_data = open_athens_url_data
    # Add source URL data to URL Object
    url.source = source_url_data[:source]
    url.template = source_url_data[:value]
    # perform templating
    url.templated = interpolate url.template
    # in the OA case, use the redirector prefix if inst and resource are
    # properly configured. otherwise set link to nil
    url.link = oa_redirect url.templated
    url
  end

  private

  # Send log messages only in development
  # @param [String] message
  def log_warn(message)
    @logger.warn(message) if Rails.env.development?
  end

  # applies variables from the configuration hash to the designated locations
  # in the url template
  # @param [String] template
  # @return [String] interpolated_url
  def interpolate(template)
    templated = template.dup
    url_variables(templated).each do |v|
      variable = @variables[v]
      # If no variable object, there was an issue looking up the variable object
      # a warning should already be logged
      next unless variable

      value = variable.encoded ? variable.value : url_encode(variable.value)
      templated.gsub!("{VAR:#{v}}", value)
    end
    templated
  end

  # Get the IP URL
  # @return [Hash] data
  def ip_url_data
    source_url_data :ip_access_url
  end

  # Get the Remote Access URL, or the IP Url if not found
  # @return [Hash] data
  def remote_url_data
    data = source_url_data(:remote_access_url)
    data[:value] ? data : ip_url_data
  end

  # Get the Remote Access URL, or the IP Url if not found
  # @return [Hash] data
  def open_athens_url_data
    data = source_url_data(:open_athens_url)
    data[:value] ? data : ip_url_data
  end

  # Get URL from source, preferring any value set on the Allocation
  # @param [Symbol] field
  def source_url_data(field)
    if @allocation.send(field).present?
      { source: :allocation, value: @allocation.send(field) }
    elsif @allocation.resource.send(field).present?
      { source: :resource, value: @allocation.resource.send(field) }
    else
      { source: :none, value: nil, error: 'No URL set' }
    end
  end

  # generates hash of discovered URL variables and their values
  # TODO: this is a bit tortured
  # @return [Hash]
  def extract_vars_from_urls
    var_hash = {}
    urls = [ip_url_data, remote_url_data, open_athens_url_data].map { |d| d[:value] }.uniq
    return var_hash unless urls.any?

    vars = urls.map { |url| url_variables(url) }.flatten.uniq
    vars.map do |var|
      variable_object = UrlVariable.new var
      begin
        unless var.in? SUPPORTED_VARIABLES
          raise StandardError, "variable not supported #{var}"
        end

        value_data = value_for var
      rescue StandardError => e
        log_warn "Problem parsing variables for Allocation #{@allocation.id}: #{e}"
        variable_object.error = e.message
        var_hash[var] = variable_object
        next
      end
      variable_object.value = value_data[:value]
      variable_object.source = value_data[:source]
      variable_object.encoded = var.in? URLENCODED_VARIABLES
      var_hash[var] = variable_object
    end
    var_hash
  end

  # Lookup Value for a given variable in a variety of sources
  # TODO: this is also tortured
  # @param [Object] var
  # @return [Hash]
  # @raise StandardError
  def value_for(var)
    value_data = case var
                 when 'timestamp'
                   { value: Time.zone.now.to_i.to_s }
                 when 'instcode'
                   { value: @allocation.institution.code, source: 'institution' }
                 when 'uc_instcode'
                   { value: @allocation.institution.code.upcase, source: 'institution' }
                 when 'url_instname'
                   # TODO: wat
                   { value: @allocation.institution.name, source: 'institution' }
                 when 'url_dbsuid'
                   find_variable_value_for :user_id
                 when 'url_dbspwd'
                   find_variable_value_for :password
                 else
                   find_variable_value_for var
                 end
    unless value_data && value_data[:value]
      raise(StandardError, "No value found for #{var} on Allocation, Resource or Resource Parent")
    end

    # What if the value is another VAR reference?
    if url_variables(value_data[:value])&.any?
      value_data = value_for(url_variables(value_data[:value]).first)
    end
    value_data
  end

  # Attempt to find supported variable in either Allocation or Resource,
  # return a Hash of data including the value and where it came from
  # @return [Hash]
  # @param [Symbol] field
  def find_variable_value_for(field)
    value = @allocation.send(field)
    return { value: value, source: 'allocation' } if value

    value = @allocation.resource.send(field)
    return { value: value, source: 'resource' } if value

    return unless @allocation.resource.parent

    value = @allocation.resource.parent.send(field)
    { value: value, source: 'parent resource' } if value
  end

  # @param [String] url
  # @return [Array] found variables
  def url_variables(url)
    unless url
      log_warn "empty url sent to url_variables for A: #{@allocation.id}"
      return []
    end

    url.scan(/{VAR:(\w+)}/).flatten
  end

  # Generate an EZproxy link for a url
  # @param [String] url
  # @return [String]
  def ezproxy(url)
    return url unless @allocation.resource.proxy_remote

    ezproxy_uri = Addressable::URI.heuristic_parse @allocation.institution.proxy_url
    return url unless ezproxy_uri

    # Ensure the scheme is 'https'
    ezproxy_uri.scheme = 'https' if ezproxy_uri.scheme.nil? || ezproxy_uri.scheme == 'http'

    ezproxy_uri.path = 'login'
    "#{ezproxy_uri}?url=#{url}"  # note that url is not encoded
  end

  # encode the URL for inclusion as a URL parameter
  # @param [String] string
  # @return [String] encoded url
  def url_encode(string)
    ERB::Util.url_encode string
  end

  # Generate an OA redirector link using a url and the Institution scope
  # The redirector link uses the Open Athens API Name.  In most cases this
  # is the same as the Open Athens Scope, but there can be exceptions.
  # @param [String] url
  # @return [String]
  def oa_redirect(url)
    unless @allocation.institution.open_athens? && @allocation.resource.open_athens?
      return nil
    end

    if @allocation.resource.without_open_athens_redirector
      return url
    end

    api_name = if @allocation.institution.open_athens_api_name.present?
                  @allocation.institution.open_athens_api_name
               else
                  @allocation.institution.open_athens_scope
               end

    uri = Addressable::URI.parse OA_REDIRECTOR_URL
    uri.path = uri.path + api_name
    uri.query_values = { url: url }
    uri.to_s
  end

  # Generate an OA proxy link for a url
  # @param [String] url
  # @return [String]
  def oa_proxy(url)
    unless @allocation.institution.oa_proxy? && @allocation.resource.oa_proxy?
      return nil
    end

    uri = Addressable::URI.parse OA_PROXY_URL
    uri.query_values = { entityID: 'https://idp.wa.galileo.usg.edu/entity', qurl: url }
    uri.to_s
  end
end
