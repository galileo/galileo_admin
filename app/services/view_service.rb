class ViewService

  # All view type codes
  # @return [Array]
  def self.view_type_codes
    %w[default elementary middle highschool educator full]
  end

  def self.view_types_for_menus(inst_type)
    case inst_type.to_sym
    when :k12
      [['Elementary School', 'elementary'],
       ['Middle School', 'middle'],
       ['High School', 'highschool'],
       %w[Educator educator]]
    when :publiclib
      [['Elementary School', 'elementary'],
       ['Middle School', 'middle'],
       ['High School', 'highschool'],
       %w[Full full]]
    else
      [%w[Default default]]
    end
  end

  def self.valid_view_types_for(inst_type)
    view_types = view_types_for_menus(inst_type)
    view_types.map{|type| type.second}
  end

  # @return [Bool]
  def self.institution_display_views?(inst_type)
    return false unless inst_type
    inst_type.to_sym == :k12 || inst_type.to_sym == :publiclib
  end

end
