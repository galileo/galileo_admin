# frozen_string_literal: true

require 'aws-sdk-s3'
require 'open3'

# Class to import Production Data dumps
class S3Service
  ALLOWED_ENVIRONMENTS = %w[staging dev].freeze

  # Empty the image bucket for the current environment
  def self.empty_image_bucket(notifier: NotificationService.new)
    notifier.start 'empty_image_bucket'

    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      message = "Not allowed to empty_image_bucket in #{Rails.env}"
      raise Exceptions::DataImportError.new, message
    end
    bucket = Rails.application.credentials[:aws][:image_bucket][Rails.env.to_sym]
    command = "aws s3 rm s3://#{bucket} --recursive"

    _stdout_str, error_str, status = Open3.capture3(command)
    if status.success?
      notifier.finish 'empty_image_bucket'
      return
    end

    message = "Unable to empty_image_bucket. #{error_str}"
    raise Exceptions::DataImportError.new, message
  end

  # Sync images from production to the current environment
  def self.sync_image_bucket(notifier: NotificationService.new)
    notifier.start 'sync_image_bucket'
    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      message = "Not allowed to sync_s3_buckets in #{Rails.env}"
      raise Exceptions::DataImportError.new, message
    end
    source = Rails.application.credentials[:aws][:image_bucket][:production]
    target = Rails.application.credentials[:aws][:image_bucket][Rails.env.to_sym]
    command = "aws s3 sync s3://#{source} s3://#{target}"

    _stdout_str, error_str, status = Open3.capture3(command)

    if status.success?
      notifier.finish 'sync_image_bucket'
      return
    end

    message = "Unable to sync s3 buckets. #{error_str}"
    raise Exceptions::DataImportError.new, message
  end

  # Download Bucket contents to local directory
  def self.download_bucket(bucket_name, local_directory = '.', notifier: NotificationService.new)
    notifier.start 'download_bucket'
    command = "aws s3 cp s3://#{bucket_name} #{local_directory} --recursive"

    _stdout_str, error_str, status = Open3.capture3(command)
    if status.success?
      notifier.finish 'download_bucket'
      return
    end

    message = "Unable to download_bucket. #{error_str}"
    raise Exceptions::DataImportError.new, message
  end

  def self.process_all_thumbnails(delete_variant_cache: false, notifier: NotificationService.new)
    if delete_variant_cache
      notifier.notify 'Deleting all rows from `active_storage_variant_records`...'
      ActiveRecord::Base.connection.execute 'DELETE FROM active_storage_variant_records'
    end
    notifier.notify 'Processing vendor logo thumbnails...'
    Vendor.find_each { |v| v.logo_thumbnail&.processed }
    notifier.notify 'Processing institution logo thumbnails...'
    Institution.find_each { |i| i.logo_thumbnail&.processed }
    notifier.notify 'Processing resource logo thumbnails...'
    Resource.find_each { |r| r.logo_thumbnail&.processed }
    notifier.notify 'Processing feature thumbnails...'
    Feature.find_each { |f| f.thumbnail&.processed }
    notifier.notify 'Processing branding thumbnails...'
    Branding.find_each { |b| b.image_thumbnail&.processed }
  end
end
