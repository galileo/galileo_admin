# frozen_string_literal: true

# Service to prepare reports
class ReportService
  # All Rows for a given ActiveRecord Klass
  # @param [String] klass_name
  def self.all_rows_for(klass_name)
    klass = Object.const_get klass_name
    cols = klass.attribute_names
    results = klass.all.map do |row|
      row.attributes.values
    end
    filename = klass_name.downcase
    [to_csv(cols, results), filename]
  end

  # Rows for a given ActiveRecord Relation
  # @param [ActiveRecord_Relation] query
  def self.rows_for(query)
    klass =query.klass
    cols = klass.attribute_names
    results = query.map do |row|
      row.attributes.values
    end
    filename = klass.to_s.underscore
    [to_csv(cols, results), filename]
  end

  # Vendors (for stats)
  def self.vendors_for_stats
    cols = %w[code name resource_codes]
    results = []
    Vendor.all.includes(:resources).each do |vendor|
      resource_codes = vendor.resources.pluck(:code).join(' ')
      results << [vendor.code, vendor.name, resource_codes]
    end
    filename = 'vendors_for_stats'
    [to_csv(cols, results), filename]
  end

  # Resources (for stats)
  def self.resources_for_stats
    cols = %w[code name vendor active glri include_in_stats institution_codes]
    results = []
    Resource.all.includes(:institutions).each do |resource|
      institution_codes = resource.institutions.pluck(:code).join(' ')
      vendor_code = if resource.glri?
                      'glri'
                    elsif resource.vendor
                      resource.vendor.code
                    else
                      ''
                    end
      results << [resource.code, resource.name, vendor_code, resource.active, resource.glri?, resource.include_in_stats,
                  institution_codes]
    end
    filename = 'resources_for_stats'
    [to_csv(cols, results), filename]
  end

  # Institutions (for stats)
  def self.institutions_for_stats
    cols = %w[code name institution_group active glri_participant]
    insts = Institution.all
    results = insts.map do |inst|
      [inst.code, inst.name, inst.institution_group&.code,
       inst.active, inst.glri_participant]
    end
    filename = 'institutions_for_stats'
    [to_csv(cols, results), filename]
  end

  # Vendor Statistics Identifier (for vendor stats)
  def self.vendor_statistics_identifier_for_stats(resource_code)
    cols = %w[institution_code resource_code vendor_statistics_identifier]
    resource = Resource.find_by_code(resource_code)
    results = []
    resource.allocations.includes(:institution).each do |allocation|
      results << [allocation.institution.code, resource.code, allocation.vendor_statistics_identifier]
    end
    filename = 'vendor_statistics_identifier_for_stats'
    [to_csv(cols, results), filename]
  end

  # Non GLRI Institutions report
  def self.non_glri_institutions
    cols = %w[code name glri_participant]
    results = Institution.active.real.where(glri_participant: 'none').map do |inst|
      [inst.code, inst.name, inst.glri_participant]
    end
    filename = 'non_glri_institutions'
    [to_csv(cols, results), filename]
  end

  # Institution Group IPs/OA report
  # @param [Integer] inst_group_id
  def self.institution_ips(inst_group_id: nil, active_state: nil)
    insts = if inst_group_id.present?
             Institution.real.includes(:institution_group).where(institution_group_id:  inst_group_id )
           else
             Institution.real.includes(:institution_group)
           end
    cols = %w[code name active galileo_ez_proxy_ip ip_addresses
              open_athens_proxy_ip open_athens_entity_id open_athens_scope open_athens_api_name
              open_athens_subscope_array open_athens_org_id institution_group]
    results = insts.active_state(active_state).map do |inst|
      [inst.code, inst.name, inst.active, inst.galileo_ez_proxy_ip, inst.ip_addresses.join(', '),
       inst.open_athens_proxy_ip, inst.open_athens_entity_id, inst.open_athens_scope, inst.open_athens_api_name,
       inst.open_athens_subscope_array.join(', '), inst.open_athens_org_id, inst.institution_group&.code]
    end
    group = InstitutionGroup.find_by_id(inst_group_id)
    filename = "institutions_ips"
    filename += "_#{group.code}" if group
    [to_csv(cols, results), filename]
  end

  # Institution Resource Report
  # @param [Integer] inst_id
  def self.institution_resources(inst_id)
    cols = %w[resource_code resource_name vendor_code vendor_name short_description long_description]
    inst = Institution.find_by_id(inst_id)
    results = []
    inst.resources.includes(:vendor).each do |resource|
      next if resource.internal?

      results << [resource.code, resource.name,
                  resource.vendor&.code, resource.vendor&.name, resource.short_description, resource.long_description]
    end
    filename = "#{inst.code}_resources"
    [to_csv(cols, results), filename]
  end

  # Institution GLRI Resources Report
  # @param [Integer] inst_id
  def self.institution_glri_resources(inst_id)
    cols = %w[created_at code active name ip_access_url remote_access_url open_athens
              open_athens_url vendor_name short_description long_description
              access_note]
    inst = Institution.find(inst_id)
    results = inst.resources.glri.map do |resource|
      [resource.created_at, resource.code, resource.active, resource.name,
       resource.ip_access_url, resource.remote_access_url, resource.open_athens,
       resource.open_athens_url, resource.vendor_name, resource.short_description,
       resource.long_description, resource.access_note]
    end
    filename = "#{inst.code}_glri_resources"
    [to_csv(cols, results), filename]
  end

  # Institution Resource Report
  # @param [Integer] inst_id
  def self.institution_express_links(inst_id)
    cols = %w[resource_code resource_name express_link]
    inst = Institution.find_by_id(inst_id)
    results = []
    inst.allocations.includes(:resource).each do |allocation|
      next if allocation.resource.internal?

      results << [allocation.resource.code, allocation.resource.name,
                  allocation.express_link]
    end
    filename = "#{inst.code}_express_links"
    [to_csv(cols, results), filename]
  end

  # GLRI Contacts report
  # @param [String] glri_type
  def self.glri_managed_contacts(glri_type)
    cols = %w[institution_code institution_name glri_participant first_name
              last_name email phone types]
    contacts = if glri_type == 'all'
                 Contact.with_type('glri').joins(:institution).includes(:institution)
                        .where.not(institutions: { glri_participant: 'none', active: false, special: true, test_site: true })
               else
                 Contact.with_type('glri').joins(:institution).includes(:institution)
                        .where(institutions: { glri_participant: glri_type, active: true, special: false, test_site: false })
               end

    results = contacts.map do |contact|
      [contact.institution.code, contact.institution.name,
       contact.institution.glri_participant, contact.first_name,
       contact.last_name, contact.email, contact.phone, contact.types]
    end
    filename = "glri_contacts_#{glri_type}"
    [to_csv(cols, results), filename]
  end

  # HTML in description report
  # @param [String] description_field
  def self.resources_with_html(description_field)
    cols = %w[resource_code resource_name active glri_record institution_name
              institution_code description]
    resources = Resource.includes(:institution).where("#{description_field} LIKE ?", '%<%>%')

    results = resources.map do |resource|
      [resource.code, resource.name,
       resource.active, resource.glri?,
       resource.institution&.name, resource.institution&.code,
       resource.send(description_field)]
    end
    filename = "#{description_field}_with_html"
    [to_csv(cols, results), filename]
  end

  # URLs in some resource fields
  # @param [String] resource_type
  def self.resources_url_check(resource_type = 'all')
    cols = %w[resource_code institution_code glri_participant url field]

    resources = case resource_type
                when 'central'
                  Resource.central
                when 'glri_locally'
                  Resource.glri_locally_managed
                when 'glri_galileo'
                  Resource.glri_galileo_managed
                when 'all'
                  Resource.all
                else
                  Resource.all
                end
    results = []
    resources.active.each do |resource|
      %w[long_description short_description access_note].each do |field|
        next if resource.send(field).blank?

        URI.extract(resource.send(field), %w[http https host]).each do |url|
          results << [resource.code, resource.institution&.code,
                      resource.institution&.glri_participant, url, field]
        end
      end
    end
    filename = "#{resource_type}_resources_url_check"
    [to_csv(cols, results), filename]
  end

  # EZproxy update report
  def self.ez_proxy_update(instcode = nil)
    cols = nil

    institutions = if instcode
                     Institution.where(code: instcode)
                   else
                     Institution.active
                   end
    results = []
    institutions.each do |institution|
      results << [institution.code, institution.ezproxy_institution_code,
                  institution.current_password] + institution.ezproxy_ip_addresses
    end
    filename = 'ez_proxy_update'
    [to_csv(cols, results), filename]
  end

  # Institution Group IPs/OA report
  def self.public_libraries_primary_contacts
    cols = %w[institution_code institution_name first_name last_name email phone]
    contacts = Contact.with_type('prim').joins(institution: :institution_group)
                      .includes(:institution)
                      .where(institution_groups: { code: 'publiclibs' },
                             institutions: { active: true, special: false,
                                             test_site: false })

    results = contacts.map do |contact|
      [contact.institution.code, contact.institution.name, contact.first_name,
       contact.last_name, contact.email, contact.phone]
    end
    filename = 'public_libraries_primary_contacts'
    [to_csv(cols, results), filename]
  end

  def self.institution_group_contacts(inst_group_code)
    roles = Contact.allowed_type_codes
    cols = %w[institution_code institution_name first_name last_name email phone] + roles
    contacts = Contact.joins(institution: :institution_group)
                      .includes(:institution)
                      .where(institution_groups: { code: inst_group_code },
                             institutions: { active: true, special: false,
                                             test_site: false })

    results = contacts.map do |contact|
      result = [contact.institution.code, contact.institution.name, contact.first_name,
       contact.last_name, contact.email, contact.phone]
      roles.each do |role|
        result << contact.types.include?(role).to_s
      end
      result
    end
    filename = "inst_group_#{inst_group_code}_contacts"
    [to_csv(cols, results), filename]
  end

  def self.vendor_resources(vendor_id)
    cols = %w[resource_name resource_code resource_category serial_count short_description ip_access_url remote_access_url open_athens_url active]
    vendor = Vendor.find_by_id(vendor_id)
    results = vendor.resources.map do |resource|
      [resource.name, resource.code, resource.resource_category, resource.serial_count, resource.short_description,
       resource.ip_access_url, resource.remote_access_url, resource.open_athens_url, resource.active]
    end
    filename = "#{vendor.code}_resources"
    [to_csv(cols, results), filename]
  end

  def self.allocations_institution(institution_ids)
    allocations = Allocation.joins(:institution).where(institutions: {id: institution_ids.reject(&:blank?)})
    allocations_report(allocations: allocations, filename: 'allocations_institution')
  end

  def self.allocations_vendor(vendor_ids)
    allocations = Allocation.joins(:resource).where(resources:{institution_id: nil, vendor_id: vendor_ids.reject(&:blank?)})
    allocations_report(allocations: allocations, filename: 'allocations_vendor')
  end

  def self.allocations_resource(resource_ids)
    allocations = Allocation.where(resource_id: resource_ids.reject(&:blank?))
    allocations_report(allocations: allocations, filename: 'allocations_resource')
  end

  def self.allocations_report(allocations: Allocation.all, filename: 'allocations' )
    cols = %w[institution_name institution_active? resource_name resource_active?
              vendor_name institution_group_code institution_code
              resource_code glri? public?
              subscription_type vendor_statistics_identifier display
              ip_access_url remote_access_url open_athens_url
              bypass_open_athens user_id password
              name_override branding_override
              override_user_views allocation_user_views(import_not_supported)
              additional_subjects(import_not_supported) additional_keywords
              note special_info
              created_at updated_at]
    results = allocations.includes(:branding, :user_views, :subjects, institution: :institution_group, resource: :vendor, ).map do |allocation|
      [allocation.institution.name, allocation.institution.active, allocation.resource.name, allocation.resource.active,
       allocation.vendor_name, allocation.institution.institution_group&.code, allocation.institution.code,
       allocation.resource.code, allocation.resource.glri?, allocation.resource.public?,
       allocation.subscription_type, allocation.vendor_statistics_identifier, allocation.display,
       allocation.ip_access_url, allocation.remote_access_url, allocation.open_athens_url,
       allocation.bypass_open_athens, allocation.user_id, allocation.password,
       allocation.name, allocation.branding&.name,
       allocation.override_user_views, allocation.override_user_view_names,
       allocation.additional_subject_names, allocation.keywords,
       allocation.note, allocation.special_info,
       allocation.created_at, allocation.updated_at]
    end
    [to_csv(cols, results), filename]
  end

  def self.sites(query)
    cols = %w[institution_code institution_name code name site_type charter]

    results = query.includes(:institution).map do |site|
      [site.institution.code, site.institution.name, site.code, site.name, site.site_type, site.charter]
    end
    filename = 'sites'
    [to_csv(cols, results), filename]
  end

  def self.institution_resources_filterable(query)
    cols = %w[resource_name resource_code vendor_name resource_active?
               glri? public? subscription_type display
               subjects_galileo additional_subjects
               keywords_galileo additional_keywords
               name_override branding_override]
    results = query.includes(institution: :institution_group, resource: :vendor).map do |allocation|
      [allocation.resource.name, allocation.resource.code, allocation.vendor_name, allocation.resource.active,
       allocation.resource.glri?, allocation.resource.public?, allocation.subscription_type, allocation.display,
       allocation.resource.subject_names, allocation.additional_subject_names,
       allocation.resource.keywords, allocation.keywords,
       allocation.name, allocation.branding&.name]
    end
    filename = 'institution_resources'
    [to_csv(cols, results), filename]
  end

  def self.glri_allocations
    cols = %w[resource_code resource_name allocated? institution_code institution_name updated_at]
    results = Resource.glri.active.includes(:allocations, :institution).map do |resource|
      [resource.code, resource.name, resource.allocations.any?, resource.institution.code,
       resource.institution.name, resource.updated_at]
    end
    filename = 'glri_allocations'
    [to_csv(cols, results), filename]
  end

  def self.inst_user_emails
    cols = %w[first_name last_name email role institutions]
    results = []
    User.with_role('institutional').includes(:institutions).each do |user|
      institution_codes = user.institutions.pluck(:code).join(' ')
      results << [user.last_name, user.first_name, user.email, user.role, institution_codes]
    end
    filename = 'inst_user_emails'
    [to_csv(cols, results), filename]
  end

  def self.overlapping_ip_addresses
    cols = %w[inst1_code inst1_conflicting_ip inst2_code inst2_conflicting_ip]
    results = []
    insts_lookup_hash = Institution.active.real.pluck(:code, :ip_addresses).map { |code, ip_addresses| [code, IpService.bookend_hash(ip_addresses)] }.to_h
    insts_lookup_hash.each do |inst_code, ip_bookend_hash|
      insts_lookup_hash.each do |other_inst_code, other_ip_bookend_hash|
        next if inst_code == other_inst_code

        ip_bookend_hash.each do |range_string, range_bookend|
          other_ip_bookend_hash.each do |other_range_string, other_range_bookend|
            if IpService.ranges_intersect? range_bookend, other_range_bookend
              results << [inst_code, range_string, other_inst_code, other_range_string]
            end
          end
        end
      end
      # Remove the institution from the hash so we don't do double comparisons
      insts_lookup_hash.delete inst_code
    end
    filename = 'overlapping_ip_addresses'
    [to_csv(cols, results), filename]
  end

  def self.institution_features
    cols = %w[inst_name inst_code view_type position link_label link_url
              link_description featuring_id created_at updated_at]

    results = Feature.where(featuring_type: 'Institution').includes(:featuring).map do |f|
      [f.featuring&.name, f.featuring&.code, f.view_type, f.position,
       f.link_label, f.link_url, f.link_description, f.featuring_id,
       f.created_at, f.updated_at]
    end
    filename = 'institution_features'
    [to_csv(cols, results), filename]
  end

  def self.institution_group_features
    cols = %w[inst_group_code inst_type view_type position link_label link_url
              link_description featuring_id created_at updated_at]

    results = Feature.where(featuring_type: 'InstitutionGroup').includes(:featuring).map do |f|
      [f.featuring&.code, f.featuring&.inst_type, f.view_type, f.position,
       f.link_label, f.link_url, f.link_description, f.featuring_id,
       f.created_at, f.updated_at]
    end
    filename = 'institution_group_features'
    [to_csv(cols, results), filename]
  end

  def self.bento_configs_all
    standard_cols = %w[institution_code name service view_types]
    json_cols = BentoService.all_configurable_fields
    timestamp_cols = %w[created_at updated_at]
    all_cols = standard_cols + json_cols + timestamp_cols

    results = BentoConfig.all.includes(:institution).map do |bento_config|
      [bento_config.institution.code, bento_config.name, bento_config.service, bento_config.view_types&.join(' ')] +
        json_cols.map {|k| bento_config.credentials&.dig(k) || ''} +
        [bento_config.created_at, bento_config.updated_at]
    end
    filename = 'bento_configs'
    [to_csv(all_cols, results), filename]
  end

  def self.contacts(query)
    cols = %w[institution_code vendor_code first_name last_name email phone types notes created_at updated_at]

    results = query.includes(:institution, :vendor).map do |contact|
      [contact.institution&.code, contact.vendor&.code, contact.first_name, contact.last_name, contact.email,
       contact.phone, contact.types, contact.notes, contact.created_at, contact.updated_at]
    end
    filename = 'contacts'
    [to_csv(cols, results), filename]
  end

  def self.galileo_passwords
    cols = %w[institution_name institution_code current_password new_password
              next_notify_date next_change_date effective_until_date
              notify_group notify_dates change_dates]

    results = Institution.active.map do |institution|
      notify_dates = institution.notify_group&.notify_dates || institution.notify_dates
      change_dates = institution.notify_group&.change_dates || institution.change_dates
      next_notify_date = to_yyyy_mm_dd(PasswordService.next_date(notify_dates))
      next_change_date = to_yyyy_mm_dd(PasswordService.next_date(change_dates))
      effective_until_date = to_yyyy_mm_dd(PasswordService.effective_until_date(change_dates))
      [institution.name, institution.code, institution.current_password, institution.new_password,
       next_notify_date, next_change_date, effective_until_date,
       institution.notify_group&.code, notify_dates, change_dates]
    end
    filename = 'galileo_passwords'
    [to_csv(cols, results), filename]
  end

  def self.resource_keywords
    cols = %w[name code active keywords]

    results = Resource.where.not(keywords: '{}').map do |resource|
      [resource.name, resource.code, resource.active, resource.keywords]
    end
    filename = 'resource_keywords'
    [to_csv(cols, results), filename]
  end

  def self.wayfinder_keywords
    cols = %w[site_or_institution name code inst_name inst_code active wayfinder_keywords]

    results = []

    Institution.where.not(wayfinder_keywords: '{}').each do |inst|
      results << ['institution', inst.name, inst.code, inst.name, inst.code, inst.active, inst.wayfinder_keywords]
    end
    Site.includes(:institution).where.not(wayfinder_keywords: '{}').each do |site|
      results << ['site', site.name, site.code, site.institution.name, site.institution.code, site.institution.active, site.wayfinder_keywords]
    end
    filename = 'wayfinder_keywords'
    [to_csv(cols, results), filename]
  end

  def self.deleted_contacts
    cols = %w[first_name last_name email institution_code contact_types date_deleted whodunnit]

    results = []

    Version.where(item_type: 'Contact', event: 'destroy').each do |version|
      contact = JSON.parse version.object
      institution = Institution.find_by_id contact['institution_id']
      deleter = User.find_by_id version.whodunnit
      results << [contact['first_name'], contact['last_name'], contact['email'], institution&.code, contact['types'], version.created_at, deleter&.email]
    end

    filename = 'deleted_contacts'
    [to_csv(cols, results), filename]
  end

  # Institution Serial Count Report
  # @param [Integer] inst_id
  def self.institution_serial_count(inst_id)
    cols = %w[galileo_resource_code resource_name vendor_name resource_category serial_count]
    institution = Institution.find inst_id
    allocations = Allocation.includes(resource: :vendor).where(institution_id: institution, resources: {institution_id: nil})
    results = []
    allocations.each do |allocation|
      next if allocation.resource.internal?

      results << [allocation.resource.code, allocation.resource.name, allocation.resource.vendor&.name, allocation.resource.resource_category,
                  allocation.resource.serial_count]
    end
    filename = "#{institution.code}_institution_serial_count"
    [to_csv(cols, results), filename]
  end

  # stats for snapshots
  def self.stats_for_snapshots(inst_id, month)
    inst = Institution.find inst_id
    mdate = (month + "-01").to_date

    counts = {}
    logins = 0
    resource_names = {}
    resource_names_sort = {}
    stats_types = {}
    Monthly.where(month: mdate, institution_code: inst.code).each do |row|
      stats_type = row.stats_type
      if stats_type == 'login'  # logins don't have a resource_code
        logins += row.count
      else
        resource_code = row.resource_code
        resource = Resource.find_by_code(resource_code)
        next unless resource.include_in_stats
        resource_name = resource_names[resource_code]
        unless resource_name.present?
          resource_name = resource_names[resource_code] = resource.name_with_code
          resource_names_sort[sort_key(resource_name)] = resource_name
        end
        counts[resource_name] = {} unless counts.key?(resource_name)
        counts[resource_name][stats_type] = 0 unless counts[resource_name].key?(stats_type)
        counts[resource_name][stats_type] += row.count
        stats_types[stats_type] = true
      end
    end

    stats_types_cells = stats_types.keys.sort
    cols = ["#{mdate.strftime("%m/%Y")} - #{inst.name_with_code}", 'login', stats_types_cells].flatten
    totals = {}
    results = []
    resource_names_sort.keys.sort.each do |key|
      resource_name = resource_names_sort[key]
      row = []
      row << resource_name
      row << ''  # for login column
      stats_types_cells.each do |stats_type|
        count = counts[resource_name][stats_type]
        if count.present?
          row << count
          totals[stats_type] = 0 unless totals.key?(stats_type)
          totals[stats_type] += count
        else
          row << 0
        end
      end
      results << row
    end
    total_row = []
    total_row << "Totals"
    total_row << logins
    stats_types_cells.each do |stats_type|
      total_row << totals[stats_type]
    end
    results << total_row
    filename = "#{inst.code}_#{month}_stats_for_snapshots"
    [to_csv(cols, results), filename]
  end

  def self.generalized_report(activerecord_relation, columns, filename: 'generalized_report')
    cols = columns
    results = activerecord_relation.map do |row|
      columns.map{|column| row.send(column)}
    end

    [to_csv(cols, results), filename]
  end

  def self.sort_key(string)
    string.downcase.sub(/^(a|an|the) (.*)$/, '\2, \1')
  end

  def self.to_yyyy_mm_dd(yyyymmdd)
    yyyymmdd&.gsub(/(\d{4})(\d{2})(\d{2})/, '\1-\2-\3')
  end

  def self.to_csv(cols, results)
    CSV.generate do |csv|
      csv << cols unless cols.blank?
      results.each do |result|
        csv << result
      end
    end
  end
end
