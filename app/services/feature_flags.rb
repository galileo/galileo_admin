# frozen_string_literal: true

class FeatureFlags
  def self.enabled?(feature_name)
    case feature_name&.to_sym
    when :csv_importer
      true
    when :site_type
      true
    when :inst_user_configure_bento
      true
    when :user_bento_customization
      false
    when :stats_tables
      !Rails.env.production?
    when :banners
      true
    when :user_subroles
      true
    when :widgets
      true
    when :flexible_bento_templates
      true
    when :view_switching
      true
    when :logos_on_index_pages
      !Rails.env.production?
    when :logo_tab
      !Rails.env.production?
    else
      false
    end
  end
end
