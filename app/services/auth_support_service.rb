# frozen_string_literal: true

# Service to lookup institutions in support of Auth Support API and return a
# nicely crafted response
class AuthSupportService

  # Lookup and Institution based on parameters
  # @param [ActionController::Parameters] params
  # @return [Hash] response
  def self.institution_lookup(params)
    case determine_auth_type_from params
    when :passphrase then passphrase_lookup params
    when :ip then ip_lookup params
    when :oa then oa_lookup params
    when :geo then geo_lookup params
    when :pines then pines_lookup params
    # when :lti
    else
      raise ActionController::BadRequest
    end
  rescue ActiveRecord::RecordNotFound => _e
    unsuccessful_response 'Institution not found'
  rescue Exceptions::IpLookupFailedError => e
    unsuccessful_response e.message, :ip
  end

  # Attempt to lookup inst by Passphrase
  # @param [ActionController::Parameters] params
  # @return [Hash] response
  # @raise ActiveRecord::RecordNotFound
  def self.passphrase_lookup(params)
    successful_response(
      Institution.find_by!(current_password: params[:passphrase]),
      auth_type: :passphrase, remote: true
    )
  end

  # Attempt to lookup inst by IP
  # @param [ActionController::Parameters] params
  # @return [Hash] response
  # @raise Exceptions::IpLookupFailedError
  def self.ip_lookup(params)
    ip = params[:ip]
    inst_code = IpService.search ip.to_s
    inst = Institution.find_by_code inst_code


    raise Exceptions::IpLookupFailedError.new, "No inst for #{ip}" unless inst

    successful_response inst, auth_type: :ip, remote: false
  end

  # Attempt to lookup inst by OA Config values
  # @param [ActionController::Parameters] params
  # @return [Hash] response
  # @raise ActiveRecord::RecordNotFound
  def self.oa_lookup(params)
    inst = inst_by_oa_id! params[:oa_id].to_s
    successful_response(
      inst,
      auth_type: :oa, remote: ip_different_from_inst?(inst, params[:ip].to_s)
    )
  end

  # Attempt to lookup inst by Zip Code for Geo lookup
  # @param [ActionController::Parameters] params
  # @return [Hash] response
  # @raise ActiveRecord::RecordNotFound
  def self.geo_lookup(params)
    inst = Institution.where('? = ANY (zip_codes)', params[:zip]).first
    raise ActiveRecord::RecordNotFound unless inst

    successful_response inst, auth_type: :geo, remote: true
  end

  # Attempt to lookup inst by PINES Code
  # @param [ActionController::Parameters] params
  # @return [Hash] response
  # @raise ActiveRecord::RecordNotFound
  def self.pines_lookup(params)
    # first try to lookup full code...
    inst = Institution.where('? = ANY (pines_codes)', params[:pines]).first
    return successful_response inst, auth_type: :pines, remote: true if inst

    if params[:pines] =~ /([^-]+)-/
      inst = Institution.where('? = ANY (pines_codes)', Regexp.last_match[1]).first
      successful_response inst, auth_type: :pines, remote: true
    else
      raise ActiveRecord::RecordNotFound
    end
  end

  # Determine auth type based on provided parameter values
  # @param [ActionController::Parameters] params
  # @return [Symbol] type
  def self.determine_auth_type_from(params)
    case params.except(:controller, :action).keys.sort
    when ['ip'] then :ip
    when ['passphrase'] then :passphrase
    when %w[ip oa_id] then :oa
    when ['zip'] then :geo
    when ['pines'] then :pines
    else
      :invalid
    end
  end

  # Determine auth type based on provided parameter values
  # @param [Institution] inst
  # @param [String] ip
  # @return [Boolean] different
  def self.ip_different_from_inst?(inst, ip)
    inst_code = IpService.search(ip)
    return true unless inst_code

    inst.code != inst_code
  end

  # Craft a successful response hash
  # @param [Institution] institution
  # @param [Hash] context
  # @return [Hash]
  def self.successful_response(institution, context = {})
    { inst: institution.response_json,
      context: context }
  end

  # Craft a unsuccessful response hash
  # @param [String] message
  # @param [Symbol] auth_type
  # @return [Hash]
  def self.unsuccessful_response(msg, auth_type = nil)
    { message: msg,
      context: { auth_type: auth_type } }
  end

  # Try and determine an institution based on an OpenAthens "ID" value
  # which may be either an Org ID or SubOrg ID
  # @param [String] oa_id
  # @return [Institution]
  # @raise ActiveRecord::RecordNotFound
  def self.inst_by_oa_id!(oa_id)
    suborg_inst = Institution.find_by(open_athens_subscope: oa_id)
    return suborg_inst if suborg_inst

    institutions = Institution.where(open_athens_scope: oa_id)
    return institutions.first if institutions.count == 1

    parent_institution = institutions.where(open_athens_subscope: nil)
    return parent_institution.first if parent_institution.count == 1

    raise ActiveRecord::RecordNotFound
  end
end