# frozen_string_literal: true

# Service to handle password generating, cycling etc.
class PasswordService
  include LegacyDatesHelper

  MAX_ATTEMPTS = 3

  attr_reader :report

  def initialize
    @report = { updated: [], changed: [], errors: [] }
  end

  def run(notifier = NotificationService.new)
    @notifier = notifier
    attempts = 0
    while attempts < MAX_ATTEMPTS
      begin
        attempts += 1
        notify
        change
        break
      rescue StandardError => e
        if attempts == MAX_ATTEMPTS
          raise e
        else
          puts "Retrying password job after exception: #{e.message}"
        end
      end
    end
    finish
  end

  def notify
    Institution.active.find_each do |inst|
      # skip if institution does not have today in notify_dates
      next unless notify_today? inst
      next if already_notified_today? inst

      # get new password object
      new_password = Password.next_available
      dates = new_password_effective_dates inst, now_mmdd, for_display: true

      # update new_password value from password object
      inst.new_password = new_password.password

      inst.password_last_notified = Time.zone.now

      # save new_password value, update Password entry, and log
      if inst.save
        @report[:updated] << "#{inst.code} has been assigned #{inst.new_password} as it's new password. It will become effective on #{dates[:on]}"
        new_password.commit!
        PasswordMailer.with(inst: inst, dates: dates)
                      .password_notification.deliver_now
      else
        error_messages = inst.errors&.map(&:message)&.join('. ')
        @report[:errors] << "#{inst.code} failed to have it's new password updated to #{new_password.password} because of #{error_messages}"
      end
    end
    @report
  end

  def change
    institutions_to_index = []
    Institution.active.find_each do |inst|
      next unless change_today? inst

      if already_changed_today? inst
        # We're retrying the script, so don't change the password again but do reindex the institution
        # For all we know, the previous attempt could have failed while indexing this institution
        institutions_to_index << inst
        next
      end

      # save old password
      inst.prev_password = inst.current_password

      if inst.new_password
        inst.current_password = inst.new_password
      else
        password = Password.next_available
        inst.current_password = password.password
      end

      dates = new_password_effective_dates inst, now_mmdd, asap: inst.change_password_asap, for_display: true

      inst.password_last_changed = Time.zone.now

      if inst.save
        institutions_to_index << inst
        @report[:changed] << "#{inst.code} has been assigned #{inst.current_password} as it's current password."
        password&.commit!
        inst.update_column :change_password_asap, false
        PasswordMailer.with(dates: dates, inst: inst)
                      .nightly_password_change.deliver_now
      else
        error_messages = inst.errors&.map(&:message).join('. ')
        @report[:errors] << "#{inst.code} failed to have it's current password changed to #{inst.current_password} because of #{error_messages}"
      end
    end
    if institutions_to_index.any?
      Sunspot.index institutions_to_index
      Sunspot.commit
    end
    @report
  end

  def finish
    PasswordMailer.with(report: report).password_action_summary.deliver_now
  end

  # Generate a new password for institution. Send notification emails.
  # @param [Institution] institution
  def new_new_password(institution)
    new_password = Password.next_available
    if institution.update(new_password: new_password.password)
      new_password.commit!
      notify_new_password institution
    else
      raise Exceptions::PasswordChangeError,
            institution.errors&.map(&:message).join('. ')
    end
  end

  # Generate a new password for institution and set it to update in the next
  # precessing job. Send notifications emails.
  # @param [Institution] institution
  def change_asap(institution)
    if institution.change_password_asap
      raise Exceptions::PasswordChangeError,
            I18n.t('app.services.password.already_updating_asap')
    end

    new_password = if institution.new_password.blank? || institution.new_password == institution.current_password
                     Password.next_available
                   else
                     Password.find_by_password institution.new_password
                   end

    if institution.update(change_password_asap: true,
                          new_password: new_password.password)

      new_password.commit!
      dates = new_password_effective_dates(
        institution, now_mmdd, asap: true,
                               for_display: true
      )
      PasswordMailer.with(inst: institution, dates: dates)
                    .password_notification.deliver_now
    else
      raise Exceptions::PasswordChangeError,
            institution.errors&.map(&:message).join('. ')
    end
  end

  # @param [Contact] contact
  def notify_current_password_contact(contact)
    PasswordMailer.with(
      inst: contact.institution,
      contact: contact,
      dates: { on: nil, until: contact.institution.next_change_date(for_display: true) }
    ).current_password.deliver_now
  end

  # @param [Institution] institution
  def notify_current_password(institution)
    PasswordMailer.with(
      inst: institution,
      dates: { on: nil, until: institution.next_change_date(for_display: true) }
    ).current_password.deliver_now
  end

  # @param [Institution] institution
  def notify_new_password(institution)
    PasswordMailer.with(
      inst: institution,
      dates: {
        on: institution.next_change_date(for_display: true),
        until: institution.next_next_change_date(for_display: true)
      }
    ).new_password.deliver_now
  end

  # Determine if an Institution should get 'change' treatment today
  # @param [Institution] institution
  # @return [Boolean]
  def change_today?(institution)
    return true if institution.change_password_asap

    if institution.notify_group
      return true if includes_today? institution.notify_group.change_dates
    elsif includes_today? institution.change_dates
      return true
    end

    false
  end

  # Determine if an Institution already got the 'change' treatment today
  # @param [Institution] institution
  # @return [Boolean]
  def already_changed_today?(institution)
    return true if institution.password_last_changed&.today?
    false
  end

  # Determine if an Institution should get the 'notify' treatment today
  # @param [Institution] institution
  # @return [Boolean]
  def notify_today?(institution)
    if institution.notify_group
      return true if includes_today? institution.notify_group.notify_dates
    elsif includes_today? institution.notify_dates
      return true
    end

    false
  end

  # Determine if an Institution already got the 'notify' treatment today
  # @param [Institution] institution
  # @return [Boolean]
  def already_notified_today?(institution)
    return true if institution.password_last_notified&.today?
    false
  end

  # get the corresponding change date from either the Institution or NotifyGroup
  # TODO: this is ugly
  # @param [Institution] inst
  # @param [String] comp_mmdd
  # @param [Boolean] asap set effective date to today
  # @return [Hash] dates in MMDD from inst or notify group
  def new_password_effective_dates(inst, comp_mmdd = now_mmdd, asap: false, for_display: false)
    source = inst.notify_group || inst
    return no_dates if source.change_dates.empty?

    if asap
      effective_on = comp_mmdd
      effective_until = first_future_date source.change_dates, comp_mmdd
      # if no future date is found, use first date in change dates array
      unless effective_until
        effective_until = source.change_dates[0]
      end
    else
      index = source.notify_dates.index comp_mmdd
      # if this is a change and not a notify, index will be nil
      effective_on = if index
                       source.change_dates[index]
                     else
                       index = source.change_dates.index now_mmdd
                       now_mmdd
                     end
      # if no next date is found, use first date in change dates array
      effective_until = if source.change_dates[index + 1]
                          source.change_dates[index + 1]
                        else
                          source.change_dates[0]
                        end
    end
    if for_display
      on_display = if effective_on < comp_mmdd
                     mmdd_display(effective_on, next_year: true)
                   else
                     mmdd_display(effective_on)
                   end
      until_display = if effective_until < comp_mmdd
                        mmdd_display(effective_until, next_year: true)
                      else
                        mmdd_display(effective_until)
                      end
      { on: on_display, until: until_display }
    else
      { on: effective_on, until: effective_until }
    end

  end

  # returns the 'next' date from an array of dates, based on a comparison date
  # @param [Object] dates
  # @param [Object] comp_mmdd
  # @return [String] next date, if found
  def first_future_date(dates, comp_mmdd)
    dates.each do |date|
      return date if comp_mmdd < date
    end
    nil
  end

  # used if there are no change dates
  def no_dates
    { on: 'N/A', until: 'FOREVER' }
  end

  # @param [Array<String>] dates (mmdd) (these can be change or notify dates), e.g., %w[0331 0630 0930 1231]
  # @param [String] this_date (yyyymmdd)
  # @return [String] next_date (yyyymmdd)
  def self.next_date(dates, this_date = now_yyyymmdd)
    return nil unless dates&.any?
    if this_date =~ /(\d\d\d\d)\d\d\d\d/
      dateline = dateline_dates(dates, $1)
      dateline.each do |date|
        return date if date > this_date
      end
    end
  end

  # @param [Array<String>] change_dates (mmdd), e.g., %w[0331 0630 0930 1231]
  # @param [String] this_date (yyyymmdd)
  # @return [String] effective_until (yyyymmdd)
  def self.effective_until_date(change_dates, this_date = now_yyyymmdd)
    next_date(change_dates, next_date(change_dates, this_date))
  end

  # the objective is to create a "number line" of dates, sufficiently long (3 years worth) to simplify the code elsewhere
  # @param [Array<String>] dates (mmdd), e.g., %w[0101 0701]
  # @param [String] start_year (yyyy)
  # @return [Array<String>] dateline (yyyymmdd), e.g., %w[20210101 20210701 20220202 20220701 20230101 20230701]
  def self.dateline_dates(dates, start_year)
    years = [start_year, (start_year.to_i+1).to_s, (start_year.to_i+2).to_s]
    dateline = []
    years.each do |year|
      dates.sort.each do |date|
        dateline << "#{year}#{date}"
      end
    end
    dateline
  end

  def self.now_yyyymmdd
    DateTime.now.strftime('%Y%m%d')
  end
end
