# frozen_string_literal: true

# Class to send logs to galileo-admin-logs
class NotificationService
  NOTIFICATION_ENVIRONMENTS = %w[production staging development dev].freeze

  def initialize
    webhook_url = Rails.application.credentials.slack_notifier_webhook[Rails.env.to_sym]
    @notifier = Slack::Notifier.new webhook_url
    @start_times = {}
  end

  def notifier
    @notifier
  end

  # Sends message to console and sends passed message to galileo-admin-logs channel
  def notify(message)
    hostname = if Rails.env == 'development'
                 " #{Socket.gethostname}"
               else
                 ''
               end
    puts message

    return unless NOTIFICATION_ENVIRONMENTS.include? Rails.env

    begin
      rc = @notifier.ping("`#{Rails.env}#{hostname}`: #{message}")
    rescue Slack::Notifier::APIError
      puts rc
      sleep(5)
    rescue StandardError => e
      puts e
      puts rc
    end
  end

  # Sends start message for given process_name and saves start time
  # @param [String] process_name
  def start(process_name)
    return "Start time for #{process_name} already exists" if @start_times[process_name]

    notify "Start: #{process_name}"
    @start_times[process_name] = Time.zone.now
  end

  # Looks up start time for process_name and sends finish message with elapsed time. Also removes saved start time.
  # @param [String] process_name
  # @param [String] additional_message
  def finish(process_name, additional_message: '')
    start_time = @start_times[process_name]
    return "No start time for #{process_name}" unless start_time

    elapsed_time = Time.zone.now - start_time
    time_message = if elapsed_time > 60
                     "`#{(elapsed_time / 60.0).round(1)}` minutes"
                   else
                     "`#{elapsed_time.round(1)}` seconds"
                   end
    notify "Finish: #{process_name} finished in #{time_message}. #{additional_message}".strip
    @start_times.delete(process_name)
  end

  def progress(process_name, processed, total)
    return unless total > 0
    if (start_time = @start_times[process_name])
      elapsed_time = Time.zone.now - start_time
      time_per_item = elapsed_time / processed
      remaining_time = (total - processed) * time_per_item
      remaining_msg = NotificationService.time_message(remaining_time)
      return sprintf("(%0.2f%% %d/%d %s remaining)", (processed.to_f/total.to_f)*100, processed, total, remaining_msg)
    else
      return sprintf("(%0.2f%% %d/%d)", (processed.to_f/total.to_f)*100, processed, total)
    end
  end

  def self.time_message(t)
    if t > 3600
      "`#{(t / 3600.0).round(1)}` hours"
    elsif t > 60
      "`#{(t / 60.0).round(1)}` minutes"
    else
      "`#{t.round(1)}` seconds"
    end
  end

end
