# frozen_string_literal: true

require 'net/ftp'
require 'aws-sdk-s3'
require 'open3'


# Class to import Production Data
class DataImportService
  ALLOWED_ENVIRONMENTS = %w[staging development dev].freeze
  LOCAL_IMPORT_PATH = File.join(Rails.root, 'tmp', 'data_import').freeze
  REMOTE_BASE_PATH = 'db_backups/galileo_admin_production'
  SQL_DUMP_FILE_NAME = 'current_db.sql.gz'

  # Starts the whole data import process
  # @return [Array<Hash<Symbol, String>>] Rows in csv file
  def self.import_all(notifier: NotificationService.new)
    notifier.start 'import_all'
    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      message = "Not allowed to run data importer in #{Rails.env}"
      raise Exceptions::DataImportError.new, message
    end

    # Load SQL Data
    sql_gz_file = retrieve_sql_dump(notifier: notifier)
    drop_all_tables notifier: notifier
    load_sql gz_file_path: sql_gz_file, notifier: notifier

    # Load Images
    rebuild_images(notifier: notifier) unless Rails.env.development?

    # Migrate
    notifier.start 'Running DB migration'
    ActiveRecord::Base.connection.migration_context.migrate
    notifier.finish 'Running DB migration'

    notifier.finish 'import_all'
  end

  def self.retrieve_sql_dump(notifier: NotificationService.new)
    retrieve(SQL_DUMP_FILE_NAME, remote_path: REMOTE_BASE_PATH, notifier: notifier)
  end

  # Retrieves the sql data dump from the ftp
  # @return [String] retrieved encrypted file path
  def self.retrieve(file_name, remote_path: REMOTE_BASE_PATH, notifier: NotificationService.new)
    notifier.start "Retrieving #{file_name}"
    initialize_path LOCAL_IMPORT_PATH
    local_file_path = File.join(LOCAL_IMPORT_PATH, file_name)

    ftp_settings = {
      debug_mode: !Rails.env.production?,
      port: 990,
      ssl: { verify_mode: OpenSSL::SSL::VERIFY_NONE,
             min_version: OpenSSL::SSL::TLS1_2_VERSION,
             max_version: OpenSSL::SSL::TLS1_2_VERSION },
      username: Rails.application.credentials.dig(:galftp, :username),
      password: Rails.application.credentials.dig(:galftp, :password)
    }
    Net::FTP.open('galftp.galib.uga.edu', ftp_settings) do |ftp|
      ftp.chdir(remote_path)
      ftp.getbinaryfile(file_name, local_file_path.to_s, 1024)
    end
    notifier.finish "Retrieving #{file_name}"
    local_file_path
  end

  def self.load_sql(gz_file_path: File.join(LOCAL_IMPORT_PATH, SQL_DUMP_FILE_NAME), notifier: NotificationService.new)
    notifier.start "load_sql dump file #{File.basename gz_file_path}"
    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      message = "Not allowed to run load_sql in #{Rails.env}"
      raise Exceptions::DataImportError.new, message
    end

    db = ActiveRecord::Base.connection_db_config.configuration_hash

    postgres_uri = "postgres://#{db[:username]}:#{db[:password]}@#{db[:host]}:#{db[:port]}/#{db[:database]}"
    postgres_uri += '?sslmode=require' unless Rails.env == 'development'

    # The sed command is just looking for stuff of the format "ALTER __ OWNER TO __;" and removing it.
    # The tables, etc will belong to the user the script is logged in as (username in the postgres_uri)
    # These SQL dumps are pretty consistent, but we made a provision for padding at the beginning or end of the line just in case.
    # The pattern is case-sensitive however (the SQL keywords need to be in caps).
    command = "gunzip -c #{gz_file_path} | sed '/^ *ALTER[^;]*OWNER TO[^;]*; *$/d' | psql '#{postgres_uri}'"

    _stdout_str, error_str, status = Open3.capture3({}, command)
    if status.success?
      notifier.finish "load_sql dump file #{File.basename gz_file_path}"
      return
    end

    message = "Unable to load sql. #{error_str}"
    raise Exceptions::DataImportError.new, message
  end

  def self.rebuild_images(notifier: NotificationService.new)
    notifier.start 'rebuild_images'
    S3Service.sync_image_bucket(notifier: notifier)
    notifier.finish 'rebuild_images'
  end

  def self.rebuild_development_image_storage(notifier: NotificationService.new)
    notifier.start 'rebuild_development_image_storage'
    unless Rails.env.development?
      message = "Not allowed to run rebuild_image_storage in #{Rails.env}"
      raise Exceptions::DataImportError.new, message
    end
    temp_path = File.join(LOCAL_IMPORT_PATH, 'images')
    initialize_path temp_path
    prod_bucket = Rails.application.credentials[:aws][:image_bucket][:production]

    S3Service.download_bucket(prod_bucket, temp_path, notifier: notifier)

    copy_images_to_storage(temp_path, notifier: notifier)
    notifier.finish 'rebuild_development_image_storage'
  end

  def self.copy_images_to_storage(temp_path, notifier: NotificationService.new)
    notifier.start 'copy_images_to_storage'
    storage_path = File.join(Rails.root, 'storage')
    initialize_path storage_path
    Dir.foreach(temp_path) do |filename|
      next if %w[. .. variants].include? filename

      target_path = File.join(storage_path, filename[0..1], filename[2..3])
      initialize_path target_path, false
      FileUtils.cp(File.join(temp_path, filename), File.join(target_path, filename))
    end

    temp_variant_path = File.join temp_path, 'variants'
    return unless  File.exist? temp_variant_path

    variant_path = File.join storage_path, 'va', 'ri'
    initialize_path variant_path
    FileUtils.cp_r(temp_variant_path, variant_path)
    notifier.finish 'copy_images_to_storage'
  end

  def self.drop_all_tables(notifier: NotificationService.new)
    notifier.start 'drop_all_tables'
    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      message = "Not allowed to run drop_all_tables in #{Rails.env}"
      raise Exceptions::DataImportError.new, message
    end
    # (SN - 09/29/21) I have omitted "if exists" from the drop command because we should have the current list. If we
    # start running into issues, we may want to add it.
    conn = ActiveRecord::Base.connection
    conn.tables.each { |t| conn.execute("DROP TABLE #{t} CASCADE") }
    notifier.finish 'drop_all_tables'
  end

  # Empties path if it exists, otherwise creates path
  # @param [String] path Path to be initialized
  # @param [String] destructive When true, will remove all files in the given path
  def self.initialize_path(path, destructive = true)
    if File.exist?(path)
      FileUtils.rm_rf(Dir["#{path}/*"]) if destructive
    else
      FileUtils.mkdir_p(path)
    end
  end
end
