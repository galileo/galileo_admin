# frozen_string_literal: true

require 'csv'

# Class to import CSV data
class CsvImporter
  attr_reader :csv_hash, :model_type, :created_models, :updated_models

  # region class_methods
  def self.allowed_types
    [
      %w[Allocation Allocation],
      %w[BentoConfig BentoConfig],
      %w[Contact Contact],
      %w[Institution Institution],
      %w[Resource Resource],
      %w[Site Site]]
  end

  def self.allowed_type_codes
    allowed_types.map(&:second)
  end

  # Parses csv_file with correct encoding and removes headers
  # @param [String] csv_file
  # @return [Array<Hash<Symbol, String>>>] Array of row hashes.
  def self.parse_hash(csv_file)
    csv_rows, csv_headers = parse_array(csv_file)
    rows = []
    csv_rows.each do |row|
      hash = {}
      row.each_with_index do |val, index|
        hash[csv_headers[index].to_sym] = val
      end
      rows << hash
    end
    rows
  end

  # Parses csv_file with correct encoding
  # @param [String] csv_file
  # @return [Array<Array<String>>, Array<String>] Array of csv rows and Array of
  # headers
  def self.parse_array(csv_file)
    array = CSV.read(csv_file)
    headers = array.shift
    if headers.any?(&:blank?)
      raise Exceptions::CsvImporterError.new, "All Header values must have at least one character. Headers: #{headers}"
    end

    rows = array.map { |row| row[0..(headers.length - 1)] }
    [rows, headers]
  end
  # endregion

  #region public_methods
  def initialize(model_type, csv_file)
    raise ArgumentError, "#{model_type} not supported" unless CsvImporter.allowed_type_codes.include? model_type

    @csv_hash = CsvImporter.parse_hash csv_file
    @model_type = model_type.constantize

    @created_models = []
    @updated_models = []
  end

  # Returns the union of field names in csv file with the column names on the active record model.
  # Need to do this because
  def field_names
    @csv_hash.first.keys & @model_type.column_names.map(&:to_sym)
  end

  # Starts the CSV import
  def import
    @csv_hash.each do |row|
      row = prepare_fields(row)

      target = find_target_model(row)
      if target
        if model_type == BentoConfig && row.include?(:credentials)
          # Allow the CSV to contain just a subset of the keys in credentials
          row[:credentials] = (target.credentials || {}).merge(row[:credentials])
        end
        target.update! row
        @updated_models << target
      else
        target = @model_type.create! row
        @created_models << target
      end
    end
  end
  # endregion

  #region private_methods
  private

  def find_target_model(hash)
    if @model_type == Allocation
      Allocation.where(institution_id: hash[:institution_id], resource_id: hash[:resource_id]).first
    elsif @model_type == BentoConfig
      if hash[:name].present?
        BentoConfig.where(institution_id: hash[:institution_id], name: hash[:name]).first
      else
        BentoConfig.where(institution_id: hash[:institution_id], service: hash[:service], view_types: hash[:view_types]).first
      end
    elsif @model_type == Contact
      Contact.where(institution_id: hash[:institution_id], email: hash[:email]).first
    else
      @model_type.find_by_code hash[:code]
    end
  end

  def prepare_fields(hash)
    prepared_fields = if @model_type == Allocation
                        prepare_allocation_fields(hash)
                      elsif @model_type == BentoConfig
                        prepare_bento_config_fields(hash)
                      elsif @model_type == Contact
                        prepare_contact_fields(hash)
                      elsif @model_type == Site
                        prepare_site_fields(hash)
                      elsif @model_type == Institution
                        prepare_institution_fields(hash)
                      else
                        hash
                      end
    prepared_fields.except :updated_at, :created_at
  end

  # Returns a hash that can be used to create/update an allocation from the csv hash
  def prepare_allocation_fields(hash)
    return hash unless @model_type == Allocation

    filtered_hash = hash.deep_dup
    if hash[:institution_code].nil?
      raise Exceptions::CsvImporterError.new, "Allocation import: institution_code missing. Row:#{hash}"
    end

    inst = Institution.find_by_code(hash[:institution_code])
    raise Exceptions::CsvImporterError.new, "Institution with code #{hash[:institution_code]} not found" if inst.nil?

    raise Exceptions::CsvImporterError.new, "Institution not active. Institution code: #{inst.code} " unless inst.active?

    raise Exceptions::CsvImporterError.new, "Allocation import: resource_code missing. Row:#{hash}" if hash[:resource_code].nil?

    res = Resource.find_by_code(hash[:resource_code])
    raise Exceptions::CsvImporterError.new, "Resource with code #{hash[:resource_code]} not found" if res.nil?
    raise Exceptions::CsvImporterError.new, "Resource not active. Resource code: #{res.code}" unless res.active?

    if hash.has_key? :name_override
      filtered_hash[:name] = hash[:name_override].present? ? hash[:name_override] : ''
    end

    if hash.has_key? :branding_override
      begin
      filtered_hash[:branding_id] = if hash[:branding_override].present?
                                      inst.brandings.find_by_name!(hash[:branding_override]).id
                                    else
                                      nil
                                    end
      rescue ActiveRecord::RecordNotFound => _e
        raise Exceptions::CsvImporterError.new, "Branding with name '#{hash[:branding_override]}' not found"
      end
    end

    filtered_hash[:institution_id] = inst.id
    filtered_hash[:resource_id] = res.id

    filtered_hash.except :institution_code, :resource_code, :institution_name, :institution_active?,
                         :resource_name, :resource_active?, :vendor_name, :institution_group_code,
                         :glri?, :public?, :name_override, :branding_override
  end

  # Returns a hash that can be used to create/update a bento config from the csv hash
  def prepare_bento_config_fields(hash)
    return hash unless @model_type == BentoConfig

    filtered_hash = hash.deep_dup
    if hash[:institution_code].nil?
      raise Exceptions::CsvImporterError.new, "BentoConfig import: institution_code column missing. Row:#{hash}"
    end

    inst = Institution.find_by_code(hash[:institution_code])
    raise Exceptions::CsvImporterError.new, "Institution with code #{hash[:institution_code]} not found" if inst.nil?

    filtered_hash[:institution_id] = inst.id
    filtered_hash[:view_types] = hash[:view_types]&.split(/\s+/)
    filtered_hash.except! :institution_code # remove these fields since not part of BentoConfig

    json_subfields = filtered_hash.keys - BentoConfig.column_names.map(&:to_sym)
    credentials = filtered_hash.extract!(*json_subfields).filter {|k, v| v&.strip.present?}.stringify_keys
    credentials.slice! *BentoService.all_configurable_fields
    filtered_hash[:credentials] = credentials if credentials.any?

    filtered_hash
  end

  # Returns a hash that can be used to create/update a contact from the csv hash
  def prepare_contact_fields(hash)
    return hash unless @model_type == Contact

    filtered_hash = hash.deep_dup
    if hash[:institution_code].nil?
      raise Exceptions::CsvImporterError.new, "Contact import: institution_code column missing. Row:#{hash}"
    end

    inst = Institution.find_by_code(hash[:institution_code])
    raise Exceptions::CsvImporterError.new, "Institution with code #{hash[:institution_code]} not found" if inst.nil?
    filtered_hash[:institution_id] = inst.id

    if hash[:email].nil?
      raise Exceptions::CsvImporterError.new, "Contact import: email column missing. Row:#{hash}"
    end

    if hash[:types]
      filtered_hash[:types] = JSON.parse hash[:types]
    end

    filtered_hash.except :institution_code # remove these fields since not part of BentoConfig
  end

  # Returns a hash that can be used to create/update a site from the csv hash
  def prepare_site_fields(hash)
    return hash unless @model_type == Site

    filtered_hash = hash.deep_dup
    if hash[:institution_code].nil?
      raise Exceptions::CsvImporterError.new, "Site import: institution_code column missing. Row:#{hash}"
    end

    inst = Institution.find_by_code(hash[:institution_code])
    raise Exceptions::CsvImporterError.new, "Institution with code #{hash[:institution_code]} not found" if inst.nil?
    filtered_hash[:institution_id] = inst.id

    if hash[:code].nil?
      raise Exceptions::CsvImporterError.new, "Site import: code column missing. Row:#{hash}"
    end

    filtered_hash.except :institution_code, :institution_name # remove these fields since not part of BentoConfig
  end

  # Returns a hash that can be used to create/update a institution from the csv hash
  # It converts strings that represent arrays into actual arrays
  # These strings are created when reports are generated
  def prepare_institution_fields(hash)
    return hash unless @model_type == Institution

    filtered_hash = hash.deep_dup

    filtered_hash.each do |key, value|
      if Institution.columns_hash[key.to_s].array?
        begin
          filtered_hash[key] = JSON.parse( value || "[]" )  # accepts blank cells
        rescue
          raise Exceptions::CsvImporterError.new, "Value for #{key} (#{value}) doesn't look like a valid array. Row #{hash}"
        end
      end
    end

    filtered_hash
  end
  # endregion
end
