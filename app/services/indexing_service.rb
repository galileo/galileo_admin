# frozen_string_literal: true

# Service that will wrap various index-related functionality
# TODO: these tasks should run async with Resque
class IndexingService
  # @param [Collection, Object] things to remove
  def self.deindex(things)
    Sunspot.remove things
  end

  # @param [Collection, Object] things
  def self.reindex(things)
    Sunspot.index things
  end

  def self.commit
    Sunspot.commit_if_dirty
  end
end
