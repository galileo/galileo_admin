# frozen_string_literal: true

# Represents a bento configuration for an institution
class ConfiguredBento < ApplicationRecord

  BULK_SOLR_OPERATION_RETRY_COUNT = 3

  # region includes
  include PgSearch::Model
  # endregion

  # region associations
  belongs_to :institution, inverse_of: :configured_bentos
  belongs_to :bento_config, inverse_of: :configured_bentos, optional: true
  belongs_to :predefined_bento, inverse_of: :configured_bentos
  belongs_to :template_view_bento, optional: true
  # endregion

  # region callbacks
  before_validation :update_slug, if: :slug_might_change?
  before_validation :set_service_credentials_if_default
  before_create :verify_template_view_bento
  # endregion

  # region validations
  validates :institution_id, presence: true
  validates :predefined_bento_id, presence: true
  validates :user_view, presence: true
  validates :slug, presence: true
  validates_length_of :display_name, maximum: PredefinedBento::DISPLAY_NAME_MAX_LEN
  validates_length_of :description, maximum: PredefinedBento::DESCRIPTION_MAX_LEN
  validate :validate_user_view
  validate :validate_service_matches
  validate :validate_credentials_name
  validate :validate_service_prereqs, if: :service_relevant_fields_changed?
  validates_uniqueness_of :slug, scope: %i[institution_id user_view]
  # endregion

  # region paper_trail
  has_paper_trail(
    versions: {
      class_name: 'BentoVersion'
    },
    meta: {
      item_name: :display_title
    }
  )
  # endregion

  # region solr index
  searchable include: [:institution, :predefined_bento] do
    string(:class, stored: true) { self.class }
    string(:display_name, stored: true) { computed_display_name }
    string(:description, stored: true) { computed_description }
    string(:user_view, stored: true)
    string(:slug, stored: true)
    double(:order, stored: true)
    boolean(:active, stored: true)
    boolean(:shown_by_default, stored: true)
    string(:inst_code, stored: true) { institution.code }
    string(:service, stored: true) { predefined_bento.service }
    string(:no_results_message, stored: true) { predefined_bento.no_results_message }
    string(:code, stored: true) { predefined_bento.code }
    string(:customizations, stored: true) { merge_customizations }
    string(:bento_config_id, stored: true) { "BentoConfig #{bento_config_id}" }
  end



  # endregion

  #region filterrific
  filterrific(
    default_filter_params: { sorted_by: 'order_asc' },
    available_filters: %i[sorted_by text_search with_user_view shown_active_state shown_default_state with_bento_config
                          with_predefined_bento created_by_state]
  )
  # endregion

  # region scopes
  scope :for_indexing, -> { includes(:institution, :predefined_bento) }
  pg_search_scope :search_for,
                  against: %i[slug display_name description],
                  associated_against: {
                    predefined_bento: %i[display_name description admin_note]
                  },
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  scope :text_search, ->(query) { search_for query }

  scope :sorted_by, lambda { |sort_option|
    with_pb = joins :predefined_bento
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at/   then order "configured_bentos.created_at #{direction}"
    when /^updated_at/   then order "configured_bentos.updated_at #{direction}"
    when /^slug/         then order "configured_bentos.slug #{direction}"
    when /^user_view/    then order "configured_bentos.user_view #{direction}"
    when /^display_name/ then with_pb.order Arel.sql("COALESCE(NULLIF(configured_bentos.display_name, ''), predefined_bentos.display_name) #{direction}")
    when /^active/ then order "configured_bentos.active #{direction}"
    when /^shown_by_default/ then order "configured_bentos.shown_by_default #{direction}"
    when /^order/ then order "configured_bentos.order #{direction}"
    when /^predefined_bento_code/ then with_pb.order "predefined_bentos.code #{direction}"

    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :with_user_view, lambda { |user_view|
    next unless user_view

    where(user_view: user_view)
  }

  scope :shown_active_state, lambda { |state|
    next if state.blank?

    where(active: state)
  }

  scope :shown_default_state, lambda { |state|
    next if state.blank?

    where(shown_by_default: state)
  }

  scope :with_bento_config, lambda { |bento_config_id|
    where(bento_config_id: bento_config_id)
  }

  scope :with_predefined_bento, lambda { |predefined_bento_id|
    where(predefined_bento_id: predefined_bento_id)
  }

  scope :created_by_state, lambda { |managed_by_state|
    case managed_by_state
    when 'galileo' then where.not(template_view_bento_id: nil)
    when 'institution' then where(template_view_bento_id: nil)
    when 'any' then next
    else
      raise(ArgumentError, "Invalid Created By option: #{managed_by_state}")
    end
  }

  scope :with_template, ->(template_view_bento_id) { where(template_view_bento_id: template_view_bento_id) }

  scope :institution_ordered, -> { includes(:institution).order('institutions.code asc') }

  scope :default_sort, -> { joins(:predefined_bento).order(Arel.sql(
    "\"order\", COALESCE(NULLIF(configured_bentos.display_name, ''), predefined_bentos.display_name)"
  )) }
  # endregion

  # region class methods
  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.shown_active_options
    [
      %w[True true],
      %w[False false]
    ]
  end

  def self.shown_default_options
    [
      %w[True true],
      %w[False false]
    ]
  end

  # @param configured_bentos [Array<ConfiguredBento>]
  def self.bulk_insert(configured_bentos, send_to_solr: true)
    return nil if configured_bentos&.empty?

    # Set timestamps because bulk operations don't do it automatically
    cb_hashes = configured_bentos.map do |cb|
      cb.created_at = cb.updated_at = DateTime.now
      cb.update_slug
      cb.attributes.except('id')
    end

    created = ConfiguredBento.insert_all cb_hashes
    if send_to_solr
      ids = created.rows.flatten
      send_bulk_insert_to_solr ids
    end
    created
  end

  # @param configured_bentos [ActiveRecord::Relation<ConfiguredBento>]
  def self.bulk_destroy(configured_bentos)
    send_bulk_delete_to_solr configured_bentos.pluck(:id)
    ConfiguredBento.delete configured_bentos
  end

  def self.send_bulk_insert_to_solr(ids, attempt=1)
    Sunspot.index! ConfiguredBento.for_indexing.where(id: ids)
  rescue RSolr::Error::ConnectionRefused => e
    notifier = NotificationService.new
    notifier.notify("Error on Solr bulk insert for ConfiguredBentos with Ids: #{ids.join(', ')}.\n" +
                    "Attempt #{attempt}/#{BULK_SOLR_OPERATION_RETRY_COUNT}\n\n#{e.backtrace}")
    send_bulk_insert_to_solr(ids, attempt + 1) if attempt < BULK_SOLR_OPERATION_RETRY_COUNT
  end

  def self.send_bulk_delete_to_solr(ids, attempt=1)
    Sunspot.remove_by_id! ConfiguredBento, ids
  rescue RSolr::Error::ConnectionRefused => e
    notifier = NotificationService.new
    notifier.notify("Error on Solr bulk delete for ConfiguredBentos with Ids: #{ids.join(', ')}.\n" +
                    "Attempt #{attempt}/#{BULK_SOLR_OPERATION_RETRY_COUNT}\n\n#{e.backtrace}")
    send_bulk_delete_to_solr(ids, attempt + 1) if attempt < BULK_SOLR_OPERATION_RETRY_COUNT
  end

  def self.supported_customizations(pb_code)
    case pb_code
    when 'eds_custom'
      return PredefinedBento.supported_params_by_service 'eds_api'
    when 'eds_resource'
      return { 'Content Provider Facet': [] }
    else
      []
    end
  end

  def self.created_by_states
    [
      %w[Any any],
      %w[GALILEO galileo],
      %w[Institution institution]
    ]
  end

  def self.slug_cache_for_view(institution_id, user_view)
    cache = {}
    ConfiguredBento.where(institution_id: institution_id, user_view: user_view).each do |cb|
      cache[cb.slug] = cb.id
    end
    cache
  end
  # endregion

  # region public methods
  # Returns a Sunspot Hit object for the allocation that represents the current values in SOLR
  def solr_hit
    search = ConfiguredBento.search do
      with(:slug, slug)
      with(:inst_code, institution.code)
      with(:user_view, user_view)
    end

    return nil if search.total < 1

    search.hits.first
  end

  def solr_stored_hash(hit = solr_hit)

    return {} if hit.nil?

    hit.instance_variable_get(:@stored_values)
  end

  def merge_customizations
    custom = customizations.clone
    if custom&.include?('Database Name') || custom&.include?('Catalog Name')
      custom['Content Provider Facet'] = [custom['Database Name'] || custom['Catalog Name']]
    end
    if predefined_bento_code == 'eds_resource'
      custom['Hide Library Catalog Only'] = 'true'
      custom['Hide Peer Reviewed Journals'] = 'true'
    end
    (predefined_bento.definition || {}).merge(custom || {}).to_json
  end

  def computed_display_name
    display_name.present? ? display_name : predefined_bento.display_name
  end

  def computed_description
    description.present? ? description : predefined_bento.description
  end

  def display_user_view
    return template_view_bento.view.name if template_view_bento

    UserView.find_by_code(user_view)&.name
  end

  def default_slug
    display_name.present? ? display_name.strip.downcase.gsub(/\W+/, '-') : predefined_bento_code
  end

  attr_accessor :slug_cache

  def slug_unique?(test_slug)
    if slug_cache.nil?
      ConfiguredBento.where(institution_id: institution_id, user_view: user_view, slug: test_slug)
                     .where.not(id: id).none?
    else
      slug_match_id = slug_cache[test_slug]
      slug_match_id.nil? || (slug_match_id == id)
    end
  end

  def unique_slug
    slug = default_slug
    i = 0
    loop do
      break if slug_unique?(slug)
      i += 1
      slug = "#{default_slug}-#{i}"
    end
    slug
  end

  def slug_might_change?
    slug.nil? || display_name_changed? || (display_name.nil? && predefined_bento_id_changed?)
  end

  def update_slug
    new_slug = unique_slug
    if slug_cache.present?
      slug_cache.delete self.slug
      slug_cache[new_slug] = id || true
    end
    self.slug = new_slug
  end

  def set_service_credentials_if_default
    return if bento_config.present?
    credentials_name = predefined_bento&.service_credentials_name
    return unless credentials_name.present?
    self.bento_config = BentoConfig.find_by institution_id: institution_id, name: credentials_name
  end

  def created_by_galileo?
    !!template_view_bento
  end

  def created_by_institution?
    !template_view_bento
  end

  def created_by
    template_view_bento ? 'GALILEO' : 'Institution'
  end

  def predefined_bento_code
    predefined_bento&.code
  end

  def predefined_bento_code=(code)
    self.predefined_bento_id = PredefinedBento.find_by(code: code)&.id
  end

  def service_and_prereqs_match?
    service_matches? && credentials_name_matches? && meets_service_prereqs?
  end

  def missing_service_prereqs
    return [] if predefined_bento.nil? # ...not that you don't have other problems
    predefined_bento.service_field_prerequisites.reject { |prereq| bento_config.credentials[prereq].present? }
  end

  def meets_service_prereqs?
    missing_service_prereqs.none?
  end

  def service
    BentoService.get predefined_bento&.service
  end

  def service_matches?
    if service&.requires_config?
      bento_config&.service == predefined_bento&.service
    else
      # Service doesn't require config, so there better not be one
      bento_config.nil?
    end
  end

  def credentials_name_matches?
    credentials_name = predefined_bento&.service_credentials_name
    if credentials_name.blank?
      true
    else
      credentials_name == bento_config&.name
    end
  end

  def display_title
    "#{institution.code}-#{user_view}-#{slug}"
  end

  def galileo_search_url
    "#{Rails.configuration.galileo_search_url}/#{institution.code}/bentos/#{slug}"
  end
  # endregion

  # region private methods
  private

  def validate_user_view
    allowed_codes = UserView.for_inst_type(institution&.inst_type).pluck(:code)
    unless allowed_codes.include? user_view
      errors.add(:user_view, "'#{user_view}' is not a supported user view. Allowed user views: #{allowed_codes.join(', ')}")
    end
  end

  def service_relevant_fields_changed?
    new_record? || predefined_bento_id_changed? || bento_config_id_changed?
  end

  def validate_service_prereqs
    missing_service_prereqs.each do |prereq|
      errors.add(:predefined_bento,
                 "'#{predefined_bento&.code}' requires #{service&.display_name} credentials to include '#{prereq}'")
    end
  end

  def validate_credentials_name
    return if credentials_name_matches?
    errors.add(:predefined_bento,
               "'#{predefined_bento&.code}' specifically requires service credentials named '#{predefined_bento&.service_credentials_name}'")
  end

  def validate_service_matches
    return if service_matches?

    if service.requires_config?
      errors.add(:predefined_bento,
                 "'#{predefined_bento&.code}' requires credentials for service '#{predefined_bento&.service}'")
    else
      errors.add(:predefined_bento,
                 "'#{predefined_bento&.code}' does not match credentials for service '#{bento_config&.service}'")
    end
  end

  def verify_template_view_bento
    return if template_view_bento_id.nil?

    return if TemplateViewBento.find_by(id: template_view_bento_id)

    self.template_view_bento_id = nil
  end
  # endregion
end
