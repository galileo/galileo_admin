# frozen_string_literal: true

# Represent a featured thing on GALILEO Search homepage
class Feature < ApplicationRecord
  # region relationships
  belongs_to :featuring, polymorphic: true
  # endregion

  # region constants
  FEATURE_DESC_LENGTH_RANGE = (10..350).freeze
  FEATURE_COUNT = 6
  # endregion

  # region gem_methods
  strip_attributes only: :link_url
  # endregion

  # region validators
  validates_presence_of :link_label, :link_url
  validates :link_description, length: FEATURE_DESC_LENGTH_RANGE,
                               allow_blank: true
  validates_inclusion_of :view_type,
                         in: :view_type_codes,
                         allow_blank: false
  validates :position, presence: true, inclusion: 1..FEATURE_COUNT
  validates_uniqueness_of :position, scope: %i[featuring_id featuring_type view_type]
  validates_with ImageValidator
  # endregion

  # region callbacks
  # This has to come before the has_one_attached so that image data is uploaded and ready prior to reindexing.
  # This can be removed once the Feature Solr Table is stable
  after_commit :reindex_featuring
  after_commit do
    unless self.destroyed?
      # preload thumbnail variant
      if image.attached?
        process_thumbnail
        Sunspot.index! self
      end
    end
  end
  has_one_attached :image
  attribute :remove_image, :boolean, default: false
  after_update :purge_image, if: :should_purge_image?
  # endregion

  # region solr_fields
  searchable include: [:featuring] do
    string(:class, stored: true) { self.class }
    string :id, stored: true
    string :featuring_type, stored: true
    string :featuring_id, stored: true
    string :featuring_code, stored: true
    string :view_type, stored: true
    string :position, stored: true
    string :link_url, stored: true
    string :link_label, stored: true
    string :link_description, stored: true
    string :thumbnail_key, stored: true
  end
  # endregion

  # region scopes
  scope :for_indexing, lambda {}
  # endregion

  # region public_methods
  # Return Image variant for display in GALILEO Search
  # @return [ActiveStorage::Variant]
  def thumbnail
    return unless image.attached?

    image.variant(resize_and_pad: [328, 164, { background: [255] }])
  end

  def thumbnail_key
    thumbnail&.key
  end

  def process_thumbnail
    thumbnail.processed if image.attached?
  end

  # All view type codes
  # @return [Array]
  def view_type_codes
    ViewService.view_type_codes
  end

  # For indexing
  # @return [Array<String (frozen)>]
  def to_a
    [link_label, link_url, link_description, thumbnail_key]
  end

  # Returns a Sunspot Hit object for the allocation that represents the current values in SOLR
  def solr_hit
    search = Feature.search { with(:id, id) }

    return nil if search.total < 1

    search.hits.first
  end

  def solr_stored_hash
    hit = solr_hit

    return {} if hit.nil?

    hit.instance_variable_get(:@stored_values)
  end

  def featuring_code
    featuring&.code
  end
  # endregion

  # region private_methods
  private

  def reindex_featuring
    featuring.reindex
  end

  def should_purge_image?
    image.attached? && remove_image
  end

  def purge_image
    image.purge
  end
  # endregion
end
