# frozen_string_literal: true

# Represents an Stat

module Stats
  class StatsRecord < ApplicationRecord
    self.abstract_class = true
    connects_to database: { writing: :stats, reading: :stats }

    #region constants
    ALLOWED_STATS_TYPES = %w[login link search].freeze
    #endregion

    #region class_methods
    def self.institution_options
      options_for(Institution.all.sort_by(&:name), :name_with_code, attribute: 'code')
    end

    def self.resource_options
      options_for(Resource.all.sort_by(&:name), :name_with_code, attribute: 'code')
    end
    # endregion
  end
end
