module Stats
  class Raw < StatsRecord
    # region attributes
    self.abstract_class = true
    # endregion

    # region constants
    INST_CODE_REGX = %r[\A[A-Z0-9]{4}\z]
    BATCH_LINES = 1000
    STAT_TYPES = %w(login link search)
    LOCAL_BASE_PATH = File.join(Rails.root, 'tmp', 'stats_import').freeze
    REMOTE_BASE_PATH = 'production'.freeze
    # endregion

    # region scopes
    scope :with_institutions, lambda { |*institution_codes|
      codes = institution_codes.flatten.reject(&:blank?)

      next if codes&.empty?

      where(institution_code: codes)
    }

    scope :with_begin_time_gte, lambda { |date_string|
      date = Date.parse date_string
      where(begin_time: date.beginning_of_day..)
    }

    scope :with_begin_time_lte, lambda { |date_string|
      date = Date.parse date_string
      where(begin_time: ..date.end_of_day)
    }

    scope :with_servers, lambda { |*servers|
      codes = servers.flatten.reject(&:blank?)

      next if codes&.empty?

      where(servers: codes)
    }

    scope :with_yyyymmdd, lambda { |yyyymmdd|
      date = Date.parse yyyymmdd
      where(begin_time: date.beginning_of_day..date.end_of_day)
    }

    scope :with_date_range, lambda { |start_date, end_date|
      where(begin_time: start_date.beginning_of_day..end_date.end_of_day)
    }
    # endregion

    # region class_methods
    def self.record(_line, _server)
      raise NoMethodError("Override this implementation")
    end

    def self.import_file_prefix
      raise NoMethodError("Override this implementation")
    end

    #region public methods
    # @param [Date] start_date
    # @param [Date] end_date
    # @param [NotificationService] notifier
    def self.import(start_date, end_date, local_base_path: LOCAL_BASE_PATH, remote_path: REMOTE_BASE_PATH, notifier: NotificationService.new)
      process = "`Importing #{self.name} #{start_date} - #{end_date}`"
      notifier.start process
      validate_import start_date, end_date

      local_file_path = File.join(local_base_path, self.name)
      totals = { line_count: 0, prev_count: self.count }

      (start_date..end_date).each do |current_date|
        yyyymmdd = current_date.strftime('%Y%m%d')
        stats_files = StatsImportService.retrieve("#{self.import_file_prefix}.stats.D#{yyyymmdd}*", remote_path: remote_path, local_path: local_file_path, notifier: notifier)
        counts = self.load_stats(yyyymmdd, stats_files, notifier: notifier)
        notifier.notify "#{process}: #{load_status(counts[:prev_count], counts[:line_count], counts[:new_count])}"
        totals[:line_count] += counts[:line_count]
      end

      totals[:new_count] = self.count
      totals[:total_count] = totals[:line_count] + totals[:prev_count]
      notifier.finish process
      totals
    end

    def self.validate_import(start_date, end_date)
      unless start_date <= end_date
        raise Exceptions::StatImportError.new, "start_date must be less than or equal to end_date. start_date: #{start_date} end_date: #{end_date}."
      end

      unless end_date < Date.today
        raise Exceptions::StatImportError.new, "end_date must be yesterday or earlier. end_date: #{end_date}."
      end

      existing_dates = self.with_date_range(start_date, end_date).order(:begin_time).limit(5)
      unless existing_dates.empty?
        raise Exceptions::StatImportError.new "#{self.name} already has rows loaded starting at date #{existing_dates.first}."
      end
    end

    # @param [String] yyyymmdd date
    # @param [Array<String>] files
    # @param [Boolean] validate
    # @param [NotificationService] notifier
    def self.load_stats(yyyymmdd, files, validate: false, notifier: NotificationService.new)
      process = "Load #{self.class.name}"
      notifier.start process
      raise Exceptions::RawStatLoadError.new, "Invalid date (format: yyyymmdd). date provided: #{yyyymmdd}." unless StatsImportService.valid_yyyymmdd?(yyyymmdd)
      raise Exceptions::RawStatLoadError.new, "Rows with date #{yyyymmdd} already loaded" if self.with_yyyymmdd(yyyymmdd).any?

      prev_count = self.count
      line_count, processed = 0, 0
      total = files.count
      files.each do |file|
        (basename, server, file_date) = StatsImportService.file_info(file)
        raise Exceptions::RawStatLoadError.new, "date argument and date on file don't match. date: #{yyyymmdd}, file: #{file}" unless yyyymmdd == file_date

        processed += 1
        lines = File.readlines(file, chomp: true)
        _column_headers = lines.shift #skip first line
        line_count += lines.size
        lines.each_slice(BATCH_LINES) do |group|
          records = []
          group.each do |line|
            next unless line.present?
            record = self.record(line, server)
            if record.present?
              raise Exceptions::RawStatLoadError.new, "date argument and date on row does not match. date: #{yyyymmdd}, line: #{line}" unless yyyymmdd == record[:begin_time].strftime('%Y%m%d')

              records << record
            else
              notifier.notify "#{process}: SKIP LINE (corrupt?): #{basename} `#{line}`"
            end
          end
          if validate # SN 20241024 No validators currently but future proofing
            records.compact.each_slice(1000) do |batch|
              batch.each do |record|
                self.create!(record)
              end
            end
          else
            self.insert_all(records.compact)
          end
        end
        notifier.notify "#{process}: #{basename} #{lines.size} lines #{notifier.progress(process, processed, total)}"
      end
      notifier.notify "#{process}: #{line_count} total lines"
      new_count = self.count
      counts = {
        line_count: line_count,
        prev_count: prev_count,
        new_count: new_count
      }
      notifier.finish process
      counts
    end

    def self.load_status(prev_count, line_count, new_count)
      if line_count == 0
        "BAD? load, no lines loaded"
      elsif prev_count + line_count == new_count
        "OKAY load (#{prev_count} + #{line_count} == #{new_count})"
      else
        diff = prev_count + line_count - new_count
        "BAD? load (#{prev_count} + #{line_count} != #{new_count} difference: #{diff})"
      end
    end

    def self.sort_options
      [
        ['Recently Created', 'created_at_desc'],
        ['Recently Updated', 'updated_at_desc']
      ]
    end

    def self.server_options
      self.all.pluck(:server).uniq.sort.map{|server| [server, server]}
    end

    def self.allowed_types
      [
        %w[Login login],
        %w[Link link],
        %w[Search search]
      ]
    end
    # endregion
  end
end
