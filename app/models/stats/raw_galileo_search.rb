module Stats
  class RawGalileoSearch < Raw
    # region includes
    include PgSearch::Model
    # endregion

    # region attributes
    self.table_name = 'raw_galileo_search'
    # endregion

    # region constants
    QUERY_TYPES = %w[bento discover resource].freeze
    # endregion

    # region scopes
    scope :text_search, ->(query) { search_for query }

    scope :sorted_by, lambda { |sort_option|
      direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at/ then order "raw_galileo_search.created_at #{direction}"
      when /^updated_at/ then order "raw_galileo_search.updated_at #{direction}"
      when /^server/ then order "raw_galileo_search.server #{direction}"
      when /^institution_code/ then order "raw_galileo_search.institution_code #{direction}"
      when /^site_code/ then order "raw_galileo_search.site_code #{direction}"
      when /^begin_time/ then order "raw_galileo_search.begin_time #{direction}"
      when /^query_type/ then order "raw_galileo_search.query_type #{direction}"
      when /^query/ then order "raw_galileo_search.query #{direction}"
      else
        raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
      end
    }

    scope :with_sites, lambda { |*site_codes|
      codes = site_codes.flatten.reject(&:blank?)

      next if codes&.empty?

      where(site_code: codes)
    }

    scope :with_query_types, lambda { |*query_types|
      types = query_types.flatten.reject(&:blank?)

      next if types&.empty?

      where(query_type: types)
    }
    # endregion

    # region pg_search and filterrific
    pg_search_scope :search_for,
                    against: %i[institution_code site_code server query],
                    using: {
                      tsearch: {
                        dictionary: 'english',
                        prefix: true
                      }
                    }

    filterrific(
      default_filter_params: { sorted_by: 'created_at_desc' },
      available_filters:
        %i[sorted_by text_search with_institutions with_begin_time_gte with_begin_time_lte with_sites with_servers
           with_query_types]
    )
    # endregion

    # region class_methods
    def self.daily_class
      Stats::DailyGalileoSearch
    end

    def self.monthly_class
      Stats::MonthlyGalileoSearch
    end

    def self.record(line, server)
      (begin_time, institution_code, site_code, query_type, query) = line.split(/,/)
      return unless QUERY_TYPES.include? query_type

      {
        begin_time: Time.zone.parse(begin_time),
        server: server,
        institution_code: institution_code.downcase,
        site_code: site_code,
        query_type: query_type,
        query: (query || '').gsub(/\0/, ''),
      }
    end

    def self.import_file_prefix
      'search'
    end

    def self.query_type_options
      self.all.pluck(:query_type).uniq.sort.map{|query_type| [query_type, query_type]}
    end
    # endregion
  end
end

