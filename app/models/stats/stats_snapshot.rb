module Stats
  class StatsSnapshot < StatsRecord

    #region relationships
    belongs_to :institution, inverse_of: :stats_snapshots
    belongs_to :resource, inverse_of: :stats_snapshots
    #end region
    #
  end
end

