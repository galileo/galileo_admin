module Stats
  class MonthlyGalileoSearch < Monthly
    # region includes
    include PgSearch::Model
    # endregion

    # region attributes
    self.table_name = 'monthly_galileo_search'
    # endregion

    # region callbacks
    before_save :set_year_month
    # endregion

    # region validations
    validate :validate_year
    validate :validate_month
    # endregion

    # region scopes
    scope :text_search, ->(query) { search_for query }

    scope :sorted_by, lambda { |sort_option|
      direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at/ then order "monthly_galileo_search.created_at #{direction}"
      when /^updated_at/ then order "monthly_galileo_search.updated_at #{direction}"
      when /^year/ then order "monthly_galileo_search.year #{direction}"
      when /^month/ then order "monthly_galileo_search.month #{direction}"
      when /^institution_code/ then order "monthly_galileo_search.institution_code #{direction}"
      when /^query_type/ then order "monthly_galileo_search.query_type #{direction}"
      when /^count/ then order "monthly_galileo_search.count #{direction}"

      else
        raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
      end
    }

    scope :with_institutions, lambda { |*institution_codes|
      codes = institution_codes.flatten.reject(&:blank?)

      next if codes&.empty?

      where(institution_code: codes)
    }

    scope :with_query_types, lambda { |*query_types|
      types = query_types.flatten.reject(&:blank?)

      next if types&.empty?

      where(query_type: types)
    }

    # endregion

    # region pg_search and filterrific
    pg_search_scope :search_for,
                    against: %i[institution_code query_type],
                    using: {
                      tsearch: {
                        dictionary: 'english',
                        prefix: true
                      }
                    }

    filterrific(
      default_filter_params: { sorted_by: 'created_at_desc' },
      available_filters:
        %i[sorted_by text_search with_institutions with_query_types with_year_month_gte with_year_month_lte]
    )
    # endregion

    # region class_methods
    def self.daily_class
      Stats::DailyGalileoSearch
    end

    def self.raw_class
      Stats::RawGalileoSearch
    end

    # @param [Array<Date>] dates
    def self.accumulate_counts(dates)
      total_counts = 0

      rows = dates.flat_map do |date|
        counts_for(date).flat_map do |institution_code, query_counts|
          query_counts.map do |query_type, count|
            total_counts += count
            {
              year: date.year,
              month: date.month,
              year_month: date,
              institution_code: institution_code,
              query_type: query_type,
              count: count
            }
          end
        end

      end
      return rows.flatten, total_counts
    end

    # @param [String] year_month
    def self.counts_for(date)
      DailyGalileoSearch.where(date: date.beginning_of_month..date.end_of_month)
                     .group_by(&:institution_code) # Group by institution_code
                     .transform_values do |searches|
                        # Group by query_type and sum the :count values
                        searches.group_by(&:query_type).transform_values do |query_searches|
                          query_searches.sum { |search| search.count }
                        end
                      end # Group by query_type and count
    end

    def self.query_type_options
      self.all.pluck(:query_type).uniq.sort.map{|query_type| [query_type, query_type]}
    end
    # endregion
  end
end

