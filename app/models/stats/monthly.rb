module Stats
  class Monthly < StatsRecord
    # region attributes
    self.abstract_class = true
    # endregion

    # region scopes
    scope :with_institutions, lambda { |*institution_codes|
      codes = institution_codes.flatten.reject(&:blank?)

      next if codes&.empty?

      where(institution_code: codes)
    }

    scope :with_year_month_gte, lambda { |reference_time|
      date = Date.parse "#{reference_time}-1"
      where(year_month: date..)
    }

    scope :with_year_month_lte, lambda { |reference_time|
      date = Date.parse "#{reference_time}-1"
      where(year_month: ..date)
    }
    # endregion

    # region class_methods
    def self.accumulate_counts(_dates)
      raise NoMethodError("Override this implementation")
    end

    # @param [String] year_month yyyy-mm
    def self.accumulate(start_year_month, end_year_month, notifier: NotificationService.new)
      process = "`Accumulate #{self.name} #{start_year_month} to #{end_year_month}"
      notifier.start process

      validate_accumulation(start_year_month, end_year_month)

      rows, total_counts = accumulate_counts(to_month_range(start_year_month, end_year_month))

      raise Exceptions::MonthlyStatAccumulateError.new, "#{year}-#{month}, no rows to accumulate?" if rows.empty?

      self.insert_all(rows.compact)

      results = {
        total_counts: total_counts,
        db_sum_count: self.where(year_month: to_date(start_year_month)..to_date(end_year_month)).sum(:count)
      }
      notifier.finish process, additional_message: "grand_total: #{results[:total_counts]}; db_sum_count: #{results[:db_sum_count]}"
      results
    end

    def self.validate_accumulation(start_year_month, end_year_month)
      unless valid_year_month?(start_year_month) && valid_year_month?(end_year_month)
        raise Exceptions::MonthlyStatAccumulateError.new, "#{start_year_month} or #{end_year_month} , invalid year_month format. Must be yyyy-mm."
      end

      if to_date(end_year_month) < to_date(start_year_month)
        raise Exceptions::MonthlyStatAccumulateError.new, "end_year_month #{end_year_month} is before start_year_month: #{start_year_month}"
      end

      if accumulated?(start_year_month, end_year_month)
        raise Exceptions::MonthlyStatAccumulateError.new, "#{start_year_month} to #{end_year_month}, has accumulated data in range."
      end
    end

    # @param [String] year_month
    def self.valid_year_month?(year_month)
      return false if year_month.nil? || year_month.strip.empty?

      # Check format (yyyy-mm-dd)
      return false unless year_month.match?(/^\d{4}-\d{2}$/)

      # Check if it's a valid year_month
      begin
        # Check if the parsed year_month matches the original string
        return to_date(year_month).strftime('%Y-%m') == year_month
      rescue ArgumentError
        return false
      end
    end

    # @param [String] year_month
    def self.to_date(year_month)
      Date.parse("#{year_month}-01")
    end

    # @param [String] start_month yyyy-mm
    # @param [String] end_month yyyy-mm
    def self.to_month_range(start_month, end_month)
      dates = (start_month..end_month).uniq
      dates.map{|date| Date.parse("#{date}-1")}
    end

    def self.accumulated?(start_date, end_date)
      self.where(year_month: to_date(start_date)..to_date(end_date)).any?
    end

    def self.sort_options
      [
        ['Recently Created', 'created_at_desc'],
        ['Recently Updated', 'updated_at_desc']
      ]
    end
    # endregion

    # region public_methods
    def validate_year
      current_year = Time.zone.now.year
      return unless self.year < 1995 || self.year > current_year

      errors.add(:year, "must be between 1995 and #{current_year}")
    end

    def validate_month
      return unless self.month < 1 || self.month > 12

      errors.add(:month, "must be between 1 and 12")
    end

    def set_year_month
      self.year_month = Date.parse("#{self.year}-#{self.month}-1")
    end
  end
end
