module Stats
  class MonthlyLink < Monthly

    # region includes
    include PgSearch::Model
    # endregion

    # region attributes
    self.table_name = 'monthly_link'
    # endregion

    # region callbacks
    before_save :set_year_month
    # endregion

    # region validations
    validate :validate_year
    validate :validate_month
    # endregion

    # region scopes
    scope :text_search, ->(query) { search_for query }

    scope :sorted_by, lambda { |sort_option|
      direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at/ then order "monthly_link.created_at #{direction}"
      when /^updated_at/ then order "monthly_link.updated_at #{direction}"
      when /^year/ then order "monthly_link.year #{direction}"
      when /^month/ then order "monthly_link.month #{direction}"
      when /^institution_code/ then order "monthly_link.institution_code #{direction}"
      when /^resource_code/ then order "monthly_link.resource_code #{direction}"
      when /^count/ then order "monthly_link.count #{direction}"

      else
        raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
      end
    }

    scope :with_institutions, lambda { |*institution_codes|
      codes = institution_codes.flatten.reject(&:blank?)

      next if codes&.empty?

      where(institution_code: codes)
    }

    scope :with_resources, lambda { |*resource_codes|
      codes = resource_codes.flatten.reject(&:blank?)

      next if codes&.empty?

      where(resource_code: codes)
    }

    # endregion

    # region pg_search and filterrific
    pg_search_scope :search_for,
                    against: %i[institution_code resource_code],
                    using: {
                      tsearch: {
                        dictionary: 'english',
                        prefix: true
                      }
                    }

    filterrific(
      default_filter_params: { sorted_by: 'created_at_desc' },
      available_filters:
        %i[sorted_by text_search with_institutions with_resources with_year_month_gte with_year_month_lte]
    )
    # endregion

    # region class_methods
    def self.daily_class
      Stats::DailyLink
    end

    def self.raw_class
      Stats::RawLink
    end

    # @param [Array<Date>] dates
    def self.accumulate_counts(dates)
      total_counts = 0

      rows = dates.flat_map do |date|
        counts_for(date).flat_map do |institution_code, resource_counts|
          resource_counts.map do |resource_code, count|
            total_counts += count
            {
              year: date.year,
              month: date.month,
              year_month: date,
              institution_code: institution_code,
              resource_code: resource_code,
              count: count
            }
          end
        end
      end

      return rows, total_counts
    end

    # @param [String] year_month
    def self.counts_for(date)
      DailyLink.where(date: date.beginning_of_month..date.end_of_month)
                     .group_by(&:institution_code) # Group by institution_code
                     .transform_values do |links|
        # Group by resource_code and sum the :count values
        links.group_by(&:resource_code).transform_values do |resource_codes|
          resource_codes.sum { |link| link.count }
        end
      end
    end
    # endregion
  end
end

