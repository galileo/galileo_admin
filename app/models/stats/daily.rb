module Stats
  class Daily < StatsRecord
    # region attributes
    self.abstract_class = true
    # endregion

    # region scopes
    scope :with_institutions, lambda { |*institution_codes|
      codes = institution_codes.flatten.reject(&:blank?)

      next if codes&.empty?

      where(institution_code: codes)
    }

    scope :with_dates_gte, lambda { |reference_time|
      where(date: reference_time..)
    }

    scope :with_dates_lte, lambda { |reference_time|
      where(date: ..reference_time)
    }
    # endregion

    # region class_methods
    def self.accumulate_counts(_date)
      raise NoMethodError("Override this implementation")
    end

    # @param [String] start_date date yyyy-mm-dd
    # @param [String] end_date date yyyy-mm-dd
    # @param [NotificationService] notifier
    def self.accumulate_range(start_date, end_date, notifier: NotificationService.new)
      process = "`Accumulate Range #{start_date} to #{end_date} #{self.name}"
      notifier.start process

      unless valid_date?(start_date) && valid_date?(end_date)
        raise Exceptions::DailyStatAccumulateError.new, "#{start_date} or #{end_date} , invalid date format. Must be yyyy-mm-dd."
      end
      results = {
        grand_total: 0
      }
      grand_total = 0
      dates = Date.parse(start_date)..Date.parse(end_date)
      dates.each do |current_date|
        results[:grand_total] += accumulate(current_date.strftime('%Y-%m-%d'), notifier: notifier)
      end
      results[:db_sum_count] = self.where(date: dates).sum(:count)
      notifier.finish process, additional_message: "grand_total: #{results[:grand_total]}; db_sum_count: #{results[:db_sum_count]}"
      results
    end

    # @param [String] date yyyy-mm-dd
    def self.accumulate(date, notifier: NotificationService.new, accumulated_check: true)
      process = "`Accumulate #{self.name}"
      notifier.start process
      total_counts = 0

      unless valid_date?(date)
        raise Exceptions::DailyStatAccumulateError.new, "#{date}, invalid date format. Must be yyyy-mm-dd."
      end

      if accumulated_check && accumulated?(date)
        raise Exceptions::DailyStatAccumulateError.new, "#{date} already accumulated."
      end

      rows, total_counts = accumulate_counts(Time.zone.parse(date))

      if rows.empty?
        raise Exceptions::DailyStatAccumulateError.new, "#{date}, no rows to accumulate?"
      end

      self.insert_all(rows.compact)

      notifier.finish process, additional_message: "added #{rows.count} #{self.name} records for #{date}"

      total_counts
    end

    # @param [String] date
    def self.valid_date?(date)
      return false if date.nil? || date.strip.empty?

      # Check format (yyyy-mm-dd)
      return false unless date.match?(/^\d{4}-\d{2}-\d{2}$/)

      # Check if it's a valid date
      begin
        parsed_date = Date.parse(date)
        # Check if the parsed date matches the original string
        return parsed_date.strftime('%Y-%m-%d') == date
      rescue ArgumentError
        return false
      end
    end

    def self.accumulated?(date)
      self.where(date: date).any?
    end

    def self.sort_options
      [
        ['Recently Created', 'created_at_desc'],
        ['Recently Updated', 'updated_at_desc']
      ]
    end
    # endregion
  end
end
