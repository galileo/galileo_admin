module Stats
  class RawLink < Raw
    # region includes
    include PgSearch::Model
    # endregion

    # region attributes
    self.table_name = 'raw_link'
    # endregion

    # region scopes
    scope :text_search, ->(query) { search_for query }

    scope :sorted_by, lambda { |sort_option|
      direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at/ then order "raw_link.created_at #{direction}"
      when /^updated_at/ then order "raw_link.updated_at #{direction}"
      when /^session_id/ then order "raw_link.session_id #{direction}"
      when /^institution_code/ then order "raw_link.institution_code #{direction}"
      when /^resource_code/ then order "raw_link.resource_code #{direction}"
      when /^begin_time/ then order "raw_link.begin_time #{direction}"
      when /^server/ then order "raw_link.server #{direction}"

      else
        raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
      end
    }

    scope :with_resources, lambda { |*resource_codes|
      codes = resource_codes.flatten.reject(&:blank?)

      next if codes&.empty?

      where(resource_code: codes)
    }
    # endregion

    # region pg_search and filterrific
    pg_search_scope :search_for,
                    against: %i[institution_code session_id server resource_code],
                    using: {
                      tsearch: {
                        dictionary: 'english',
                        prefix: true
                      }
                    }

    filterrific(
      default_filter_params: { sorted_by: 'created_at_desc' },
      available_filters:
        %i[sorted_by text_search with_institutions with_begin_time_gte with_begin_time_lte with_resources with_servers]
    )
    # endregion

    # region class_methods
    def self.daily_class
      Stats::DailyLink
    end

    def self.monthly_class
      Stats::MonthlyLink
    end

    def self.record(line, _server)
      (session_id, begin_time, _action, resource_code, server, institution_code) = line.split(/,/)
      return unless institution_code&.match(INST_CODE_REGX)

      {
        session_id: session_id,
        begin_time: Time.zone.parse(begin_time),
        resource_code: resource_code.downcase,
        server: server,
        institution_code: institution_code.downcase
      }
    end
    def self.import_file_prefix
      'links'
    end
    # endregion
  end
end
