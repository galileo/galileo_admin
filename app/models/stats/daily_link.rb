module Stats
  class DailyLink < Daily

    # region includes
    include PgSearch::Model
    # endregion

    # region attributes
    self.table_name = 'daily_link'
    # endregion

    # region scopes
    scope :text_search, ->(query) { search_for query }

    scope :sorted_by, lambda { |sort_option|
      direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at/ then order "daily_link.created_at #{direction}"
      when /^updated_at/ then order "daily_link.updated_at #{direction}"
      when /^date/ then order "daily_link.date #{direction}"
      when /^institution_code/ then order "daily_link.institution_code #{direction}"
      when /^resource_code/ then order "daily_link.resource_code #{direction}"
      when /^count/ then order "daily_link.count #{direction}"

      else
        raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
      end
    }

    scope :with_resources, lambda { |*resource_codes|
      codes = resource_codes.flatten.reject(&:blank?)

      next if codes&.empty?

      where(resource_code: codes)
    }



    # endregion

    # region pg_search and filterrific
    pg_search_scope :search_for,
                    against: %i[institution_code resource_code],
                    using: {
                      tsearch: {
                        dictionary: 'english',
                        prefix: true
                      }
                    }

    filterrific(
      default_filter_params: { sorted_by: 'created_at_desc' },
      available_filters:
        %i[sorted_by text_search with_institutions with_resources with_dates_gte with_dates_lte]
    )

    # endregion

    # region class_methods
    def self.monthly_class
      Stats::MonthlyLink
    end

    def self.raw_class
      Stats::RawLink
    end

    def self.accumulate_counts(date)
      total_counts = 0
      rows = counts_for(date).flat_map do |institution_code, resource_counts|
        resource_counts.map do |resource_code, count|
          total_counts += count
          {
            date: date.to_date,
            institution_code: institution_code,
            resource_code: resource_code,
            count: count
          }
        end
      end
      return rows, total_counts
    end

    def self.counts_for(date)
      RawLink.where(begin_time: date.beginning_of_day..date.end_of_day)
      .group_by(&:institution_code)
      .transform_values do |links|
        links.group_by(&:resource_code).transform_values(&:count)
      end
    end
    # endregion

  end
end
