module Stats
  class DailyGalileoSearch < Daily
    # region includes
    include PgSearch::Model
    # endregion

    # region attributes
    self.table_name = 'daily_galileo_search'
    # endregion

    # region scopes
    scope :text_search, ->(query) { search_for query }

    scope :sorted_by, lambda { |sort_option|
      direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at/ then order "daily_galileo_search.created_at #{direction}"
      when /^updated_at/ then order "daily_galileo_search.updated_at #{direction}"
      when /^date/ then order "daily_galileo_search.date #{direction}"
      when /^institution_code/ then order "daily_galileo_search.institution_code #{direction}"
      when /^query_type/ then order "daily_galileo_search.query_type #{direction}"
      when /^count/ then order "daily_galileo_search.count #{direction}"

      else
        raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
      end
    }

    scope :with_query_types, lambda { |*query_types|
      types = query_types.flatten.reject(&:blank?)

      next if types&.empty?

      where(query_type: types)
    }
    # endregion

    # region pg_search and filterrific
    pg_search_scope :search_for,
                    against: %i[institution_code query_type],
                    using: {
                      tsearch: {
                        dictionary: 'english',
                        prefix: true
                      }
                    }

    filterrific(
      default_filter_params: { sorted_by: 'created_at_desc' },
      available_filters:
        %i[sorted_by text_search with_institutions with_query_types with_dates_gte with_dates_lte]
    )
    # endregion

    # region class_methods
    def self.monthly_class
      Stats::MonthlyGalileoSearch
    end

    def self.raw_class
      Stats::RawGalileoSearch
    end

    # @param [Date] date
    def self.accumulate_counts(date)
      total_counts = 0
      rows = counts_for(date).flat_map do |institution_code, query_counts|
        query_counts.map do |query_type, count|
          total_counts += count
          {
            date: date.to_date,
            institution_code: institution_code,
            query_type: query_type,
            count: count
          }
        end
      end
      return rows, total_counts
    end

    def self.counts_for(date)
      RawGalileoSearch.where(begin_time: date.beginning_of_day..date.end_of_day)
      .group_by(&:institution_code) # Group by institution_code
      .transform_values do |searches|
        searches.group_by(&:query_type).transform_values(&:count)
      end # Group by query_type and count
    end

    def self.query_type_options
      self.all.pluck(:query_type).uniq.sort.map{|query_type| [query_type, query_type]}
    end
    # endregion
  end
end
