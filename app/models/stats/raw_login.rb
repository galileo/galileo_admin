module Stats
  class RawLogin < Raw
    # region includes
    include PgSearch::Model
    # endregion

    # region attributes
    self.table_name = 'raw_login'
    # endregion

    # region constants
    AUTH_METHODS = %w[ip password ip2loc
    pinesid pines_remote pines_ip
    casid_remote cas_remote casid_ip cas_ip
    openathensid_remote oa_remote openathensid_ip oa_ip
    d2lid_remote d2l_remote d2lid_ip d2l_ip
    bbid_remote bbid_ip
    gilid_remote gilid_ip galid
    remote_ip].freeze
    # endregion

    # endregion

    # region scopes
    scope :text_search, ->(query) { search_for query }

    scope :sorted_by, lambda { |sort_option|
      direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at/ then order "raw_login.created_at #{direction}"
      when /^updated_at/ then order "raw_login.updated_at #{direction}"
      when /^session_id/ then order "raw_login.session_id #{direction}"
      when /^ip/ then order "raw_login.ip #{direction}"
      when /^server/ then order "raw_login.server #{direction}"
      when /^institution_code/ then order "raw_login.institution_code #{direction}"
      when /^auth_method/ then order "raw_login.auth_method #{direction}"
      when /^site_code/ then order "raw_login.site_code #{direction}"
      when /^begin_time/ then order "raw_login.begin_time #{direction}"
      else
        raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
      end
    }

    scope :with_sites, lambda { |*site_codes|
      codes = site_codes.flatten.reject(&:blank?)

      next if codes&.empty?

      where(site_code: codes)
    }

    scope :with_auth_methods, lambda { |*auth_methods|
      codes = auth_methods.flatten.reject(&:blank?)

      next if codes&.empty?

      where(auth_method: codes)
    }
    # endregion

    # region pg_search and filterrific
    pg_search_scope :search_for,
                    against: %i[institution_code session_id ip site_code server auth_method],
                    using: {
                      tsearch: {
                        dictionary: 'english',
                        prefix: true
                      }
                    }

    filterrific(
      default_filter_params: { sorted_by: 'created_at_desc' },
      available_filters:
        %i[sorted_by text_search with_institutions with_begin_time_gte with_begin_time_lte with_sites with_servers
           with_auth_methods]
    )
    # endregion

    # region class_methods
    def self.daily_class
      Stats::DailyLogin
    end

    def self.monthly_class
      Stats::MonthlyLogin
    end

    def self.auth_method_options
      self.all.pluck(:auth_method).uniq.sort.map{|auth_method| [auth_method, auth_method]}
    end

    def self.record(line, _server)
      (session_id, begin_time, _action, ip, server, institution_code, auth_method, site_code) = line.split(/,/)
      return unless AUTH_METHODS.include? auth_method

      {
        session_id: session_id,
        begin_time: Time.zone.parse(begin_time),
        ip: ip,
        server: server,
        institution_code: institution_code.downcase,
        auth_method: auth_method,
        site_code: ( site_code || '' )
      }
    end

    def self.import_file_prefix
      'login'
    end
    # endregion
  end
end
