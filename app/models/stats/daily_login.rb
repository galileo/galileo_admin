module Stats
  class DailyLogin < Daily
    # region includes
    include PgSearch::Model
    # endregion

    # region attributes
    self.table_name = 'daily_login'
    # endregion

    # region scopes
    scope :text_search, ->(query) { search_for query }

    scope :sorted_by, lambda { |sort_option|
      direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at/ then order "daily_login.created_at #{direction}"
      when /^updated_at/ then order "daily_login.updated_at #{direction}"
      when /^date/ then order "daily_login.date #{direction}"
      when /^institution_code/ then order "daily_login.institution_code #{direction}"
      when /^count/ then order "daily_login.count #{direction}"

      else
        raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
      end
    }
    # endregion

    # region pg_search and filterrific
    pg_search_scope :search_for,
                    against: %i[institution_code],
                    using: {
                      tsearch: {
                        dictionary: 'english',
                        prefix: true
                      }
                    }

    filterrific(
      default_filter_params: { sorted_by: 'created_at_desc' },
      available_filters:
        %i[sorted_by text_search with_institutions with_dates_gte with_dates_lte]
    )
    # endregion


    # region class_methods
    def self.monthly_class
      Stats::MonthlyLogin
    end

    def self.raw_class
      Stats::RawLogin
    end

    # @param [DateTime] date
    def self.accumulate_counts(date)
      counts = counts_for(date)
      total_counts = 0

      rows = counts.keys.sort.map do |institution_code|
        total_counts += counts[institution_code]
        {
          date: date.to_date,
          institution_code: institution_code,
          count: counts[institution_code]
        }
      end
      return rows, total_counts
    end

    # @param [DateTime] date
    def self.counts_for(date)
      RawLogin.where(begin_time: date.beginning_of_day..date.end_of_day)
        .group(:institution_code)
        .count
    end
    # endregion
  end
end
