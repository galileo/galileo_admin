module Stats
  class MonthlyLogin < Monthly
    # region includes
    include PgSearch::Model
    # endregion

    # region attributes
    self.table_name = 'monthly_login'
    # endregion

    # region callbacks
    before_save :set_year_month
    # endregion

    # region validations
    validate :validate_year
    validate :validate_month
    # endregion

    # region scopes
    scope :text_search, ->(query) { search_for query }

    scope :sorted_by, lambda { |sort_option|
      direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at/ then order "monthly_login.created_at #{direction}"
      when /^updated_at/ then order "monthly_login.updated_at #{direction}"
      when /^year/ then order "monthly_login.year #{direction}"
      when /^month/ then order "monthly_login.month #{direction}"
      when /^institution_code/ then order "monthly_login.institution_code #{direction}"
      when /^count/ then order "monthly_login.count #{direction}"

      else
        raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
      end
    }

    scope :with_institutions, lambda { |*institution_codes|
      codes = institution_codes.flatten.reject(&:blank?)

      next if codes&.empty?

      where(institution_code: codes)
    }



    # endregion

    # region pg_search and filterrific
    pg_search_scope :search_for,
                    against: %i[institution_code],
                    using: {
                      tsearch: {
                        dictionary: 'english',
                        prefix: true
                      }
                    }

    filterrific(
      default_filter_params: { sorted_by: 'created_at_desc' },
      available_filters:
        %i[sorted_by text_search with_institutions with_year_month_gte with_year_month_lte]
    )
    # endregion

    # region class_methods
    def self.daily_class
      Stats::DailyLogin
    end

    def self.raw_class
      Stats::RawLogin
    end

    # @param [Array<Date>] dates
    def self.accumulate_counts(dates)
      total_counts = 0

      rows = dates.flat_map do |date|
        counts = counts_for(date)
        counts.keys.sort.map do |institution_code|
          total_counts += counts[institution_code]
          {
            year: date.year,
            month: date.month,
            year_month: date,
            institution_code: institution_code,
            count: counts[institution_code]
          }
        end

      end
      return rows, total_counts
    end

    # @param [String] year_month
    def self.counts_for(date)
      DailyLogin.where(date: date.beginning_of_month..date.end_of_month)
                  .group(:institution_code)
                  .sum(:count)
    end
    # endregion
  end
end

