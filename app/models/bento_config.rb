# frozen_string_literal: true

# Represents a configuration for a Bento box
class BentoConfig < ApplicationRecord
  # region relationships
  belongs_to :institution, inverse_of: :bento_configs
  has_many :configured_bentos, dependent: :destroy
  # endregion

  # region callbacks
  before_validation :generate_name
  before_validation :generate_inst_code
  before_validation :generate_blacklight_id
  before_validation :ensure_blank_is_empty_array
  # endregion

  # region validations
  validates :institution_id, presence: true
  validates :service, presence: true
  validates :name, presence: true
  validate :allowed_bento_services
  validate :validate_view_types
  validate :validate_view_types_unique
  validates_uniqueness_of :name, scope: %i[institution_id]
  # endregion

  # region scopes
  scope :for_indexing, -> { includes(:institution) }
  scope :for_user_view, ->(user_view) { where('? = ANY(view_types)', user_view) }
  scope :for_service, ->(service) { where(service: service) }
  scope :for_view_and_service, ->(user_view, service) { for_user_view(user_view).for_service(service) }
  scope :unique_service, -> { select('distinct on (service) *') }
  # endregion

  # region paper_trail
  has_paper_trail(
    versions: {
      class_name: 'BentoVersion'
    },
    meta: {
      item_name: :display_name
    }
  )
  # endregion

  # region solr_fields
  searchable include: [:institution] do
    string(:class, stored: true) { self.class }
    string(:blacklight_id, stored: true)
    string(:bento_type, stored: true) { service } # TODO: Remove
    string(:service, stored: true)
    string(:inst_code, stored: true)
    string(:name, stored: true)
    string(:user_id, stored: true)
    string(:password, stored: true)
    string(:view_type, stored: true) { view_types.first }# TODO: Remove
    string(:view_types, multiple: true, stored: true)
    string(:api_profile, stored: true)
    string(:credentials, stored: true) { credentials.to_json }
  end
  # endregion

  # region class methods
  # Returns array of valid bento types for drop downs in the view
  # Note: values should be snake_case (no spaces or caps)
  def self.service_types_for_menu
    BentoService.all_configurable.map {|bs| [bs.display_name, bs.code]}
  end
  # endregion

  # region public methods

  # Returns a Sunspot Hit object for the allocation that represents the current values in SOLR
  def solr_hit
    search = BentoConfig.search do
      with(:inst_code, inst_code)
      with(:name, name)
    end

    return nil if search.total < 1

    search.hits.first
  end

  def solr_stored_hash(hit = solr_hit)

    return {} if hit.nil?

    hit.instance_variable_get(:@stored_values)
  end


  def service_name
    BentoService.get(service)&.display_name
  end

  def display_name
    case service
    when 'eds_api'
      "#{service_name} (#{credential('API Profile')})"
    when 'primo'
      "#{service_name} (#{credential('VID')})"
    else
      "#{service_name} (#{view_types.join ', '})"
    end
  end

  # Lookup credential in credentials json field
  # @param [String] credential_key
  # @return [String]
  def credential(credential_key)
    credentials&.dig(credential_key)
  end

  # Returns the credentials hash sorted in a consistent order
  def sorted_credentials
    unsorted = credentials&.clone ||  {}
    key_order = BentoService.get(service)&.config_fields
    return unsorted if key_order.nil?
    sorted = {}
    key_order.each do |k|
      sorted[k] = unsorted.delete(k) if unsorted.include? k
    end
    sorted.merge(unsorted)
  end

  def eds_content_providers
    return [] unless service == 'eds_api'

    eds_api = EdsApi.new self, use_auth_token_cache: false
    (eds_api.get_available_databases || []).pluck(:label).sort
  end
  # endregion

  # region private methods
  private

  # Returns all allowed bento types as an array
  def allowed_bento_service_codes
    BentoService.all_configurable.map &:code
  end

  # Custom verification for site types
  def allowed_bento_services
    return if allowed_bento_service_codes.include? service

    errors.add(:service,
               "'#{service}' is not a supported type. Allowed types: #{allowed_bento_service_codes.join(', ')}")
  end

  def validate_view_types
    allowed_codes = allowed_view_codes
    view_types.each do |view_code|
      next if allowed_codes.include? view_code

      errors.add(:view_types, "'#{view_code}' is not a supported view_type. Allowed types: #{allowed_codes.join(', ')}")
    end
  end

  def validate_view_types_unique
    return if institution.nil? # Leave this to other validations
    view_types.each do |view_code|
      if institution.bento_configs.for_view_and_service(view_code, service).where.not(id: id).any?
        errors.add(:view_types, "'#{view_code}' already has default service credentials for #{service_name}")
      end
    end
  end

  def ensure_blank_is_empty_array
    self.view_types = (view_types || []).filter &:present?
  end

  # All view type codes
  # @return [Array]
  def allowed_view_codes
    UserView.for_inst_type(institution&.inst_type).pluck(:code)
  end

  def generate_name
    self.name = "#{service}_#{view_types.join('_')}" if name.blank?
  end

  def generate_inst_code
    self.inst_code = institution&.code
  end

  def generate_blacklight_id
    self.blacklight_id = "#{inst_code}_#{service}_#{view_type}"
  end
  # endregion

end
