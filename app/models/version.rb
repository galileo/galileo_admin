# frozen_string_literal: true

# represent a paper_trail version
class Version < PaperTrail::Version
  #region includes
  include PgSearch::Model
  # endregion

  # region constants
  ITEM_TYPES = %w[Contact Institution Resource Banner].freeze
  # endregion

  # region relationships
  belongs_to :user, primary_key: 'id', foreign_key: 'whodunnit', required: false
  # endregion

  # region scopes
  scope :text_search, ->(query) { search_for query }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at_/ then order "versions.created_at #{direction}"
    when /^item_type/ then order "versions.item_type #{direction}"
    when /^event/ then order "versions.event #{direction}"

    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :with_item_type, lambda { |item_type|
    next unless item_type

    where(item_type: item_type)
  }

  scope :with_event, lambda { |event|
    next unless event

    where(event: event)
  }

  scope :with_user_id, lambda { |user_id|
    next unless user_id

    where(whodunnit: user_id)
  }

  scope :with_role, lambda { |role|
    next unless role
    user_ids = User.with_role(role).pluck(:id)
    where(whodunnit: user_ids)
  }

  scope :with_created_at_gte, lambda { |reference_time|
    where(created_at: reference_time..)
  }

  scope :with_created_at_lt, lambda { |reference_time|
    where(created_at: ...reference_time)
  }
  # endregion

  # region pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:object, :object_changes],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc' },
    available_filters: %i[sorted_by text_search with_item_type with_event
                          with_user_id with_role with_created_at_gte
                          with_created_at_lt]
  )
  # endregion

  #region class_methods
  def self.sort_options
    [
      %w[Newest created_at_desc],
      %w[Oldest created_at_asc]
    ]
  end
  # endregion

  # region public_methods
  def item_name
    json_object = JSON.parse object if object
    json_changes = JSON.parse object_changes if object_changes

    return type_and_id unless (json_object || json_changes)

    name = if item_type == 'Contact'
            first = json_changes&.dig('first_name')&.last || json_object&.dig('first_name')
            last = json_changes&.dig('last_name')&.last || json_object&.dig('last_name')
            "#{first} #{last}"
           elsif item_type == 'Banner'
             json_changes&.dig('content')&.last || json_object&.dig('content') || item&.content&.to_plain_text
           else
             json_changes&.dig('name')&.last || json_object&.dig('name')
           end
    name || type_and_id
  end

  def type_and_id
    "#{item_type} #{item_id}"
  end
  # endregion
end
