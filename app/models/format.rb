# frozen_string_literal: true

# Represent a Format for Resources
class Format < ApplicationRecord
  include PgSearch::Model

  has_and_belongs_to_many :resources
  has_many :allocations, through: :resources

  validates_presence_of :name
  validates_uniqueness_of :name

  # reindex allocations if a subject has been saved or destroyed
  after_commit :reindex_allocations
  before_destroy :save_allocation_ids
  after_destroy :reindex_allocations_after_destroy

  pg_search_scope :search_for,
                  against: :name,
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  def self.text_search(query)
    if query.present?
      Format.search_for query
    else
      all
    end
  end

  # Reindex all Allocations for this format
  def reindex_allocations
    Sunspot.index! allocations.for_indexing
  end

  def save_allocation_ids
    @associated_allocation_ids = allocations.pluck(:id)
  end

  def reindex_allocations_after_destroy
    Sunspot.index! Allocation.for_indexing.where(id: @associated_allocation_ids)
  end
end
