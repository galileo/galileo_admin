# frozen_string_literal: true

# represent an assignment of a User View to an Allocation
class AllocationsUserView < ApplicationRecord
  belongs_to :user_view
  belongs_to :allocation
  validates_uniqueness_of :user_view_id, scope: :allocation_id
end
