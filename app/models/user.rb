# frozen_string_literal: true

# GALILEO Admin site user
class User < ApplicationRecord
  # region includes
  include PgSearch::Model
  include InstitutionFilterable
  # endregion

  # region constants
  VALID_ROLES = %w[admin institutional helpdesk].freeze
  VALID_SUBROLES = {
    admin: [],
    institutional: %w[advanced portal],
    helpdesk: []
  }.freeze
  # endregion

  # region relationships
  has_and_belongs_to_many :institutions
  has_many :versions, :class_name => 'Version', :foreign_key => 'whodunnit'
  has_many :contacts, primary_key: 'email', foreign_key: 'email'
  # endregion

  # region  devise
  devise :database_authenticatable, :recoverable, :rememberable, :validatable, :invitable, validate_on_invite: true
  # endregion

  #region callbacks
  before_validation :clean_subroles
  # end_region

  # region validations
  validates_presence_of :role, :first_name, :last_name
  validates_inclusion_of :role, in: VALID_ROLES
  validate :allowed_subroles
  # endregion

  # region pg_search
  pg_search_scope :search_for,
                  against: %i[email role first_name last_name],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }
  # endregion

  #region filterrific
  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc' },
    available_filters:
      %i[sorted_by text_search with_role with_subroles with_institutions]
  )
  # endregion

  # region scopes
  scope :text_search, ->(query) { search_for query }


  scope :active, lambda {
    where('deleted_at IS NULL AND (invitation_sent_at IS NULL OR invitation_accepted_at IS NOT NULL)')
  }

  scope :deleted, lambda {
    where.not(deleted_at: nil)
  }

  scope :pending_invitation_response, lambda {
    where('invitation_sent_at IS NOT NULL AND invitation_accepted_at IS NULL')
  }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at/ then order "users.created_at #{direction}"
    when /^updated_at/ then order "users.updated_at #{direction}"
    when /^email/ then order "users.email #{direction}"
    when /^role/ then order "users.role #{direction}"
    when /^first_name/ then order "users.first_name #{direction}"
    when /^last_name/ then order "users.last_name #{direction}"

    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :with_role, lambda { |role|
    next unless role

    where(role: role)
  }

  scope :with_subroles, lambda { |*subroles|
    roles = subroles.flatten.reject(&:blank?)
    next if roles.empty?

    where('subroles @> ARRAY[?]::varchar[]', roles)
  }

  many_to_many_institution_scope :with_institutions
  # endregion

  # region public_methods
  def has_role?(role)
    self.role == role
  end
  def has_subrole?(role)
    self.include? role
  end

  def all_roles
    roles = [role]
    roles + subroles
  end


  def admin?
    role == 'admin'
  end

  def non_admin?
    !admin?
  end

  def institutional?
    role == 'institutional'
  end

  def helpdesk?
    role == 'helpdesk'
  end

  def status
    if deleted_at.nil? && (invitation_sent_at.nil? || !invitation_accepted_at.nil?)
      'Active'
    elsif !invitation_sent_at.nil? && invitation_accepted_at.nil?
      'Invited'
    elsif !deleted_at.nil?
      'Deleted'
    else
      'Unknown'
    end
  end

  # instead of deleting, indicate the user requested a delete & timestamp it
  def soft_delete
    update_attribute(:deleted_at, Time.current)
  end

  # ensure user account is active
  def active_for_authentication?
    super && !deleted_at
  end

  # provide a custom message for a deleted account
  def inactive_message
    !deleted_at ? super : :deleted_account
  end

  def name
    "#{first_name} #{last_name}"
  end

  def name_with_email
    "#{name} (#{email})"
  end

  def clean_subroles
    self.subroles = subroles.uniq.reject(&:blank?)
  end

  def allowed_subroles
    return unless subroles.any?

    bad_subroles = subroles - VALID_SUBROLES[role.to_sym]

    return unless bad_subroles&.any?

    errors.add(:subroles, "invalid subrole for this role: #{bad_subroles.join}")
  end
  # endregion

  # region class_methods
  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  # Used to populate user emails for filteriffic
  def self.options_for_select
    order('LOWER(email)').map { |e| [e.email, e.id] }
  end
  # endregion
end
