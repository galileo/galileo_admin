
# frozen_string_literal: true

# Represents an Institution site
class Site < ApplicationRecord
  #region includes
  include PgSearch::Model
  include InstitutionFilterable
  #endregion

  #region callbacks
  before_validation :sanitize_name
  #endregion

  #region relationships
  belongs_to :institution, inverse_of: :sites
  #endregion

  #region validations
  validates_presence_of :code, :name, :site_type
  validates_uniqueness_of :code, :name
  validate :allowed_site_types, if: :site_type
  #endregion

  #region scopes
  scope :for_indexing, -> { includes(:institution) }

  scope :text_search, ->(query) { search_for query }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at/ then order "sites.created_at #{direction}"
    when /^updated_at/ then order "sites.updated_at #{direction}"
    when /^name/ then order "sites.name #{direction}"
    when /^code/ then order "sites.code #{direction}"
    when /^site_type/ then order "sites.site_type #{direction}"
    when /^charter/ then order "sites.charter #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :for_site_type, lambda { |site_type|
    next unless site_type

    where(site_type: site_type)
  }

  scope :charter_state, lambda { |charter|
    case charter
    when 'yes' then where(charter: true)
    when 'no' then where(charter: false)
    when '' then next
    else
      raise(ArgumentError, "Invalid charter option: #{charter}")
    end
  }
  institution_scope :for_institution

  scope :for_institution_group, lambda { |group_ids|
    next unless group_ids.map(&:to_s).reject(&:blank?)&.any?

    where(institution: { institution_group_id: group_ids}).joins(:institution)
  }
  #endregion

  # region pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:name, :code, :wayfinder_keywords],
                  associated_against: {
                    institution: %i[code name]
                  },
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'name_asc' },
    available_filters: %i[sorted_by text_search for_site_type charter_state for_institution for_institution_group]
  )
  # endregion

  #region solr_fields
  searchable include: [:institution] do
    string(:class, stored: true) { self.class }
    string(:name, stored: true)
    string(:name_lower) { name.downcase }
    string(:code, stored: true)
    if FeatureFlags.enabled?(:site_type)
      string(:type, stored: true) { indexed_site_type( site_type ) }
    else
      string(:site_type, stored: true, as: 'type_ss')
    end
    string(:parent_inst_code, stored: true) { institution.code }

    boolean(:active, stored: true, as: 'active_bs') { parent_is_active? }

    # Suggestions field
    string :name_suggest, as: 'instSuggest'

    # Wayfinder
    boolean :show_in_wayfinder
    string(:wayfinder_keywords, stored: true) { solr_wayfinder_keywords }
  end
  # endregion

  #region class_methods
  # Returns array of valid site types for drop downs in the view
  def self.allowed_site_types
    [
      ['Elementary School', 'elem'],
      ['Middle School', 'midd'],
      ['High School', 'highschool'],
      ['K-12 School', 'k12'],
      ['Higher Education', 'highered'],
      ['Public Library', 'publiclib']
    ]
  end

  def self.options_for_select
    order('LOWER(name)').map { |site| [site.name, site.code] }
  end

  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.charter_states
    [%w[Charter yes], %w[Non-Charter no]]
  end
  # endregion

  # region public_methods
  def show_in_wayfinder
    parent_is_active?
  end

  def name_suggest
    name
  end

  def parent_is_active?
    institution.active?
  end

  # Returns all allowed site types as an array
  def allowed_site_type_codes
    Site.allowed_site_types.map { |t| t[1] }
  end

  # Custom verification for site types
  def allowed_site_types
    return if allowed_site_type_codes.include? site_type

    errors.add(:site_type, 'not a permitted site type')
  end

  def indexed_site_type( t )
    if t == 'elem'
      'elementary'
    elsif t == 'midd'
      'middle'
    else
      t
    end
  end

  def sanitize_name
    name&.squish!
  end

  def solr_wayfinder_keywords
    if wayfinder_keywords.is_a? Array
      wayfinder_keywords.uniq.join("\n")
    else
      wayfinder_keywords
    end
  end

  def name_with_code
    "#{name} (#{code})"
  end
# endregion
end

