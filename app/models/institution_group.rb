# frozen_string_literal: true

# represent a grouping of institutions
class InstitutionGroup < ApplicationRecord
  #region includes
  include Featuring
  #endregion

  # region relationships
  has_and_belongs_to_many :legacy_group_institutions, class_name: 'Institution'
  has_many :institutions
  has_many :banners
  # endregion

  # region validations
  validates_presence_of :code
  validates_uniqueness_of :code
  validates_presence_of :inst_type
  validate :allowed_inst_types
  # endregion

  # region scopes
  scope :code_sort, -> { order(code: :asc) }

  scope :with_sites, -> { where(id: InstitutionGroup.joins(:institutions).joins(institutions: :sites).group(:id).select(:id)) }
  # endregion

  # region callbacks
  after_save :reindex_institutions
  # callbacks

  # region class_methods
  # Returns array of valid view types for drop downs in the form
  def self.allowed_inst_types
    [
      %w[Default default],
      ['K-12 School', 'k12'],
      ['Higher Education', 'highered'],
      ['Public Library', 'publiclib']
    ]
  end

  # Returns all allowed inst types as an array
  def self.allowed_inst_type_codes
    InstitutionGroup.allowed_inst_types.map { |t| t[1] }
  end

  def self.options_for_select
    order('LOWER(name)').map { |group| [group.name, group.id] }
  end
  # endregion

  # region public_methods
  # return code if asked for name (useful in polymorphic `Featuring` views)
  def name
    code
  end

  # @return [TrueClass, FalseClass]
  def k12?
    inst_type == 'k12'
  end

  # @return [TrueClass, FalseClass]
  def publiclib?
    inst_type == 'publiclib'
  end

  # Custom verifications
  def allowed_inst_types
    return if InstitutionGroup.allowed_inst_type_codes.include? inst_type

    errors.add(:inst_type, 'not a permitted inst_type')
  end

  def reindex_institutions
    Sunspot.index! institutions if previous_changes['inst_type']
  end

  def code_with_type
    "#{code} (#{inst_type})"
  end

  def display_views?
    ViewService.institution_display_views?(inst_type)
  end
  #endregion
end
