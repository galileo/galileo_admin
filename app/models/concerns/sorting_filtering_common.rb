# frozen_string_literal: true

require 'active_support/concern'

# Shared behavior for Resource and Institution sorting and filtering
module SortingFilteringCommon
  extend ActiveSupport::Concern

  included do
    include PgSearch::Model
    scope :active, -> { where(active: true) }
    scope :text_search, ->(query) { search_for query }
    scope :open_athens_state, lambda { |oa|
      case oa
      when 'yes' then where(open_athens: true)
      when 'no' then where(open_athens: false)
      when '' then next
      else
        raise(ArgumentError, "Invalid OpenAthens option: #{oa}")
      end
    }
    scope :active_state, lambda { |active|
      case active
      when 'yes' then where(active: true)
      when 'no' then where(active: false)
      when '' then next
      else
        raise(ArgumentError, "Invalid Active option: #{active}")
      end
    }
    scope :display_state, lambda { |display|
      case display
      when 'yes' then where(display: true)
      when 'no' then where(display: false)
      when '' then next
      else
        raise(ArgumentError, "Invalid Active option: #{display}")
      end
    }
    configure_filterrific
    configure_pg_search
  end

  class_methods do
    # Active states
    def active_states
      [['Either', ''], %w[Active yes], ['Not Active', 'no']]
    end

    def display_states
      [['Either', ''], %w[Display yes], ['Don\'t Display', 'no']]
    end

    # OpenAthens states
    def open_athens_states
      [['Either', ''], %w[Enabled yes], ['Not Enabled', 'no']]
    end
  end
end
