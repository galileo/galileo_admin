# frozen_string_literal: true

module InstitutionFilterable
  extend ActiveSupport::Concern

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods

    # This creates a scope that can filter by a single institution ID, an array of institution ids, or a hash/params
    # that represent a set of institution filters that the Institution model's filterrific knows how to handle.
    #
    # This assumes the target model has a field called `institution_id`
    #
    # @param name [Symbol] the name of the the scope to be created
    def institution_scope(name)
      scope name, lambda { |id_or_query|
        next unless id_or_query.present?
        if id_or_query.is_a?(Numeric) || id_or_query.is_a?(String)
          where institution_id: id_or_query
        elsif id_or_query.is_a?(Array)
          inst_ids = id_or_query.reject(&:blank?)
          next if inst_ids.empty?
          where institution_id: inst_ids
        else
          where institution_id: Institution.filterrific_find(
            Filterrific::ParamSet.new(Institution, id_or_query)
          ).select(:id)
        end
      }
    end

    # This creates a scope that can filter by a single institution ID, an array of institution ids, or a hash/params
    # that represent a set of institution filters that the Institution model's filterrific knows how to handle.
    #
    # This assumes the target model has many-to-many association with institutions accessible through a relation called
    # `institutions`,
    #
    # @param name [Symbol] the name of the the scope to be created
    def many_to_many_institution_scope(name)
      scope name, lambda { |id_or_query|
        next unless id_or_query.present?
        if id_or_query.is_a?(Numeric) || id_or_query.is_a?(String)
          where institutions: { id: id_or_query }
        elsif id_or_query.is_a?(Array)
          inst_ids = id_or_query.reject(&:blank?)
          next if inst_ids.empty?
          where institutions: { id: inst_ids }
        else
          where institutions: { id: Institution.filterrific_find(
            Filterrific::ParamSet.new(Institution, id_or_query)
          ).select(:id) }
        end
      }
    end
  end
end

