# frozen_string_literal: true

require 'active_support/concern'

# Feature stuff shared by Institution and InstitutionGroup models
module Featuring
  extend ActiveSupport::Concern

  included do
    has_many :features, as: :featuring
  end

  # This method is called by the after_commit callback in the Feature model,
  # and therefore is execute after create, update and destroy of any Feature
  # record
  def reindex
    if is_a? InstitutionGroup
      # TODO: speed this up or queue it
      Sunspot.index! institutions.includes(:features).where(active: true)
    else
      Sunspot.index! self
    end
  end
end
