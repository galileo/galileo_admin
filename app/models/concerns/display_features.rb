# frozen_string_literal: true

require 'active_support/concern'

# Determine features intended for Indexing/Display
module DisplayFeatures
  ##
  # Return Feature records for Indexing/Display
  # Cases:
  # - no local features, no group features - return []
  # - no local features, group features - return group features
  # - some local features, group features - return features, with loc preferred
  # - some local features, no group - ???
  # - all local features - return 'em
  # @return [Hash]
  def display_features
    @display_features ||= determine_display_features
  end

  private

  def view_features_hash(view_type)
    { view_type => determine_features(view_type) }
  end

  def determine_display_features
    if k12?
      determine_k12_features
    elsif publiclib?
      determine_publiclib_features
    else
      view_features_hash 'default'
    end
  end

  def determine_k12_features
    view_features_hash('elementary')
      .merge(view_features_hash('middle'))
      .merge(view_features_hash('highschool'))
      .merge(view_features_hash('educator'))
  end

  def determine_publiclib_features
    view_features_hash('elementary')
      .merge(view_features_hash('middle'))
      .merge(view_features_hash('highschool'))
      .merge(view_features_hash('full'))
      .merge(view_features_hash('default'))
  end

  def determine_features(view_type)
    return [] unless institution_group

    # Handle common case where no local features defined
    if features.empty?
      return institution_group.features.where(view_type: view_type)
                              .order(:position).map(&:to_a)
    end

    (1..Feature::FEATURE_COUNT).map do |i|
      if Feature.exists?(featuring_id: id, position: i, view_type: view_type)
        features.find_by(position: i, view_type: view_type).to_a
      else
        institution_group.features.find_by(position: i, view_type: view_type)
                         .to_a
      end
    end
  end
end
