# frozen_string_literal: true

require 'active_support/concern'

# Sorting and Filtering behavior for Resource
module SortingFilteringResource
  extend ActiveSupport::Concern

  included do
    include SortingFilteringCommon
    include InstitutionFilterable

    scope :central, -> { where(institution_id: nil) }
    scope :glri, -> { where.not(institution_id: nil) }
    scope :glri_locally_managed, -> { glri.joins(:institution).merge(Institution.real.glri_locally_managed)}
    scope :glri_galileo_managed, -> { glri.joins(:institution).merge(Institution.real.glri_galileo_managed)}
    scope :has_allocations, -> { where(id: Allocation.all.select(:resource_id)) }

    scope :glri_state, lambda { |glri|
      case glri
      when 'glri' then where('institution_id IS NOT NULL')
      when 'nonglri' then where('institution_id IS NULL')
      when '' then next
      else
        raise(ArgumentError, "Invalid GLRI option: #{glri}")
      end
    }

    institution_scope :for_institutions
    scope :for_vendor, lambda { |vendor_id|
      next unless vendor_id

      where(vendor_id: vendor_id)
    }

    # this scope assumes a join with Allocations for use in InstitutionResources
    # context
    scope :resource_type, lambda { |type|
      case type
      when 'all' then next
      when 'glri' then glri
      when 'core' then central
      when 'bypass'
        where(resources: { bypass_galileo_authentication: true })
      else
        next
      end
    }
    scope :sorted_by, lambda { |sort_option|
      direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at/ then order "resources.created_at #{direction}"
      when /^updated_at/ then order "resources.updated_at #{direction}"
      when /^code/ then order "resources.code #{direction}"
      when /^name/ then order "resources.name #{direction}"
      when /^institution_name/ then order "institutions.name #{direction}"
      else
        raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
      end
    }
    scope :with_vendor_name, -> (vendor_name){
      where(vendor_name: [*vendor_name]) }

    scope :name_sort, -> { order(name: :asc) }

    scope :bypass_galileo_authentication_state, lambda { |bga|
      case bga
      when 'yes' then where(bypass_galileo_authentication: true)
      when 'no' then where(bypass_galileo_authentication: false)
      when '' then next
      else
        raise(ArgumentError, "Invalid bypass_galileo_authentication option: #{bga}")
      end
    }
  end

  class_methods do
    def sort_options
      [
        ['Recently Created', 'created_at_desc'],
        ['Recently Updated', 'updated_at_desc'],
        ['Code (a-z)', 'code_asc'],
        ['Code (z-a)', 'code_desc'],
        ['Name (a-z)', 'name_asc'],
        ['Name (z-a)', 'name_desc']
      ]
    end

    def glri_states
      [
        ['Either', ''],
        ['GLRI', 'glri'],
        ['Non-GLRI', 'nonglri']
      ]
    end

    def vendor_names
      Resource.select(:vendor_name).distinct.map(&:vendor_name).compact.sort
    end

    def resource_types
      [
        ['All', 'all'],
        ['GLRI', 'glri'],
        ['GALILEO Core', 'core'],
        ['Bypass Galileo Authentication', 'bypass']
      ]
    end

    def bypass_galileo_authentication_states
      [%w[True yes], %w[False no]]
    end

    def configure_filterrific
      filterrific(
        default_filter_params: { sorted_by: 'created_at_desc' },
        available_filters:
          %i[sorted_by active_state display_state glri_state open_athens_state bypass_galileo_authentication_state text_search
             for_institutions for_vendor with_vendor_name resource_type]
      )
    end

    def configure_pg_search
      pg_search_scope :search_for,
                      against: %i[code name vendor_name],
                      using: {
                        tsearch: {
                          dictionary: 'english',
                          prefix: true
                        }
                      }
    end
  end
end
