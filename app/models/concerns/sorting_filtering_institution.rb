# frozen_string_literal: true

require 'active_support/concern'

# Sorting and Filtering behavior for Institution
module SortingFilteringInstitution
  extend ActiveSupport::Concern

  included do
    include SortingFilteringCommon
    scope :glri, lambda {
      where('EXISTS (
             SELECT 1 from resources
             WHERE institutions.id = resources.institution_id)')
    }
    scope :sorted_by, lambda { |sort_option|
      direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at/ then order "institutions.created_at #{direction}"
      when /^updated_at/ then order "institutions.updated_at #{direction}"
      when /^code/ then order "institutions.code #{direction}"
      when /^name/ then order "institutions.name #{direction}"
      else
        raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
      end
    }
    scope :glri_state, lambda { |glri|
      case glri
      when 'galileo' then where(glri_participant: 'galileo')
      when 'locally' then where(glri_participant: 'locally')
      when 'nope' then where(glri_participant: 'none')
      when 'any' then next
      else
        raise(ArgumentError, "Invalid GLRI option: #{glri}")
      end
    }
    scope :for_institution_group, lambda { |group_id|
      group_id = [group_id] unless group_id.is_a? Array
      next unless group_id.reject(&:blank?).any?

      where(institution_group_id: group_id)
    }

    scope :for_site, lambda { |site_code|
      next unless site_code.present?

      includes(:sites).where(sites:{code: site_code})
    }

    scope :with_sites, -> { where(id: Site.select(:institution_id)) }
    scope :for_allocations, lambda { |allocations|
      where(id: allocations.select(:institution_id))
    }
    scope :for_allocation_filter, lambda { |allocation_filter|
      for_allocations(Allocation.filterrific_find(Filterrific::ParamSet.new(Allocation, allocation_filter)))
    }
    scope :with_bento_template, lambda {|bento_template|
      next unless bento_template.present?
      where(bento_template: bento_template)
    }
  end

  class_methods do
    def sort_options
      [
        ['Recently Created', 'created_at_desc'],
        ['Recently Updated', 'updated_at_desc'],
        ['Code (a-z)', 'code_asc'],
        ['Code (z-a)', 'code_desc'],
        ['Name (a-z)', 'name_asc'],
        ['Name (z-a)', 'name_desc']
      ]
    end

    def glri_options
      [
        ['Any', 'any'],
        ['Not a participant', 'nope'],
        ['GALILEO-managed', 'galileo'],
        ['Institution-managed', 'locally']
      ]
    end

    def configure_filterrific
      filterrific(
        default_filter_params: { sorted_by: 'created_at_desc' },
        available_filters:
          %i[sorted_by active_state glri_state open_athens_state text_search for_institution_group for_site
             for_allocation_filter with_bento_template]
      )
    end

    def configure_pg_search
      pg_search_scope :search_for,
                      against: %i[code public_code name current_password wayfinder_keywords],
                      using: {
                        tsearch: {
                          dictionary: 'english',
                          prefix: true
                        }
                      }
    end
  end
end