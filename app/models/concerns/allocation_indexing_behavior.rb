# frozen_string_literal: true

require 'active_support/concern'

# Shared behavior for Resource and Institution data reindexing of Allocations
module AllocationIndexingBehavior
  extend ActiveSupport::Concern

  included do
    after_commit :commit
    after_commit :reindex_allocations, if: :should_reindex_allocations, unless: :skip_indexing
    attr_accessor :should_reindex_allocations, :should_commit, :skip_indexing
    after_save :flag_for_reindex, if: :indexed_attributes_changed?, unless: :skip_indexing
  end

  def save_without_indexing(options)
    self.skip_indexing = true
    save options
  end

  # mark record as needing reindexing of Allocations
  def flag_for_reindex(relation = nil)
    return unless relation.nil? || relation.class.name.in?(%w[Format Subject UserView])

    self.should_reindex_allocations = true
    self.should_commit = true
  end

  # TODO: is this used?
  def flag_for_commit(_ = nil)
    self.should_commit = true
  end

  def indexed_attributes_changed?
    (saved_changes.keys & self.class.indexed_attributes).any? ||
      attachment_changes.any?
  end

  def reindex_allocations
    return unless allocations.any?

    IndexingService.reindex allocations.includes(:institution)
  end

  # guard clause is here for the case where this is called by the
  # has_many :institutions callbacks
  def commit
    return unless should_commit

    IndexingService.commit
  end
end
