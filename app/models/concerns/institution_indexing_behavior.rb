# frozen_string_literal: true

require 'active_support/concern'

# Helper methods for indexing Institution data
module InstitutionIndexingBehavior
  extend ActiveSupport::Concern

  def name_suggest
    name
  end

  def show_in_wayfinder
    active && !special && !test_site
  end

  def index_institution
    active?
  end

  def custom_link(label_field, url_field)
    url = send url_field

    return unless url

    label = send(label_field) || url

    "<a href=\"#{url}\">#{label}</a>"
  end

  def link1
    custom_link :institution_url_label_1, :institution_url_1
  end

  def link2
    custom_link :institution_url_label_2, :institution_url_2
  end

  def link3
    custom_link :institution_url_label_3, :institution_url_3
  end

  def link4
    custom_link :institution_url_label_4, :institution_url_4
  end

  def link5
    custom_link :institution_url_label_5, :institution_url_5
  end

  # @return [FeaturedService]
  def featured_service
    @featured_service ||= FeaturedService.new self
  end
end