# frozen_string_literal: true

# base app model
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # Generate an Array for use in a select box
  # @param [Object] things
  # @param [Symbol] method
  # @return [Array]
  def self.options_for(things, method, attribute: 'id')
    things.collect { |t| [t.send(method), t.send(attribute)] }
  end
end
