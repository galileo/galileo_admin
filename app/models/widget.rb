# frozen_string_literal: true

# Represents an Institution Widget
class Widget < ApplicationRecord
  # region relationships
  belongs_to :institution, inverse_of: :widgets
  # endregion

  # region enums
  enum service: {
    libchat: 0,
    libraryh3lp: 1
  }
  # endregion

  #region constants
  DEFAULT_DISPLAY_NAME = 'Ask a Librarian'.freeze
  # endregion

  #region callbacks
  before_save :set_default_display_name
  before_save :credentials_remove_trailing
  after_save :reindex_institution
  after_destroy :reindex_institution
  #endregion

  #region validations
  validates :service, presence: true, uniqueness: {scope: :institution_id, message: "already exists for this institution"}
  validate :credentials_url
  # endregion

  #region scopes
  scope :active, -> { where(active: true) }
  #endregion


  # region class_methods
  def self.services_for_select
    [
      ['Libchat', :libchat]
    ]
  end
  # endregion
  # region public_methods
  def reindex_institution
    Sunspot.index institution
  end

  def parse_credentials
    return unless self.libchat? && valid_url?(credentials)
    uri = URI.parse(credentials)
    parsed_credentials = uri.path.split('/').last
    self.credentials = parsed_credentials if parsed_credentials.present?
  end

  def valid_url?(string)
    uri = URI.parse(string)
    uri.is_a?(URI::HTTP) && uri.host.present?
  rescue URI::InvalidURIError
    false
  end

  def credentials_url
    return if credentials.blank?

    errors.add(:credentials, "must be valid URL") unless valid_url?(credentials)
  end

  def credentials_remove_trailing
    return if credentials.blank?

    self.credentials = credentials.sub(/\/+$/,'')
  end
  # endregion

  # region private
  private
  def set_default_display_name
    return unless display_name.blank?

    self.display_name = DEFAULT_DISPLAY_NAME
  end
  # endregion

end
