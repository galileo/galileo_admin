class BentoVersion < PaperTrail::Version
  # region includes
  include PgSearch::Model
  # endregion

  # region relationships
  belongs_to :user, foreign_key: :whodunnit, required: false
  # endregion

  # region paper_trail
  self.table_name = :bento_versions
  #If you are using Postgres, you should also define the sequence that your custom version class will use
  self.sequence_name = :bento_versions_id_seq
  # endregion

  # region scopes
  scope :text_search, ->(query) { search_for query }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at_/ then order "bento_versions.created_at #{direction}"
    when /^item_type/ then order "bento_versions.item_type #{direction}"
    when /^event/ then order "bento_versions.event #{direction}"

    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :with_item_type, lambda { |item_type|
    next unless item_type

    where(item_type: item_type)
  }

  scope :with_event, lambda { |event|
    next unless event

    where(event: event)
  }

  scope :with_user_id, lambda { |user_id|
    next unless user_id

    where(whodunnit: user_id)
  }

  scope :with_role, lambda { |role|
    next unless role
    user_ids = User.with_role(role).pluck(:id)
    where(whodunnit: user_ids)
  }

  scope :with_created_at_gte, lambda { |reference_time|
    where(created_at: reference_time..)
  }

  scope :with_created_at_lt, lambda { |reference_time|
    where(created_at: ...reference_time)
  }
  # endregion

  # region pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:object, :object_changes],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc' },
    available_filters: %i[sorted_by text_search with_item_type with_event
                          with_user_id with_role with_created_at_gte
                          with_created_at_lt]
  )
  # endregion

  # region constants
  TYPES_HASH = {
    'BentoConfig' => 'Bento Credentials',
    'ConfiguredBento' => 'Bento',
    'PredefinedBento' => 'Bento Type',
    'TemplateViewBento' => 'Default Bento'
  }.freeze
  # endregion

  #region class_methods
  def self.sort_options
    [
      %w[Newest created_at_desc],
      %w[Oldest created_at_asc]
    ]
  end

  def self.item_type_options
    TYPES_HASH.to_a.map { |type| [type.second, type.first] }
  end

  def self.display_item_type(item_type)
    TYPES_HASH[item_type]
  end
  # endregion

  # region public_methods
  def user
    User.find_by_id whodunnit if whodunnit
  end

  def fields_changed
    return [] unless %w[update].include?(event) && changeset.present?

    fields = changeset.keys - %w[updated_at created_at]
    fields
  end
  # endregion
end
