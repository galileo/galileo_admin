# frozen_string_literal: true

# represent an assignment of a resource to an institution
class Allocation < ApplicationRecord
  # region includes
  include PgSearch::Model
  include InstitutionFilterable
  # endregion

  # region relationships
  belongs_to :institution, inverse_of: :allocations
  belongs_to :resource, inverse_of: :allocations
  belongs_to :branding, inverse_of: :resources, optional: true
  has_many :allocation_categorizations
  has_many :subjects, through: :allocation_categorizations
  has_many :allocations_user_views,
           dependent: :delete_all
  has_many :user_views, through: :allocations_user_views
  has_many :vendors, through: :resource
  # endregion

  # region callbacks
  before_validation :set_default_subscription_type
  # endregion

  # region validations
  validate :valid_subscription_type
  validate :valid_branding
  # endregion

  # region filterrific
  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc' },
    available_filters:
      %i[sorted_by text_search display_state glri_state bypass_galileo_authentication_state open_athens_state
         for_vendor with_vendor_names for_subscription_type for_resource for_institution for_institution_group
         name_override_state any_link_contains]
  )
  # endregion

  # region scopes
  pg_search_scope :search_for,
                  against: %i[keywords],
                  associated_against: {
                    resource: %i[code name]
                  },
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  scope :text_search, ->(query) { search_for query }

  scope :for_indexing, lambda { includes(:subjects,
                                         branding: { image_attachment: {blob: :variant_records}},
                                         institution: {logo_attachment: {blob: :variant_records}},
                                         resource: [:formats, :subjects, :user_views,
                                                    { vendor: {logo_attachment: {blob: :variant_records}} },
                                                    { branding: { image_attachment: {blob: :variant_records}} },
                                                    { logo_attachment: {blob: :variant_records} }])
                       }
  scope :central, -> { where(resource_id: Resource.central.select(:id)) }
  scope :glri, -> { where(resource_id: Resource.glri.select(:id)) }

  scope :glri_state, lambda { |glri|
    case glri
    when 'glri' then where.not(resource: {institution_id: nil}).joins(:resource)
    when 'nonglri' then where(resource: {institution_id: nil}).joins(:resource)
    when '' then next
    else
      raise(ArgumentError, "Invalid GLRI option: #{glri}")
    end
  }

  scope :display_state, lambda { |display|
    case display
    when 'yes' then where(resource: {display: true}, display: true).joins(:resource)
    when 'no' then where.not(resource: {display: true}, display: true).joins(:resource)
    when '' then next
    else
      raise(ArgumentError, "Invalid display option: #{display}")
    end
  }

  scope :bypass_galileo_authentication_state, lambda { |bga|
    case bga
    when 'yes' then where(resource: {bypass_galileo_authentication: true}).joins(:resource)
    when 'no' then where(resource: {bypass_galileo_authentication: false}).joins(:resource)
    when '' then next
    else
      raise(ArgumentError, "Invalid bypass option: #{bga}")
    end
  }

  scope :open_athens_state, lambda { |oa|
    case oa
    when 'yes' then where(resource: {open_athens: true}).joins(:resource)
    when 'no' then where(resource: {open_athens: false}).joins(:resource)
    when '' then next
    else
      raise(ArgumentError, "Invalid open_athens option: #{oa}")
    end
  }

  scope :name_override_state, lambda { |name_overridden|
    case name_overridden
    when 'yes' then where.not(allocations: {name: ""})
    when 'no' then where(allocations: {name: ""})
    when '' then next
    else
      raise(ArgumentError, "Invalid name_override option: #{oa}")
    end
  }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    with_res = joins :resource
    with_inst = joins :institution

    case sort_option.to_s
    when /^created_at/ then order "allocations.created_at #{direction}"
    when /^updated_at/ then order "allocations.updated_at #{direction}"
    when /^resource_code/ then with_res.order "resources.code #{direction}"
    when /^resource_name/ then with_res.order "resources.name #{direction}"
    when /^institution_code/ then with_inst.order "institutions.code #{direction}"
    when /^institution_name/ then with_inst.order "institutions.name #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :for_vendor, lambda { |vendor_ids|
    next unless vendor_ids.map(&:to_s).reject(&:blank?)&.any?

    where(resource: { vendor_id: vendor_ids}).joins(:resource)
  }

  scope :with_vendor_names, lambda { |vendor_names|
    next unless vendor_names.map(&:to_s).reject(&:blank?)&.any?

    where(resource_id: Resource.where(vendor_name: vendor_names).or(Resource.where(vendor_id: Vendor.where(name: vendor_names).select(:id))).select(:id))
  }

  scope :for_resource, lambda { |resource_ids|
    next unless resource_ids.map(&:to_s).reject(&:blank?)&.any?

    where(resource_id: resource_ids)
  }

  scope :for_institution_group, lambda { |group_ids|
    next unless group_ids.map(&:to_s).reject(&:blank?)&.any?

    where(institution_id: Institution.where(institution_group_id: group_ids).select(:id))
  }

  institution_scope :for_institution

  scope :for_subscription_type, lambda { |sub_types|
    next unless sub_types.map(&:to_s).reject(&:blank?)&.any?

    where(subscription_type: sub_types)
  }

  scope :any_link_contains, lambda { |url_fragment|
    next if url_fragment.blank?
    url_fragment = url_fragment.to_s
    where(id: search_ids {
      any_of do
        with(:ip_link).string_contains url_fragment
        with(:remote_link).string_contains url_fragment
        with(:open_athens_link).string_contains url_fragment
      end
      paginate per_page: 999_999
      adjust_solr_params { |params| params[:fl] = 'id' }
    })
  }
  # endregion

  #region solr_fields
  searchable include: [:institution, { resource: %i[formats subjects user_views vendor] }] do
    string(:class, stored: true) { self.class }
    boolean(:glri) { glri? }
    boolean :smart_display, stored: true, as: 'display_bs' do
      smart_display?
    end
    boolean :active_resource, stored: true, as: 'active_bs' do
      active_resource?
    end
    boolean :oa_proxy, stored: true, as: 'oa_proxy_bs' do
      resource.oa_proxy?
    end
    boolean :bypass_authentication, stored: true, as: 'bypass_authentication_bs' do
      bypass_galileo_authentication?
    end
    boolean :bypass_open_athens, stored: true
    string :for_institution, stored: true do
      institution.code
    end

    # Resource Info
    string(:blacklight_id, stored: true)
    string(:for_resource, stored: true) { resource.code }
    string(:resource_name, stored: true) { name.present? ? name : resource.name }
    string(:vendor_name, stored: true) { vendor_name }
    string(:format, multiple: true, stored: true) { resource.format_names }
    string(:subject, multiple: true, stored: true) { all_subject_names }
    string(:user_view, multiple: true, stored: true) { user_view_names }
    string(:keywords, multiple: true, stored: true) { all_keyword_names }
    string(:resource_name_first_char, stored: true)
    integer(:subject_rank, stored: true) { all_subject_names.any? ? all_subject_names.length : 1_000_000_000 }
    text(:resource_name, stored: true) { name.present? ? name : resource.name }
    text(:vendor_name, stored: true) { vendor_name }
    text(:resource_short_description, stored: true) { resource.short_description }
    text(:resource_long_description, stored: true) { resource.long_description }
    text(:resource_keywords, stored: true) { all_keyword_names }
    text(:resource_formats) { resource.format_names }
    text(:resource_subjects) { all_subject_names }
    text(:resource_user_views) { user_view_names }
    string(:coverage_dates, stored: true, as: 'coverage_dates_us') do
      resource.coverage_dates
    end
    string(:update_frequency, stored: true, as: 'update_frequency_us') do
      resource.update_frequency
    end

    # Remote Help & Note
    string(:remote_credentials, multiple: true, stored: true,
                                as: 'remote_credentials_usm') do
      remote_credentials
    end
    string(:access_note, stored: true, as: 'access_note_us') do
      resource.access_note
    end
    # TODO: remove remote_note once GALILEO Search reads access_note_us
    string(:remote_note, stored: true, as: 'remote_note_us') do
      resource.access_note
    end
    string(:credential_display, stored: true, as: 'credential_display_us') do
      resource.credential_display
    end

    # Thumbnail
    string(:thumbnail, stored: true, as: 'thumbnail_us') { logo_thumbnail_key }

    # Sort fields
    string(:resource_name_sort)

    # Branding
    string :branding_image, stored: true, as: 'branding_image_us'
    string :branding_text, stored: true, as: 'branding_text_us'

    # Links
    string(:ip_link, stored: true)
    string(:remote_link, stored: true)
    string(:open_athens_link, stored: true)

    # Suggest
    string :name_suggest, as: 'nameSuggest'
  end
  #endregion

  # region class_methods
  def self.subscription_types
    [
      %w[Core core],
      %w[Community community],
      %w[Cost-Share costshare],
      %w[GLRI glri],
      %w[Public(GLRI) public_glri],
      %w[Public(Core) public_core],
      %w[None none]
    ]
  end

  def self.valid_subscription_type_codes
    subscription_types.map(&:second)
  end

  def self.central_subscription_types
    subscription_types.reject{|array|
      %w[glri public_glri public_core].include? array.second
    }
  end

  def self.central_subscription_type_codes
    central_subscription_types.map(&:second)
  end

  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc'],
      ['Resource Code (a-z)', 'resource_code_asc'],
      ['Resource Code (z-a)', 'resource_code_desc'],
      ['Resource Name (a-z)', 'resource_name_asc'],
      ['Resource Name (z-a)', 'resource_name_desc'],
      ['Institution Code (a-z)', 'institution_code_asc'],
      ['Institution Code (z-a)', 'institution_code_desc'],
      ['Institution Name (a-z)', 'institution_name_asc'],
      ['Institution Name (z-a)', 'institution_name_desc']
    ]
  end

  def self.institution_resources_sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc'],
      ['Resource Code (a-z)', 'resource_code_asc'],
      ['Resource Code (z-a)', 'resource_code_desc'],
      ['Resource Name (a-z)', 'resource_name_asc'],
      ['Resource Name (z-a)', 'resource_name_desc'],
    ]
  end

  def self.glri_states
    [
      %w[GLRI glri],
      %w[Non-GLRI nonglri]
    ]
  end

  def self.display_states
    [
      %w[Yes yes],
      %w[No no]
    ]
  end

  def self.bypass_galileo_authentication_states
    [
      %w[Yes yes],
      %w[No no]
    ]
  end

  def self.name_override_states
    [
      %w[Yes yes],
      %w[No no]
    ]
  end

  def self.open_athens_states
    [%w[Enabled yes], ['Not Enabled', 'no']]
  end

  def self.vendor_name_options(institution = nil)
    resource_vendor_names = if institution
                              institution.resources.pluck(:vendor_name)
                            else
                              Resource.all.pluck(:vendor_name)
                            end
    vendor_names = if institution
                     institution.vendors.pluck(:name)
                   else
                     Vendor.all.pluck(:name)
                   end
    all_vendor_names = (resource_vendor_names + vendor_names).reject(&:blank?).uniq.sort_by(&:downcase)
    all_vendor_names.map{|name| [name, name]}
  end
  # endregion

  # region public_methods
  def logo_thumbnail
    return resource.logo_thumbnail if resource.logo_thumbnail
    return resource.vendor&.logo_thumbnail if resource.vendor&.logo_thumbnail

    institution.logo_thumbnail
  end

  def logo_thumbnail_key
    logo_thumbnail&.key
  end

  # Array of credentials for indexing, only if will_show_credentials? is true
  # Prefers values stored on the Allocation over those on the Resource
  # @return [Array<String>]
  def remote_credentials
    return nil unless resource.will_display_credentials?

    [
      user_id? ? user_id : resource.user_id,
      password? ? password : resource.password
    ]
  end

  def actual_branding
    subscription_type == 'costshare' && branding ? branding : resource.branding
  end

  def branding_image
    return nil unless actual_branding

    return nil unless actual_branding.image.attached?

    actual_branding.image_thumbnail_key
  end

  def branding_text
    actual_branding&.try :text
  end

  # this regex normalizes resource name for sorting in results lists, e.g.,
  # "The Serials Directory" becomes
  # "the serials directory" and then
  # "serials directory"
  # we leave spaces in the normalized name because they're significant
  def resource_name_sort
    sort_name = name.present? ? name : resource.name
    sort_name.downcase.gsub(/[^a-z0-9 ]/, '').sub(/^(?:the |an |a )/, '')
  end

  def resource_name_first_char
    first_char = resource_name_sort[0]
    if first_char.match(/^[0-9]$/)
      '0-9'
    else
      first_char.upcase
    end
  end

  def name_with_codes
    "#{resource.name} (#{resource.code}-#{institution.code})"
  end

  def name_suggest
    resource.name
  end

  def active_institution_and_resource?
    institution.active? && resource.active?
  end

  # TODO: refactor into concern
  def link_service
    @link_service ||= LinkService.new self
  end

  def ip_link
    link_service.ip.link
  end

  def remote_link
    link_service.remote.link
  end

  def open_athens_link
    link_service.open_athens.link
  end

  def express_link
    if glri?
      "https://www.galileo.usg.edu/express?link=#{resource.code}"
    else
      "https://www.galileo.usg.edu/express?link=#{resource.code}&inst=#{institution.code}"
    end
  end

  def vendor_name
    if glri?
      resource.vendor_name
    else
      resource.vendor&.name
    end
  end

  def has_override_urls?
    ip_access_url.present? || remote_access_url.present? || open_athens_url.present?
  end

  # returns true or false based on the status of the Allocation and the Resource
  def smart_display?
    if !resource.display
      false
    else
      display
    end
  end

  def active_resource?
    resource.active?
  end

  def bypass_galileo_authentication?
    resource.bypass_galileo_authentication
  end
  alias public? bypass_galileo_authentication?

  def nonpublic?
    !public?
  end

  def glri?
    resource.glri?
  end

  def central?
    resource.central?
  end

  def blacklight_id
    return resource.code if resource.code.include? '-'

    "#{resource.code}-#{institution.code}"
  end

  def allocation
    self
  end

  def resource_name_and_code
    "#{resource.name} (#{resource.code})"
  end

  # Returns a single allocation from an institution code and a resource code
  def self.from_codes(institution_code, resource_code)
    Allocation.joins(:institution, :resource)
              .where(institutions: { code: institution_code },
                     resources: { code: resource_code })
              .first
  end

  def additional_subject_names
    subjects.collect(&:name)
  end

  def override_user_view_names
    user_views.collect(&:name)
  end

  def user_view_names
    user_views = if override_user_views
                   if user_view_ids
                     override_user_view_names
                   else
                     []
                   end
                 else
                   resource.user_view_names
                 end
  end

  def all_subject_names
    all_subjects = subjects.collect(&:name) + resource.subject_names
    all_subjects.uniq.sort
  end

  def all_keyword_names
    all_keywords = keywords + resource.keywords
    all_keywords.uniq.sort
  end

  # Returns a Sunspot Hit object for the allocation that represents the current values in SOLR
  def solr_hit
    search = Allocation.search do
      with(:for_institution, institution.code)
      with(:for_resource, resource.code)
    end

    return nil if search.total < 1

    search.hits.first
  end

  def solr_stored_hash
    hit = solr_hit

    return {} if hit.nil?

    hit.instance_variable_get(:@stored_values)
  end

  def set_default_subscription_type
    #Always set glri and public values
   if resource.glri? && resource.public?
     self.subscription_type = 'public_glri'
   elsif resource.central? && resource.public?
     self.subscription_type = 'public_core'
   elsif resource.glri?
     self.subscription_type ='glri'
   elsif resource.central? && subscription_type == 'public_core'
     self.subscription_type = 'none'
   end

   return if subscription_type.present?
   self.subscription_type = 'none'
  end

  def valid_subscription_type
    if subscription_type.blank?
      errors.add(:subscription_type, "is not allowed to be blank. Valid subscription_types: #{Allocation.valid_subscription_type_codes.join(', ')}")
      return
    end

    unless Allocation.valid_subscription_type_codes.include? subscription_type
      errors.add(:subscription_type, "'#{subscription_type}' is not valid. Valid subscription_types: #{Allocation.valid_subscription_type_codes.join(', ')}")
    end

    validate_public_glri
    validate_public_core
    validate_glri
    validate_central_sub_types
  end

  def validate_public_glri
    return unless subscription_type == 'public_glri'

    errors.add(:subscription_type, "'public_glri' can only be set on GLRI allocations") if central?
    errors.add(:subscription_type, "'public_glri' can only be set on public allocations") if nonpublic?
  end

  def validate_public_core
    return unless subscription_type == 'public_core'

    errors.add(:subscription_type, "'public_core' can only be set on non-GLRI allocations") if glri?
    errors.add(:subscription_type, "'public_core' can only be set on public allocations") if nonpublic?
  end

  def validate_glri
    return unless subscription_type == 'glri'

    errors.add(:subscription_type, "'glri' can only be set on GLRI allocations") if central?
    errors.add(:subscription_type, "'glri' can only be set on nonpublic allocations") if public?
  end

  def validate_central_sub_types
    return unless Allocation.central_subscription_type_codes.include? subscription_type

    errors.add(:subscription_type, "'#{subscription_type}' can only be set on non-GLRI allocations") if glri?
    errors.add(:subscription_type, "'#{subscription_type}' can only be set on nonpublic allocations") if public?
  end

  def valid_branding
    return unless branding_id

    return if institution.brandings.pluck(:id).include? branding_id

    errors.add(:branding_id, I18n.t('app.allocations.messages.valid_branding'))
  end
  #endregion
end
