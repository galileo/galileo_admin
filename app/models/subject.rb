# frozen_string_literal: true

# Represent a Subject for Resources
class Subject < ApplicationRecord
  include PgSearch::Model

  has_many :categorizations
  has_many :resources, through: :categorizations

  has_many :allocation_categorizations

  validates_presence_of :name
  validates_uniqueness_of :name

  # reindex allocations if a subject has been saved or destroyed
  after_commit :reindex_allocations
  before_destroy :save_allocation_ids
  after_destroy :reindex_allocations_after_destroy

  pg_search_scope :search_for,
                  against: :name,
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  def allocations
    galileo_subject_allocations.or(additional_subject_allocations)
  end

  def galileo_subject_allocations
    Allocation.where(resource_id: Categorization.where(subject_id: id).select(:resource_id))
  end

  def additional_subject_allocations
    Allocation.where(id: AllocationCategorization.where(subject_id: id).select(:allocation_id))
  end

  def self.text_search(query)
    if query.present?
      Subject.search_for query
    else
      all
    end
  end

  # Reindex all Allocations for this subject
  def reindex_allocations
    Sunspot.index! allocations.for_indexing
  end

  def save_allocation_ids
    @associated_allocation_ids = allocations.pluck(:id)
  end

  def reindex_allocations_after_destroy
    Sunspot.index! Allocation.for_indexing.where(id: @associated_allocation_ids)
  end
end
