# frozen_string_literal: true

# Represents a bento template for an inst type and view
class TemplateViewBento < ApplicationRecord
  # region Constants
  LAST_ORDER_PLACE = 9_999
  # endregion

  # region associations
  belongs_to :predefined_bento, inverse_of: :template_view_bentos
  has_many :configured_bentos, dependent: :destroy
  belongs_to :view, class_name: "UserView", primary_key: :code, foreign_key: :user_view
    # endregion

  # region callbacks
  after_create :create_configured_bentos
  after_update :update_configured_bentos
  # endregion

  # region validations
  validates :predefined_bento_id, presence: true
  validates :user_view, presence: true
  validates :template_name, presence: true
  validate :validate_user_view
  validates_uniqueness_of :predefined_bento_id, scope: %i[template_name user_view]

  # endregion

  # region paper_trail
  has_paper_trail(
    versions: {
      class_name: 'BentoVersion'
    },
    meta: {
      item_name: :title
    }
  )
  # endregion

  # region filterrific
  filterrific(
    default_filter_params: { with_template_name: 'k12', with_user_view: 'educator', sorted_by: 'default_order_asc' },
    available_filters: %i[sorted_by with_template_name with_user_view shown_default_state with_predefined_bento
                          with_service_type]
  )
  # endregion

  # region scopes
  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at/ then order "template_view_bentos.created_at #{direction}"
    when /^updated_at/ then order "template_view_bentos.updated_at #{direction}"
    when /^template_name/ then order "template_view_bentos.template_name #{direction}"
    when /^user_view/ then order "template_view_bentos.user_view #{direction}"
    when /^default_order/ then order "template_view_bentos.default_order #{direction}"
    when /^shown_by_default/ then order "template_view_bentos.shown_by_default #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :with_template_name, lambda { |template_name|
    next unless template_name

    where(template_name: template_name)
  }

  scope :with_user_view, lambda { |user_view|
    next unless user_view

    where(user_view: user_view)
  }
  scope :for_template_name_and_view, lambda { |template_name, view_type|
    with_template_name(template_name).with_user_view(view_type)
  }

  scope :with_predefined_bento, lambda { |predefined_bento_code|
    next unless predefined_bento_code

    includes(:predefined_bento).where(predefined_bentos: { code: predefined_bento_code })
  }

  scope :with_service_type, lambda { |service_type|
    next unless service_type

    includes(:predefined_bento).where(predefined_bentos: { service: service_type })
  }

  scope :shown_default_state, lambda { |state|
    next if state.blank?

    where(shown_by_default: state)
  }
  # endregion

  # region class methods
  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.shown_default_options
    [
      %w[True true],
      %w[False false]
    ]
  end

  def self.template_names
    pluck('DISTINCT template_name')
  end

  # @param user_view [String]
  def self.push_default_configured_bentos(institution, user_view, send_to_solr: true, scope: TemplateViewBento.all)
    to_add = []
    skipped = []

    slug_cache = ConfiguredBento.slug_cache_for_view(institution.id, user_view)
    bento_config_cache = institution.bento_configs.for_user_view(user_view).unique_service.index_by &:service

    scope.includes(:predefined_bento).for_template_name_and_view(institution.bento_template, user_view)
                     .each do |template_bento|
      credentials_name = template_bento.predefined_bento.service_credentials_name
      bento_config = if credentials_name.present?
                       institution.bento_configs.find_by_name(credentials_name)
                     else
                       bento_config_cache[template_bento.service_type]
                     end
      if template_bento.bento_service.requires_config? && bento_config.nil?
        # consistent with the decision for "service field prerequisites", we won't show a message saying
        # that a bento that required specific a credentials name was skipped
        skipped << { user_view: user_view,
                     service: template_bento.bento_service.code } unless credentials_name.present?
        next
      end

      cb = template_bento.new_configure_bento(institution, template_bento.default_order, bento_config)
      cb.slug_cache = slug_cache
      if cb.service_and_prereqs_match?
        to_add << cb
      # else # This is useful info but disabling it for now as it may be confusing to users
      #   fields = template_bento.predefined_bento.service_field_prerequisites.join ', '
      #   skipped << { user_view: user_view, service: "#{template_bento.bento_service.code} (#{fields})" }
      end
    end
    ConfiguredBento.bulk_insert(to_add, send_to_solr: send_to_solr)
    skipped
  end

  def service_type
    predefined_bento.service
  end

  def bento_service
    BentoService.get predefined_bento.service
  end
  # endregion

  # @param bento_config [BentoConfig]
  # @return [ConfiguredBento]
  def new_configure_bento(institution, order, bento_config = nil)
    ConfiguredBento.new(
      institution_id: institution.id,
      predefined_bento: predefined_bento,
      template_view_bento_id: id,
      bento_config: bento_config,
      slug: predefined_bento.code,
      user_view: user_view,
      shown_by_default: shown_by_default,
      order: order,
      active: true
    )
  end

  # region public methods
  def title
    "#{predefined_bento.display_name} (#{template_name}, #{user_view})"
  end
  # endregion

  # region private methods
  private

  def validate_user_view
    # Only validate if the template name is an inst type
    return unless InstitutionGroup.allowed_inst_type_codes.include? template_name

    allowed_codes = UserView.for_inst_type(template_name).pluck(:code)
    return if allowed_codes.include? user_view

    errors.add(:user_view, 
"'#{user_view}' is not a supported user view. Allowed user views: #{allowed_codes.join(', ')}")
  end

  # Creates configured_bentos from the template for all active institutions that have the required bento_config
  def create_configured_bentos
    institutions = if BentoService.get(predefined_bento.service)&.requires_config?
                     Institution.active.includes(:configured_bentos, :bento_configs)
                                .where(bento_template: template_name, bento_configs: { service: service_type })
                                .where.not(configured_bentos: { template_view_bento_id: id })
                   else
                     Institution.active.includes(:configured_bentos, :bento_configs)
                                .where(bento_template: template_name)
                                .where.not(configured_bentos: { template_view_bento_id: id })
                   end
    to_add = institutions.map do |institution|
      credentials_name = predefined_bento.service_credentials_name
      bento_config = if credentials_name.present?
                       institution.bento_configs.find_by_name(credentials_name)
                     else
                       institution.bento_configs.for_view_and_service(user_view, service_type)&.first
                     end
      new_configure_bento(institution, LAST_ORDER_PLACE, bento_config)
    end
    ConfiguredBento.bulk_insert(to_add.filter &:service_and_prereqs_match?)
  end

  def update_configured_bentos
    saved_change_to_predefined_bento_id?
    return unless saved_change_to_predefined_bento_id?

    configured_bentos.update_all(predefined_bento_id: predefined_bento_id)
    Sunspot.index configured_bentos
  end
  # endregion
end
