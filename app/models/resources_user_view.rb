# frozen_string_literal: true

# represent an assignment of a User View to an Resource
class ResourcesUserView < ApplicationRecord
  belongs_to :user_view
  belongs_to :resource
  validates_uniqueness_of :user_view_id, scope: :resource_id
end
