# frozen_string_literal: true

# Represents an Institution Branding
class Branding < ApplicationRecord
  belongs_to :institution, inverse_of: :brandings
  has_many :resources
  has_many :allocations

  # reindex allocations on save
  after_commit :reindex_allocations

  # reindex allocations on destroy
  after_destroy :reindex_allocations

  # this callback has to be declared _BEFORE_ the has_attached declaration for
  # the variant original. after_commit callbacks are executed LIFO
  after_commit do
    # preload thumbnail variant
    image_thumbnail.processed if image.attached?
  end
  has_one_attached :image

  # Validations
  validates_presence_of :name
  validates_with ImageValidator

  attribute :remove_image, :boolean, default: false
  after_save :purge_image, if: :remove_image

  def image_thumbnail
    image.variant(resize_to_limit: [250,25]) if image.attached?
  end

  def image_thumbnail_key
    image_thumbnail&.key
  end

  # Determine if the attachment (image) has changed
  # @return [TrueClass, FalseClass]
  def image_updated?
    attachment_changes.any? || remove_image
  end

  def resource_allocations
    Allocation.where(resource_id: resources)
  end

  def all_allocations
    resource_allocations.or(allocations)
  end

  # Reindex all Allocations for this brandings resources
  def reindex_allocations
    Sunspot.index! all_allocations.for_indexing
  end

  private

  def purge_image
    return unless image.attached?

    image.purge
  end

end
