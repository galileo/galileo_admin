# frozen_string_literal: true

# define authorization rules for GALILEO Admin users
class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user

    if FeatureFlags.enabled? :user_subroles
      set_new_abilities(user)
    else
      set_old_abilities(user)
    end
  end

  # Should be removed and the logic placed in initialize once the feature flag is removed
  def set_new_abilities(user)
    case user.role
    when 'admin'
      self.merge AdminAbility.new(user)
    when 'institutional'
      self.merge InstitutionalAbility.new(user)
      set_subroles(user)
    when 'helpdesk'
      self.merge HelpdeskAbility.new(user)
    else
      false
    end
  end

  def set_subroles(user)
    roles = user.subroles.reject(&:blank?)
    return unless roles.any?

    roles.each do |role|
      self.merge "#{role}_subrole_ability".classify.constantize.new(user)
    end
  end

  def set_old_abilities(user)
    case user.role
    when 'admin'
      can :manage, :all
      can :manage, :reports
      can :manage, :tools
    when 'institutional'
      all_inst_user_resource_ids = Allocation.where(institution_id: @user.institution_ids).pluck(:resource_id)
      inst_user_glri_resource_ids = Resource.where(institution_id: @user.institution_ids).pluck(:id)
      all_user_inst_group_ids = @user.institutions.collect(&:institution_group).collect{|x| x&.id}

      # can view/update Institution data for only assigned Institutions
      can %i[read edit_institutional update_institutional], Institution, id: user.institution_ids
      can %i[read edit_institutional update_institutional], Site, institution_id: user.institution_ids

      can :express_links_report, Institution, id: user.institution_ids
      can :resources_report, Institution, id: user.institution_ids
      can :glri_resources_report, Institution, id: user.institution_ids
      can :institution_serial_count_report, Institution, id: user.institution_ids

      # can update Resource data only for GLRI records user by their Institutions
      can %i[read new copy], Resource
      can :update, Resource, id: inst_user_glri_resource_ids

      # can create only GLRI resources
      can :create, Resource, institution_id: user.institution_ids

      # full control over GLRI resources for their Institutions
      can :manage, Allocation,
          institution_id: user.institution_ids,
          resource_id: all_inst_user_resource_ids

      # full control over Features for their Institutions
      can :manage, Feature, featuring_type: 'Institution',
          featuring_id: user.institution_ids

      # read rights over Feature records for institution groups
      can :read, Feature, featuring_type: 'InstitutionGroup',
          featuring_id: all_user_inst_group_ids

      can :read, InstitutionGroup, id: all_user_inst_group_ids

      # full control over Branding for their Institutions
      can :manage, Branding, institution_id: user.institution_ids

      # full control over ConfiguredBento for their Institutions
      if FeatureFlags.enabled?(:inst_user_configure_bento)
        can :manage, ConfiguredBento, institution_id: user.institution_ids
        can :read, PredefinedBento
      end
    when 'helpdesk'
      can :read, Institution
      can :express_links_report, Institution
      can :resources_report, Institution
      can :glri_resources_report, Institution
      can %i[read create update destroy diff deleted], Contact
      can :notify_current_password, Contact
      # ability to see version history for contacts
      can :show, Version
      can :deleted, Version
      can :read, PaperTrail::Version
    else
      # don't authorize any action in this case
      false
    end
  end
end

