# frozen_string_literal: true

# Represent a User View
class UserView < ApplicationRecord
  # region includes
  include PgSearch::Model
  # endregion

  # region relationships
  has_many :resources_user_views,
           dependent: :delete_all
  has_many :resources, through: :resources_user_views

  has_many :allocations_user_views,
           dependent: :delete_all
  has_many :allocations, through: :allocations_user_views
  # endregion

  # region validates
  validates_presence_of :code, :name
  validates_uniqueness_of :code, :name
  # endregion

  # region callbacks
  # reindex allocations if a user view has been saved or destroyed
  after_commit :reindex_allocations
  # endregion

  # region scopes
  scope :for_inst_type, lambda { |inst_type|
    views = if inst_type&.downcase == 'all'
              all
            else
              where('? = ANY(inst_type)', inst_type)
            end

    return views if views.any?

    where(code: 'default')
  }

  pg_search_scope :search_for,
                  against: :name,
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  scope :k12_publiclib_views, -> {where.not(code: 'default')}
  # endregion

  # region class_methods

  def self.text_search(query)
    if query.present?
      Subject.search_for query
    else
      all
    end
  end

  def self.values_for_menus(inst_type)
    views = UserView.for_inst_type inst_type
    
    views.map { |view| [view.name, view.code] }
  end

  # @return [Array]
  def self.allowed_view_codes
    UserView.all.pluck(:code)
  end
  # endregion

  #region public_methods
  # Reindex all Allocations for this subject
  def reindex_allocations
    Sunspot.index! allocations.for_indexing
  end

  def full?
    full
  end
  #endregion
end
