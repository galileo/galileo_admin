class NotifyGroup < ApplicationRecord
  include PgSearch::Model

  has_many :institutions

  validates_presence_of :code, :name
  validates_uniqueness_of :code
  validates :change_dates, date_array: true
  validates :notify_dates, date_array: true

  pg_search_scope :search_for,
                  against: %i[code name],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  def self.text_search(query)
    if query.present?
      NotifyGroup.search_for query
    else
      all
    end
  end
end
