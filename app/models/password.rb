# frozen_string_literal: true

# represent a GALILEO Password
class Password < ApplicationRecord
  include PgSearch::Model

  validates_presence_of :password
  validates_uniqueness_of :password

  pg_search_scope :search_for,
                  against: :password,
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  def self.text_search(query)
    if query.present?
      Password.search_for query
    else
      all
    end
  end

  # return next available password
  def self.next_available
    where(exclude: false).order('commit_date ASC NULLS FIRST').limit(1).first
  end

  # set commit date to current date and save the record
  def commit!
    self.commit_date = Date.today
    save
  end
end
