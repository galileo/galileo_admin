# frozen_string_literal: true

# represent an assignment of a Subject to an Allocation
class AllocationCategorization < ApplicationRecord
  belongs_to :subject
  belongs_to :allocation
end
