# frozen_string_literal: true

# represent an assignment of a User View to an Resource
class BannersUserView < ApplicationRecord
  belongs_to :user_view
  belongs_to :banner
  validates_uniqueness_of :user_view_id, scope: :banner_id
end
