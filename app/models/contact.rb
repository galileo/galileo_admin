# frozen_string_literal: true

# represent a Contact
class Contact < ApplicationRecord
  # region includes
  include PgSearch::Model
  include InstitutionFilterable
  # endregion

  # region relationships
  belongs_to :institution, required: false
  belongs_to :vendor, required: false
  belongs_to :user, required: false, primary_key: 'email', foreign_key: 'email'
  # endregion

  # region validations
  validates_presence_of :first_name, :last_name, :email
  validates_uniqueness_of :email, scope: [:institution_id, :vendor_id ], case_sensitive: false
  validate :association_type
  validate :allowed_types
  # endregion

  # region gem_configs
  has_paper_trail
  strip_attributes
  # endregion

  # region scopes
  scope :text_search, ->(query) { search_for query }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at/ then order "contacts.created_at #{direction}"
    when /^updated_at/ then order "contacts.updated_at #{direction}"
    when /^first_name/ then order "contacts.first_name #{direction}"
    when /^last_name/ then order "contacts.last_name #{direction}"
    when /^email/ then order "contacts.email #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :institution_contacts, -> {where.not(institution_id: nil)}
  scope :vendor_contacts, -> {where.not(vendor_id: nil)}
  scope :no_association_contacts, -> {where(institution_id: nil, vendor_id: nil)}

  institution_scope :with_institution

  scope :with_vendor, lambda { |vendor_id|
    next unless vendor_id

    where(vendor_id: vendor_id)
  }

  scope :with_associated_type, lambda { |associated_type|
    next if associated_type.blank?

    case associated_type
    when 'institution'
      institution_contacts
    when 'vendor'
      vendor_contacts
    when 'none'
      no_association_contacts
    else
      next
    end
  }

  scope :with_type, lambda { |*contact_types|
    types = contact_types.flatten.reject(&:blank?)
    next if types&.empty?
    where('types && ARRAY[?]::varchar[]', types)
  }
  # endregion

  # region pg_search and filterrific
  pg_search_scope :search_for,
                  against: %i[first_name last_name email types],
                  associated_against: {
                    institution: %i[code name]
                  },
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc' },
    available_filters:
      %i[sorted_by text_search with_associated_type with_institution with_vendor with_type]
  )
  # endregion

  #region class_methods
  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  # Used to populate user emails for filteriffic
  def self.options_for_select
    order('LOWER(email)').map { |e| [e.email, e.id] }
  end

  def self.associated_types
    [
      %w[Institution institution],
      %w[Vendor vendor],
      %w[None none],
    ]
  end

  def self.allowed_types
    [
      %w[Password pass],
      %w[Primary prim],
      %w[EDS eds],
      %w[GLRI glri],
      %w[GKR gkr],
      %w[IdP idp],
      %w[Institutional inst],
      %w[Inst-General gen],
      %w[Inst-Advanced adv],
      %w[Inst-Portal port],
      %w[Vendor-Sales sales],
      %w[Vendor-Support support],
      %w[Vendor-Training training]
    ]
  end

  def self.allowed_type_codes
    Contact.allowed_types.map { |t| t[1] }
  end
  # endregion

  # region public_methods
  def full_name
    first_name + ' ' + last_name
  end

  def name
    full_name
  end

  def password?
    types.include? 'pass'
  end

  def glri?
    types.include? 'glri'
  end

  def primary?
    types.include? 'prim'
  end

  def types=(types)
    super types.reject(&:blank?)
  end

  def associated_name
    if institution_id
      institution.name
    elsif vendor_id
      vendor.name
    end
  end
  # endregion

  # region private_methods
  private

  def allowed_types
    return unless (types - Contact.allowed_type_codes).any?

    errors.add(:types, 'is not in the permitted list')
  end

  def association_type
    return unless institution_id && vendor_id

    errors.add(:institution_id, "institution and vendor can't both be set")
    errors.add(:vendor_id, "institution and vendor can't both be set")
  end
  # endregion
end
