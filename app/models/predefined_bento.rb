# frozen_string_literal: true


# Represents a predefined bento for a service and user view
class PredefinedBento < ApplicationRecord

  PROTECTED_CODES = %w[databases eds_custom eds_resource research_starters].freeze
  ADMIN_ONLY_CODES = %w[eds_custom].freeze
  DISPLAY_NAME_MAX_LEN = 50
  DESCRIPTION_MAX_LEN = 1000

  # region includes
  include PgSearch::Model
  # endregion

  # region associations
  has_many :configured_bentos # effectively, dependent: destroy using the :destroy_configured_bentos action below
  has_many :institutions, through: :configured_bentos
  has_many :template_view_bentos, dependent: :delete_all
  # endregion

  # region callbacks
  after_save :reindex_configured_bentos
  before_destroy :destroy_configured_bentos
  # endregion

  # region validations
  validates :service, presence: true
  validates :code, presence: true
  validates :display_name, presence: true
  validates_length_of :display_name, maximum: DISPLAY_NAME_MAX_LEN
  validates_length_of :description, maximum: DESCRIPTION_MAX_LEN
  validate :allowed_bento_services
  #endregion

  # region paper_trail
  has_paper_trail(
    versions: {
      class_name: 'BentoVersion'
    },
    meta: {
      item_name: :display_name
    }
  )
  # endregion

  #region filterrific
  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc' },
    available_filters: %i[sorted_by text_search with_service]
  )
  # endregion

  # region scopes
  pg_search_scope :search_for,
                  against: %i[code display_name admin_note description],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  scope :text_search, ->(query) { search_for query }

  scope :alphabetic, -> { order('LOWER(predefined_bentos.display_name)') }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at/ then order "predefined_bentos.created_at #{direction}"
    when /^updated_at/ then order "predefined_bentos.updated_at #{direction}"
    when /^service/ then order "predefined_bentos.service #{direction}"
    when /^code/ then order "predefined_bentos.code #{direction}"
    when /^display_name/ then order "LOWER(predefined_bentos.display_name) #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }
  scope :with_service, lambda { |service|
    next unless service

    where(service: service)
  }
  # endregion

  # region class methods

  def self.menu_values
    PredefinedBento.all.alphabetic.map { |bs| [bs.disambiguated_name, bs.code] }
  end

  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.supported_params_by_service(service_code)
    case service_code
    when 'eds_api'
      return [
        'Additional Limiters',
        'Has Parent Publication',
        'Equivalent Limiter Clause',
        'Limit to Custom Catalog',
        'Bibliographic Info Style',
        'Hide Library Catalog Only',
        'Hide Peer Reviewed Journals'
      ], {
        'Source Type Facet': [], 'Content Provider Facet': []
      }
    when 'eds_publications'
      return ['Profile Name']
    else
      []
    end
  end

  def self.definition_display_order(service_code)
    case service_code
    when 'eds_api'
      return [
        'Source Type Facet',
        'Content Provider Facet',
        'Additional Limiters',
        'Has Parent Publication',
        'Equivalent Limiter Clause',
        'Bibliographic Info Style',
        'Limit to Custom Catalog'
      ]
    else
      nil
    end
  end

  # endregion

  #region public methods
  def service_field_prerequisites
    if service == 'eds_api'
      prereqs = []
      if definition&.dig 'Limit to Custom Catalog'
        prereqs << 'Custom Catalog Number'
      end
      return prereqs
    end
    []
  end
  def definition_display_order
    PredefinedBento.definition_display_order service
  end

  def sorted_definition
    unsorted = definition&.clone ||  {}
    key_order = PredefinedBento.definition_display_order service
    return unsorted if key_order.nil?
    sorted = {}
    key_order.each do |k|
      sorted[k] = unsorted.delete(k) if unsorted.include? k
    end
    sorted.merge(unsorted)
  end

  def service_name
    BentoService.get(service)&.display_name
  end

  def has_definition
    !definition.nil? && definition.size > 0
  end

  def disambiguated_name
    "#{display_name} (#{code})"
  end

  def destroy_configured_bentos
    ConfiguredBento.bulk_destroy configured_bentos.all
  end

  def protected?
    PROTECTED_CODES.include? code
  end
  # endregion

  #region private methods
  private

  def reindex_configured_bentos
    if saved_change_to_code?
      ConfiguredBento.transaction do
        configured_bentos.each do |cb|
          cb.update_slug
          # saving with update_columns avoids callbacks
          # ...which is okay because we reindex in bulk below
          cb.update_columns slug: cb.slug
        end
      end
    end
    Sunspot.index configured_bentos.for_indexing
  end

  # Returns all allowed bento types as an array
  def allowed_bento_service_codes
    BentoService.all.map &:code
  end

  # Custom verification for site types
  def allowed_bento_services
    return if allowed_bento_service_codes.include? service

    errors.add(:service,
               "'#{service}' is not a supported type. Allowed types: #{allowed_bento_service_codes.join(', ')}")
  end
  #endregion
end
