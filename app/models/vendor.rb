# frozen_string_literal: true

# Represent a Vendor for a Resource or set of Resources
class Vendor < ApplicationRecord
  include PgSearch::Model

  # this callback has to be declared _BEFORE_ the has_one_attached declaration for
  # the variant original. after_commit callbacks are executed LIFO
  after_commit do
    # preload thumbnail variant
    logo_thumbnail.processed if logo.attached?
    reindex_allocations if @reindex_allocations
  end
  has_one_attached :logo
  validates_with LogoValidator

  has_many :contacts
  has_many :resources
  has_many :allocations, through: :resources
  has_many :institutions, through: :allocations
  attribute :remove_logo, :boolean, default: false
  after_save :purge_logo, if: :remove_logo
  after_save :should_reindex_allocations?, if: :logo_updated?

  pg_search_scope :search_for,
                  against: %i[code name],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc' },
    available_filters:
      %i[sorted_by text_search]
  )

  # Scopes
  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at/ then order "vendors.created_at #{direction}"
    when /^updated_at/ then order "vendors.updated_at #{direction}"
    when /^code/ then order "vendors.code #{direction}"
    when /^name/ then order "vendors.name #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :name_sort, -> { order(name: :asc) }

  # Determine if attachment (logo) was updated
  def logo_updated?
    attachment_changes.any? || remove_logo
  end

  def should_reindex_allocations?
    @reindex_allocations = true
  end

  def reindex_allocations
    allocations = Allocation.where(resource_id: resource_ids)
    Sunspot.index! allocations.for_indexing
  end

  def name_with_code
    "#{name} (#{code})"
  end

  def logo_thumbnail
    logo.variant(resize_to_limit: [80,70]) if logo.attached?
  end

  def logo_thumbnail_key
    logo_thumbnail&.key
  end

  def self.text_search(query)
    if query.present?
      Vendor.search_for query
    else
      all
    end
  end

  def self.options_for_select
    order('LOWER(name)').map { |e| [e.name, e.id] }
  end

  private

  def purge_logo
    return unless logo.attached?

    logo.purge
  end
end
