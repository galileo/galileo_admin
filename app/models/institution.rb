# frozen_string_literal: true

# represent a GALILEO Institution
class Institution < ActiveRecord::Base
  #region includes
  include AllocationIndexingBehavior
  include SortingFilteringInstitution
  include InstitutionsHelper
  include LegacyDatesHelper
  include InstitutionIndexingBehavior
  include Featuring
  include DisplayFeatures
  # endregion

  #region relationships
  has_and_belongs_to_many :users
  has_and_belongs_to_many :legacy_institution_groups, class_name: 'InstitutionGroup'

  # New single valued Institution Group
  belongs_to :institution_group, required: false

  has_many :glri_resources, foreign_key: :institution_id, class_name: 'Resource'
  has_many :contacts
  has_many :allocations
  has_many :stats_snapshots
  has_many :resources, through: :allocations
  has_many :vendors, through: :resources
  has_many :banners

  belongs_to :notify_group, required: false
  has_many :sites
  has_many :brandings
  has_many :widgets
  has_many :bento_configs
  has_many :configured_bentos
  has_many :predefined_bentos, through: :configured_bentos

  attribute :remove_logo, :boolean, default: false
  #endregion

  # region enums
  enum view_switching: {
    on: 0,
    medium: 1,
    strict: 2,
    off: 4
  }
  # endregion

  # region callbacks
  before_validation :sanitize_name
  before_validation :default_bento_template_to_inst_type
  after_save :purge_logo, if: :remove_logo

  # Updates to IP cache on model changes.
  after_save :update_ips_in_cache
  after_destroy :remove_ips_in_cache

  # sets GLRI record to inactive if the institution is set to inactive
  after_save :deactivate_glri_resources, if: :marked_inactive?

  # remove all Allocations if marked inactive
  after_save :purge_allocations, if: :marked_inactive?

  # reindex allocations if institution thumbnail has changed
  after_save :reindex_allocations, if: :logo_updated?

  # change all configured bentos to new template if bento template has changed
  after_save :convert_bentos_to_new_template!, if: :saved_change_to_bento_template?
  # endregion

  #region validations
  validates_inclusion_of :glri_participant,
                         in: :glri_types, allow_blank: false, allow_nil: false
  validates_presence_of :code, :name
  validates_uniqueness_of :code, :name
  validates :ip_addresses, ip_array: true
  validate :unique_ip_addresses
  validates :local_ez_proxy_url, presence: true, if: :use_local_ez_proxy
  validate :oa_proxy_org_is_proxy_enabled
  validates :code, format: { with: /\A[a-z0-9]{4}\z/,
                             message: I18n.t('app.institution.messages.validate_code') }
  validates_with LogoValidator

  # Password-related validations
  validates :change_dates, date_array: true
  validates :notify_dates, date_array: true
  validate :valid_current_password
  validate :valid_new_password

  # OpenAthens configuration related validation
  with_options if: :open_athens do
    validates_presence_of :open_athens_scope
    validates_uniqueness_of :open_athens_subscope, scope: :open_athens_scope, allow_blank: true
    validates_presence_of :open_athens_org_id
    validates_uniqueness_of :open_athens_org_id
    validates_presence_of :open_athens_entity_id
    validate :open_athens_and_oa_proxy
    validate :unique_open_athens_subscopes
  end
  #endregion

  #region logo config
  # this callback has to be declared _BEFORE_ the has_one_attached declaration
  # for the variant original. after_commit callbacks are executed LIFO
  after_commit do
    unless self.destroyed?
      # preload thumbnail variant
      logo_thumbnail.processed if logo.attached?
      Sunspot.index! self
    end
  end
  # this adds an after_commit callback to upload the image to storage
  # no variants can be uploaded until the original has been uploaded
  has_one_attached :logo
  #endregion

  # region gem configs
  has_paper_trail
  strip_attributes
  # endregion

  # region scopes
  scope :for_indexing, lambda { includes(:institution_group, logo_attachment: :blob) }

  # Not test records or special class institutions
  scope :real, -> { where(special: false, test_site: false) }
  scope :name_sort, -> { order(name: :asc) }
  scope :with_brandings, lambda {
    where("EXISTS (SELECT 1 from brandings WHERE institutions.id =
          brandings.institution_id)").order(:name).includes(:brandings)
  }
  scope :glri_managed, -> { where.not(glri_participant: 'none') }
  scope :glri_locally_managed, -> { where(glri_participant: 'locally') }
  scope :glri_galileo_managed, -> { where(glri_participant: 'galileo') }

  scope :public_libraries, -> { includes(:institution_group).where(institution_groups: { inst_type: 'publiclib' }) }
  #endregion

  # region solr_fields
  searchable include: [institution_group: :features] do
    string(:class, stored: true) { self.class }
    string :code, stored: true
    string :name, stored: true
    string(:name_lower) { name.downcase }
    string :public_code, stored: true
    string :type, stored: true
    string(:institution_group_code, stored: true) { institution_group&.code }
    string(:current_passphrase, stored: true) { current_password }
    boolean :active, stored: true
    boolean(:glri, stored: true) { glri? }
    string :view_switching, stored: true

    # index the GALILEO EZproxy value for auto-login
    string(:galileo_ez_proxy_url, as: 'gal_proxy_us')

    # OpenAthens stuff
    boolean :open_athens, stored: true
    boolean :oa_proxy, stored: true
    string :oa_proxy_org, stored: true
    string :open_athens_scope, stored: true
    string :open_athens_api_name, stored: true
    string :open_athens_subscope_array, stored: true, multiple: true, as: 'open_athens_subscope_sms'
    string :open_athens_org_id, stored: true
    string :open_athens_entity_id, stored: true

    # EDS stuff
    string :eds_profile_default, stored: true, as: 'eds_profile_default_us'
    string :eds_profile_kids, stored: true, as: 'eds_profile_kids_us'
    string :eds_profile_teen, stored: true, as: 'eds_profile_teen_us'
    string :eds_profile_high_school, stored: true, as: 'eds_profile_high_school_us'
    string :eds_customer_id, stored: true

    string :journals_link, as: 'journals_link_us'

    string(:thumbnail, stored: true, as: 'thumbnail_us') { logo_thumbnail_key }

    # PINES codes
    string :pines_codes, stored: true, multiple: true

    # LTI fields
    string :lti_consumer_key, stored: true
    string :lti_secret, stored: true
    string :lti_custom_inst_id, stored: true

    # Customization stuff
    # Links
    string :link1, as: 'link1_us'
    string :link2, as: 'link2_us'
    string :link3, as: 'link3_us'
    string :link4, as: 'link4_us'
    string :link5, as: 'link5_us'
    boolean :zotero_bib, stored: true

    # Suggestions field
    string :name_suggest, as: 'instSuggest'

    # Wayfinder
    boolean :show_in_wayfinder
    string(:wayfinder_keywords, stored: true) { solr_wayfinder_keywords }

    string(:widgets, stored: true) { active_widgets_hash.to_json}
  end
  #endregion

  # region class_methods
  # these are indexed attributes on the Allocation(confusing now that Institutions are also indexed)
  def self.indexed_attributes
    %w[code open_athens open_athens_scope open_athens_api_name open_athens_subscope open_athens_subscope_array
       oa_proxy oa_proxy_org galileo_ez_proxy_url local_ez_proxy_url use_local_ez_proxy
       eds_profile_default eds_profile_kids eds_profile_teen eds_profile_high_school]
  end

  def self.text_search(query)
    if query.present?
      Institution.search_for query
    else
      all
    end
  end
  def self.options_for_select
    order('LOWER(name)').map { |e| [e.name, e.id] }
  end
  # endregion

  #region public_methods
  def core_resource_ids
    resources.central.pluck(:id)
  end

  # return an Institution Type based on Institution Group Values
  # TODO: change type to inst_type where it's used
  # NOTE: don't use this method for new code--use inst_type instead
  def type
    return 'none' unless institution_group

    institution_group.inst_type
  end

  # return an Institution Type based on Institution Group Values
  def inst_type
    return 'none' unless institution_group

    institution_group.inst_type
  end

  def k12?
    institution_group&.k12?
  end

  def publiclib?
    institution_group&.publiclib?
  end

  # Determine if the attachment (logo) has changed
  # @return [TrueClass, FalseClass]
  def logo_updated?
    attachment_changes.any? || remove_logo
  end

  # Reindex all Allocations for this institution
  def reindex_allocations
    Sunspot.index! allocations.for_indexing
  end

  # @return [ActiveRecord::Relation<Allocation>]
  def institutional_allocations
    allocations.includes(:resource).where.not(resources: { institution_id: nil })
  end

  # @return [ActiveRecord::Relation<Allocation>]
  def core_allocations
    allocations.includes(:resource).where(resources: { institution_id: nil })
  end

  def ip_number
    ip_addresses
  end

  # @return [String]
  def name_with_code
    "#{name} (#{code})"
  end

  # @return [Hash]
  def response_json
    { code: code, name: name, public_code: public_code,
      passphrase: current_password }
  end

  def glri?
    glri_participant != 'none'
  end

  def proxy_url
    use_local_ez_proxy ? local_ez_proxy_url : galileo_ez_proxy_url
  end

  def password_contacts
    contacts.where('? = ANY (types)', 'pass')
  end

  # TODO: ugly
  def next_change_date(comp = now_mmdd, for_display: false)
    dates = notify_group&.change_dates || change_dates
    next_date = nil
    dates.each do |date|
      if comp < date
        next_date = date
        break
      end
    end
    if for_display
      if next_date
        mmdd_display(next_date)
      else
        mmdd_display(dates[0], next_year: true)
      end
    else
      next_date || dates[0]
    end
  end

  def next_next_change_date(comp = now_mmdd, for_display: false)
    next_change_date(next_change_date(comp), for_display: for_display)
  end

  def valid_current_password
    return if current_password.blank?

    institution = anyone_else_using_password? current_password
    errors.add(:current_password, "Password in use by #{institution.code}") if institution
  end

  def valid_new_password
    return if current_password.blank?

    institution = anyone_else_using_password? new_password
    errors.add(:new_password, "Password in use by #{institution.code}") if institution
  end

  def anyone_else_using_password?(password)
    Institution.active.where.not(code: code)
            .where('lower(current_password) = lower(?)', password)
      .or(Institution.active.where.not(code: code)
            .where('lower(new_password) = lower(?)', password))
      .first
  end

  def open_athens_and_oa_proxy
    return unless open_athens and oa_proxy

    errors.add(:oa_proxy, 'OpenAthens and OAProxy cannot both be true')
  end

  def oa_proxy_org_is_proxy_enabled
    return unless oa_proxy_org

    oa_proxy_orgs = Institution.active.where(oa_proxy: true).pluck(:code)
    return if oa_proxy_orgs.include? oa_proxy_org

    errors.add(:oa_proxy_org, "#{oa_proxy_org} must be active with OAProxy: true")
  end

  def unique_open_athens_subscopes
    insts = Institution.where.not(code: code).where("open_athens_subscope_array && ARRAY[?]::varchar[]", open_athens_subscope_array)
    return unless insts.any?
    inst = insts.first
    overlap = open_athens_subscope_array & inst.open_athens_subscope_array
    errors.add(:open_athens_subscope, "#{overlap} is already in use at #{inst.name} (#{inst.code})")
  end

  # Update the institutions ips in the IP Cache.
  def update_ips_in_cache

    # institution must be active/ip_authentication:true for IPs to be in IpService
    if active && ip_authentication

      # if either of these was changed (to true) then all ips must be added
      if previous_changes['active'] || previous_changes['ip_authentication']
        IpService.add_ip_ranges ip_addresses, code

      # otherwise, institution may have had IPs in IpService, so change them
      elsif previous_changes['ip_addresses']
        old, new = previous_changes['ip_addresses']
        ips_to_remove = old - new
        ips_to_add = new - old

        # remove ips first (before adding), so we don't remove crossover ranges
        IpService.delete_ip_ranges ips_to_remove
        IpService.add_ip_ranges ips_to_add, code
      else
        # (nothing pertinent to IpService changed)
      end

    # institution not active/ip_authentication:true, but IPs changed
    # (in case 'active'/'ip_authentication' also set to false)
    elsif previous_changes['ip_addresses']
      old, new = previous_changes['ip_addresses']
      ips_to_remove = old - new

      # so remove deleted IPs
      IpService.delete_ip_ranges ips_to_remove

      # then remove remaining IPs
      remove_ips_in_cache

    # institution not active/ip_authentication:true, and IPs not changed
    else
      remove_ips_in_cache
    end
  end

  # Removes ips in cache
  def remove_ips_in_cache
    IpService.delete_ip_ranges ip_addresses
  end

  def marked_inactive?
    previous_changes['active'] && !active?
  end

  def purge_allocations
    allocations.destroy_all
    IndexingService.commit
  end

  def deactivate_glri_resources
    glri_resources.update active: false
  end

  # Returns the institution code in the galileo_ez_proxy_url if it's different
  # than it's own
  def ezproxy_institution_code
    return if galileo_ez_proxy_url.blank?

    if galileo_ez_proxy_url =~ /\.proxygsu-(\w+)\./
      ezproxy_inst_code = $1
      ezproxy_inst_code if ezproxy_inst_code != code
    end
  end

  # Institution ip_addresses in the ezproxy format
  def ezproxy_ip_addresses
    ip_addresses.map do |ip|
      IpService.ezproxy_format ip
    end
  end

  def logo_thumbnail
    logo.variant(resize_to_limit: [80,70]) if logo.attached?
  end

  def logo_thumbnail_key
    logo_thumbnail&.key
  end

  def purge_logo
    return unless logo.attached?

    logo.purge
  end

  def vendor_names
    resources.select(:vendor_name).distinct.map(&:vendor_name).compact.sort
  end

  # Returns a Sunspot Hit object for the allocation that represents the current values in SOLR
  def solr_hit
    search = Institution.search do
      with(:code, code)
    end

    return nil if search.total < 1

    search.hits.first
  end

  def solr_stored_hash
    hit = solr_hit

    return {} if hit.nil?

    hit.instance_variable_get(:@stored_values)
  end

  # @return [Array<BentoService>]
  def supported_bento_services
    BentoService.all_unconfigurable + bento_configs.pluck(:service).uniq.map { |code| BentoService.get code }
  end

  # @return [ActiveRecord::Relation<PredefinedBento>]
  def supported_predefined_bentos
    service_codes = supported_bento_services.map { |s| s&.code }
    PredefinedBento.where(service: service_codes)
  end

  # Restore Bentos from Defaults. If a user_view is supplied, only bentos for that view will be restored. Otherwise
  # bentos for all view will be restored. If preserve_institution_created_bentos is set to true, the bentos created by an
  # institution will not be removed during the process, but their order will be changed to last.
  # @param user_view String
  # @param preserve_institution_created_bentos boolean
  # @return [Array<Hash>] skipped items
  def restore_defaults_configured_bentos!(user_view: nil, preserve_institution_created_bentos: true)
    user_views = valid_user_views(user_view)

    destroy_institution_bentos(user_views, preserve_institution_created_bentos)

    if preserve_institution_created_bentos
      configured_bentos.created_by_state('institution').update_all(order: TemplateViewBento::LAST_ORDER_PLACE)
    end

    skipped = user_views.map do |user_view|
      TemplateViewBento.push_default_configured_bentos self, user_view, send_to_solr: false
    end
    Sunspot.index! configured_bentos.for_indexing
    skipped.flatten
  end

  def active_widgets_hash
    widget_hash = {}
    widgets.active.each {|widget| widget_hash[widget.service] = widget }
    widget_hash
  end

  def convert_bentos_to_new_template!
    return unless FeatureFlags.enabled? :flexible_bento_templates

    per_view_templates = TemplateViewBento.where(template_name: bento_template).to_a.group_by {|tvb| tvb.user_view}
    by_view_and_pb = per_view_templates.transform_values {|tvbs| tvbs.group_by {|tvb| tvb.predefined_bento_id}}
    to_add_by_view = per_view_templates.transform_values {|tvbs| tvbs.pluck :id }
    to_delete = []
    configured_bentos.each do |cb|
      equivalents_from_template = by_view_and_pb.dig(cb.user_view, cb.predefined_bento_id) || []
      equivalent = equivalents_from_template.first if equivalents_from_template.size == 1
      if equivalent
        # Note, cb might be a GALILEO-managed bento or an inst-added bento
        # But, since it lines up with a bento in the new template, it's going to become a GALILEO-managed bento
        cb.update_columns template_view_bento_id: equivalent.id, updated_at: Time.now # skips callbacks
        # And we won't need to add this bento
        to_add_by_view[cb.user_view].delete equivalent.id
      elsif cb.template_view_bento_id.present?
        # GALILEO managed bento that's not in the new template. Delete it
        to_delete << cb
      end
    end
    ConfiguredBento.bulk_destroy to_delete
    to_add_by_view.each do |view, tvb_ids|
      TemplateViewBento.push_default_configured_bentos self, view, scope: TemplateViewBento.where(id: tvb_ids)
    end
  end

  def display_views?
    ViewService.institution_display_views?(inst_type)
  end
  # endregion

  #region private_methods
  private

  def unique_ip_addresses
    # Don't run if institution is inactive and no ip_addresses listed or invalid ip ranges
    return unless active && ip_addresses.any? && errors[:ip_addresses].empty?

    insts_lookup_hash = Institution.active.real.pluck(:code, :ip_addresses).map { |code, ip_addresses| [code, IpService.bookend_hash(ip_addresses)] }.to_h

    return if insts_lookup_hash.empty? # Institution db is likely empty

    ip_bookend_hash = IpService.bookend_hash ip_addresses

    insts_lookup_hash.each do |other_inst_code, other_ip_bookend_hash|
      next if code == other_inst_code

      ip_bookend_hash.each do |range_string, range_bookend|
        other_ip_bookend_hash.each do |other_range_string, other_range_bookend|
          if IpService.ranges_intersect? range_bookend, other_range_bookend
            message = "#{range_string} contains an ip address in #{other_range_string} that is registered to #{other_inst_code}"
            errors.add :ip_addresses, message: message
          end
        end
      end
    end
  end

  def sanitize_name
    name&.squish!
  end

  def default_bento_template_to_inst_type
    return if bento_template.present?

    self.bento_template = inst_type
  end

  def solr_wayfinder_keywords
    if wayfinder_keywords.is_a? Array
      wayfinder_keywords.uniq.join("\n")
    else
      wayfinder_keywords
    end
  end

  def valid_user_views(user_view)
    if user_view.blank?
      ViewService.valid_view_types_for inst_type
    else
      unless ViewService.valid_view_types_for(inst_type).include?(user_view)
        raise ArgumentError,
              "#{user_view} is not a valid user_view for an institution of type #{inst_type}"
      end
      [user_view]
    end
  end

  def destroy_institution_bentos(user_views, preserve_institution_created_bentos)
    bentos_for_destruction = if preserve_institution_created_bentos
                               configured_bentos.created_by_state 'galileo'
                             else
                               configured_bentos
                             end
    ConfiguredBento.bulk_destroy bentos_for_destruction.with_user_view user_views
  end
  #endregion
end

