class Banner < ApplicationRecord
  # region includes
  include PgSearch::Model
  include InstitutionFilterable
  # endregion

  # region enums
  enum style: {
    primary: 0,
    secondary: 1,
    success: 2,
    danger: 3,
    warning: 4,
    info: 5,
    light: 6,
    dark: 7
  }

  enum audience: {
    everyone: 0,
    institution: 1,
    institution_group: 2,
  }
  # endregion

  has_paper_trail
  has_rich_text :content

  # region relationships
  belongs_to :institution, required: false
  belongs_to :institution_group, required: false
  has_many :versions, -> { where(item_type: 'Banner').order('id') }, :foreign_key => 'item_id'
  has_many :banners_user_views,
           dependent: :delete_all
  has_many :user_views, through: :banners_user_views
  # endregion

  # region validations
  validates_presence_of :content
  validate :valid_date_range
  validate :valid_audience
  validate :content_text_length
  validate :valid_institution_user_views
  validate :valid_institution_group_user_views
  # endregion

  # region callbacks
  before_validation :cleanup_data
  # endregion

  # region solr_fields
  searchable include: [:institution] do
    string(:class, stored: true) { self.class }
    string :id, stored: true
    string :audience, stored: true
    string :user_view_codes, stored: true, multiple: true
    string(:message, stored: true) { self.content}
    time(:start_time, stored: true)
    time(:end_time, stored: true)
    string(:institution_code, stored: true) {institution&.code}
    string(:institution_group_code, stored: true){institution_group&.code}
    string(:style, stored: true)
  end
  # endregion

  # region filterrific
  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc' },
    available_filters:
      %i[sorted_by text_search with_institution with_institution_group active_state with_audience created_by_user]
  )
  # endregion

  # region scopes
  scope :text_search, ->(query) { search_for query }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^content/ then order "banners.content #{direction}"
    when /^start_time/ then order "banners.start_time #{direction}"
    when /^end_time/ then order "banners.end_time #{direction}"
    when /^created_at/ then order "banners.created_at #{direction}"
    when /^updated_at/ then order "banners.updated_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  pg_search_scope :search_for,
                  against: %i[content],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  scope :for_indexing, lambda {
    includes(:institution, :user_views)
  }
  scope :institution, -> { where.not(institution_id: nil) }
  scope :galileo, -> { where(institution_id: nil) }
  scope :active, -> do
    date = DateTime.now
    where('start_time <= ? AND end_time >= ?',date, date)
  end
  scope :inactive, -> do
    date = DateTime.now
    where('start_time > ? OR end_time < ?',date, date)
  end

  institution_scope :with_institution

  scope :active_state, lambda { |state|
    case state
    when 'active' then active
    when 'inactive' then inactive
    when 'any' then next
    else
      raise(ArgumentError, "Invalid Active State option: #{state}")
    end
  }

  scope :created_by_user, lambda { |*user_ids|
    ids = user_ids.flatten.reject(&:blank?)
    next if ids&.empty?

    joins(:versions).where(versions: {item_type: 'Banner', event: 'create', whodunnit: ids})
  }

  scope :with_audience, lambda { |audiences|
    audiences = [audiences] unless audiences.is_a? Array
    next unless audiences.reject(&:blank?).any?

    where(audience: audiences.map{|aud| Banner.audiences[aud]})
  }

  scope :with_institution_group, lambda { |group_id|
    group_id = [group_id] unless group_id.is_a? Array
    next unless group_id.reject(&:blank?).any?

    where(institution_group_id: group_id)
  }
  # endregion

  # region class_methods
  def self.styles_for_select
    [
      ['Blue', :primary],
      ['Green', :success],
      ['Red', :danger],
      ['Yellow', :warning],
      ['Cyan', :info],
      ['White', :light],
      ['Light Gray', :secondary],
      ['Dark Gray', :dark],
    ]
  end

  def self.active_states
    [['Either', ''], %w[Active active], ['Not Active', 'inactive']]
  end

  def self.audiences_for_select
    [
      ['Everyone', :everyone],
      ['Institution', :institution],
      ['Institution Group', :institution_group],
    ]
  end

  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end
  # endregion

  # region public_methods
  def active?
    DateTime.now.between?(start_time,end_time)
  end

  # Returns a Sunspot Hit object for the allocation that represents the current values in SOLR
  def solr_hit
    search = Banner.search do
      with(:id, id)
    end

    return nil if search.total < 1

    search.hits.first
  end

  def solr_stored_hash
    hit = solr_hit

    return {} if hit.nil?

    hit.instance_variable_get(:@stored_values)
  end

  def institution_name
    institution&.name
  end

  def created_by
    versions.where(event: 'create')&.first&.user
  end

  def user_view_codes
    user_views.collect(&:code)
  end
  # endregion

  # region private_methods
  private
  def valid_date_range
    if end_time < start_time
      message = "must be greater than start time"
      errors.add(:end_time, message)
    end

    if end_time > (start_time + 1.month)
      message = "can't be more than a month past the start time. Consider creating an Institution Feature for messages that need to be posted longer than a month."
      errors.add(:end_time, message)
    end

    return unless audience == 'institution'

    existing_banners = Banner.where.not(id: id).where(institution_id: institution_id, start_time: ..end_time, end_time: start_time..)
    if existing_banners.any?
      dates = "start:(#{ApplicationController.helpers.date_time_humanize existing_banners.first.start_time}) end:(#{ApplicationController.helpers.date_time_humanize existing_banners.first.end_time})"
      message = "There is already an institution banner that overlaps this date range: #{dates}"
      errors.add(:start_time, message)
    end
  end

  def valid_audience
    if audience == 'institution' && institution_id.nil?
      message = "must be selected if audience is set to institution"
      errors.add(:institution_id, message)
    end

    if audience == 'institution_group' && institution_group_id.nil?
      message = "must be selected if audience is set to institution_group"
      errors.add(:institution_group_id, message)
    end
  end

  def content_text_length
    return if content.blank?

    if content.to_plain_text.size >= 150
      message = "must be less than or equal to 150 characters"
      errors.add(:content, message)
    end
  end

  def valid_institution_user_views
    return unless institution.present? && user_view_codes.any?

    if institution.display_views?
      valid_view_types = ViewService.valid_view_types_for institution.inst_type
      user_view_codes.each do |user_view|
        unless valid_view_types.include?(user_view)
          message = "#{user_view} is not a valid user_view for institution #{institution.code}. Valid views: #{valid_view_types}"
          errors.add(:user_views, message)
        end
      end
    else
      message = "not available for institution #{institution.code}."
      errors.add(:user_views, message)
    end
  end

  def valid_institution_group_user_views
    return unless institution_group.present? && user_view_codes.any?

    if institution_group.display_views?
      valid_view_types = ViewService.valid_view_types_for institution_group.inst_type
      user_view_codes.each do |user_view|
        unless valid_view_types.include?(user_view)
          message = "#{user_view} is not a valid user_view for institution_group #{institution_group.code}. Valid views: #{valid_view_types}"
          errors.add(:user_views, message)
        end
      end
    else
      message = "not available for institution_group #{institution_group.code}."
      errors.add(:user_views, message)
    end
  end

  def cleanup_data
    case audience
    when 'everyone'
      self.institution_id= nil
      self.institution_group_id= nil
    when 'institution'
      self.institution_group_id= nil
    when 'institution_group'
      self.institution_id= nil
    else
      raise "audience #{audience} cant be cleaned."
    end
  end
  # endregion
end
