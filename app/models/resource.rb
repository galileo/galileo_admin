# frozen_string_literal: true

# represent a GALILEO Resource
class Resource < ApplicationRecord
  # region includes
  include ActionView::Helpers::SanitizeHelper
  include AllocationIndexingBehavior
  include SortingFilteringResource
  include ActiveModel::Validations
  # endregion

  strip_attributes only: [:name, :short_description, :long_description, :vendor_name, :ip_access_url,
    :remote_access_url, :open_athens_url, :access_note, :audience, :language, :title_list_url, :product_suite, :note, :special_information]

  # region relations
  belongs_to :institution, class_name: "Institution", inverse_of: :glri_resources, optional: true
  has_many :allocations
  has_many :stats_snapshots
  has_many :institutions, through: :allocations,
                          dependent: :destroy,
                          after_remove: :sync_to_children
  has_many :categorizations
  has_many :subjects, through: :categorizations,
                      after_add: :flag_for_reindex,
                      after_remove: :flag_for_reindex
  has_many :children, class_name: "Resource", foreign_key: :parent_id
  has_and_belongs_to_many :formats,
    uniq: true,
    after_add: :flag_for_reindex,
    after_remove: :flag_for_reindex
  has_many :resources_user_views,
    dependent: :delete_all
  has_many :user_views, through: :resources_user_views,
                      after_add: :flag_for_reindex,
                      after_remove: :flag_for_reindex
  belongs_to :parent, class_name: "Resource", optional: true
  belongs_to :vendor, optional: true, counter_cache: true
  belongs_to :branding, inverse_of: :resources, optional: true

  # this callback has to be declared _BEFORE_ the has_attached declaration for
  # the variant original. after_commit callbacks are executed LIFO
  after_commit do
    # preload thumbnail variant
    logo_thumbnail.processed if logo.attached?
  end
  # Images
  has_one_attached :logo
  # endregion

  # region Validations
  validates_presence_of :institution_id, unless: :institution_ids
  validates_presence_of :code, :name
  validates_uniqueness_of :code
  validates_with OpenAthensUrlValidator
  validate :validate_ip_access_url
  validate :validate_remote_access_url
  validates :code, format: { with: /\A[a-z0-9]{4}(?::[a-z0-9_\-]+|-[a-z0-9]{4})?\z/,
                             message: I18n.t("app.resources.messages.validate_code") }
  validate :allowed_resource_category_types
  validates :serial_count, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates_with LogoValidator
  validates_with WithoutOpenAthensRedirectorValidator

  # Require the IP Authentication URL field for active Resources
  with_options if: :active do
    validates_presence_of :ip_access_url
  end

  with_options if: :will_display_credentials? do
    validate :valid_credential_state
  end

  with_options if: :branding_id do
    validate :valid_institution_branding
  end
  # endregion

  # region callbacks
  after_save :add_allocation, if: :glri?
  after_save :purge_allocations_and_update_children, if: :marked_inactive?
  after_update :update_allocations_after_change
  before_validation :sanitize_html_in_descriptions

  attribute :remove_logo, :boolean, default: false
  after_save :purge_logo, if: :remove_logo
  #endregion

  # region versioning
  has_paper_trail
  strip_attributes
  #endregion

  #region class_methods
  # Define indexed fields (see Allocation searchable block) for relation
  # indexing configuration. these attributes must be tracked and included in the
  # `saved_changes` method after #save
  def self.indexed_attributes
    %w[code name short_description long_description keywords user_id password
      access_note credential_display remove_logo update_frequency
      coverage_dates branding_id bypass_galileo_authentication
      ip_access_url remote_access_url audience open_athens without_open_athens_redirector open_athens_url
      user_id password language vendor_id display oa_proxy proxy_remote vendor_name]
  end

  # Define fields to be copied when using the Record 'copy' functionality
  def self.admin_copiable_attributes
    %w[name short_description long_description keywords subject_ids user_view_ids
      format_ids audience coverage_dates update_frequency title_list_url
      product_suite bypass_galileo_authentication ip_access_url remote_access_url open_athens_url]
  end

  # Define fields to be copied when using the Record 'copy' functionality
  def self.copiable_attributes
    %w[name short_description long_description keywords subject_ids user_view_ids
      format_ids audience coverage_dates update_frequency title_list_url
      product_suite bypass_galileo_authentication]
  end

  # Returns array of valid resource_category types for drop downs in the view
  # Note: values should be snake_case (no spaces or caps)
  def self.allowed_resource_category_types
    [
      %w[None None],
      %w[Aggregator Aggregator],
      ["Aggregator (S)", "Aggregator (S)"],
      %w[A&I A&I],
      %w[eBooks eBooks],
      %w[Reference Reference]
    ]
  end

  #region public_methods
  # @return [TrueClass, FalseClass]
  def will_display_credentials?
    credential_display != "never"
  end

  # @return [String]
  def name_with_code
    "#{name} (#{code})"
  end

  def truncated_name_with_code
    "#{name.truncate(30)} (#{code})"
  end

  # @return [String]
  def code_with_name
    "#{code} (#{name})"
  end

  def add_allocation
    return unless active

    self.institution_ids = [institution_id]
  end

  def glri?
    institution_id ? true : false
  end

  def central?
    institution_id ? false : true
  end

  def internal?
    central? && display == false
  end

  def public?
    bypass_galileo_authentication
  end

  def user_view_names
    user_views.collect(&:name)
  end

  def subject_names
    subjects.collect(&:name)
  end

  def format_names
    formats.collect(&:name)
  end

  def logo_thumbnail
    logo.variant(resize_to_limit: [80,70]) if logo.attached?
  end

  def logo_thumbnail_key
    logo_thumbnail&.key
  end

  def express_link
    "https://www.galileo.usg.edu/express?link=#{code}"
  end

  # endregion

  private

  def marked_inactive?
    previous_changes["active"] && !active?
  end

  def sync_to_children(institution)
    return unless children.any?

    children.each do |child|
      child.institutions.delete institution
    end
  end

  def purge_allocations_and_update_children
    update(institution_ids: [])
    children.each do |child|
      child.update(active: false, institution_ids: [])
    end
    IndexingService.commit
  end

  def valid_institution_branding
    return unless branding_id

    branding = Branding.find branding_id

    if branding.nil?
      errors.add(:branding_id, "Branding with id #{branding_id} not found")
      return
    end

    if institution_id.nil?
      errors.add(:branding_id, "GLRI institution must be selected")
      return
    end

    unless branding.institution_id == institution_id
      inst = Institution.find institution_id
      errors.add(:branding_id, "#{branding.name} is not a valid branding for institution #{inst.name}")
    end
  end

  def valid_credential_state
    return unless will_display_credentials?

    return unless user_id.blank? && password.blank?

    errors.add(:credential_display, "#{credential_display} can't be selected when Access User ID and Access Password are both blank")
  end

  def purge_logo
    return unless logo.attached?

    logo.purge
  end

  def sanitize_html_in_descriptions
    allowed_html = { tags: %w[strong em a p br], attributes: %w[href] }
    self.short_description = sanitize(short_description, allowed_html)
    self.long_description = sanitize(long_description, allowed_html)
  end

  def validate_ip_access_url
    validate_url_field :ip_access_url
  end

  def validate_remote_access_url
    validate_url_field :remote_access_url
  end

  #
  # @param [Symbol] field_symbol
  def validate_url_field(field_symbol)
    url = self[field_symbol]
    return if url.blank?

    unless url[0..3] == "http"
      errors.add(field_symbol, "should start with http:// or https://")
      nil
    end
  end



  # Returns all allowed resource_category types as an array
  def allowed_resource_category_type_codes
    Resource.allowed_resource_category_types.map { |t| t[1] }
  end

  # Custom verification for resource_category types
  def allowed_resource_category_types
    return if allowed_resource_category_type_codes.include? resource_category

    errors.add(:resource_category, "'#{resource_category}' is not a supported category. Allowed types: #{allowed_resource_category_type_codes.join(", ")}")
  end

  def update_allocations_after_change
    return unless saved_change_to_bypass_galileo_authentication?
    if self.glri? && self.bypass_galileo_authentication? == true
      allocations.update_all(subscription_type: 'public_glri')
    elsif self.glri? && self.bypass_galileo_authentication? == false
      allocations.update_all(subscription_type: 'glri')
    elsif self.central? && self.bypass_galileo_authentication? == true
      allocations.update_all(subscription_type: 'public_core')
    else
      allocations.update_all(subscription_type: 'none')
    end
    allocations.reload
  end
end
