# frozen_string_literal: true

# represent an assignment of a Subject to a Resource
class Categorization < ApplicationRecord
  belongs_to :subject
  belongs_to :resource
end
