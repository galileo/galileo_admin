# frozen_string_literal: true

# base mailer
class ApplicationMailer < ActionMailer::Base
  env_tag = Rails.env == "production" ? "" : "-#{Rails.env}"
  default from: "galileo-admin#{env_tag}-no-reply@galileo.usg.edu"
  layout 'mailer'
end
