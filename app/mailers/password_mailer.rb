# frozen_string_literal: true

# methods for sending GALILEO Password notifications
# NOTE: we do NOT want these emails going out to people until we are LIVE!
class PasswordMailer < ApplicationMailer
  before_action :setup, except: :password_action_summary
  before_action :password_contacts_exist?, except: :password_action_summary
  after_action :change_recipients

  ADMIN_RECIPIENT = Rails.configuration.admin_email_recipients

  # email for nightly run password generating & adhoc notifying
  def password_notification
    mail(to: @contacts, bcc: ADMIN_RECIPIENT, subject:
      I18n.t('app.password_mailer.password_notification.subject'))
  end

  # email for nightly run password swapping & notifying
  def nightly_password_change
    mail(to: @contacts, bcc: ADMIN_RECIPIENT, subject: I18n.t('app.password_mailer.nightly_password_change.subject'))
  end

  # email for on-demand notifications about current password
  def current_password
    if @contact.present?
      mail(to: @contact.email, bcc: ADMIN_RECIPIENT, subject: I18n.t('app.password_mailer.current_password.subject'))
    else
      mail(to: @contacts, bcc: ADMIN_RECIPIENT, subject: I18n.t('app.password_mailer.current_password.subject'))
    end
  end

  # email for on-demand notifications about new (next) password
  def new_password
    mail(to: @institution.password_contacts.collect(&:email),
         bcc: ADMIN_RECIPIENT,
         subject: I18n.t('app.password_mailer.new_password.subject'))
  end

  def password_action_summary
    @report = params[:report]
    mail(to: ADMIN_RECIPIENT,
         subject: I18n.t('app.password_mailer.password_action_summary.subject'))
  end

  private

  # set param values for user notification emails
  def setup
    @institution = params[:inst]
    @dates = params[:dates]
    @contact = params[:contact]
    @contacts = @institution.password_contacts.collect(&:email)
  end

  # check if password contacts exist and redirects if none present
  def password_contacts_exist?
    return true if @contacts.any?

    update_and_redirect(
      'No password contacts defined for this institution!!',
      ADMIN_RECIPIENT
    )
  end

  # if staging or dev, don't send email - instead send to some poor sucker
  def change_recipients
    return unless Rails.env.staging? || Rails.env.dev? || Rails.env.development?

    update_and_redirect(
      "This email to be sent to: #{mail.to.join(', ')}",
      ADMIN_RECIPIENT
    )
  end

  # updates to: value for email and prepends a message
  # @param [String] message to be prepended to email
  # @param [String] to email for redirecting
  def update_and_redirect(message, to)
    mail.to = [to]
    mail.body.raw_source.prepend("#{message}\n\n")
  end
end
