$(document).on('ready turbo:load', function () {
    var audienceInput = $('#banner_audience');

    audienceInput.change(function () {
        var instField = $('#banner-institution-field');
        var instInput = $('#banner_institution_id');
        var instGroupField = $('#banner-institution-group-field');
        var instGroupInput = $('#banner_institution_group_id');
        if(audienceInput.chosen().val() === 'institution'){
            instField.show(400);
            instInput.attr('disabled', false);
            instGroupField.hide(400);
            instGroupInput.attr('disabled', true);
        } else if (audienceInput.chosen().val() === 'institution_group') {
            instGroupField.show(400);
            instGroupInput.attr('disabled', false);
            instField.hide(400);
            instInput.attr('disabled', true);
        }
        else {
            instField.hide(400);
            instInput.attr('disabled', true);
            instGroupField.hide(400);
            instGroupInput.attr('disabled', true);
        }
        instInput.trigger("chosen:updated")
        instGroupInput.trigger("chosen:updated")
    });
    window.addEventListener("trix-file-accept", function(event) {
        event.preventDefault()
        alert("File attachment not supported!")
    })
});