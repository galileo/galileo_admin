// This is a manifest file that'll be compiled into application_pipeline.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require jquery3
//= require chosen-jquery
//= require popper
//= require bootstrap-sprockets
//= require filterrific/filterrific
//= require multiselect/multiselect
//= require_tree .

// Patch filterrific so we can include it everywhere, even on pages that don't have a #filterrific_filter
// ...which was supported pre-5.2.4
Filterrific.conditionallyInit = function () {
    if (document.querySelector('#filterrific_filter')) Filterrific.init();
}
document.removeEventListener('turbo:load', Filterrific.init);
document.removeEventListener('DOMContentLoaded', Filterrific.init);
document.addEventListener('turbo:load', Filterrific.conditionallyInit);
document.addEventListener('DOMContentLoaded', Filterrific.conditionallyInit);


$(document).on("turbo:before-cache", function() {
    // Remove any created chosen elements before turbo caching
    // to avoid duplication on browser nav (back, forward, reload)
    $('.chosen-container').remove();
});

$(document).on('ready turbo:load', function() {

    /*
     INACTIVE CHANGE CONFIRMATION MESSAGE
     */
    var $switch = $('.active_toggle_allocation_warning');
    $switch.on('change', function(e) {
       if($switch.prop('checked') === false) {
           $('#modal-window').modal('show');
       }else {
           $('#modal-window').modal('hide');
       }
    });

    /*
     FOR MULTISELECT - COMMON
     */

    var new_allocation_option_class = 'new-allocation';
    var removed_allocation_option_class = 'removed-allocation';

    function update_selected_count($select) {
        if($select.length === 0) { return }
        var count = $select.find('option:selected').length;
        $('#selected_count').html('<span class="badge badge-primary">' + count + ' Selected</span>')
    }

    function update_allocated_count($select) {
        if($select.length === 0) { return }
        var count = $select.find('option').length;
        $('#allocated_count').html('<span class="badge badge-primary">' + count + ' Allocated</span>')
    }

    function unselect_all_from($element) {
        $element.val([]);
    }

    function trigger_change_for($element) {
        $element.trigger('change')
    }

    function sort_options_by_selected($element) {
        $element.html($element.find('option').sort(function (a, _) {
            return a.selected ? -1 : 1;
        }))
    }

    function color_and_move_to_top($select, $options) {
        $.each($options, function(i, option) {
            if(option.classList.contains(removed_allocation_option_class)) {
                option.classList.remove(removed_allocation_option_class);
            } else {
                option.classList.add(new_allocation_option_class);
            }
            $select.prepend(option);
        })
    }

    function set_color_for($options) {
        $.each($options, function (i, option){
            if(option.classList.contains(new_allocation_option_class)) {
                option.classList.remove(new_allocation_option_class);
            } else {
                option.classList.add(removed_allocation_option_class)
            }
        });
    }

    /*
     FOR ALLOCATION MULTI-SELECT - RESOURCE PAGE
     */

    var $all_institutions = $('#institutions_select');
    var $institutions_select_to = $('#institutions_select_to');

    unselect_all_from($institutions_select_to);

    update_selected_count($all_institutions);
    $all_institutions.change(function() {
       update_selected_count($all_institutions);
    });

    update_allocated_count($institutions_select_to);
    $institutions_select_to.change(function() {
       update_allocated_count($institutions_select_to);
    });

    function select_from_institutions_list_by_code(codes) {
        var codes_array = codes.split(',');
        codes_array.filter(Boolean).forEach(function (code, _) {
            var select_code = code.trim();
            $all_institutions.find('option[data-code=' + select_code + ']')
                .prop('selected', 'selected');
            $institutions_select_to.find('option[data-code=' + select_code + ']')
                .prop('selected', 'selected');
        });
    }

    function select_from_institutions_list_by_value(values) {
        var values_array = values.split(',');
        values_array.filter(Boolean).forEach(function (value, _) {
            var select_value = value.trim();
            $all_institutions.find('option[value=' + select_value + ']')
                .prop('selected', 'selected');
        });
    }

    function select_from_allocations_list_by_value(values) {
        var values_array = values.split(',');
        values_array.filter(Boolean).forEach(function (value, _) {
            var select_value = value.trim();
            $institutions_select_to.find('option[value=' + select_value + ']')
                .prop('selected', 'selected');
        });
    }

    $('#institutions_select_clear').click(function(e) {
        unselect_all_from($all_institutions);
        unselect_all_from($institutions_select_to);
        trigger_change_for($all_institutions);
    });

    $('#select_from_institution_codes').click(function(e) {
        var codes = $('#institution_codes').val();
        select_from_institutions_list_by_code(codes);
        sort_options_by_selected($all_institutions);
        sort_options_by_selected($institutions_select_to);
        trigger_change_for($all_institutions);
        trigger_change_for($institutions_select_to);
    });

    $('#select_from_institution_group').click(function(e) {
        var values = $('#institution_groups option:selected').data('institution-ids');
        select_from_institutions_list_by_value(values);
        select_from_allocations_list_by_value(values);
        unselect_all_from($('#institution_groups'));
        sort_options_by_selected($all_institutions);
        sort_options_by_selected($institutions_select_to);
        trigger_change_for($all_institutions);
        trigger_change_for($institutions_select_to);
    });

    $all_institutions.multiselect({
        search: {
            left: '<input id="institution-list-search" type="text" name="institution-list-q" class="form-control" placeholder="Search..." />'
        },
        fireSearch: function(value) {
            return value.length > 3;
        },
        submitAllLeft: false,
        afterMoveToLeft: function($left, $right, $options) {
            trigger_change_for($left);
            trigger_change_for($right);
            set_color_for($options);
        },
        afterMoveToRight: function($left, $right, $options) {
            trigger_change_for($left);
            trigger_change_for($right);
            color_and_move_to_top($right, $options);
        }
    });

    /*
     FOR ALLOCATION MULTI-SELECT - INSTITUTION PAGE
     */

    var $all_resources = $('#resources_select');
    var $resources_select_to = $('#resources_select_to');

    unselect_all_from($resources_select_to);

    update_selected_count($all_resources);
    $all_resources.change(function() {
        update_selected_count($all_resources);
    });

    update_allocated_count($resources_select_to);
    $resources_select_to.change(function() {
        update_allocated_count($resources_select_to);
    });

    function select_from_resources_list_by_code(codes) {
        var codes_array = codes.split(',');
        codes_array.filter(Boolean).forEach(function (code, _) {
            var select_code = code.trim();
            $all_resources.find('option[data-code=' + select_code + ']')
                          .prop('selected', 'selected');
            $resources_select_to.find('option[data-code=' + select_code + ']')
                                .prop('selected', 'selected');
        });
    }

    function select_from_resources_list_by_vendor(vendor) {
        $all_resources.find('option[data-vendor-id=' + vendor + ']')
                      .prop('selected', 'selected');
        $resources_select_to.find('option[data-vendor-id=' + vendor + ']')
                      .prop('selected', 'selected');
    }

    function select_from_resources_list_by_value(values) {
        var values_array = values.split(',');
        values_array.filter(Boolean).forEach(function (value, _) {
            var select_value = value.trim();
            $all_resources.find('option[value=' + select_value + ']')
                          .prop('selected', 'selected');
            $resources_select_to.find('option[value=' + select_value + ']')
                                .prop('selected', 'selected');
        });
    }


    $('#resources_select_clear').click(function(e) {
        unselect_all_from($all_resources);
        unselect_all_from($resources_select_to);
        trigger_change_for($all_resources);
    });

    $('#select_from_resource_codes').click(function(e) {
        var codes = $('#resource_codes').val();
        select_from_resources_list_by_code(codes);
        sort_options_by_selected($all_resources);
        trigger_change_for($all_resources);
    });

    $('#select_from_vendor').click(function(e) {
        var $vendors = $('#vendors');
        var vendor = $vendors.val();
        $.each(vendor, function(i, v) {
           select_from_resources_list_by_vendor(v)
        });
        unselect_all_from($vendors);
        sort_options_by_selected($all_resources);
        sort_options_by_selected($resources_select_to);
        trigger_change_for($all_resources);
        trigger_change_for($resources_select_to);
    });

    $('#select_from_institution').click(function(e) {
        var values = $('#sibling_institutions option:selected').data('resource-ids');
        select_from_resources_list_by_value(values);
        unselect_all_from($('#sibling_institutions'));
        sort_options_by_selected($all_resources);
        sort_options_by_selected($resources_select_to);
        trigger_change_for($all_resources);
        trigger_change_for($resources_select_to);
    });

    $all_resources.multiselect({
        search: {
            left: '<input id="resource-list-search" type="text" name="resource-list-q" class="form-control" placeholder="Search..." />'
        },
        fireSearch: function(value) {
            return value.length > 3;
        },
        submitAllLeft: false,
        afterMoveToLeft: function($left, $right, $options) {
            trigger_change_for($left);
            trigger_change_for($right);
            set_color_for($options);
        },
        afterMoveToRight: function($left, $right, $options) {
            trigger_change_for($left);
            trigger_change_for($right);
            color_and_move_to_top($right, $options);
        }
    });

    /*
        FOR OTHER CHOSEN-ified MULTISELECTS
     */

    function updateFilterrificIfRelevant() {
        // Filterrific is listening to native JS change events which chosen does NOT fire
        //   when updating underlying <selects>. This function will trigger filterrific's
        //   normal form element change handler if Filterrific is present and `this` is
        //   a descendent of a filterrific form.
        // Apply this function as a change handler (using JQuery's `.change()`) for elements that
        //   are only updated via JQuery (e.g., chosen selects).
        if ($(this).closest('#filterrific_filter').length && window.Filterrific) {
            Filterrific.submitFilterForm.apply(this);
        }
    }

    $('.chosen-multiselect').chosen().change(updateFilterrificIfRelevant);

    $('.chosen-select').chosen({
        allow_single_deselect: true
    }).change(updateFilterrificIfRelevant);

    $('.chosen-select-required').chosen({
        allow_single_deselect: false
    }).change(updateFilterrificIfRelevant);


    /*
        support deeplinking to tab content
     */
    var url = window.location.href;
    if (url.indexOf("#") > 0){
        // TODO: support subtab links?
        var activeTab = url.substring(url.indexOf("#") + 1);
        $('.nav[role="tablist"] a[href="#' + activeTab + '"]').tab('show');
    }

    $('a[role="tab"]').click(function(e) {
        var hash = $(this).attr("href");
        var newUrl = url.split("#")[0] + hash;
        history.replaceState(null, null, newUrl);
    });

    var inst_field = $('#user-institutions-selection-area');
    var inst_select = $('select#user_institution_ids');
    var $role_select = $('select#user_role');
    if($role_select.val() === 'institutional' ) {
        inst_field.show();
    } else {
        inst_field.hide();
    }
    $role_select.on('change', function() {
        if ($(this).val() === 'institutional') {
            inst_field.show();
        } else {
            inst_select.val([]);
            inst_field.hide();
        }
    });


    /*
        support for file_field updates
     */
    $('.custom-file-input').change(function(e){
        var fileName = e.target.files[0].name;
        $('.custom-file-label[for=' + e.currentTarget['id'] + ']').html(fileName);
    });

    $('[data-advanced-filter] abbr').click(function () {
        var filter = $(this).closest('[data-advanced-filter]');
        var alternate = filter.next('[data-alternate-filter]').attr('data-alternate-filter', 'false');
        if (alternate.length) {
            filter.remove();
            alternate.find('input, select').attr('disabled', false)
            Filterrific.submitFilterForm.apply(alternate[0]);
        } else {
            var formGroup = filter.closest('.form-group')
            var parent = formGroup.parent();
            formGroup.remove();
            Filterrific.submitFilterForm.apply(parent[0]);
        }
    });
    $('[data-alternate-filter] input, [data-alternate-filter] select').attr('disabled', true)
});
