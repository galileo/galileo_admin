function within(selector, setupFunction) {
    $(document).on('ready turbo:load', function() {
        $(selector).each(function (i, element) {
            var scopedJQuery = function (selector) {
                return $(element).find(selector);
            };
            setupFunction(scopedJQuery, element);
        });
    });
}

function defaultBentoConfigName($serviceSelect, $viewsMultiselect) {
    var service = $serviceSelect.val();
    if (!service) return '';
    var views = $viewsMultiselect.val() || [];
    if (!views.length) return '';
    return service + '_' + views.join('_');
}

within('[data-bento-config-form]', function ($) {
    var $nameField = $('#bento_config_name');
    var initialName = $nameField.val();
    var $serviceSelect = $('#bento_config_service');
    var $viewsMultiselect = $('#bento_config_view_types');
    var defaultNameForInitialValues = defaultBentoConfigName($serviceSelect, $viewsMultiselect);
    var shouldUpdateValue = (initialName && initialName === defaultNameForInitialValues);
    var updateDefaultName = function () {
        var name = defaultBentoConfigName($serviceSelect, $viewsMultiselect);
        if (shouldUpdateValue) $nameField.val(name);
        $nameField.attr('placeholder', name);
    };
    $serviceSelect.on('change', updateDefaultName);
    $viewsMultiselect.on('change', updateDefaultName);
    $nameField.on('input', function (){
        shouldUpdateValue = false;
    });
});

within('[data-configured-bento-form]', function ($){
    $('[name="configured_bento[predefined_bento_code]"]').change(function (){
        var $option = $(this.options[this.selectedIndex]);
        $('[name="configured_bento[display_name]"]').attr('placeholder', $option.data('display_name'));
        $('[name="configured_bento[description]"]').attr('placeholder', $option.data('description'));
    });
});

within('form#reorderable-bento-list', function ($, thisForm) {

    var dirty = false;
    var pendingDeletes = [];
    var initialState = null;
    var serializeForm = function () { return 1; };
    if (window.FormData && window.URLSearchParams) {
        serializeForm = function () {
            return (new URLSearchParams(new FormData(thisForm))).toString();
        }
        initialState = serializeForm();
    }

    function updateDirtyStatus() {
        dirty = serializeForm() !== initialState;
        if (dirty) {
            $('.dirty-only').prop('disabled', false);
        } else {
            $('.dirty-only').prop('disabled', true);
        }
    }

    var recordNewState = function () {
        $('[data-bento-sublist-visible]').each(function (i, sublist) {
            var visible = $(sublist).data('bento-sublist-visible');
            $(sublist).find('.bento-draggable').filter(':visible').each(function (j, bento) {
                $(bento).find('[name$="[order]"]').attr('value', '' + (j + 1) + '.0');
                $(bento).find('[name$="[shown_by_default]"]').attr('value', visible);
            });
        });
        updateDirtyStatus();
    };

    $('input:not([type=hidden])').on('change input', updateDirtyStatus);

    var counters = [];
    var containerCounters = [];
    var concludeDrag = function () {
        for (var i = 0; i < counters.length; i++) {
            counters[i] = 0;
        }
        for (var i = 0; i < containerCounters.length; i++) {
            containerCounters[i] = 0;
        }
        $('.bento-drop-zone').removeClass('may-drop-before');
        $('.bento-sublist').removeClass('may-drop-in').removeClass('may-drop-in-directly');
    };
    $('.bento-drop-zone').each(function (i, el) {
        counters[i] = 0;
        $(el).on('dragenter', function (e) {
            counters[i]++;
            if (counters[i]) $(el).addClass('may-drop-before');
            e.preventDefault();
        }).on('dragleave', function () {
            counters[i]--;
            if (!counters[i]) $(el).removeClass('may-drop-before');
        }).on('dragover', function (e) {
            e.preventDefault();
        }).on('drop', function (e) {
            var droppedId = e.originalEvent.dataTransfer.getData("text/plain");
            $('[data-bento-config-id=' + droppedId + ']').closest('.bento-drop-zone').insertBefore(el);
            concludeDrag();
            e.preventDefault();
            e.stopPropagation();
            recordNewState();
        });
    });
    $('.bento-sublist').each(function (i, el) {
        containerCounters[i] = 0;
        $(el).on('dragenter', function (e) {
            containerCounters[i]++;
            if (containerCounters[i]) $(el).addClass('may-drop-in');
            e.preventDefault();
        }).on('dragleave', function () {
            containerCounters[i]--;
            if (!containerCounters[i]) $(el).removeClass('may-drop-in');
        }).on('dragover', function (e) {
            e.preventDefault();
        }).on('drop', function (e) {
            var droppedId = e.originalEvent.dataTransfer.getData("text/plain");
            $('[data-bento-config-id=' + droppedId + ']').closest('.bento-drop-zone').appendTo(this);
            $('.bento-drop-zone').removeClass('may-drop-before');
            concludeDrag();
            e.preventDefault();
            e.stopPropagation();
            recordNewState();
        });
    });
    $('.bento-draggable').on('dragstart', function (e) {
        e.originalEvent.dataTransfer.setData("text/plain", $(this).data('bento-config-id'));
    });

    $('[data-bento-delete-button]').on('click', function () {
       $(this).find('input').val(1);
       var $item = $(this).closest('.bento-draggable');
       var display_name = $item.find('.bento-display-name input')[0]
       pendingDeletes.push(display_name.value || display_name.placeholder);
       $item.hide();
       recordNewState();
       return false;
    });

    $('[data-discard-button]').on('click', function (){
        dirty = false;
        location.reload();
    });

    // since $ is jQuery scoped to our form, use 'jQuery' to get to the global one
    jQuery('[data-unsaved-change-confirm]').each(function (i, el) {
        var message = jQuery(el).data('unsaved-change-confirm');
        jQuery(el).on('click', function (e) {
            if (dirty) {
                if (confirm(message)) {
                    dirty = false;
                    return true;
                } else {
                    e.preventDefault();
                    return false;
                }
            }
        });
    });

    var pendingDeletesMessage = jQuery(thisForm).data('pending-deletes-confirm') || '';
    thisForm.addEventListener('submit', function (e) {
        if (pendingDeletes.length) {
            if (!confirm(pendingDeletesMessage.replace('<<items>>', joinWithAnd(pendingDeletes)))) {
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
        }
    });

});

function joinWithAnd(list) {
    if (list.length === 1) return list[0];
    if (list.length === 2) return '' + list[0] + ' and ' + list[1]
    var final = list.pop();
    return list.join(', ') + ', and ' + final;
}
