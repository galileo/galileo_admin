var formElements = 'input:not(.chosen-search-input), textarea, select';
// .chosen-search-input is the text input used internally in a chosen select

$(document).on('ready turbo:load', function () {
    // text field array
    $('[data-field-array-for]').each(function (i, arrayContainer){
        var template = $(arrayContainer).find('[data-field-template]').remove().removeAttr('data-field-template');
        $(arrayContainer).on('change input', formElements, function (e) {
            var fieldArray = $(arrayContainer).find(formElements);
            var encounteredNonEmpty = false;
            for (var i = fieldArray.length; i > 0; i--) {
                if (fieldArray[i - 1].value) {
                    encounteredNonEmpty = true;
                    if (i === fieldArray.length) {
                        $(arrayContainer).append(template.clone());
                    }
                } else if (i < fieldArray.length) {
                    if (encounteredNonEmpty) {
                        if (e.type === 'change') {
                            if (i === 1) {
                                // We want to remove the first item, but don't want to lose the first container's styles
                                // So we move in values from the second item, and delete that
                                $(fieldArray[i-1]).val($(fieldArray[i]).val());
                                $(fieldArray[i]).closest('[data-field-wrapper]').remove();
                            } else {
                                $(fieldArray[i-1]).closest('[data-field-wrapper]').remove();
                            }
                        }
                    } else {
                        $(fieldArray[i]).closest('[data-field-wrapper]').remove();
                    }
                }
            }
        });
        $(arrayContainer).on('keypress', formElements, function (e){
            if (e.which === 13) {
                var inputs = $(arrayContainer).find(formElements);
                var index = inputs.index(this);
                if (index < inputs.length - 1) {
                    e.preventDefault();
                    e.stopPropagation();
                    inputs[index + 1].focus();
                }
            }
        });
        // disable empty elements before the form is submitted to avoid sending blanks to server
        $(arrayContainer).closest('form').on('submit', function (){
            $(arrayContainer).find(formElements).each(function (i, el){
                if (!el.value) el.disabled = true;
            });
        });
    });

    // show/hide json subform depending on another field
    $('[data-subform-by]').each(function (i, element) {
        var testField = $(element).data('subform-by');
        // using .data() here may do unwanted conversions from string to int
        var testValue = $(element).attr('data-subform-test');
        var watchFormElement = $(element).closest('form').find('[name="' + testField +'"]');
        var doCheck = function () {
            if (watchFormElement.val() === testValue) {
                $(element).find(formElements).prop('disabled', false)
                    .filter('.chosen-select, .chosen-multiselect').trigger('chosen:updated');
                $(element).show();
                $(element).find('[data-options-source]:not([data-loading-initialized])').each(function(i, select) {
                    if ($(select).closest('[data-subform-by]:hidden').length) return;
                    $.get($(select).attr('data-loading-initialized', true).data('options-source')).then(function (results) {
                        $(select).empty();
                        results.forEach(function (option){
                           $('<option>').val(option.value).text(option.label).appendTo(select);
                        });
                        $(select).trigger('chosen:updated')
                    });
                });
            } else {
                $(element).hide();
                $(element).find(formElements).prop('disabled', true)
                    .filter('.chosen-select, .chosen-multiselect').trigger('chosen:updated');
            }
        };
        watchFormElement.on('change input', doCheck);
        doCheck();
    });
});