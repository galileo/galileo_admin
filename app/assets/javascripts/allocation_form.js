$(document).on('ready turbo:load', function () {
    var subscriptionInput = $('#allocation_subscription_type');

    subscriptionInput.change(function () {
        var brandingField = $('#institutional-branding-field');
        var brandingInput = $('#allocation_branding_id');
        if(subscriptionInput.chosen().val() === 'costshare'){
            brandingField.show(400);
            brandingInput.attr('disabled', false);
        } else {
            brandingField.hide(400);
            brandingInput.attr('disabled', true);
        }
        brandingInput.trigger("chosen:updated")
    });

});