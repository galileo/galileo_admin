$(document).on('ready turbo:load', function () {
    $('[data-filterable-select]').each(function (i, el) {
        var key;
        // Hard Filters: Hide items that don't meet their criteria
        // Soft Filter: don't show/hide items, but items that don't meet their criteria are ignored when selecting default value
        // Suffixes: Append a suffix to items that do meet their criteria; optionally also a soft filter
        var hardFilters = JSON.parse($(el).attr('data-filterable-select'));
        var suffixJSON = $(el).attr('data-suffixed-select');
        var alwaysDisabled = $(el).attr('data-always-disabled') === 'true';
        var suffixes = suffixJSON ? JSON.parse(suffixJSON) : {};
        var softFilters = {}, allFilters = {}, allWatchedFields = [];
        for (key in hardFilters) {
            allFilters[key] = hardFilters[key];
            allWatchedFields.push(key);
        }
        for (key in suffixes) {
            var suffixInfo = suffixes[key];
            if (suffixInfo.softFilter) {
                softFilters[key] = suffixInfo.rule;
                allFilters[key] = suffixInfo.rule;
            }
            allWatchedFields.push(key);
        }
        var containerSelector = $(el).attr('data-filterable-select-container');
        var $container = containerSelector ? $(el).closest(containerSelector) : $(el);

        // We won't change selection based on filters if this is an edit form and the user hasn't changed the selection
        // So we initialize originalSelectionUntouched to true for an edit form (something already selected)
        //    and false for a create form.
        // This will be changed to false if the user changes the selection.
        var originalSelectionsUntouched = ($(el).find('option[selected]').length > 0);

        var evaluateFilters = function () {
            var value, matchedValues, i;
            var hardMatchCounts = {}, allMatchCounts = {};
            var hardFilterCount = 0, allFilterCount = 0;
            for (var fieldToWatch in allFilters) {
                allFilterCount++;
                value = $(document.getElementsByName(fieldToWatch)).val();
                matchedValues = allFilters[fieldToWatch][value] || [];
                for (i = 0; i < matchedValues.length; i++) {
                    allMatchCounts[matchedValues[i]] = (allMatchCounts[matchedValues[i]] || 0) + 1;
                }
                if (hardFilters[fieldToWatch]) { // dealing with a "hard" filter, which can actually show/hide items
                    hardFilterCount++;
                    if (!matchedValues || matchedValues.length === 0) {
                        $container.hide();
                        el.disabled = true;
                        if ($(el).is('.chosen-select, .chosen-multiselect')) {
                            $(el).trigger('chosen:update');
                        }
                        return;
                    }
                    for (i = 0; i < matchedValues.length; i++) {
                        hardMatchCounts[matchedValues[i]] = (hardMatchCounts[matchedValues[i]] || 0) + 1;
                    }
                }
            }
            $(el).find('option').each(function (i, opt) {
                if (opt.value) opt.hidden = opt.disabled = (hardMatchCounts[opt.value] !== hardFilterCount);
            });
            for (var field in suffixes) {
                var suffixInfo = suffixes[field];
                value = $(document.getElementsByName(field)).val();
                matchedValues = $.map(suffixInfo.rule[value], function (x) { return x.toString(); });
                $(el).find('option').each(function (i, opt) {
                    if ($.inArray(opt.value, matchedValues) > -1) {
                        if ($(opt).data('originalText') === undefined) {
                            $(opt).data('originalText', opt.text);
                        }
                        opt.text = $(opt).data('originalText') + suffixInfo.suffix;
                    } else {
                        var originalText = $(opt).data('originalText');
                        if (originalText) {
                            opt.text = originalText;
                        }
                    }
                });
            }

            if (!el.multiple && !originalSelectionsUntouched) {
                // this logic deals with selecting the first value that meets all filters
                // not relevant for a multi-select
                // Also skip if it's an edit form and we haven't changed any of the watched fields
                var meetsAllFilters = function(opt) {
                    return (allMatchCounts[opt.value] === allFilterCount) && !opt.hidden;
                };
                if (!meetsAllFilters(el.options[el.selectedIndex])) {
                    var matchFound = false;
                    for (i=0; i<el.options.length; i++) {
                        if (meetsAllFilters(el.options[i])) {
                            el.selectedIndex = i;
                            $(el).trigger('change');
                            matchFound = true;
                            break;
                        }
                    }
                    if (!matchFound) { // We'll just look for one that's valid, not necessarily the one that's default
                        for (i = 0; i < el.options.length; i++) {
                            if (!el.options[i].hidden) {
                                el.selectedIndex = i;
                                $(el).trigger('change');
                                break;
                            }
                        }
                    }
                }
            }
            if (!alwaysDisabled) {
                el.disabled = false;
                $container.show();
            }
            if ($(el).is('.chosen-select, .chosen-multiselect')) {
                $(el).trigger('chosen:updated');
            }
        }

        $.each(allWatchedFields, function(i, field) {
            $(document.getElementsByName(field)).on('change', function () {
                originalSelectionsUntouched = false;
                evaluateFilters();
            });
        });
        evaluateFilters();
    });
});