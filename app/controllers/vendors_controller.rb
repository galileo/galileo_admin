# frozen_string_literal: true

# handle Vendor actions
class VendorsController < ApplicationController
  load_resource except: %i[index]
  authorize_resource

  add_breadcrumb 'Vendors', :vendors_path

  def index
    @filterrific = initialize_filterrific(
      Vendor,
      params[:filterrific],
      select_options: {
        sorted_by: Vendor.sort_options
      }
    )

    @pagy, @vendors = pagy(
      @filterrific.find.accessible_by(
        current_ability, params[:action].to_sym
      )
    )
  end

  def new
    @vendor = Vendor.new
    add_breadcrumb 'New', new_vendor_path
  end

  def create
    @vendor = Vendor.new(vendor_params)

    if @vendor.save
      redirect_to @vendor
    else
      render :new, status: :unprocessable_entity
    end
  end

  def show
    add_breadcrumb @vendor.name, vendor_path(@vendor)
  end

  def edit
    add_breadcrumb @vendor.name, vendor_path(@vendor)
    add_breadcrumb 'Edit', edit_vendor_path(@vendor)
  end

  def update
    if @vendor.update(vendor_params)
      redirect_to @vendor
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @vendor.destroy
    redirect_to vendors_path
  end

  private

  def vendor_params
    params.require(:vendor).permit(:code, :name, :logo, :remove_logo, :resources_count,
                                   institution_ids: [])
  end
end
