# frozen_string_literal: true

require 'csv'

# Nightly Report related actions
class EzProxyUpdateController < ActionController::Base
  AUTHORIZED_IP = Rails.application.credentials[:ez_proxy_update][:authorized_ip][Rails.env.to_sym]

  before_action :verify_ip_address

  def report
    csv_report, filename = ReportService.ez_proxy_update(params[:inst_code])

    send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
  end

  private

  def verify_ip_address
    unless request.remote_ip == AUTHORIZED_IP
      head :unauthorized
    end
  end
end
