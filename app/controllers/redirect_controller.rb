# frozen_string_literal: true

# :nodoc:
class RedirectController < ActionController::Base
  skip_authorize_resource

  # redirect user based on role
  def redirect
    case current_user.role.to_sym
    when :admin
      redirect_to institutions_path
    when :institutional
      redirect_to resources_path
    when :helpdesk
      redirect_to contacts_path
    else
      redirect_to root_path
    end
  end
end
