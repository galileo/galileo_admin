# frozen_string_literal: true

require 'csv'

# Nightly Report related actions
class VendorsForStatsController < ActionController::Base
  AUTHORIZED_IPS = Rails.application.credentials[:vendors_for_stats][:authorized_ip][Rails.env.to_sym]

  before_action :verify_ip_address

  def report
    csv_report, filename = ReportService.vendors_for_stats

    send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
  end

  private

  def verify_ip_address
    unless AUTHORIZED_IPS.include? request.remote_ip
      head :unauthorized
    end
  end
end
