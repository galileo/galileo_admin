# frozen_string_literal: true

# This is the Versions controller
class VersionsController < ApplicationController
  load_resource class: Version, except: %i[index deleted]
  authorize_resource

  before_action :redirect_unless_eligible, only: :undelete

  add_breadcrumb 'History', :versions_path


  def index
    @filterrific = initialize_filterrific(
      Version.accessible_by(current_ability),
      params[:filterrific],
      select_options: {
        sorted_by: Version.sort_options,
        with_item_type: Version::ITEM_TYPES,
        with_event: %w[create update destroy],
        with_user_id: User.options_for_select,
        with_role: User::VALID_ROLES
      },
      persistence_id: false
    )

    @pagy, @versions = pagy(
      @filterrific.find.accessible_by(
        current_ability, params[:action].to_sym)
    )

    respond_to do |format|
      format.html
      format.js
    end
  end

  def deleted
    @model_name = params[:item_type].classify
    add_breadcrumb "Deleted #{@model_name}s", deleted_versions_path(@model_name)

    @filterrific = initialize_filterrific(
      Version.where(event: 'destroy', item_type: @model_name),
      params[:filterrific],
      select_options: {
        sorted_by: Version.sort_options,
        with_event: %w[create update destroy],
        with_user_id: User.options_for_select,
        with_role: User::VALID_ROLES
      },
      persistence_id: false
    )

    @pagy, @versions = pagy(@filterrific.find)

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    klass = @version.item_type.constantize

    add_breadcrumb klass.name.pluralize, polymorphic_path(klass)
    add_breadcrumb @version.item_name, version_path(@version, @version.item_name.truncate(25))
  end

  def undelete
    item = @version.item_type.constantize
                   .new(id: @version.item_id).versions.last.reify
    if item.save
      redirect_to polymorphic_path(item), alert: 'Record undeleted!'
    else
      redirect_to version_path(@version),
                  alert: "Record not undeleted: #{item.errors.collect(&:message)}",
                  status: :unprocessable_entity
    end
  end

  def sidebar_label
    t("app.menu.versions")
  end

  private

  def redirect_unless_eligible
    return true unless @version.item_type.constantize.exists? @version.item_id

    redirect_to version_path(@version),
                alert: 'Record has already been undeleted!'
  end
end
