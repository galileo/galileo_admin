# frozen_string_literal: true

# resource related actions for a given institution
class InstitutionResourcesController < ApplicationController
  include Filterrificable

  before_action :set_institution, only: :index

  add_breadcrumb 'Institutions', :institutions_path

  def index
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Resources', institution_resources_path(@institution)

    authorize! :show, @institution
    @filterrific = initialize_filterrific(
      Allocation, params[:filterrific],
      select_options: {
        sorted_by: Allocation.institution_resources_sort_options,
        display_states: Allocation.display_states,
        glri_states: Allocation.glri_states,
        bypass_galileo_authentication_states: Allocation.bypass_galileo_authentication_states,
        open_athens_states: Allocation.open_athens_states,
        name_override_states: Allocation.name_override_states,
        vendors: options_for(@institution.vendors.uniq.sort_by(&:name), :name),
        vendor_names: Allocation.vendor_name_options(@institution),
        subscription_types: Allocation.subscription_types,
        resources: options_for(@institution.resources.uniq.sort_by(&:name), :name_with_code)
      }
    )

    @pagy, @allocations = pagy(
      @filterrific.find.includes(:resource, :institution).where(institution_id: @institution.id )
    )

    respond_to do |format|
      format.html
      format.js
      format.csv do
        csv_report, _filename = ReportService.institution_resources_filterable(
          @filterrific.find.includes(:resource, :institution).where(institution_id: @institution.id )
        )
        send_data csv_report, filename: "institution_resources_#{Time.now.to_s.underscore}.csv"
      end
    end
  end

  def sidebar_label
    t('app.menu.institutions')
  end

  private

  def set_institution
    @institution = Institution.find params[:institution_id]
  end
end
