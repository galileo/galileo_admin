# frozen_string_literal: true

# resource related actions
class ResourcesController < ApplicationController
  include Versionable
  include Filterrificable

  load_resource except: %i[index show diff]
  authorize_resource

  MULTIVALUED_TEXT_FIELDS = %w[keywords].freeze

  before_action :set_form_data, only: %i[new edit copy]
  before_action :inst_user_create_actions, only: :create, if: :inst_user?
  before_action :inst_user_defaults, only: :index, if: :inst_user?
  before_action :admin_user_create_actions, only: :create, if: :admin_user?

  add_breadcrumb 'Resources', :resources_path
  add_breadcrumb 'Deleted', :deleted_resources_path, only: [:deleted]

  def index
    @filterrific = initialize_filterrific(
      Resource, params[:filterrific]&.permit!&.to_h,
      select_options: {
        sorted_by: Resource.sort_options,
        institutions: options_for(Institution.active.real.glri.or(Institution.where(id: current_user.institution_ids)).name_sort, :name_with_code),
        glri_states: Resource.glri_states,
        active_states: Resource.active_states,
        display_states: Resource.display_states,
        bypass_galileo_authentication_states: Resource.bypass_galileo_authentication_states,
        open_athens_states: Resource.open_athens_states,
        vendors: options_for(Vendor.all.name_sort, :name),
        vendor_names: Resource.vendor_names
      }
    )

    @pagy, @resources = pagy(
      @filterrific.find.accessible_by(current_ability, params[:action].to_sym)
    )

    respond_to do |format|
      format.html
      format.js
      format.csv do
        csv_report, filename = ReportService.rows_for(@filterrific.find.accessible_by(current_ability, params[:action].to_sym))
        send_data csv_report, filename: "#{filename}-#{Time.now.to_formatted_s(:number)}.csv"
      end
    end
  end

  def new
    add_breadcrumb 'New', :new_resource_path
  end

  def create
    if @resource.save
      redirect_to resource_path(@resource), flash:
        { success: I18n.t('app.resource.messages.created') }
    else
      set_form_data
      render :new, status: :unprocessable_entity
    end
  end

  def show
    @resource = Resource.includes(:institutions).find(params[:id])
    if @resource.glri? && @resource.active? && @resource.allocations&.any?
      @link_data = LinkService.new(@resource.allocations.first).link_data
    end
    add_breadcrumb @resource.name, resource_path(@resource)
  end

  def edit
    add_breadcrumb @resource.name, resource_path(@resource)
    add_breadcrumb 'Edit', edit_resource_path(@resource)
  end

  def update
    if @resource.update(resource_params)
      redirect_to resource_path(@resource), flash:
        { success: I18n.t('app.resource.messages.updated') }
    else
      set_form_data
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @resource.destroy
    redirect_to deleted_resources_path
  end

  def copy
    add_breadcrumb @resource.name, resource_path(@resource)
    add_breadcrumb 'Copy', copy_resource_path(@resource)
    copied_attrs = if current_user.admin? || current_user.institutions.ids.include?(@resource.institution&.id)
                     @resource.attributes.slice(*Resource.admin_copiable_attributes)
                   else
                     @resource.attributes.slice(*Resource.copiable_attributes)
                   end

    copied_attrs.merge!('subject_ids' => @resource.subject_ids,
                        'format_ids' => @resource.format_ids,
                        'user_view_ids' => @resource.user_view_ids)
    @resource = Resource.new copied_attrs
    render :copy
  end

  private

  def inst_user?
    current_user.institutional?
  end

  def admin_user?
    current_user.admin?
  end

  def inst_user_create_actions
    set_resource_code
    flash[:alert] = I18n.t('app.resources.messages.copy_info_inst',
                           contact_href: t('app.links.contact'))
  end

  def admin_user_create_actions
    # automatically set resource code if the resource is a GLRI resource
    set_resource_code if @resource.institution
    flash[:alert] = I18n.t('app.resources.messages.copy_info_admin',
                           contact_href: t('app.links.contact'))
  end

  def set_resource_code
    @resource.code = ResourceCodeService.new_code_for @resource
  end

  def set_form_data
    @data = {}
    @data[:credential_display_options] = [
      %w[Never never], ['When Remote', 'remote'], %w[Always always]
    ]
    @data[:all_institutions] =
      Institution.active.order(:name)
                 .map do |i|
                   [i.name_with_code, i.id, { 'data-code': i.code }]
                 end
    @data[:allocated_institutions] =
      @resource.institutions.order(name: :asc)
               .map do |i|
                 [i.name_with_code, i.id, { 'data-code': i.code }]
               end
    @data[:institution_groups] =
      InstitutionGroup.all.where('id != 1').order(:code)
                      .map do |ig|
                        [ig.code, ig.code,
                         { 'data-institution-ids':
                             ig.institution_ids.join(',') }]
                      end
  end

  def resource_params
    prepare_params(
      params.require(:resource).permit(
        :name, :code, :short_description, :long_description, :display, :active, :show_institutional_branding,
        :parent_id, :ip_access_url, :remote_access_url, :open_athens, :without_open_athens_redirector, :open_athens_url,
        :user_id, :password, :audience, :keywords, :coverage_dates, :update_frequency, :title_list_url, :product_suite,
        :vendor_id, :bypass_galileo_authentication, :language, :proxy_remote, :include_in_stats, :institution_id, :note,
        :special_information, :logo, :remove_logo, :branding_id, :access_note, :credential_display, :vendor_name,
        :oa_proxy, :serial_count, :resource_category, institution_ids: [], format_ids: [], subject_ids: [], user_view_ids: []
      )
    )
  end

  def inst_user_defaults
    params[:filterrific] ||= {}
    params[:filterrific]['sorted_by'] ||= 'name_asc'
    params[:filterrific]['for_institutions'] ||= current_user.institution_ids.map(&:to_s)
    params[:filterrific]['active_state'] ||= 'yes'
    params[:filterrific]['glri_state'] ||= 'glri'
  end

  def prepare_params(params)
    params.each do |f, v|
      if v.is_a? Array
        params[f] = v.reject(&:empty?)
        next
      end
      if MULTIVALUED_TEXT_FIELDS.include? f
        params[f] = v.gsub("\r\n", "\n").strip.split("\n")
      end
    end
  end

end
