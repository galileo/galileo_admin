# frozen_string_literal: true

# This is the InstitutionGroups controller
class InstitutionGroupsController < ApplicationController
  load_and_authorize_resource

  add_breadcrumb 'Institution Groups', :institution_groups_path

  def index
    @pagy, @institution_groups = pagy(@institution_groups.code_sort)
  end

  def show
    add_breadcrumb @institution_group.code, institution_group_path(@institution_group)
  end

  def new
    @institution_group = InstitutionGroup.new
    add_breadcrumb 'New', new_institution_group_path

  end

  def edit
    add_breadcrumb @institution_group.code, institution_group_path(@institution_group)
    add_breadcrumb 'Edit', edit_institution_group_path(@institution_group)
  end

  def create
    @institution_group = InstitutionGroup.new(institution_group_params)

    if @institution_group.save
      redirect_to @institution_group
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @institution_group.update(institution_group_params)
      redirect_to @institution_group
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @institution_group.destroy
    redirect_to institution_groups_path
  end

  def sidebar_label
    t("app.menu.institution_groups")
  end

  private

  def institution_group_params
    params.require(:institution_group).permit(
      :code, :inst_type,
      institution_ids: []
    )
  end
end
