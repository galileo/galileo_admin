class PredefinedBentosController < ApplicationController
  load_resource except: :index
  authorize_resource

  add_breadcrumb 'Default Bentos', :template_view_bentos_path
  add_breadcrumb 'Bento Types', :predefined_bentos_path

  def index
    @filterrific = initialize_filterrific(
      PredefinedBento,
      params[:filterrific],
      select_options: {
        sorted_by: PredefinedBento.sort_options,
        with_service: BentoService.menu_values,
      }
    )

    @pagy, @predefined_bentos = pagy(
      @filterrific.find.accessible_by(current_ability, params[:action].to_sym))

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    add_breadcrumb @predefined_bento.disambiguated_name, predefined_bento_path(@predefined_bento)
    @num_configed = @predefined_bento.configured_bentos.count
    @num_active = @predefined_bento.configured_bentos.where(active: true).count
    @distinct_insts = @predefined_bento.institutions.distinct.count
  end

  def new
    @predefined_bento = PredefinedBento.new
    add_breadcrumb 'New', new_predefined_bento_path
  end

  def edit
    add_breadcrumb @predefined_bento.disambiguated_name, predefined_bento_path(@predefined_bento)
    add_breadcrumb 'Edit', edit_predefined_bento_path(@predefined_bento)
  end

  def create
    @predefined_bento = PredefinedBento.new(predefined_bento_params)

    respond_to do |format|
      if @predefined_bento.save
        format.html { redirect_to @predefined_bento, notice: 'Bento Type was successfully created.' }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @predefined_bento.update(predefined_bento_params)
        format.html { redirect_to @predefined_bento, notice: 'Bento Type was successfully updated.' }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if @predefined_bento.protected?
      respond_to do |format|
        format.html { redirect_to predefined_bento_url(@predefined_bento), alert: 'Cannot delete protected Bento Type.' }
      end
      return
    end
    @predefined_bento.destroy
    respond_to do |format|
      format.html { redirect_to predefined_bentos_url, notice: 'Bento Type was successfully destroyed.' }
    end
  end

  def sidebar_label
    t('app.menu.template_view_bentos')
  end

  private

  def predefined_bento_params
    p = params.require(:predefined_bento).permit(:service, :code, :display_name, :description, :admin_note,
                                                 :no_results_message, :service_credentials_name, definition: {})
    if p[:definition].nil?
      p.delete :definition
    else
      p[:definition] = p[:definition].permit(PredefinedBento.supported_params_by_service(p[:service]))
      prepare_bool_fields(p[:definition], ['Has Parent Publication', 'Limit to Custom Catalog'])
    end
    p
  end
end
