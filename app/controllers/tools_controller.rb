# frozen_string_literal: true

class ToolsController < ApplicationController
  authorize_resource class: false

  add_breadcrumb 'Tools', :tools_path


  def index; end

  def csv_import_tool
    add_breadcrumb 'CSV Importer', :csv_import_tools_path

    render 'tools/csv_imports/index'
  end

  def csv_import
    @model_type = csv_imports_params[:model_type]
    @csv_importer = CsvImporter.new @model_type, csv_imports_params[:csv_file]
    @csv_importer.import
  rescue StandardError => e
    @error = e
  ensure
    add_breadcrumb 'CSV Importer', :csv_import_tools_path
    render 'tools/csv_imports/results', status: :accepted
  end

  def ip_cache_status_tool
    add_breadcrumb 'IP Cache Status', ip_cache_status_tools_path
    render 'tools/ip_cache_status/index'
  end

  def ip_cache_status
    add_breadcrumb 'IP Cache Status', ip_cache_status_tools_path
    @ip_range = ip_cache_status_params[:ip_range]
    @results = IpService.search_range @ip_range
  rescue StandardError => e
    @error = e
  ensure
    render 'tools/ip_cache_status/results', status: :accepted
  end

  private

  def csv_imports_params
    params.permit(:csv_file, :model_type)
  end

  def ip_cache_status_params
    params.permit(:ip_range)
  end

end
