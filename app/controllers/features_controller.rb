# frozen_string_literal: true

# Handle feature actions
class FeaturesController < ApplicationController
  before_action :set_featuring
  before_action :set_group_features, only: :index, unless: :institution_group?

  authorize_resource :featuring
  load_and_authorize_resource :feature, except: %i[new create]

  # GET /features
  def index
    add_breadcrumb @featuring.class.name.pluralize, polymorphic_path(@featuring.class)
    add_breadcrumb @featuring.name, polymorphic_path(@featuring)
    add_breadcrumb 'Features', polymorphic_path([@featuring, :features])
    @features = @featuring.features.order(:view_type, :position).includes([image_attachment: :blob])
  end

  # GET /features/1
  def show
    add_breadcrumb @featuring.class.name.pluralize, polymorphic_path(@featuring.class)
    add_breadcrumb @featuring.name, polymorphic_path(@featuring)
    add_breadcrumb 'Features', polymorphic_path([@featuring, :features])
    add_breadcrumb @feature.link_label, polymorphic_path([@featuring, @feature])
  end

  # GET /features/new
  def new
    add_breadcrumb @featuring.class.name.pluralize, polymorphic_path(@featuring.class)
    add_breadcrumb @featuring.name, polymorphic_path(@featuring)
    add_breadcrumb 'Features', polymorphic_path([@featuring, :features])
    add_breadcrumb 'New', polymorphic_path([:new, @featuring, :feature])
    @feature = Feature.new(featuring: @featuring)
  end

  # GET /features/1/edit
  def edit
    add_breadcrumb @featuring.class.name.pluralize, polymorphic_path(@featuring.class)
    add_breadcrumb @featuring.name, polymorphic_path(@featuring)
    add_breadcrumb 'Features', polymorphic_path([@featuring, :features])
    add_breadcrumb @feature.link_label, polymorphic_path([@featuring, @feature])
    add_breadcrumb 'Edit', polymorphic_path([:edit, @featuring, :feature])
  end

  # POST /features
  # Only when @featuring is an Institution
  def create
    @feature = Feature.new(feature_params.merge(featuring: @featuring))

    raise CanCan::AccessDenied unless can? :create, @feature

    if @feature.save
      redirect_to polymorphic_path([@featuring, @feature]),
                  notice: I18n.t('app.feature.messages.created.success')
    else
      render :new, alert: I18n.t('app.feature.messages.created.error'), status: :unprocessable_entity
    end
  end

  # PATCH/PUT /features/1
  def update
    if @feature.update(feature_params)
      redirect_to polymorphic_path([@featuring, @feature]),
                  notice: I18n.t('app.feature.messages.updated.success')
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /features/1
  # Only when @featuring is an Institution
  def destroy
    @feature.destroy
    respond_to do |format|
      format.html do
        redirect_to polymorphic_path([@featuring, @feature]),
                    notice: I18n.t('app.feature.messages.deleted.success')
      end
    end
  end

  def sidebar_label
    if institution_group?
      I18n.t('app.menu.institution_groups')
    else
      I18n.t('app.menu.institutions')
    end
  end

  private

  def feature_params
    params.require(:feature).permit(:view_type, :position, :link_url,
                                    :link_label, :link_description, :image,
                                    :remove_image, :featuring_id)
  end

  def set_featuring
    @featuring = if params.key? :institution_id
                   Institution.find(params[:institution_id])
                 elsif params.key? :institution_group_id
                   InstitutionGroup.find(params[:institution_group_id])
                 end
  end

  def set_group_features
    @group_features = @featuring.institution_group.features
                                .order(:view_type, :position)
  end

  def institution_group?
    @featuring.is_a? InstitutionGroup
  end

end
