# frozen_string_literal: true

# This is the User Views controller
class UserViewsController < ApplicationController
  load_and_authorize_resource

  add_breadcrumb 'K-12 Views', :user_views_path

  def index
    @pagy, @user_views = pagy(@user_views.order(:full, :inst_type, :name))
  end

  def show
    add_breadcrumb @user_view.name, user_view_path(@user_view)
  end

  def new
    @user_view = UserView.new
    add_breadcrumb 'New', :new_user_view_path
  end

  def edit
    add_breadcrumb @user_view.name, user_view_path(@user_view)
    add_breadcrumb 'Edit', edit_user_view_path(@user_view)
  end

  def create
    @user_view = UserView.new(user_view_params)

    if @user_view.save
      redirect_to @user_view
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @user_view.update(user_view_params)
      redirect_to @user_view
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @user_view.destroy
    redirect_to user_views_path
  end

  def sidebar_label
    t('app.menu.user_views')
  end

  private

  def user_view_params
    params.require(:user_view).permit(
      :name, :code, :full, inst_type: []
    )
  end

end
