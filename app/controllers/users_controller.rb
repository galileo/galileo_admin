# frozen_string_literal: true

# This is the User controller
class UsersController < ApplicationController
  load_resource except: %i[index]
  authorize_resource

  add_breadcrumb 'Users', :users_path
  add_breadcrumb 'Deleted', :deleted_users_path, only: [:deleted]

  def index
    @filterrific = initialize_filterrific(
      User,
      params[:filterrific]&.permit!&.to_h,
      select_options: {
        sorted_by: User.sort_options,
        with_role: User::VALID_ROLES,
        with_subroles: User::VALID_SUBROLES.values.flatten,
        with_institutions: Institution.options_for_select
      }
    )

    @pagy, @users = pagy(
      @filterrific.find.active.accessible_by(
        current_ability, params[:action].to_sym
      ).includes(:institutions)
    )

    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    @user = User.new
    add_breadcrumb 'New', :new_user_path
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to @user
    else
      render :new, status: :unprocessable_entity
    end
  end

  def show
    add_breadcrumb @user.email, user_path(@user)
  end

  def edit
    add_breadcrumb @user.email, user_path(@user)
    add_breadcrumb 'Edit', edit_user_path(@user)
  end

  def update
    if @user.update(user_params)
      redirect_to @user
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.status == 'Invited'
      @user.destroy
    else
      @user.soft_delete
    end
    redirect_to users_path
  end

  def deleted
    @filterrific = initialize_filterrific(
      User,
      params[:filterrific],
      select_options: {
        sorted_by: User.sort_options
      }
    )

    @pagy, @users = pagy(
      @filterrific.find.deleted.accessible_by(
        current_ability, params[:action].to_sym
      )
    )

    respond_to do |format|
      format.html
      format.js
    end
  end

  def undelete
    @user.deleted_at = nil
    @user.save!
    redirect_to @user
  end

  def sidebar_label
    t("app.menu.users")
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation,
                                 :role, :first_name, :last_name, subroles: [],
                                 institution_ids: [])
  end
end
