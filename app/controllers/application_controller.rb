# frozen_string_literal: true

# parent controller for GALILEO Admin
class ApplicationController < ActionController::Base
  include Pagy::Backend

  helper_method :per_page_setting

  # authenticate user
  before_action :authenticate_user!

  # authorize user, unless it's a Devise action (login, Invitation)
  check_authorization unless: :devise_controller?

  rescue_from CanCan::AccessDenied, with: :unauthorized

  # Track whose responsible for changes in paper-trail
  before_action :set_paper_trail_whodunnit

  before_action :block_non_admin_users

  add_breadcrumb 'Home', :root_path

  def prepare_bool_fields(in_params, fields)
    fields.each do |field|
      unless in_params[field].nil?
        in_params[field] = !(!in_params[field] || in_params[field] == 0 || in_params[field] == '0')
      end
    end
  end

  def per_page_setting
    return Pagy::DEFAULT[:limit] if params[:per_page].blank?

    params[:per_page].to_i
  end

  private

  def unauthorized(exception)
    flash[:error] = exception.message
    redirect_back fallback_location: root_path
  end

  def block_non_admin_users
    return unless Rails.configuration.block_non_admin_users && current_user&.admin? == false

    sign_out current_user

    flash[:error] = 'Only Admin users can sign in at this time. Please contact Galileo Support Services if you have any questions.'
    sign_out current_user
    redirect_to root_path
  end
end

