class WidgetsController < ApplicationController
  load_and_authorize_resource :institution
  load_and_authorize_resource :widget, through: :institution

  add_breadcrumb 'Institutions', :institutions_path
  # GET /widgets
  # GET /widgets.json
  def index
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Widgets', institution_widgets_path(@institution)

    @pagy, @widgets = pagy(@institution.widgets)
  end

  # GET /widgets/1
  def show
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Widgets', institution_widgets_path(@institution)
    add_breadcrumb @widget.service, institution_widgets_path(@institution, @widget)
  end

  # GET /widgets/new
  def new
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Widgets', institution_widgets_path(@institution)
    add_breadcrumb 'New', new_institution_widget_path(@institution)
    @widget = Widget.new
  end

  # GET /widgets/1/edit
  def edit
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Widgets', institution_widgets_path(@institution)
    add_breadcrumb @widget.service, institution_widget_path(@institution, @widget)
    add_breadcrumb 'Edit', edit_institution_widget_path(@institution, @widget)
  end

  # POST /widgets
  def create
    respond_to do |format|
      if @widget.save
        format.html {
          redirect_to institution_widget_path(@institution, @widget),
                      notice: 'Widget was successfully created.'
        }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /widgets/1
  def update
    respond_to do |format|
      if @widget.update(widget_params)
        format.html {
          redirect_to institution_widget_path(@institution, @widget),
                      notice: 'Widget was successfully updated.'
        }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /widgets/1
  def destroy
    @widget.destroy
    respond_to do |format|
      format.html {
        redirect_to institution_widgets_path(@institution),
                    notice: 'Widget was successfully destroyed.'
      }
    end
  end

  def sidebar_label
    t("app.menu.institutions")
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_widget
      @widget = Widget.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def widget_params
      params.require(:widget).permit(
        :institution_id, :service, :credentials, :active, :display_name
      )
    end
end
