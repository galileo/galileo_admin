class Stats::DailyController < ApplicationController
  include Stats
  include Filterrificable

  authorize_resource class: false
  add_breadcrumb I18n.translate("app.menu.stats"), :stats_index_path

  def index
    @table_name = params[:table_name]
    @stats_class = Stats.const_get(@table_name.camelize)
    add_breadcrumb t("app.stats.daily.titles.#{@table_name}"), stats_daily_index_path(@table_name)
    @filterrific = initialize_filterrific(
      @stats_class,
      params[:filterrific]&.permit!&.to_h,
      )

    @pagy, @daily_stats = pagy(@filterrific.find.accessible_by(current_ability, params[:action].to_sym), limit: per_page_setting)

    respond_to do |format|
      format.html
      format.js { render 'stats/stat_table' }
      # format.csv do
      #   csv_report, filename = ReportService.contacts(@filterrific.find.accessible_by(current_ability, params[:action].to_sym))
      #   send_data csv_report, filename: "#{filename}-#{Time.now.to_formatted_s(:number)}.csv"
      # end
    end
  end

  def new_accumulate_range
    @table_name = daily_params[:table_name]
    add_breadcrumb t("app.stats.daily.titles.#{@table_name}"), stats_daily_index_path(@table_name)
  end

  def accumulate_range
    @table_name = daily_params[:table_name]
    @stats_class = Stats.const_get(@table_name.camelize)
    @begin_date = Date.parse daily_params[:begin_date]
    @end_date = Date.parse daily_params[:end_date]
    @results = @stats_class.accumulate_range @begin_date.strftime('%Y-%m-%d'), @end_date.strftime('%Y-%m-%d')
  rescue StandardError => e
    @error = e
  ensure
    add_breadcrumb t("app.stats.daily.titles.#{@table_name}"), stats_daily_index_path(@table_name)
    render 'stats/daily/accumulate_results', status: :accepted
  end

  def new_delete_range
    @table_name = daily_params[:table_name]
    add_breadcrumb t("app.stats.daily.titles.#{@table_name}"), stats_daily_index_path(@table_name)
  end

  def delete_range
    @table_name = daily_params[:table_name]
    stats_class = Stats.const_get(@table_name.camelize)
    begin_date = Date.parse daily_params[:begin_date]
    end_date = Date.parse daily_params[:end_date]
    if stats_class.where(date: begin_date..end_date).delete_all
      redirect_to stats_daily_index_path(@table_name), notice: 'Stat range successfully deleted.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def sidebar_label
    t("app.menu.stats")
  end

  private

  def daily_params
    params.permit(:table_name, :begin_date, :end_date)
  end
end
