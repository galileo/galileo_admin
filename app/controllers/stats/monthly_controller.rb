class Stats::MonthlyController < ApplicationController
  include Stats
  include Filterrificable

  authorize_resource class: false
  add_breadcrumb I18n.translate("app.menu.stats"), :stats_index_path

  def index
    @table_name = params[:table_name]
    @stats_class = Stats.const_get(@table_name.camelize)
    add_breadcrumb t("app.stats.monthly.titles.#{@table_name}"), stats_monthly_index_path(@table_name)
    @filterrific = initialize_filterrific(@stats_class, params[:filterrific]&.permit!&.to_h)

    @pagy, @monthly_stats = pagy(@filterrific.find.accessible_by(current_ability, params[:action].to_sym), limit: per_page_setting)

    respond_to do |format|
      format.html
      format.js { render 'stats/stat_table' }
      # format.csv do
      #   csv_report, filename = ReportService.contacts(@filterrific.find.accessible_by(current_ability, params[:action].to_sym))
      #   send_data csv_report, filename: "#{filename}-#{Time.now.to_formatted_s(:number)}.csv"
      # end
    end
  end

  def new_accumulate
    @table_name = monthly_params[:table_name]
    add_breadcrumb t("app.stats.monthly.titles.#{@table_name}"), stats_monthly_index_path(@table_name)
  end

  def accumulate
    @table_name = monthly_params[:table_name]
    @stats_class = Stats.const_get(@table_name.camelize)
    @begin_date = monthly_params[:begin_date]
    @end_date = monthly_params[:end_date]
    @results = @stats_class.accumulate @begin_date, @end_date
  rescue StandardError => e
    @error = e
  ensure
    add_breadcrumb t("app.stats.monthly.titles.#{@table_name}"), stats_monthly_index_path(@table_name)
    render 'stats/monthly/accumulate_results', status: :accepted
  end

  def new_delete_range
    @table_name = monthly_params[:table_name]
    add_breadcrumb t("app.stats.monthly.titles.#{@table_name}"), stats_monthly_index_path(@table_name)
  end

  def delete_range
    @table_name = monthly_params[:table_name]
    stats_class = Stats.const_get(@table_name.camelize)
    begin_date = Date.parse "#{monthly_params[:begin_date]}-1"
    end_date = Date.parse "#{monthly_params[:end_date]}-1"
    if stats_class.where(year_month: begin_date..end_date).delete_all
      redirect_to stats_monthly_index_path(@table_name), notice: 'Stat range successfully deleted.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def sidebar_label
    t("app.menu.stats")
  end

  private

  def monthly_params
    params.permit(:table_name, :begin_date, :end_date)
  end

end
