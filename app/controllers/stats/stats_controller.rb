class Stats::StatsController < ApplicationController
  authorize_resource class: false
  add_breadcrumb I18n.translate("app.menu.stats"), :stats_index_path

  def index
  end

  def delete_all
    period = params[:period]
    table_name = params[:table_name]
    stat_class = Stats.const_get(table_name.camelize)
    stat_class.delete_all
    respond_to do |format|
      format.html {
        redirect_to public_send("stats_#{period}_index_path", table_name),
                    notice: "All #{table_name.camelize} successfully destroyed."
      }
    end
  end

  def sidebar_label
    t("app.menu.stats")
  end

end
