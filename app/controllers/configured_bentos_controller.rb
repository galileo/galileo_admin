class ConfiguredBentosController < ApplicationController
  load_and_authorize_resource :institution, except: [:show]
  load_and_authorize_resource :configured_bento, through: :institution, except: [:show]

  before_action :set_configured_bento, only: [:show, :edit, :update, :destroy]

  add_breadcrumb 'Institutions', :institutions_path

  def prepare_user_view
    user_view_code = params[:user_view]
    valid_user_views = ViewService.valid_view_types_for(@institution.inst_type) || []
    unless user_view_code.present? && valid_user_views.include?(user_view_code)
      if valid_user_views.empty?
        redirect_to institution_configured_bento_path(@institution)
      else
        redirect_to institution_configured_bentos_path(@institution),
                    notice: (user_view_code.present? ? "No view called #{user_view_code} for this institution" : nil)
      end
      return nil
    end
    @user_view = UserView.find_by_code user_view_code
  end

  def reorder
    user_view = prepare_user_view
    return if user_view.nil?

    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb t('activerecord.attributes.institution.configured_bentos'),
                   institution_configured_bentos_path(@institution)
    add_breadcrumb user_view.name
    @configured_bentos = @institution.configured_bentos.where(user_view: user_view.code).default_sort
  end

  def save_reorder
    user_view = prepare_user_view
    configured_bentos = abridged_configured_bento_params
    configured_bentos.each do |cb_params|
      requested_destroy = %w[1 true].include?(cb_params.delete :_destroy)
      cb = ConfiguredBento.find(cb_params[:id])
      if requested_destroy && cb.created_by_institution?
        cb.destroy
      else
        cb.update cb_params
      end
    end
    redirect_to reorder_institution_configured_bentos_path(@institution, user_view.code),
                notice: "Changes saved."
  end

  def index
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb t('activerecord.attributes.institution.configured_bentos'),
                   institution_configured_bentos_path(@institution)
    user_views = ViewService.valid_view_types_for(@institution.inst_type)
    @configured_bentos_by_view = user_views.map do |uv|
      {
        user_view_code: uv,
        user_view_name: UserView.find_by_code(uv).name,
        configured_bentos: @institution.configured_bentos
                                       .where(user_view: uv, active: true, shown_by_default: true)
                                       .default_sort.to_a
      }
    end
  end

  def show
    @configured_bento = ConfiguredBento.includes(:institution).find(params[:id])
    authorize! :show, @configured_bento
    @institution = @configured_bento.institution

    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb t('activerecord.attributes.institution.configured_bentos'),
                   institution_configured_bentos_path(@institution)
    add_breadcrumb @configured_bento.display_user_view,
                   reorder_institution_configured_bentos_path(@institution, @configured_bento.user_view)
    add_breadcrumb @configured_bento.computed_display_name,
                   institution_configured_bento_path(@institution, @configured_bento)
  end

  def new
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb t('activerecord.attributes.institution.configured_bentos'),
                   institution_configured_bentos_path(@institution)
    add_breadcrumb 'New', new_institution_configured_bento_path(@institution)
    @configured_bento = ConfiguredBento.new
    @configured_bento.user_view = params[:user_view] if params[:user_view]
  end

  def edit
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb t('activerecord.attributes.institution.configured_bentos'),
                   institution_configured_bentos_path(@institution)
    add_breadcrumb @configured_bento.display_user_view,
                   reorder_institution_configured_bentos_path(@institution, @configured_bento.user_view)
    add_breadcrumb @configured_bento.computed_display_name,
                   institution_configured_bento_path(@institution, @configured_bento)
    add_breadcrumb 'Edit'

  end

  def create
    if @configured_bento.bento_config.blank? && @configured_bento.service&.requires_config?
      @configured_bento.bento_config_id = default_bento_config_for_bento @configured_bento
    end
    respond_to do |format|
      if @configured_bento.save
        format.html do
          redirect_to institution_configured_bento_path(@institution, @configured_bento),
                      notice: 'Bento was successfully created.'
        end
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  def update
    @configured_bento.assign_attributes configured_bento_params
    # If we didn't explicitly assign a bento_config_id (possibly because we're an inst user and aren't allowed to)
    #   then select the default if we've changed fields (predefined bento or user view) that would affect the default.
    # Otherwise, try to keep it the same.
    if configured_bento_params[:bento_config_id].blank? && (@configured_bento.predefined_bento_changed? ||
                                                            @configured_bento.user_view_changed? ||
                                                            !@configured_bento.service_and_prereqs_match?)
      @configured_bento.bento_config_id = default_bento_config_for_bento @configured_bento
    end

    respond_to do |format|
      if @configured_bento.update(configured_bento_params)
        format.html do
          redirect_to institution_configured_bento_path(@institution, @configured_bento),
                      notice: 'Bento was successfully updated.'
        end
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @configured_bento.destroy
    respond_to do |format|
      format.html do
        destination = institution_configured_bentos_path(@institution)
        redirect_to destination, notice: 'Bento was successfully destroyed.'
      end
    end
  end

  def restore_defaults
    skipped = @institution.restore_defaults_configured_bentos!
    if skipped.any?
      missing_configs = skipped.group_by { |x| x[:service] }.transform_values { |g| g.pluck(:user_view).uniq }
      options = {
        alert: "Bentos were restored from the defaults for '#{@institution.inst_type}'. " +
          "#{skipped.size} items were skipped because the following services lack credentials: " +
          (missing_configs.map{ |service, views| "#{service} (#{views.join(', ')})" }).join('; ') + '.'
      }
    else
      options = {
        notice: "All bentos were restored from the defaults for '#{@institution.inst_type}'."
      }
    end
    respond_to do |format|
      format.html do
        redirect_to institution_configured_bentos_path(@institution), options
      end
    end
  end

  def restore_defaults_for_view
    user_view = prepare_user_view
    skipped = @institution.restore_defaults_configured_bentos! user_view: user_view.code
    if skipped.any?
      missing_configs = skipped.group_by { |x| x[:service] }.transform_values { |g| g.pluck(:user_view).uniq }
      options = {
        alert: "Bentos for #{user_view.name} were restored from the defaults for '#{@institution.inst_type}'." +
          " #{skipped.size} items were skipped because of the following services lack" +
          " #{t('app.bento_configs.label.bento_configs')}: " +
          (missing_configs.map{ |service, views| "#{service} (#{views.join(', ')})" }).join('; ') + '.'
      }
    else
      options = {
        notice: "Bentos for #{user_view.name} were restored from the defaults for '#{@institution.inst_type}'."
      }
    end
    respond_to do |format|
      format.html do
        redirect_to reorder_institution_configured_bentos_path(@institution, user_view.code), options
      end
    end
  end

  def sidebar_label
    t("app.menu.institutions")
  end

  private

    # Use callbacks to share common setup or constraints between actions.
  def set_configured_bento
    @configured_bento = ConfiguredBento.find(params[:id])
  end

    # Never trust parameters from the scary internet, only allow the white list through.
  def configured_bento_params
    permit_list = [:institution_id, :predefined_bento_id, :user_view,
                   :display_name, :description, :slug, :order, :active, :shown_by_default,
                   :predefined_bento_code]
    permit_list << :bento_config_id if current_user.admin?
    p = params.require(:configured_bento).permit(*permit_list, customizations: {})
    if p[:customizations].nil?
      p.delete :customizations
    else
      p[:customizations] = p[:customizations].permit(
        ConfiguredBento.supported_customizations(p[:predefined_bento_code])
      )
      prepare_bool_fields(p[:customizations], ['Has Parent Publication'])
      # remove blanks from arrays
      p[:customizations].each do |k, v|
        if v.is_a? Array
          p[:customizations][k] = v.reject {|x| x.blank?}
        end
      end
    end
    p
  end

  def abridged_configured_bento_params
    params.require(:reorder_form).permit(configured_bentos: [
      :id, :display_name, :description, :order, :shown_by_default, :_destroy
    ])[:configured_bentos].values
  end

  def default_bento_config_for_bento(configured_bento)
    bento_configs = helpers.bento_config_availability_by_predefined_bento(
      configured_bento.institution
    )[configured_bento.predefined_bento.code] || []
    if bento_configs.size > 1
      bento_configs &= helpers.bento_config_availability_by_user_view(
        configured_bento.institution
      )[configured_bento.user_view]
    end
    bento_configs.first
  end

end
