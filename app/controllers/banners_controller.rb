class BannersController < ApplicationController
  include Filterrificable
  include Versionable



  load_and_authorize_resource
  add_breadcrumb 'Banners', :banners_path
  add_breadcrumb 'Deleted', :deleted_banners_path, only: [:deleted]


  # GET /banners
  def index
    select_insts = if current_user.institutional?
                     current_user.institutions
                   else
                     Institution.active.uniq
                   end

    select_users = if current_user.institutional?
                     User.joins(:institutions).where(institutions: {id: current_user.institutions} )
                   else
                     User.all
                   end

    @filterrific = initialize_filterrific(
      Banner,
      params[:filterrific]&.permit!&.to_h,
      select_options: {
        sorted_by: Banner.sort_options,
        with_audience: Banner.audiences_for_select,
        with_institution: options_for(select_insts.sort_by(&:name), :name_with_code),
        with_institution_group: options_for(InstitutionGroup.all.code_sort, :code),
        created_by_user: options_for(select_users.sort_by(&:first_name), :name_with_email),
        active_states: Banner.active_states
      }
    )

    @pagy, @banners = pagy(
      @filterrific.find.accessible_by(
        current_ability, params[:action].to_sym).includes(:institution)
    )

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /banners/1
  def show
    add_breadcrumb @banner.id, banner_path(@banner)
  end

  # GET /banners/new
  def new
    @banner = Banner.new
    @banner.audience = 'institution' if current_user.institutional?
    add_breadcrumb 'New', :new_banner_path

  end

  # GET /banners/1/edit
  def edit
    add_breadcrumb @banner.id, banner_path(@banner)
    add_breadcrumb 'Edit', edit_banner_path(@banner)
  end

  # POST /banners
  def create
    @banner = Banner.new(banner_params)
    @banner.audience = 'institution' if current_user.institutional?

    respond_to do |format|
      if @banner.save
        format.html { redirect_to @banner, notice: 'Banner was successfully created.' }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /banners/1
  def update
    respond_to do |format|
      if @banner.update(banner_params)
        format.html { redirect_to @banner, notice: 'Banner was successfully updated.' }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /banners/1
  def destroy
    @banner.destroy
    respond_to do |format|
      format.html { redirect_to banners_url, notice: 'Banner was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_banner
      @banner = Banner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def banner_params
      params.require(:banner).permit(
        :institution_id, :institution_group_id, :audience, :content, :start_time, :end_time, :style,
        user_view_ids: []
      )
    end
end
