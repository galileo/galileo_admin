# frozen_string_literal: true

# This is the Contact controller
class ContactsController < ApplicationController
  include Versionable

  load_resource except: [:index, :bulk_destroy_confirm]
  authorize_resource

  add_breadcrumb 'Contacts', :contacts_path
  add_breadcrumb 'Deleted', :deleted_contacts_path, only: [:deleted]

  def index
    @filterrific = initialize_filterrific(
      Contact,
      params[:filterrific]&.permit!&.to_h,
      select_options: {
        sorted_by: Contact.sort_options,
        with_associated_type: Contact.associated_types,
        with_institution: Institution.options_for_select,
        with_vendor: Vendor.options_for_select,
        with_type: Contact.allowed_types
      }
    )

    @pagy, @contacts = pagy(
      @filterrific.find.accessible_by(
        current_ability, params[:action].to_sym).includes(:institution, :vendor)
    )

    respond_to do |format|
      format.html
      format.js
      format.csv do
        csv_report, filename = ReportService.contacts(@filterrific.find.accessible_by(current_ability, params[:action].to_sym))
        send_data csv_report, filename: "#{filename}-#{Time.now.to_formatted_s(:number)}.csv"
      end
    end
  end

  def new
    @contact = Contact.new(institution_id: params[:institution_id], vendor_id: params[:vendor_id])
    add_breadcrumb 'New', :new_contact_path
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      redirect_to contact_path @contact
    else
      render :new, status: :unprocessable_entity
    end
  end

  def show
    add_breadcrumb @contact.full_name, contact_path(@contact)
  end

  def edit
    add_breadcrumb @contact.full_name, contact_path(@contact)
    add_breadcrumb 'Edit', edit_contact_path(@contact)
  end

  def update
    if @contact.update(contact_params)
      redirect_to contact_path @contact
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @contact.destroy
    redirect_to deleted_versions_path('contact')
  end

  def bulk_destroy
    contacts = Contact.where(id: params[:contact_ids]).destroy_all
    redirect_to deleted_versions_path('contact'), notice: "#{contacts.size} contacts successfully deleted."
  end

  def bulk_destroy_confirm
    if params[:contact_ids].nil?
      redirect_to contacts_path, alert: "No contacts selected"
    end

    @contacts = Contact.includes(:institution).where(id: params[:contact_ids]).accessible_by(current_ability, params[:action].to_sym)
  end

  def notify_current_password
    send_and_redirect(
      :notify_current_password_contact,
      'app.institution.messages.notify_password.success',
      'app.institution.messages.notify_password.error'
    )
  end

  private

  def contact_params
    params.require(:contact).permit(:first_name, :last_name, :email, :phone,
                                    :notes, :institution_id, :vendor_id,  types: [])
  end

  def send_and_redirect(message, sent_msg_key, failed_msg_key)
    PasswordService.new.send(message, @contact)
    flash[:success] = I18n.t(sent_msg_key)
  rescue Exceptions::PasswordChangeError => e
    flash[:error] = I18n.t(failed_msg_key, message: e.message)
  ensure
    redirect_to contact_path @contact
  end

end
