# frozen_string_literal: true

# This is the Subjects controller
class SubjectsController < ApplicationController
  load_and_authorize_resource

  add_breadcrumb 'Subjects', :subjects_path

  def index
    @pagy, @subjects = pagy(@subjects.order(:name))
  end

  def show
    add_breadcrumb @subject.name, subject_path(@subject)
  end

  def new
    @subject = Subject.new
    add_breadcrumb 'New', :new_subject_path

  end

  def edit
    add_breadcrumb @subject.name, subject_path(@subject)
    add_breadcrumb 'Edit', edit_subject_path(@subject)

  end

  def create
    @subject = Subject.new(subject_params)

    if @subject.save
      redirect_to @subject
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @subject.update(subject_params)
      redirect_to @subject
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @subject.destroy
    redirect_to subjects_path
  end

  private

  def subject_params
    params.require(:subject).permit(:name)
  end
end
