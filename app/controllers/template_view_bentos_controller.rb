class TemplateViewBentosController < ApplicationController
  include Filterrificable
  load_and_authorize_resource

  add_breadcrumb 'Default Bentos', :bento_templates_path

  def index
    add_breadcrumb 'Advanced'
    @filterrific = initialize_filterrific(
      TemplateViewBento,
      params[:filterrific],
      select_options: {
        sorted_by: TemplateViewBento.sort_options,
        with_template_name: TemplateViewBento.template_names,
        with_user_view: UserView.values_for_menus('all'),
        shown_default_state: TemplateViewBento.shown_default_options
      }
    )

    @pagy, @template_view_bentos = pagy(@filterrific.find.accessible_by(current_ability, params[:action].to_sym).includes(:predefined_bento))

    respond_to do |format|
      format.html
      format.js
    end
  end

  def templates
    # @templates will be a hash such that @templates[template_name][user_view][shown_by_default] is an array of TVBs
    @user_view_lookup = UserView.all.to_a.index_by {|uv| uv.code}
    @templates = TemplateViewBento.accessible_by(current_ability, params[:action].to_sym)
                                  .includes(:predefined_bento)
                                  .order(:template_name)
                                  .to_a
                                  .group_by {|tvb| tvb.template_name}
                                  .transform_values do |tvbs|
                                    tvbs.sort_by {|tvb| @user_view_lookup[tvb.user_view]&.id || 9999}
                                        .group_by {|tvb| tvb.user_view }
                                        .transform_values {|tvbs| tvbs.sort_by {|tvb| tvb.default_order || 0}
                                                                      .group_by {|tvb| tvb.shown_by_default}}
                                  end
    @institution_counts = Institution.group(:bento_template).count
  end

  def fork_template_form
    @from_template = params[:from]
    @institutions = options_for(Institution.where(
      id: ConfiguredBento.where(
        template_view_bento_id: TemplateViewBento.where(
          template_name: @from_template
        )
      ).select(:institution_id)
    ).active.uniq.sort_by(&:name), :name_with_code)
  end

  def fork_template
    old_name = fork_params[:from]
    new_name = fork_params[:to]
    institution_ids = fork_params[:institution_ids]&.filter(&:present?) || []
    unless old_name.present? && new_name.present?
      flash[:error] = 'Please select a template name'
      redirect_to fork_bento_template_form_path(from: old_name)
      return
    end
    if TemplateViewBento.where(template_name: new_name).any?
      flash[:error] = "Template '#{new_name}' already exists"
      redirect_to fork_bento_template_form_path(from: old_name)
      return
    end
    if Institution.where(id: institution_ids).where.not(bento_template: old_name).any?
      flash[:error] = "All selected institutions must have bento template #{old_name}"
      redirect_to fork_bento_template_form_path(from: old_name)
      return
    end
    old_tvbs = TemplateViewBento.where(template_name: old_name)
    new_tvbs = old_tvbs.map do |old|
      now = DateTime.now
      TemplateViewBento.new template_name: new_name,
                            user_view: old.user_view,
                            predefined_bento_id: old.predefined_bento_id,
                            shown_by_default: old.shown_by_default,
                            default_order: old.default_order,
                            updated_at: now,
                            created_at: now
    end
    old_new_mapping = {}
    insert_result = TemplateViewBento.insert_all(new_tvbs.map {|tvb| tvb.attributes.except('id')})
    insert_result.rows.each_with_index do |row, index|
      old_id = old_tvbs[index].id
      new_id = row.first
      old_new_mapping[old_id] = new_id
    end
    Institution.where(id: institution_ids).update_all(bento_template: new_name)
    ConfiguredBento.where(institution_id: institution_ids).where.not(template_view_bento_id: nil).each do |cb|
      cb.update_columns template_view_bento_id: old_new_mapping[cb.template_view_bento_id]
    end
    flash[:success] = "Successfully converted #{institution_ids.count} institutions to new template \"#{new_name}\"."
    redirect_to template_view_bentos_path(filterrific: {with_template_name: new_name, with_user_view: ''})
  end

  def show
    add_breadcrumb @template_view_bento.title, template_view_bento_path(@template_view_bento)
  end

  def new
    @template_view_bento = TemplateViewBento.new
    add_breadcrumb 'New', new_template_view_bento_path

  end

  def edit
    add_breadcrumb @template_view_bento.title, template_view_bento_path(@template_view_bento)
    add_breadcrumb 'Edit', edit_template_view_bento_path(@template_view_bento)
  end

  def create
    @template_view_bento = TemplateViewBento.new(template_view_bento_params)

    respond_to do |format|
      if @template_view_bento.save
        format.html { redirect_to @template_view_bento, notice: 'Default Bento was successfully created.' }
        format.json { render :show, status: :created, location: @template_view_bento }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @template_view_bento.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @template_view_bento.update(template_view_bento_params)
        format.html { redirect_to @template_view_bento, notice: 'Default bento was successfully updated.' }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @template_view_bento.destroy
    respond_to do |format|
      format.html { redirect_to template_view_bentos_url, notice: 'Default Bento was successfully destroyed.' }
    end
  end

  def sidebar_label
    t('app.menu.template_view_bentos')
  end

  private

  def template_view_bento_params
    params.require(:template_view_bento).permit(:template_name, :user_view, :default_order, :shown_by_default,
                                                :predefined_bento_id)
  end

  def fork_params
    params.require(:fork_info).permit(:from, :to, institution_ids: [])
  end
end
