# frozen_string_literal: true
require 'csv'

# This is the Institution controller
class InstitutionsController < ApplicationController
  include Versionable
  include Filterrificable
  include AllocationSyncable

  load_resource except: :index
  authorize_resource

  MULTIVALUED_TEXT_FIELDS = %w[ip_addresses change_dates notify_dates
                               zip_codes counties pines_codes wayfinder_keywords
                               open_athens_subscope_array].freeze

  before_action :set_form_data, only: %i[new create edit update]

  add_breadcrumb 'Institutions', :institutions_path
  add_breadcrumb 'Deleted', :deleted_institutions_path, only: [:deleted]


  def index
    @filterrific = initialize_filterrific(
      Institution, params[:filterrific]&.permit!&.to_h,
      select_options: {
        sorted_by: Institution.sort_options,
        glri_states: Institution.glri_options,
        active_states: Institution.active_states,
        open_athens_states: Institution.open_athens_states,
        institution_groups: options_for(InstitutionGroup.all.code_sort, :code),
        institution_sites: Site.options_for_select,
        bento_templates: TemplateViewBento.template_names
      }
    )

    @pagy, @institutions = pagy(
      @filterrific.find.accessible_by(
        current_ability, params[:action].to_sym).includes(:institution_group)
    )

    respond_to do |format|
      format.html
      format.js
      format.csv do
        csv_report, filename = ReportService.rows_for(@filterrific.find.accessible_by(current_ability, params[:action].to_sym).includes(:institution_group))
        send_data csv_report, filename: "#{filename}-#{Time.now.to_formatted_s(:number)}.csv"
      end
    end
  end

  def new
    @institution = Institution.new
    add_breadcrumb 'New', :new_institution_path
  end

  def create
    @institution = Institution.new(institution_params)

    if @institution.save
      redirect_to institution_path @institution
    else
      render :new, status: :unprocessable_entity
    end
  end

  def show
    add_breadcrumb @institution.name, institution_path(@institution)
  end

  def edit
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Edit', edit_institution_path(@institution)
  end

  def update
    new_attributes = institution_params.dup
    new_attributes[:resource_ids].push(*@institution.resources.glri.pluck(:id))
    purge_allocations
    if @institution.update(new_attributes)
      redirect_to institution_path @institution
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def edit_institutional
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Edit', edit_institutional_institution_path(@institution)
  end

  def update_institutional
    new_attributes = institutional_options_params

    if @institution.update(new_attributes)
      redirect_to institution_path @institution
    else
      render :edit_institutional, status: :unprocessable_entity
    end
  end

  def destroy
    purge_all_allocations
    @institution.destroy
    redirect_to deleted_institutions_path
  end

  def new_new_password
    safe_send_and_redirect(
      :new_new_password,
      'app.institution.messages.new_password.success',
      'app.institution.messages.new_password.error'
    )
  end

  def update_password_asap
    safe_send_and_redirect(
      :change_asap,
      'app.institution.messages.update_password.success',
      'app.institution.messages.update_password.error'
    )
  end

  def notify_current_password
    safe_send_and_redirect(
      :notify_current_password,
      'app.institution.messages.notify_password.success',
      'app.institution.messages.notify_password.error'
    )
  end

  def notify_new_password
    safe_send_and_redirect(
      :notify_new_password,
      'app.institution.messages.notify_password.success',
      'app.institution.messages.notify_password.error'
    )
  end

  def express_links_report
    csv_report, filename = ReportService.institution_express_links(params[:id])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def resources_report
    csv_report, filename = ReportService.institution_resources(params[:id])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def glri_resources_report
    csv_report, filename = ReportService.institution_glri_resources(params[:id])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def institution_serial_count_report
    csv_report, filename = ReportService.institution_serial_count((params[:id]))
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  private

  def safe_send_and_redirect(message, sent_msg_key, failed_msg_key)
    PasswordService.new.send(message, @institution)
    flash[:success] = I18n.t(sent_msg_key)
  rescue Exceptions::PasswordChangeError => e
    message = @institution.errors&.map(&:message)&.join(', ')
    flash[:error] = I18n.t(failed_msg_key, message: message)
  ensure
    redirect_to institution_path @institution
  end

  def set_form_data
    @data = {}
    @data[:nonglri_resources] =
      Resource.central.active.includes(:vendor).order(name: :asc)
              .map do |r|
                [r.name_with_code, r.id, {
                  'data-code': r.code,
                  'data-vendor-id': r.vendor&.id
                }]
              end
    @data[:sibling_institutions] = sibling_institution_data

    @data[:nonglri_allocation_resources] =
      @institution.resources.includes(:vendor).central.order(name: :asc)
                  .map do |r|
                  [r.name_with_code, r.id, {
                    'data-code': r.code,
                    'data-vendor-id': r.vendor&.id
                  }]
                end
    @data[:vendors] =
      Vendor.all.order(name: :asc).map do |v|
        [v.name_with_code, v.id]
      end
  end

  def sibling_institution_data
    return [] unless @institution.institution_group

    @institution.institution_group.institutions.real.active.includes(:resources)
                .order(name: :asc).map do |i|
      [
        i.name_with_code, i.id,
        { 'data-resource-ids' => i.resource_ids.join(',') }
      ]
    end
  end

  def institution_params
    prepare_params(
      params.require(:institution).permit(
        :name, :code, :public_code, :active, :open_athens, :ip_addresses,
        :ip_authentication, :address, :galileo_ez_proxy_ip, :use_local_ez_proxy,
        :galileo_ez_proxy_url, :local_ez_proxy_ip, :local_ez_proxy_url,
        :glri_participant, :current_password, :new_password, :prev_password,
        :password_last_notified, :password_last_changed, :notify_group_id,
        :notify_dates, :change_dates, :open_athens_scope, :open_athens_api_name,
        :open_athens_subscope, :open_athens_subscope_array,
        :open_athens_org_id, :open_athens_entity_id, :open_athens_proxy_ip,
        :oa_proxy, :oa_proxy_org, :view_switching,
        :zip_codes, :counties, :eds_profile_default, :eds_profile_kids,
        :eds_profile_teen, :eds_profile_high_school, :eds_customer_id,
        :eds_managed_by, :special, :test_site, :note, :special_information,
        :lti_consumer_key, :lti_secret, :lti_custom_inst_id,
        :journals_link, :pines_codes,
        :institution_url_1, :institution_url_label_1,
        :institution_url_2, :institution_url_label_2,
        :institution_url_3, :institution_url_label_3,
        :institution_url_4, :institution_url_label_4,
        :institution_url_5, :institution_url_label_5,
        :zotero_bib,
        :logo, :remove_logo, :wayfinder_keywords,
        :institution_group_id, :bento_template, resource_ids: []
      )
    )
  end

  def institutional_options_params
    prepare_params(
      params.require(:institution).permit(
        :institution_url_1, :institution_url_label_1,
        :institution_url_2, :institution_url_label_2,
        :institution_url_3, :institution_url_label_3,
        :institution_url_4, :institution_url_label_4,
        :institution_url_5, :institution_url_label_5,
        :zotero_bib,
        :logo, :remove_logo, :wayfinder_keywords
      )
    )
  end


  def prepare_params(params)
    params.each do |f, v|
      if v.is_a? Array
        params[f] = v.reject(&:empty?)
        next
      end
      if MULTIVALUED_TEXT_FIELDS.include? f
        params[f] = v.gsub("\r\n", "\n").strip.split("\n")
      end
    end
  end
end
