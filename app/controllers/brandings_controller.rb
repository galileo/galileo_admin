# frozen_string_literal: true

class BrandingsController < ApplicationController
  load_and_authorize_resource :institution
  load_and_authorize_resource :branding, through: :institution

  add_breadcrumb 'Institutions', :institutions_path

  # GET /brandings
  def index
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Brandings', institution_brandings_path(@institution)

    @pagy, @brandings = pagy(@institution.brandings)
  end

  # GET /brandings/1
  def show
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Brandings', institution_brandings_path(@institution)
    add_breadcrumb @branding.name, institution_branding_path(@institution, @branding)
  end

  # GET /brandings/new
  def new
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Brandings', institution_brandings_path(@institution)
    add_breadcrumb 'New', new_institution_branding_path(@institution)
    @branding = Branding.new
  end

  # GET /brandings/1/edit
  def edit
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Brandings', institution_brandings_path(@institution)
    add_breadcrumb @branding.name, institution_branding_path(@institution, @branding)
    add_breadcrumb 'Edit', edit_institution_branding_path(@institution, @branding)
  end

  # POST /brandings
  def create
    respond_to do |format|
      if @branding.save
        format.html {
          redirect_to institution_branding_path(@institution, @branding),
                      notice: 'Branding was successfully created.'
        }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /brandings/1
  def update
    respond_to do |format|
      if @branding.update(branding_params)
        format.html {
          redirect_to institution_branding_path(@institution, @branding),
                      notice: 'Branding was successfully updated.'
        }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /brandings/1
  def destroy
    @branding.destroy
    respond_to do |format|
      format.html {
        redirect_to institution_brandings_path(@institution),
                    notice: 'Branding was successfully destroyed.'
      }
    end
  end

  def sidebar_label
    t("app.menu.institutions")
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def branding_params
    params.require(:branding).permit(
      :institution_id, :name, :code, :text, :image, :remove_image
    )
  end
end
