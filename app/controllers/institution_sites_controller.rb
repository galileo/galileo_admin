# frozen_string_literal: true

# resource related actions for a given institution
class InstitutionSitesController < ApplicationController
  before_action :set_institution
  before_action :set_site, except: [:index, :new]

  add_breadcrumb 'Institutions', :institutions_path

  MULTIVALUED_TEXT_FIELDS = %w[wayfinder_keywords].freeze

  def index
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Sites', institution_sites_path(@institution)
    @filterrific = initialize_filterrific(
    @institution.sites,
      params[:filterrific],
      select_options: {
        sorted_by: Site.sort_options,
        for_site_type: Site.allowed_site_types,
        charter_states: Site.charter_states
      }
    )

    @pagy, @sites = pagy(@filterrific.find.accessible_by(current_ability, params[:action].to_sym))
    respond_to do |format|
      format.html
      format.js
      format.csv do
        csv_report, filename = ReportService.sites(@filterrific.find
                                                               .accessible_by(current_ability, params[:action].to_sym))
        send_data csv_report, filename: "#{filename}_#{Time.now.to_s.underscore}.csv"
      end
    end
  end

  def show
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Sites', institution_sites_path(@institution)
    add_breadcrumb @site.name, institution_site_path(@institution, @site)

    render 'sites/show'
  end

  def edit
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb 'Sites', institution_sites_path(@institution)
    add_breadcrumb @site.name, institution_site_path(@institution, @site)
    add_breadcrumb 'Edit', edit_institution_site_path(@institution, @site)
  end

  def update
    if @site.update(institution_sites_params)
      redirect_to institution_site_path(@institution, @site), notice: 'Site was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def sidebar_label
    t('app.menu.institutions')
  end

  private

  def set_institution
    @institution = Institution.find params[:institution_id]
    authorize! :show, @institution
  end

  def set_site
    @site = Site.find params[:id]
  end

  def institution_sites_params
    prepare_params(
      params.require(:site).permit(:wayfinder_keywords)
    )
  end

  def prepare_params(params)
    params.each do |f, v|
      if v.is_a? Array
        params[f] = v.reject(&:empty?)
        next
      end
      if MULTIVALUED_TEXT_FIELDS.include? f
        params[f] = v.gsub("\r\n", "\n").strip.split("\n")
      end
    end
  end
end
