# frozen_string_literal: true

class SitesController < ApplicationController
  include Filterrificable

  MULTIVALUED_TEXT_FIELDS = %w[wayfinder_keywords].freeze

  load_and_authorize_resource :site

  add_breadcrumb 'Sites', :sites_path

  def index
    @filterrific = initialize_filterrific(
      Site.accessible_by(current_ability),
      params[:filterrific]&.permit!&.to_h,
      select_options: {
        sorted_by: Site.sort_options,
        for_site_type: Site.allowed_site_types,
        charter_states: Site.charter_states,
        institutions: options_for(Institution.with_sites.active.uniq.sort_by(&:name), :name_with_code),
        institution_groups: options_for(InstitutionGroup.with_sites.sort_by(&:code), :code_with_type)
      }
    )

    @pagy, @sites = pagy(@filterrific.find)
    respond_to do |format|
      format.html
      format.js
      format.csv do
        csv_report, filename = ReportService.sites(@filterrific.find)
        send_data csv_report, filename: "#{filename}_#{Time.now.to_s.underscore}.csv"
      end
    end
  end

  def show
    add_breadcrumb @site.name, site_path(@site)
  end

  def new
    add_breadcrumb 'New', new_site_path(institution_id: @institution)
    @site = Site.new(institution_id: params[:institution_id])
  end

  def edit
    add_breadcrumb @site.name, site_path(@site)
    add_breadcrumb 'Edit', edit_site_path(@site)

  end

  def create
    respond_to do |format|
      if @site.save
        format.html { redirect_to site_path(@site), notice: 'Site was successfully created.' }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sites/1
  # PATCH/PUT /sites/1.json
  def update
    respond_to do |format|
      if @site.update(site_params)
        format.html { redirect_to site_path(@site), notice: 'Site was successfully updated.' }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sites/1
  def destroy
    @site.destroy
    respond_to do |format|
      format.html { redirect_to sites_path, notice: 'Site was successfully destroyed.' }
    end
  end

  def sidebar_label
    t("app.menu.sites")
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def site_params
    prepare_params(
      params.require(:site).permit(
        :institution_id, :name, :code, :site_type, :wayfinder_keywords, :charter
      )
    )
    end

  def prepare_params(params)
    params.each do |f, v|
      if v.is_a? Array
        params[f] = v.reject(&:empty?)
        next
      end
      if MULTIVALUED_TEXT_FIELDS.include? f
        params[f] = v.gsub("\r\n", "\n").strip.split("\n")
      end
    end
  end
end
