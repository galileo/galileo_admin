# frozen_string_literal: true

require 'csv'

# Nightly Report related actions
class VendorStatisticsIdentifierForStatsController < ActionController::Base
  AUTHORIZED_IPS = Rails.application.credentials[:vendor_statistics_identifier_for_stats][:authorized_ip][Rails.env.to_sym]

  before_action :verify_ip_address

  def report
    csv_report, filename = ReportService.vendor_statistics_identifier_for_stats(params[:resource_code])

    send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
  end

  private

  def vendor_statistics_identifier_for_stats_params
    params.require(:resource_code)
  end

  def verify_ip_address
    unless AUTHORIZED_IPS.include? request.remote_ip
      head :unauthorized
    end
  end
end
