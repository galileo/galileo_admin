# frozen_string_literal: true

# actions for Notify Groups
class NotifyGroupsController < ApplicationController
  load_and_authorize_resource

  MULTIVALUED_TEXT_FIELDS = %w[update_history change_dates notify_dates].freeze

  add_breadcrumb 'Notify Groups', :notify_groups_path

  # GET /notify_groups
  def index
    @pagy, @notify_groups = pagy(@notify_groups.text_search(params[:query]))
  end

  # GET /notify_groups/1
  def show
    add_breadcrumb @notify_group.name, notify_group_path(@notify_group)
  end

  # GET /notify_groups/new
  def new
    @notify_group = NotifyGroup.new
    add_breadcrumb 'New', new_notify_group_path
  end

  # GET /notify_groups/1/edit
  def edit
    add_breadcrumb @notify_group.name, notify_group_path(@notify_group)
    add_breadcrumb 'Edit', edit_notify_group_path(@notify_group)
  end

  # POST /notify_groups
  def create
    @notify_group = NotifyGroup.new(notify_group_params)

    respond_to do |format|
      if @notify_group.save
        format.html { redirect_to @notify_group, notice: 'Notify group was successfully created.' }
        format.json { render :show, status: :created, location: @notify_group }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @notify_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notify_groups/1
  def update
    respond_to do |format|
      if @notify_group.update(notify_group_params)
        format.html { redirect_to @notify_group, notice: 'Notify group was successfully updated.' }
        format.json { render :show, status: :ok, location: @notify_group }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @notify_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notify_groups/1
  def destroy
    @notify_group.destroy
    respond_to do |format|
      format.html { redirect_to notify_groups_url, notice: 'Notify group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_notify_group
    @notify_group = NotifyGroup.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def notify_group_params
    prepare_params(
      params.require(:notify_group).permit(
        :code, :name, :notify_dates, :change_dates,
        :note, :special_information
      )
    )
  end

  def prepare_params(params)
    params.each do |f, v|
      if v.is_a? Array
        params[f] = v.reject(&:empty?)
        next
      end
      params[f] = v.gsub("\r\n", "\n").strip.split("\n") if MULTIVALUED_TEXT_FIELDS.include? f
    end
  end

end
