# frozen_string_literal: true

module Api
  module V1
    # shared controller behavior for all API::V1 endpoints
    class BaseController < ActionController::Base
      respond_to :json

      before_action :authenticate_token

      # for checking status
      def ok
        head :ok
      end

      private

      def authenticate_token
        if Devise.secure_compare(
          request.headers['X-User-Token'],
          Rails.application.credentials.auth_support_api_token
        )
          true
        else
          head :unauthorized
        end
      end
    end
  end
end