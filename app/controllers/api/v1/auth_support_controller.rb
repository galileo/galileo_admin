# frozen_string_literal: true

module Api
  module V1
    # Handle turning any of a variety of identifiers into an institution
    class AuthSupportController < BaseController
      def inst
        render json: AuthSupportService.institution_lookup(params)
      rescue ActionController::BadRequest
        head :bad_request
      end
    end
  end
end