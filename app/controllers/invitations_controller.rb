# frozen_string_literal: true

# handle logic for composing, sending and receiving invitations
class InvitationsController < Devise::InvitationsController
  authorize_resource class: false, except: %i[edit update]

  before_action :configure_permitted_parameters, only: [:create]

  add_breadcrumb 'Users', :users_path, except: [:edit]
  add_breadcrumb 'Invitations', :auth_invitations_path, except: [:edit]
  add_breadcrumb 'New', :new_user_invitation_path, only: [:new]

  # show all pending invitations
  def index
    @pagy, @pending_invitations = pagy(User.pending_invitation_response)
  end

  def create
    super
  end

  def after_invite_path_for(resource)
    users_path
  end

  def sidebar_label
    t("app.menu.users")
  end

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(
      :invite, keys: [:role, :first_name, :last_name, institution_ids: [], subroles: []]
    )
  end

end
