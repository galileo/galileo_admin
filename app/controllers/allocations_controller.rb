# frozen_string_literal: true

# handle Allocation actions
class AllocationsController < ApplicationController
  include Filterrificable

  load_and_authorize_resource

  MULTIVALUED_TEXT_FIELDS = %w[keywords].freeze

  def index
    add_breadcrumb 'Allocations', allocations_path

    @filterrific = initialize_filterrific(
      Allocation.accessible_by(current_ability), params[:filterrific]&.permit!&.to_h,
      select_options: {
        sorted_by: Allocation.sort_options,
        display_states: Allocation.display_states,
        glri_states: Allocation.glri_states,
        bypass_galileo_authentication_states: Allocation.bypass_galileo_authentication_states,
        open_athens_states: Allocation.open_athens_states,
        name_override_states: Allocation.name_override_states,
        vendors: options_for(Vendor.all.uniq.sort_by(&:name), :name),
        subscription_types: Allocation.subscription_types,
        resources: options_for(Resource.active.uniq.sort_by(&:name), :name_with_code),
        institutions: options_for(Institution.active.uniq.sort_by(&:name), :name_with_code),
        institution_groups: options_for(InstitutionGroup.all.sort_by(&:code), :code_with_type),
      }
    )
    @pagy, @allocations = pagy(
      @filterrific.find
    )
    respond_to do |format|
      format.html
      format.js
      format.csv do
        csv_report, filename = ReportService.allocations_report(allocations: @filterrific.find.includes(:resource, institution: :institution_group))
        send_data csv_report, filename: "#{filename}-#{Time.now.to_formatted_s(:number)}.csv"
      end
    end
  end

  def show
    add_breadcrumb @allocation.institution.name, institution_path(@allocation.institution)
    add_breadcrumb 'Resources', institution_resources_path(@allocation.institution)
    add_breadcrumb @allocation.resource.name, allocation_path(@allocation)

    @link_data = LinkService.new(@allocation).link_data
  end

  def edit
    add_breadcrumb @allocation.institution.name, institution_path(@allocation.institution)
    add_breadcrumb @allocation.resource.name, allocation_path(@allocation)
    add_breadcrumb 'Edit', edit_allocation_path(@allocation)
  end

  def update
    if @allocation.update(allocation_params)
      redirect_to @allocation
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # Don't allow destroy controller action - allocation creation/destruction
  # should be handled via the Resource/Institution forms
  #def destroy
  #  @allocation.destroy
  #  redirect_to institutions_path
  #end

  def sidebar_label
    if current_user&.admin?
      t("app.menu.allocations")
    else
      t("app.menu.institutions")
    end
  end

  private

  def allocation_params
    prepare_params(
      params.require(:allocation).permit(
        :display, :bypass_open_athens, :ip_access_url, :remote_access_url, :open_athens_url, :keywords,
        :user_id, :password, :note, :special_info, :override_user_views, :subscription_type, :branding_id,
        :vendor_statistics_identifier, :name, subject_ids: [], user_view_ids: [])
    )
  end

  def prepare_params(params)
    params.each do |f, v|
      if v.is_a? Array
        params[f] = v.reject(&:empty?)
        next
      end
      if MULTIVALUED_TEXT_FIELDS.include? f
        params[f] = v.gsub("\r\n", "\n").strip.split("\n")
      end
    end
  end
end
