# frozen_string_literal: true

# define shared controller behaviors for Filterrific implementations
module Filterrificable
  extend ActiveSupport::Concern

  included do
    helper_method :distilled_filters
  end

  # Generate an Array for use in a select box
  # @param [Object] things
  # @param [Symbol] method
  # @return [Array]
  def options_for(things, method, attribute: 'id')
    things.collect { |t| [t.send(method), t.send(attribute)] }
  end

  def distilled_filters
    return nil if @filterrific.nil?
    @filterrific.to_hash
                .without('sorted_by')
                .transform_values{ |v| v.is_a?(Array) ? v.reject(&:blank?) : v }
                .filter{ |_k, v| v.present? }
  end
end