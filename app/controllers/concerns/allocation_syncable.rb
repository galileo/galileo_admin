# frozen_string_literal: true

# define shared controller behaviors for Allocation sync (removal) in the index
module AllocationSyncable
  extend ActiveSupport::Concern

  def core_resources_diff
    current_core_resource_ids = @institution.core_resource_ids
    return [] if institution_params[:resource_ids].nil?

    new_core_resource_ids = institution_params[:resource_ids].reject(&:blank?)
                                                             .map(&:to_i)
    current_core_resource_ids - new_core_resource_ids
  end

  def institutions_diff
    current_institution_ids = @resource.institution_ids
    new_institution_ids = resource_params[:institution_ids].reject(&:blank?)
                                                           .map(&:to_i)
    current_institution_ids - new_institution_ids
  end

  # @param [Resource[]] resources
  # @param [Institution[]] institutions
  # @returns [Allocations[]] allocations
  def retrieve_allocations_for(resources, institutions)
    Allocation.where(
      resource: resources,
      institution: institutions
    )
  end

  def purge_allocations
    allocations =
      case controller_name.classify
      when 'Resource'
        removed_institutions = institutions_diff
        return unless removed_institutions.any?

        resources = @resource.children ? @resource.child_ids + [@resource.id] : @resource.id
        retrieve_allocations_for(
          resources,
          removed_institutions
        )
      when 'Institution'
        removed_core_resources = core_resources_diff
        return unless removed_core_resources.any?

        retrieve_allocations_for(
          removed_core_resources,
          @institution
        )
      else
        raise ArgumentError, 'AllocationSyncable methods used in an inappropriate context!'
      end

    deindex allocations
  end

  def purge_all_allocations
    case controller_name.classify
    when 'Resource'
      deindex @resource.allocations
    when 'Institution'
      deindex @institution.allocations
    else
      raise ArgumentError, 'AllocationSyncable methods used in an inappropriate context!'
    end
  end

  def deindex(allocations)
    IndexingService.deindex allocations
  end
end