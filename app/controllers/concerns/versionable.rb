# frozen_string_literal: true

# define shared behaviors for versioned entities
module Versionable
  extend ActiveSupport::Concern

  included do
    before_action :set_record, only: %i[diff rollback]
  end

  def diff
    @version = @record.versions.find params[:version]
  end

  def rollback
    version = @record.versions.find params[:version]
    if version.reify.save
      redirect_to polymorphic_path @record
    else
      render :show, alert: "Record could not be rolled back: #{version.errors}", status: :unprocessable_entity
    end
  end

  private

  def set_record
    @record = controller_name.classify.constantize.find params[:id]
  end

end