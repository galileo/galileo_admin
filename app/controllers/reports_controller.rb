# frozen_string_literal: true

require 'csv'

# Reports related actions
class ReportsController < ApplicationController
  authorize_resource class: false

  add_breadcrumb 'Reports', :reports_path

  def index; end

  def all_rows_for
    csv_report, filename = case params[:table_name]
                           when 'Allocation'
                             ReportService.allocations_report
                           when 'BentoConfig'
                             ReportService.bento_configs_all
                           when 'Contact'
                             ReportService.contacts Contact.all
                           when 'Site'
                             ReportService.sites Site.all
                           else
                             ReportService.all_rows_for params[:table_name]
                           end
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def institution_group_ips
    csv_report, filename = ReportService.institution_ips(inst_group_id: params[:inst_group_id], active_state: params[:active_status])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def institution_resources
    csv_report, filename = ReportService.institution_resources(params[:inst_id])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def institution_glri_resources
    csv_report, filename = ReportService.institution_glri_resources(params[:inst_id])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def institution_express_links
    csv_report, filename = ReportService.institution_express_links(params[:inst_id])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def glri_contacts
    csv_report, filename = ReportService.glri_managed_contacts(params[:glri_type])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def non_glri_institutions
    csv_report, filename = ReportService.non_glri_institutions
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def resources_with_html
    csv_report, filename = ReportService.resources_with_html(params[:description_field])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def resources_url_check
    csv_report, filename = ReportService.resources_url_check(params[:resource_type])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end


  def ez_proxy_update
    csv_report, filename = ReportService.ez_proxy_update
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def public_libraries_primary_contacts
    csv_report, filename = ReportService.public_libraries_primary_contacts
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def institution_group_contacts
    csv_report, filename = ReportService.institution_group_contacts(params[:inst_group_code])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def vendor_resources
    csv_report, filename = ReportService.vendor_resources((params[:vendor_id]))
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def allocations_vendor
    csv_report, filename = ReportService.allocations_vendor((params[:vendor_id]))
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def allocations_resource
    csv_report, filename = ReportService.allocations_resource((params[:resource_id]))
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def allocations_institution
    csv_report, filename = ReportService.allocations_institution((params[:institution_id]))
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def vendors_for_stats
    csv_report, filename = ReportService.vendors_for_stats
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def resources_for_stats
    csv_report, filename = ReportService.resources_for_stats
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def institutions_for_stats
    csv_report, filename = ReportService.institutions_for_stats
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def vendor_statistics_identifier_for_stats
    csv_report, filename = ReportService.vendor_statistics_identifier_for_stats(params[:resource_code])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def glri_allocations
    csv_report, filename = ReportService.glri_allocations
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def inst_user_emails
    csv_report, filename = ReportService.inst_user_emails
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def overlapping_ip_addresses
    csv_report, filename = ReportService.overlapping_ip_addresses
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def institution_features
    csv_report, filename = ReportService.institution_features
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def institution_group_features
    csv_report, filename = ReportService.institution_group_features
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def galileo_passwords
    csv_report, filename = ReportService.galileo_passwords
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def resource_keywords
    csv_report, filename = ReportService.resource_keywords
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def wayfinder_keywords
    csv_report, filename = ReportService.wayfinder_keywords
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def deleted_contacts
    csv_report, filename = ReportService.deleted_contacts
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def stats_for_snapshots
    csv_report, filename = ReportService.stats_for_snapshots(params[:inst_id], params[:month])
    respond_to do |format|
      format.csv do
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

end
