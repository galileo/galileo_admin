# frozen_string_literal: true

# This is the Formats controller
class FormatsController < ApplicationController
  load_and_authorize_resource

  add_breadcrumb 'Formats', :formats_path

  def index
    @pagy, @formats = pagy(@formats.order(:name))
  end

  def show
    add_breadcrumb @format.name, format_path(@format)
  end

  def new
    @format = Format.new
    add_breadcrumb 'New', new_format_path
  end

  def edit
    add_breadcrumb @format.name, format_path(@format)
    add_breadcrumb 'Edit', edit_format_path(@format)
  end

  def create
    @format = Format.new(format_params)

    if @format.save
      redirect_to @format
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @format.update(format_params)
      redirect_to @format
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @format.destroy
    redirect_to formats_path
  end

  private

  def format_params
    params.require(:format).permit(:name)
  end
end
