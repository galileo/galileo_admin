class BentoConfigsController < ApplicationController
  load_and_authorize_resource :institution, except: [:show]
  load_and_authorize_resource :bento_config, through: :institution, except: [:show]
  skip_authorize_resource :bento_config, only: [:eds_content_providers]

  add_breadcrumb 'Institutions', :institutions_path

  # GET /bento_configs
  def index
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb t('activerecord.attributes.institution.configured_bentos'), institution_configured_bentos_path(@institution)
    add_breadcrumb t('app.bento_configs.label.bento_configs'), institution_bento_configs_path(@institution)
    @pagy, @bento_config_configs = pagy(@institution.bento_configs)
  end

  # GET /bento_configs/1
  def show
    @bento_config = BentoConfig.includes(:institution).find(params[:id])
    authorize! :show, @bento_config
    @institution = @bento_config.institution

    add_breadcrumb(@institution.name, institution_path(@institution))
    add_breadcrumb t('activerecord.attributes.institution.configured_bentos'), institution_configured_bentos_path(@institution)
    add_breadcrumb t('app.bento_configs.label.bento_configs'), institution_bento_configs_path(@institution)
    add_breadcrumb @bento_config.display_name, institution_bento_config_path(@institution, @bento_config)
  end

  # GET /bento_configs/new
  def new
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb t('activerecord.attributes.institution.configured_bentos'), institution_configured_bentos_path(@institution)
    add_breadcrumb t('app.bento_configs.label.bento_configs'), institution_bento_configs_path(@institution)
    add_breadcrumb 'New', new_institution_bento_config_path(@institution)
    @bento_config = BentoConfig.new
  end

  # GET /bento_configs/1/edit
  def edit
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb t('activerecord.attributes.institution.configured_bentos'), institution_configured_bentos_path(@institution)
    add_breadcrumb t('app.bento_configs.label.bento_configs'), institution_bento_configs_path(@institution)
    add_breadcrumb @bento_config.display_name, institution_bento_config_path(@institution, @bento_config)
    add_breadcrumb 'Edit', edit_institution_bento_config_path(@institution, @bento_config)
  end

  # POST /bento_configs
  def create
    respond_to do |format|
      if @bento_config.save
        format.html do
          redirect_to institution_bento_config_path(@institution, @bento_config),
                      notice: 'API Configuration was successfully created.'
        end
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bento_configs/1
  def update
    respond_to do |format|
      if @bento_config.update(bento_config_params)
        format.html do
          redirect_to institution_bento_config_path(@institution, @bento_config),
                      notice: 'API Configuration was successfully updated.'
        end
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bento_configs/1
  def destroy
    @bento_config.destroy
    respond_to do |format|
      format.html do
        redirect_to institution_bento_configs_path(@institution),
                    notice: 'API Configuration was successfully destroyed.'
      end
    end
  end

  def test_config
    add_breadcrumb @institution.name, institution_path(@institution)
    add_breadcrumb t('activerecord.attributes.institution.configured_bentos'), institution_configured_bentos_path(@institution)
    add_breadcrumb t('app.bento_configs.label.bento_configs'), institution_bento_configs_path(@institution)
    add_breadcrumb @bento_config.display_name, institution_bento_config_path(@institution, @bento_config)
    add_breadcrumb 'Test Credentials', test_config_institution_bento_config_path(@institution, @bento_config)
    begin
      eds_api = EdsApi.new @bento_config, use_auth_token_cache: false
      @eds_response = eds_api.search({ query: 'test' })
    rescue EBSCO::EDS::Error, EdsApi::EdsApiError => e
      @error_info = if e.is_a? EBSCO::EDS::Error
                      { class: e.class.to_s,
                        description: e.fault&.dig(:error_body, 'ErrorDescription'),
                        detailed_description: e&.fault.dig(:error_body, 'DetailedErrorDescription'),
                        eds_error_number: e.fault&.dig(:error_body, 'ErrorNumber') }
                    else
                      { class: e.class.to_s,
                        description: e.message }
                   end
    end
  end

  def eds_content_providers
    return head(:bad_request) unless @bento_config.service == 'eds_api'
    render json: @bento_config.eds_content_providers.map {|provider_name| {
      value: provider_name,
      label: provider_name
    }}
  end

  def sidebar_label
    t('app.menu.institutions')
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def bento_config_params
    p = params.require(:bento_config).permit(:service, :institution_id,  :name, :api_profile, :user_id, :password,
                                             credentials: {}, view_types: [])
    p[:credentials] = p[:credentials].permit(BentoService.get(p[:service])&.config_fields)
    p
  end

end
