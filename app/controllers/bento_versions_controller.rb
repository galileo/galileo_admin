# frozen_string_literal: true

# This is the Versions controller
class BentoVersionsController < ApplicationController
  load_resource except: %i[index deleted]
  authorize_resource

  before_action :redirect_unless_eligible, only: :undelete

  add_breadcrumb 'History', :versions_path
  add_breadcrumb 'Bento History', :bento_versions_path

  def index
    @filterrific = initialize_filterrific(
      BentoVersion.accessible_by(current_ability),
      params[:filterrific],
      select_options: {
        sorted_by: BentoVersion.sort_options,
        with_item_type: BentoVersion.item_type_options,
        with_event: %w[create update destroy],
        with_user_id: User.options_for_select,
        with_role: User::VALID_ROLES
      },
      persistence_id: false
    )

    @pagy, @bento_versions = pagy(
      @filterrific.find.accessible_by(
        current_ability, params[:action].to_sym)
    )

    respond_to do |format|
      format.html
      format.js
    end
  end

  def deleted
    @klass = params[:item_type].classify
    add_breadcrumb "Deleted #{@klass}s", deleted_versions_path(@klass)

    @filterrific = initialize_filterrific(
      BentoVersion.where(event: 'destroy', item_type: @klass),
      params[:filterrific],
      select_options: {
        sorted_by: BentoVersion.sort_options,
        with_event: %w[create update destroy],
        with_user_id: User.options_for_select,
        with_role: User::VALID_ROLES
      },
      persistence_id: false
    )

    @pagy, @bento_versions = pagy(@filterrific.find)

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    klass = @bento_version.item_type.constantize
    @item = klass.find_or_initialize_by id: @bento_version.item_id
    @bento_version = @item.versions.find(params[:id])

    add_breadcrumb @bento_version.id, bento_version_path(@bento_version)
  end

  def undelete
    klass = @bento_version.item_type.constantize
    item = klass.new(id: @bento_version.item_id).versions.last.reify
    if item.save
      redirect_to polymorphic_path(item), alert: 'Record undeleted!'
    else
      redirect_to bento_version_path(@bento_version),
                  alert: "Record not undeleted: #{item.errors}",
                  status: :unprocessable_entity
    end
  end

  def sidebar_label
    t('app.menu.versions')
  end

  private

  def redirect_unless_eligible
    return true unless @bento_version.item_type.constantize.exists? @bento_version.item_id

    redirect_to version_path(@bento_version),
                alert: 'Record has already been undeleted!'
  end
end
