# frozen_string_literal: true

# handle Password actions
class PasswordsController < ApplicationController
  load_and_authorize_resource

  include Sortable

  add_breadcrumb 'Passwords', :passwords_path

  # GET /passwords
  def index
    @pagy, @passwords = pagy(@passwords
                               .text_search(params[:query])
                               .order(sort_column + ' ' + sort_direction))
  end

  # GET /passwords/1
  def show
    add_breadcrumb @password.password, password_path(@password)
  end

  # GET /passwords/new
  def new
    @password = Password.new
    add_breadcrumb 'New', new_password_path
  end

  # GET /passwords/1/edit
  def edit
    add_breadcrumb @password.password, password_path(@password)
    add_breadcrumb 'Edit', edit_password_path(@password)
  end

  # POST /passwords
  def create
    @password = Password.new(password_params)

    if @password.save
      redirect_to @password
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /passwords/1
  def update
    if @password.update(password_params)
      redirect_to @password
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /passwords/1
  def destroy
    @password.destroy
    redirect_to passwords_path
  end

  private

  def password_params
    params.require(:password).permit(:password, :commit_date, :institution_id,
                                     :exclude)
  end
end
