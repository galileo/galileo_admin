
# frozen_string_literal: true

class HelpdeskAbility
  include CanCan::Ability

  def initialize(user)
    can :read, Institution
    can :express_links_report, Institution
    can :resources_report, Institution
    can :glri_resources_report, Institution
    can %i[read create update destroy diff deleted], Contact
    can :notify_current_password, Contact
    # ability to see version history for contacts
    can :show, Version
    can :deleted, Version
    can :read, PaperTrail::Version
  end
end
