
# frozen_string_literal: true

class AdminAbility
  include CanCan::Ability

  def initialize(user)
    can :manage, :all
    can :manage, :reports
    can :manage, :tools
  end
end
