
# frozen_string_literal: true

class AdvancedSubroleAbility
  include CanCan::Ability
  def initialize(user)
    # can update Resource data only for GLRI records user by their Institutions
    insts_glri_ids = Resource.where(institution_id: user.institution_ids).pluck(:id)
    can %i[read new copy], Resource
    can :update, Resource, id: insts_glri_ids

    # can create only GLRI resources
    can :create, Resource, institution_id: user.institution_ids

    # full control over GLRI resources for their Institutions
    insts_resource_ids = Allocation.where(institution_id: user.institution_ids).pluck(:resource_id)
    can :manage, Allocation,institution_id: user.institution_ids, resource_id: insts_resource_ids

    # full control over Branding for their Institutions
    can :manage, Branding, institution_id: user.institution_ids
  end
end