# frozen_string_literal: true

class InstitutionalAbility
  include CanCan::Ability
  def initialize(user)
    # can read Institution data for assigned Institutions
    can :read, Institution, id: user.institution_ids
    can :read, Site, institution_id: user.institution_ids

    # Read Banners for their Institutions
    can :read, Banner, institution_id: user.institution_ids

    # Read Widgets for their Institutions
    can :read, Widget, institution_id: user.institution_ids

    # can read reports for assigned Institutions
    can :express_links_report, Institution, id: user.institution_ids
    can :resources_report, Institution, id: user.institution_ids
    can :glri_resources_report, Institution, id: user.institution_ids
    can :institution_serial_count_report, Institution, id: user.institution_ids

    # can read Resource data
    can :read, Resource

    # read over Allocations for their Institutions
    can :read, Allocation, institution_id: user.institution_ids

    # full read Branding for their Institutions
    can :read, Branding, institution_id: user.institution_ids

    # read rights over Feature records for institution groups
    insts_group_ids = user.institutions.collect(&:institution_group).collect{|x| x&.id}
    can :read, Feature, featuring_type: 'InstitutionGroup', featuring_id: insts_group_ids
    can :read, InstitutionGroup, id: insts_group_ids

    # full control over ConfiguredBento for their Institutions
    can :read, ConfiguredBento, institution_id: user.institution_ids
    can :read, PredefinedBento
  end
end
