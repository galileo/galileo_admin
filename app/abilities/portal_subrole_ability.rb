
# frozen_string_literal: true

class PortalSubroleAbility
  include CanCan::Ability
  def initialize(user)
    # can view/update Institution data for only assigned Institutions
    can %i[read edit_institutional update_institutional], Institution, id: user.institution_ids
    can %i[read edit_institutional update_institutional], Site, institution_id: user.institution_ids

    # full control over GLRI resources for their Institutions
    insts_resource_ids = Allocation.where(institution_id: user.institution_ids).pluck(:resource_id)
    can :manage, Allocation,institution_id: user.institution_ids, resource_id: insts_resource_ids

    # full control over Features for their Institutions
    can :manage, Feature, featuring_type: 'Institution', featuring_id: user.institution_ids

    # full control over Banners for their Institutions
    can :manage, Banner, institution_id: user.institution_ids
    can :new, Banner

    # full control over Widgets for their Institutions
    can :manage, Widget, institution_id: user.institution_ids

    # read rights over Feature records for institution groups
    insts_group_ids = user.institutions.collect(&:institution_group).collect{|x| x&.id}
    can :read, Feature, featuring_type: 'InstitutionGroup', featuring_id: insts_group_ids
    can :read, InstitutionGroup, id: insts_group_ids

    # full control over ConfiguredBento for their Institutions
    can :manage, ConfiguredBento, institution_id: user.institution_ids
    can :read, PredefinedBento
  end
end
