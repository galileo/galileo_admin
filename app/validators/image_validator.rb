class ImageValidator < ActiveModel::Validator
  VALID_CONTENT_TYPES = %w[image/png image/gif image/jpeg image/jpg]

  def validate(record)
    return unless record.image.attached?

    unless  VALID_CONTENT_TYPES.include? record.image.content_type
      record.errors.add :image, 'Must be a PNG, JPG, or GIF image type'
    end
  end
end