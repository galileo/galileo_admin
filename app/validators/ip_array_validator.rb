# frozen_string_literal: true

# validator for legacy ip range arrays
class IpArrayValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return true if value.empty?

    allow_private_ranges = record.code == 'uga1'

    current_range = nil

    if value.any? do |n|
      current_range = n
      !IpService.valid_ip_range? current_range, allow_private_ranges
    end
      message = options[:message] || "has invalid ip range: #{current_range}"
      record.errors.add attribute, message: message
    end
  end
end