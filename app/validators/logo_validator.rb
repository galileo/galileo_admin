class LogoValidator < ActiveModel::Validator
  VALID_CONTENT_TYPES = %w[image/png image/gif image/jpeg image/jpg]

  def validate(record)
    return unless record.logo.attached?

    unless  VALID_CONTENT_TYPES.include? record.logo.content_type
      record.errors.add :logo, 'Must be a PNG, JPG, or GIF image type'
    end
  end
end