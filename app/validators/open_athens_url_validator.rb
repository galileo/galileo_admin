class OpenAthensUrlValidator < ActiveModel::Validator
  def validate(record)
    if record.open_athens_url&.start_with? LinkService::OA_REDIRECTOR_URL
      record.errors.add :open_athens_url,
                        'OpenAthens URL should not contain the OpenAthens redirector prefix. The URL is automatically encoded when "OpenAthens Enabled?" is selected'
    end
  end
end