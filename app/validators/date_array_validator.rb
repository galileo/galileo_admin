# frozen_string_literal: true

# validator for legacy date arrays
class DateArrayValidator < ActiveModel::EachValidator
  include LegacyDatesHelper

  def validate_each(record, attribute, value)
    return true if value.empty?

    if value.any? { |n| !valid_yearless_date? n }
      message = options[:message] || 'has invalid values'
      record.errors.add attribute, :invalid, message: message
    end
  end
end