class WithoutOpenAthensRedirectorValidator < ActiveModel::Validator
  def validate(record)
    if record.without_open_athens_redirector && !record.open_athens
      record.errors.add :without_open_athens_redirector,
                        I18n.t("app.resources.messages.validate_without_open_athens_redirector")
    end
  end
end
