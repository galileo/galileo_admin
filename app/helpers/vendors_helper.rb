# frozen_string_literal: true

# Vendor related helpers
module VendorsHelper
  def general_vendor_fields(vendor)
    {
      I18n.t('activerecord.attributes.vendor.name') => vendor.name,
      I18n.t('activerecord.attributes.vendor.code') => vendor.code,
      I18n.t('activerecord.attributes.vendor.logo') => (image_tag(@vendor.logo_thumbnail) if @vendor.logo.attached?),
      I18n.t('activerecord.attributes.vendor.created_at') => vendor.created_at,
      I18n.t('activerecord.attributes.vendor.updated_at') => vendor.updated_at
    }
  end
  def contacts_vendor_fields(vendor)
    label_and_size = "#{I18n.t('activerecord.attributes.vendor.contacts')} (#{vendor.contacts.size})"

    {
      label_and_size => render('shared/associated_contacts_table', model: vendor)
    }
  end

  def resources_vendor_fields(vendor)
    label_and_size = "#{I18n.t('activerecord.attributes.vendor.resources')} (#{vendor.resources.size})"
    {
      label_and_size => linked_item_group(vendor.resources, %i[code name])
    }
  end
  def vendor_institution_data_table(vendor)
    render 'institution_data_table', vendor: vendor
  end
end
