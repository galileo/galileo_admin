# frozen_string_literal: true

# view-related helper methods common Bootstrap form-related components
module BootstrapFormHelper
  # @param [Object] form
  # @param [Symbol] field
  def bs4_switch(form, field, options = {})
    element = form.check_box(field, custom: :switch, class: options[:class])
    element += "<small class='form-text text-muted'>#{options[:help]}</small>".html_safe if options[:help]
    element
  end
end
