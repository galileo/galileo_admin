# frozen_string_literal: true

# Application-wide button helper methods
module ButtonHelper
  # @param [String] label label for link
  # @param [] path href for link
  # @param [String] classes additional classes for link
  def btn_primary_link(label, path, classes = nil)
    classes ||= ''
    link_to(label, path, class: 'btn btn-primary ' + classes)
  end

  # @param [String] label label for link
  # @param [] path href for link
  # @param [String] classes additional classes for link
  def btn_primary_post(label, path, confirm = nil, classes = nil)
    classes ||= ''
    options = {
      class: 'btn btn-primary ' + classes,
      method: :post
    }
    unless confirm.nil?
      options[:data] = {
        confirm: confirm
      }
    end
    link_to(label, path, options)
  end

  # @param [ActiveRecord_Relation] relation
  # @param [String] classes additional HTML classes to append
  def new_btn_link(relation, classes = '')
    model = relation.klass
    btn_primary_link(
      I18n.t('app.defaults.labels.new'),
      new_polymorphic_path(model),
      classes
    )
  end

  # @param [ActiveRecord::Base] model
  # @param [String] classes additional HTML classes to append
  def edit_btn_link(model, classes = '')
    btn_primary_link I18n.t('app.defaults.labels.edit'),
                     edit_polymorphic_path(model), classes
  end

  # @param [ActiveRecord::Base] model
  # @param [String] classes additional HTML classes to append
  def delete_btn_link(model, classes = '')
    link_to(I18n.t('app.defaults.labels.delete'), polymorphic_path(model),
            class: 'btn btn-primary ' + classes,
            method: :delete, data: {
              confirm: I18n.t('app.defaults.confirmation.confirm')
            })
  end

  # @param [String] table_name
  # @param [String] classes additional HTML classes to append
  def delete_all_stat_btn_link(period, table_name, classes = '')

    link_to('Delete All', stats_delete_all_path(period, table_name),
            class: 'btn btn-primary ' + classes,
            method: :delete, data: {
        confirm: I18n.t('app.defaults.confirmation.confirm')
      })
  end

  # @param [ActiveRecord::Base] model
  # @param [String] classes additional HTML classes to append
  def undelete_btn_link(model, classes = '')
    link_to(I18n.t('app.defaults.labels.undelete'), polymorphic_path([:undelete, model]),
            class: 'btn btn-primary ' + classes,
            method: :post, data: {
              confirm: I18n.t('app.defaults.confirmation.confirm')
            })
  end

  # @param [ActiveRecord::Base] model
  # @param [String] classes additional HTML classes to append
  def show_deleted_btn_link(model, classes = '')
    model_type = model.klass.name.underscore.pluralize
    link_to(I18n.t('app.defaults.labels.deleted'),
            polymorphic_path([:deleted, model_type.to_sym]),
            class: 'btn btn-primary ' + classes)
  end

  def show_deleted_bento_btn_link(klass, classes = '')
    link_to(I18n.t('app.defaults.labels.deleted'),
            deleted_bento_versions_path(klass.table_name),
            class: 'btn btn-primary ' + classes)
  end

  def invitations_page_btn_link
    btn_primary_link I18n.t('app.users.labels.invitations'),
                     auth_invitations_path
  end

  def new_invitation_btn_link
    btn_primary_link I18n.t('app.users.labels.new_invitation'),
                     new_user_invitation_path
  end

  # @param [Institution] institution
  # @param [String] classes additional classes for link
  def new_institution_contact_btn_link(institution, classes = '')
    btn_primary_link I18n.t('app.contacts.labels.new_contact'),
                     new_contact_path(institution_id: institution),
                     classes
  end

  # @param [ApplicationRecord] model
  # @param [String] classes additional classes for link
  def new_associated_contact_btn_link(model, classes = '')
    key_name = "#{model.class.name.underscore}_id"
    btn_primary_link I18n.t('app.contacts.labels.new_contact'),
                     new_contact_path(key_name => model),
                     classes
  end

  # @param [ActiveRecord_Relation] relation
  # @param [String] classes additional HTML classes to append
  def index_page_buttons(relation, classes = '')
    model = relation.klass
    buttons = new_btn_link(relation, classes)
    buttons += invitations_page_btn_link if model.name == 'User'
    tag.div(
      class: 'btn-group',
      role: 'group',
      'aria-label': "#{pretty_model_name(model)} Actions"
    ) do
      buttons
    end
  end

  # @param [ActiveRecord::Base] model
  def show_page_buttons(model)
    edit_btn_link(model) + ' ' + delete_btn_link(model)
  end

  def copy_resource_button(resource)
    btn_primary_link I18n.t('app.resources.labels.copy'),
                     copy_resource_path(resource)
  end

  # @param [ActiveRecord::Base] model
  def action_buttons(model)
    tag.div(
      class: 'btn-group',
      role: 'group',
      'aria-label': "#{model.class.name} Actions"
    ) do
      edit_btn_link(model, 'btn-sm')
    end
  end

  # @param [Constant] model
  def pretty_model_name(model)
    model.name.underscore.titleize
  end
end