# frozen_string_literal: true

# view-related helper methods common Bootstrap components
module BootstrapHelper
  # generate a tab for bootstrap tabs component
  # must be within a <ul class="nav nav-tabs"></ul>
  # @param [String] label for tab
  # @param [Boolean] selected or not
  def nav_tab_for(label, selected = false)
    tag.li(class: 'nav-item') do
      name = label.downcase.dasherize.parameterize
      link_to label, "##{name}", class: 'nav-link', role: 'tab',
              id: "#{name}-tab", data: { toggle: 'tab' },
              aria: { controls: name, selected: selected }
    end
  end

  # generate a div for tab content, referred to by a tab nav
  # must be inside a <div class="tab-content"></div>
  # @param [String] label
  # @param [Boolean] active or not
  def nav_tab_pane_for(label, active = false)
    classes = %w[tab-pane fade]
    classes += %w[show active] if active
    name = label.downcase.dasherize.parameterize
    attributes = { class: classes, id: name, role: 'tabpanel',
                   aria: { labelledby: "#{name}-tab" } }
    if block_given?
      tag.div(**attributes) { yield }
    else
      tag.div(**attributes)
    end
  end

  # generate a link for inclusion in a <div class="list-group">
  # @param [String] label
  # @param [String] link
  # @param [Array] additional_classes
  def list_group_link(label, link, target = '_self', additional_classes = [])
    classes = %w[list-group-item list-group-item-action py-2]
    classes += additional_classes
    if controller.respond_to?(:sidebar_label)
      classes << 'active' if controller.sidebar_label == label
    else
      classes << 'active' if controller_name.pluralize == label.delete(' ').underscore.downcase
    end
    link_to label, link, target: target, class: classes
  end

  # Format array into a set of Bootstrap badges
  # @param [Array<String>] array
  # @return [String]
  def format_array_field_as_badges(array, color: 'primary')
    return '' unless array.present?

    badges = array.map do |value|
      badge value, color: color
    end
    badges.join(' ').html_safe
  end

  # Tag for a primary badge
  # @param [String] value
  def badge(value, color: 'primary')
    "<span class='badge badge-#{color}'>#{value}</span>".html_safe
  end
end
