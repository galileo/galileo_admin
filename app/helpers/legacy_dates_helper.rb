# frozen_string_literal: true

# helpers for working with legacy date syntax
module LegacyDatesHelper
  # @param [Object] yearless_date im MMDD format
  # @return [Boolean]
  def valid_yearless_date?(yearless_date)
    return false if yearless_date.length != 4

    true if DateTime.strptime(
      "2000 #{yearless_date[0..1]} #{yearless_date[2..3]}",
      '%Y %m %d'
    )
  rescue ArgumentError => _e
    false
  end

  # Format a mmdd date for display, typically in a mailer
  def mmdd_display(mmdd, next_year: false)
    return unless mmdd.present?
    year = next_year ? Time.now.year + 1 : Time.now.year
    DateTime.strptime(
      "#{year} #{mmdd[0..1]} #{mmdd[2..3]}",
      '%Y %m %d'
    ).strftime('%B %-d, %Y')
  rescue ArgumentError => _e
    'Date error'
  end

  # @param [Array] dates in MMDD format
  # @return [Boolean]
  def includes_today?(dates)
    dates.include? now_mmdd
  end

  def now_mmdd
    DateTime.now.strftime('%m%d')
  end
end
