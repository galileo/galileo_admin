module BentoConfigsHelper
  def general_bento_config_fields(bento_config)
    {
      I18n.t('activerecord.attributes.bento_config.institution_id') => bento_config.institution.name,
      I18n.t('activerecord.attributes.bento_config.service') => bento_config.service,
      I18n.t('activerecord.attributes.bento_config.view_types') => format_array_field_as_badges(bento_config.view_types),
      I18n.t('activerecord.attributes.bento_config.name') => bento_config.name,
      I18n.t('activerecord.attributes.bento_config.display_name') => bento_config.display_name,
      I18n.t('activerecord.attributes.bento_config.created_at') => bento_config.created_at,
      I18n.t('activerecord.attributes.bento_config.updated_at') => bento_config.updated_at
    }
  end
end
