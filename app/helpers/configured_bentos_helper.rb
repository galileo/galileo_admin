# frozen_string_literal: true

# ConfiguredBento related helpers
module ConfiguredBentosHelper
  def general_configured_bento_fields(configured_bento)
    fields = {
      I18n.t('activerecord.attributes.configured_bento.display_name') =>
        configured_bento.computed_display_name,
      I18n.t('activerecord.attributes.configured_bento.predefined_bento_id') => 
        link_to(configured_bento.predefined_bento.disambiguated_name,
                predefined_bento_path(configured_bento.predefined_bento)),
      I18n.t('activerecord.attributes.configured_bento.bento_config_id') =>
        if configured_bento&.bento_config 
          link_to(configured_bento.bento_config&.display_name,
                  institution_bento_config_path(configured_bento.institution,
                                                configured_bento.bento_config))
         end,
      I18n.t('activerecord.attributes.configured_bento.user_view') => configured_bento.display_user_view,
      I18n.t('activerecord.attributes.configured_bento.created_by') =>
        configured_bento.created_by,
      I18n.t('activerecord.attributes.configured_bento.shown_by_default') =>
        configured_bento.shown_by_default,
      I18n.t('activerecord.attributes.configured_bento.slug') => link_to(configured_bento.slug, configured_bento.galileo_search_url),
      I18n.t('activerecord.attributes.configured_bento.order') => configured_bento.order,
      I18n.t('activerecord.attributes.configured_bento.description') =>
        configured_bento.computed_description
    }

    if FeatureFlags.enabled? :user_bento_customization
      fields[I18n.t('activerecord.attributes.configured_bento.active')] = configured_bento.active
    end

    fields.merge({
                   I18n.t('activerecord.attributes.contact.created_at') => configured_bento.created_at,
                   I18n.t('activerecord.attributes.contact.updated_at') => configured_bento.updated_at
                 })
  end
  # @param institution [Institution]
  # @return [Hash]
  def predefined_bento_availability_by_user_view(institution, current_code = nil)
    user_views = ViewService.valid_view_types_for institution.inst_type
    default_services = BentoService.all_unconfigurable.map(&:code).to_set
    services_by_view = {}
    credential_names = []
    availability = {}
    user_views.each do |uv|
      # each value in services by view will be a hash of service_name => field_names[]
      # e.g., services_by_view['elementary'] = {'galileo' => [], 'eds_api' => ['User ID', 'Password', ...], ...}
      services_by_view[uv] = default_services.map {|s| [s, []]}.to_h
      availability[uv] = []
    end
    institution.bento_configs.each do |bc|
      credential_names << bc.name
      bc.view_types.each do |uv|
        services_by_view[uv][bc.service] ||= []
        services_by_view[uv][bc.service] |= bc.credentials.filter {|k, v| v.present?}.keys if bc.credentials.present?
      end
    end
    pbs = institution.supported_predefined_bentos.to_a
    pbs.each do |pb|
      user_views.each do |uv|
        if services_by_view[uv].include? pb.service
          if current_code != pb.code
            next if (pb.service_field_prerequisites - services_by_view[uv][pb.service]).any?
            next unless pb.service_credentials_name.blank? || credential_names.include?(pb.service_credentials_name)
            next if PredefinedBento::ADMIN_ONLY_CODES.include?(pb.code) && !current_user.admin?
          end
          availability[uv] << pb.code
        end
      end
    end
    availability
  end

  # @param institution [Institution]
  # @return [Hash]
  def bento_config_availability_by_predefined_bento(institution)
    configs_by_service = institution.bento_configs.group_by { |bc| bc.service }
    pbs = institution.supported_predefined_bentos.to_a
    availability = {}
    pbs.each do |pb|
      available = configs_by_service[pb.service] || []
      if pb.service_credentials_name.present?
        available = available.filter { |bc| bc.name == pb.service_credentials_name }
      end
      availability[pb.code] = available&.map &:id if available.any?
    end
    availability
  end

  # @param institution [Institution]
  # @return [Hash]
  def bento_config_availability_by_user_view(institution)
    availability = {}
    institution.bento_configs.each do |bc|
      bc.view_types.each do |uv|
        availability[uv] = [] unless availability.include? uv
        availability[uv] << bc.id
      end
    end
    availability
  end

  def edit_configured_bento_button(configured_bento)
    link_to I18n.t('app.defaults.labels.edit'),
            edit_institution_configured_bento_path(configured_bento.institution, configured_bento),
            class: 'btn btn-outline-primary btn-sm edit-allocation-button'
  end
end

