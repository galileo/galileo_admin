# frozen_string_literal: true

# view-related helper methods for Widgets
module WidgetsHelper
  def general_widget_fields(widget)
    {
      I18n.t('activerecord.attributes.widget.service') => widget.service,
      I18n.t('activerecord.attributes.widget.active') => widget.active,
      I18n.t('activerecord.attributes.widget.credentials') => widget.credentials,
      I18n.t('activerecord.attributes.widget.display_name') => widget.display_name
    }
  end

end
