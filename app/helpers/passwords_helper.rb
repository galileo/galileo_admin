module PasswordsHelper
  def general_password_fields(password)
    {
      I18n.t('activerecord.attributes.password.password') => password.password,
      I18n.t('activerecord.attributes.password.commit_date') => password.commit_date,
      I18n.t('activerecord.attributes.password.exclude') => password.exclude,
      I18n.t('activerecord.attributes.password.user') => password.user,
      I18n.t('activerecord.attributes.password.created_at') => password.created_at,
      I18n.t('activerecord.attributes.password.updated_at') => password.updated_at
    }
  end

end
