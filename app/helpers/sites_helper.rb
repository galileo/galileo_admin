# frozen_string_literal: true

# view-related helper methods for Sites
module SitesHelper
  def general_site_fields(site)
    {
      I18n.t('activerecord.attributes.site.institution') => link_to(site.institution.name, site.institution),
      I18n.t('activerecord.attributes.site.name') => site.name,
      I18n.t('activerecord.attributes.site.code') => site.code,
      I18n.t('activerecord.attributes.site.site_type') => site.site_type,
      I18n.t('activerecord.attributes.site.charter') => site.charter,
      I18n.t('activerecord.attributes.site.wayfinder_keywords') => format_array_field(site.wayfinder_keywords)
    }
  end
end
