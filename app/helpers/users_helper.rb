# frozen_string_literal: true

# view-related helper methods for Users
module UsersHelper
  def general_user_fields(user)
    fields = {}
    fields[t('activerecord.attributes.user.email')] = user.email
    fields[t('activerecord.attributes.user.status')] = user.status
    fields[t('activerecord.attributes.user.role')] = badge(user.role)
    if FeatureFlags.enabled? :user_subroles
      fields[t('activerecord.attributes.user.subroles')] = format_array_field_as_badges(@user.subroles, color: 'secondary')
    end
    fields[t('activerecord.attributes.user.first_name')] = user.first_name
    fields[t('activerecord.attributes.user.last_name')] = user.last_name
    fields[t('activerecord.attributes.user.institution_ids')] = institution_list(user.institutions)
    fields[t('activerecord.attributes.user.created_at')] = user.created_at
    fields[t('activerecord.attributes.user.updated_at')] = user.updated_at
    fields[t('activerecord.attributes.user.deleted_at')] = user.deleted_at
    fields
  end

  def contacts_user_fields(user)
    {
      "#{I18n.t('activerecord.attributes.user.contacts')} (#{user.contacts.size})" => render('shared/associated_contacts_table', model: user, show_new_button: false)
    }
  end
end
