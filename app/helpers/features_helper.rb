# frozen_string_literal: true

# view-related helper methods for Feature
module FeaturesHelper
  def general_feature_fields(feature)
    {
      I18n.t('activerecord.attributes.feature.view_type') => feature.view_type,
      I18n.t('activerecord.attributes.feature.position') => feature.position,
      I18n.t('activerecord.attributes.feature.link_label') => feature.link_label,
      I18n.t('activerecord.attributes.feature.link_url') => feature.link_url,
      I18n.t('activerecord.attributes.feature.link_description') => feature.link_description,
      I18n.t('activerecord.attributes.feature.thumbnail') =>
        (image_tag(feature.thumbnail) if feature.image.attached?),
      I18n.t('activerecord.attributes.feature.image') =>
        (image_tag(feature.image) if feature.image.attached?)
    }
  end

  def group?
    @featuring.is_a? InstitutionGroup
  end

  def k12?
    @featuring.k12?
  end

  def group_feature_overriden?(group_feature)
    return true if group?

    @featuring.features.where(
      view_type: group_feature.view_type, position: group_feature.position
    ).exists?
  end

  def view_type_as_badge(view_type)
    case view_type
    when 'elementary' then span_badge('badge-primary', view_type)
    when 'middle' then span_badge('badge-success', view_type)
    when 'highschool' then span_badge('badge-info', view_type)
    when 'educator' then span_badge('badge-dark', view_type)
    when 'full' then span_badge('badge-dark', view_type)
    else
      span_badge('badge-secondary', view_type)
    end
  end

  def span_badge(badge_type, content)
    return unless content

    tag.span(class: %w[badge badge-pill].append(badge_type)) do
      content.to_s
    end
  end
end
