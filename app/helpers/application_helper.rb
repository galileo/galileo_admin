# frozen_string_literal: true

require 'json'

# Application-wide helper methods
module ApplicationHelper
  include Pagy::Frontend

  # Render a checked icon, or not
  # @param [Object] value booleanish
  # @return [String]
  def boolean_check_icon(value)
    if value
      fa_icon 'check'
    else
      fa_icon 'times'
    end
  end

  # @param [DateTime] date from DB or wherever
  # @return [String] date for display in view
  def date_display(date)
    date.to_fs(:long)
  end

  # Render a simple list of Institution links for use in tables
  # @param [Collection[Institution]] institutions
  def institution_list(institutions)
    return I18n.t('app.defaults.none') unless institutions.present?

    institutions.collect do |i|
      link_to i.name_with_code, institution_path(i)
    end.join('<br />').html_safe
  end

  # create a link for a sortable column header
  # @param [String] field_name in database
  # @param [String] title for header cell
  # @return [String] link
  def sortable(field_name, title = nil)
    title ||= field_name.titleize
    css_class = field_name == sort_column ? "current #{sort_direction}" : nil
    direction = field_name == sort_column && sort_direction == 'asc' ? 'desc' : 'asc'
    link_to title, params.permit(:sort, :direction, :query)
                     .merge(sort: field_name, direction: direction), class: css_class
  end

  # @param [ActiveRecord_Relation] relation
  def render_table(relation)
    model = relation.klass.name
    render "#{model.underscore}s/#{model.underscore}s_table" if relation.any?
  end

  # Creates a Bootstrap linked item group given an ActiveRecord relation and the
  # attributes to be displayed for each item
  # @param [ActiveRecord::Relation] relation
  # @param [Array<String>] attributes
  def linked_item_group(relation, attributes)
    tag.div(
      class: 'list-group',
      role: 'group',
      'aria-label': "#{relation.klass.name} List"
    ) do
      linked_items(relation, attributes)
    end
  end

  # Creates a Bootstrap linked item given an ActiveRecord relation and the
  # attributes to be displayed for that item
  # @param [ActiveRecord::Relation] relation
  # @param [Array<String>] attributes
  def linked_items(relation, attributes)
    items = ''
    relation.each_with_index do |model, index|
      display = "#{index + 1}."
      attributes.each { |attribute| display += " #{model.send(attribute)}." }
      items += link_to(display,
                       polymorphic_path(model),
                       class: 'list-group-item list-group-item-action')
    end
    items.html_safe
  end

  def attribute(label, value)
    render 'shared/attribute', label: label, value: value
  end

  def tab_fields_for(relation, tab)
    tab_fields_for_hash send(tab, relation)
  end

  def tab_fields_for_hash(hash)
    divs = ''
    hash&.each do |label, value|
      divs += tag.div(class: 'row mb-2') do
        tag.div(class: 'col') do
          attribute label, value
        end
      end
    end
    divs.html_safe
  end

  def array_textarea(object, term, help = '')
    klass = object.class.name.demodulize.underscore
    html = '<div class="form-group">'
    # html += label_tag "#{term}", t("activerecord.attributes.#{klass}.#{term}")
    html += label_tag term.to_s, t("activerecord.attributes.#{klass}.#{term}")
    html += text_area_tag"#{klass}[#{term}]", object.method(term).call&.join("\n"), { rows: '5', class: 'form-control', id: term }
    html += tag.small(help, class: 'form-text text-muted') if help.present?
    html += '</div>'
    html.html_safe
  end

  # Format array into a html string
  # @param [Array<String>] array
  # @return [String]
  def format_array_textarea(array)
    array&.join('<br>').html_safe
  end

  # Format array into an html string
  # @param [Array<String>] array
  # @return [String]
  def format_array_field(array)
    html_string = array&.join('<br>')
    html_string&.html_safe
  end

  def pretty_json(json_string)
    return '' unless json_string

    json = JSON.parse(json_string)
    tag.pre(JSON.pretty_generate(json))
  end

  # shows a right-justified div with Pagy-generated info
  # @param [Pagy] pagy
  # @return [String]
  def displayed_items(pagy)
    tag.div(class: 'float-right ') do
      pagy_info(pagy).html_safe
    end
  end

  def title_heading(text)
    content_for :title, text
    content_tag :h1, text
  end

  def page_title(title)
    if title.blank?
      t('app.title')
    else
      "#{title} — #{t('app.title')}"
    end
  end

  def date_time_humanize(date_time)
    date_time.strftime("%b %e, %Y %l:%M%P")
  end

  def convert_to_date(date_or_string)
    return nil unless date_or_string.present?
    return date_or_string if date_or_string.is_a? Date
    return Date.strptime(date_or_string) if date_or_string.is_a? String
    nil
  end

  def filterrific_model_class
    model = @filterrific.model_class
    if model.is_a? ActiveRecord::Relation
      return model.klass
    end
    model
  end

  def per_page_selected?(pp)
    per_page_setting == pp
  end
end


