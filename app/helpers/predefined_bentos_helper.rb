module PredefinedBentosHelper
  def general_predefined_bento_fields(predefined_bento)
    {
      I18n.t('activerecord.attributes.predefined_bento.code') => predefined_bento.code,
      I18n.t('activerecord.attributes.predefined_bento.display_name') => predefined_bento.display_name,
      I18n.t('activerecord.attributes.predefined_bento.service') => predefined_bento.service,
      I18n.t('activerecord.attributes.predefined_bento.description') => predefined_bento.description,
      I18n.t('activerecord.attributes.predefined_bento.admin_note') => predefined_bento.admin_note,
      I18n.t('activerecord.attributes.predefined_bento.no_results_message') => predefined_bento.no_results_message,
      I18n.t('activerecord.attributes.predefined_bento.service_credentials_name') => predefined_bento.service_credentials_name,
      I18n.t('activerecord.attributes.bento_config.created_at') => predefined_bento.created_at,
      I18n.t('activerecord.attributes.bento_config.updated_at') => predefined_bento.updated_at
    }
  end
end
