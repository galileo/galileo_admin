# frozen_string_literal: true

module JsonFieldsHelper

  class ConditionWrapper

    def initialize(test_object, subform, view)
      @test_object = test_object
      @subform = subform
      @view = view
    end

    def form_element_name_for(symbol)
      "#{@test_object.class.name.underscore}[#{symbol.to_s}]"
    end

    def anything(&block)
      block.call(@subform)
    end

    def method_missing(symbol, test_value, &block)
      @view.content_tag :div, 'data-subform-by' => form_element_name_for(symbol), 'data-subform-test' => test_value do
        block.call(@subform)
      end
    end

  end

  def subform_from_json(form_builder, json_field, &inner)
    form_builder.fields_for json_field, OpenStruct.new(form_builder.object.send(json_field)) do |subform|
      switcher = ConditionWrapper.new form_builder.object, subform, self
      inner.call(switcher)
    end
  end

  def text_field_array(form_builder, field_name, conjunction = '', required: false, label: nil,
                       help: nil, additional_item_help: nil, chosen_options: nil)
    element_name = "#{form_builder.object_name}[#{field_name}][]"
    values = form_builder.object.send(field_name)&.map(&:to_s) || []
    values += ['', '']
    text_fields = values.each_with_index.map do |value, i|
      options = {
        name: element_name,
        value: value,
        label: label || field_name,
        wrapper: {data: { 'field-wrapper' => true }}
      }
      options[:help] = help unless help.blank?
      if i > 0
        options[:hide_label] = true
        options[:prepend] = conjunction unless conjunction.blank?
        options[:help] = additional_item_help unless additional_item_help.nil?
      elsif required
        options[:required] = true
      end
      if i == values.size - 1
        options[:wrapper][:data]['field-template'] = true
      end
      if chosen_options
        options[:include_blank] = true
        form_builder.select field_name, chosen_options, options, {class: 'chosen-select'}
      else
        form_builder.text_field field_name, options
      end
    end
    content_tag :div, text_fields.join.html_safe, {'data-field-array-for' => element_name}
  end

  def render_hash_values(hash)
    hash.map {|key, value| [key, render_json(value)]}.to_h
  end

  def render_json(node)
    if node.nil?
      ''
    elsif node == false
      'false'
    elsif node == true
      'true'
    elsif node.is_a? Array
      content_tag :ul do
        (node.map { |child| content_tag(:li, render_json(child)) }).join.html_safe
      end
    elsif node.is_a? Hash
      content_tag :dl do
        (node.map do |key, value|
          content_tag(:dt, key) + content_tag(:dd, render_json(value))
        end).join.html_safe
        end
    else
      node
    end
  end


end
