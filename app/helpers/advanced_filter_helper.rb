# frozen_string_literal: true

# Helpers related to advanced fiterrific filters (e.g. Institutions > Related... > Contacts)
module AdvancedFilterHelper

  # use a block to pass in the regular filter to show (if no advanced criteria is provided)
  # @param form [ActionView::Helpers::FormBuilder]
  # @param scope_name [Symbol, String] the name of the scope associated with this filter
  # @yield the markup for the filter UI that will be shown if no advanced filter is applied
  # @return [ActiveSupport::SafeBuffer]
  def advanced_filter(form, scope_name, klass)
    if @filterrific.send(scope_name).is_a? OpenStruct
      advanced = render partial: 'shared/advanced_filter',
                        locals: { scope_name: scope_name, filterrific: @filterrific, f: form, klass: klass }
      if block_given?
        simple = content_tag :div, data: { 'alternate-filter': true } do
          yield
        end
        advanced + simple
      else
        advanced
      end

    else
      yield
    end
  end

  # This method takes a hash like {filter_a: true, filter_b:[1,2,3]} and spits hidden fields representing that object
  # To support nested hashes, it calls itself recursively.
  #
  # @param form [ActionView::Helpers::FormBuilder]
  # @param name [Symbol, String]
  # @param data [OpenStruct, Hash, Array<String, Number>, String, Numeric]
  # @return [ActiveSupport::SafeBuffer]
  def hidden_fields_for(form, name, data)
    data = data.to_h if data.is_a? OpenStruct
    if data.is_a? Array
      safe_join(data.map{ |item| form.hidden_field name, value: item, multiple: true })
    elsif data.is_a? Hash
      form.fields_for name.to_s do |sf|
        safe_join(data.map{ |k, v| hidden_fields_for(sf, k, v) })
      end
    else
      form.hidden_field name, value: data
    end
  end

  # Recursively convert a tree of filterrific filters from a heterogeneous mix of hashes and OpenStructs to all hashes.
  # This is necessary when passing them into URL helpers.
  def hashify_filters(filters)
    if filters.is_a? OpenStruct
      filters = filters.to_h
    elsif filters.is_a? Filterrific::ParamSet
      filters = filters.to_hash
    end
    if filters.is_a? Hash
      filters.transform_values! { |v| hashify_filters v }
    end
    filters
  end

end
