# frozen_string_literal: true

# Allocation related helpers
module AllocationsHelper
  def general_allocation_fields(allocation)
    fields = {}
    fields[I18n.t('app.allocations.labels.allocation_logo')] = (image_tag(allocation.logo_thumbnail) if allocation.logo_thumbnail)
    fields[I18n.t('app.allocations.labels.express_link')] = allocation.express_link
    fields[I18n.t('app.allocations.labels.for_institution')] = link_to(allocation.institution.name, allocation.institution)
    fields[I18n.t('app.allocations.labels.for_resource')] = link_to(allocation.resource.name, allocation.resource)
    fields[I18n.t('app.allocations.labels.name')] = allocation.name
    fields[I18n.t('app.allocations.labels.subscription_type')] = badge(allocation.subscription_type)
    fields[I18n.t('activerecord.attributes.allocation.display')] = allocation.display
    if allocation.actual_branding
      fields[I18n.t('activerecord.attributes.allocation.branding')] = link_to(allocation.actual_branding.name, institution_branding_path(allocation.actual_branding.institution, allocation.actual_branding))
    end
    fields[I18n.t('app.allocations.labels.subjects_all')] = format_array_field_as_badges(allocation.all_subject_names)
    fields[I18n.t('app.allocations.labels.subjects_galileo')] = format_array_field_as_badges(allocation.resource.subject_names.sort)
    fields[I18n.t('activerecord.attributes.allocation.subject_ids')] = format_array_field_as_badges(allocation.additional_subject_names)
    fields[I18n.t('app.allocations.labels.keywords_all')] = format_array_field_as_badges(allocation.all_keyword_names)
    fields[I18n.t('app.allocations.labels.keywords_galileo')] = format_array_field_as_badges(allocation.resource.keywords.sort)
    fields[I18n.t('activerecord.attributes.allocation.keywords')] = format_array_field_as_badges(allocation.keywords)
    fields[I18n.t('activerecord.attributes.allocation.vendor_statistics_identifier')] = allocation.vendor_statistics_identifier
    fields[I18n.t('activerecord.attributes.allocation.note')] = allocation.note
    fields[I18n.t('activerecord.attributes.allocation.special_info')] = allocation.special_info
    fields[I18n.t('activerecord.attributes.allocation.created_at')] = allocation.created_at
    fields[I18n.t('activerecord.attributes.allocation.updated_at')] = allocation.updated_at
    fields
  end

  def authentication_allocation_fields(allocation)
    {
      I18n.t('activerecord.attributes.allocation.bypass_open_athens') => allocation.bypass_open_athens,
      I18n.t('activerecord.attributes.allocation.ip_access_url') => allocation.ip_access_url,
      I18n.t('activerecord.attributes.allocation.remote_access_url') => allocation.remote_access_url,
      I18n.t('activerecord.attributes.allocation.open_athens_url') => allocation.open_athens_url,
      I18n.t('activerecord.attributes.resource.oa_proxy') => allocation.resource.oa_proxy?,
      I18n.t('activerecord.attributes.allocation.user_id') => allocation.user_id,
      I18n.t('activerecord.attributes.allocation.password') => allocation.password
    }
  end

  def view_allocation_fields(allocation)
    {
      I18n.t('app.allocations.labels.user_views_galileo') => format_array_field_as_badges(allocation.resource.user_view_names),
    }
  end

  def admin_view_allocation_fields(allocation)
    {
      I18n.t('app.allocations.labels.user_views_galileo') => format_array_field_as_badges(allocation.resource.user_view_names),
      I18n.t('activerecord.attributes.allocation.user_view_ids') => format_array_field_as_badges(allocation.override_user_view_names),
      I18n.t('activerecord.attributes.allocation.override_user_views') => allocation.override_user_views
    }
  end

  def logo_allocation_fields(allocation)
    {
      I18n.t('app.allocations.labels.allocation_logo') =>(image_tag(allocation.logo_thumbnail) if allocation.logo_thumbnail),
      I18n.t('app.allocations.labels.resource_logo') => (image_tag(allocation.resource.logo_thumbnail) if allocation.resource.logo_thumbnail),
      I18n.t('app.allocations.labels.vendor_logo') => (image_tag(allocation.resource.vendor&.logo_thumbnail) if allocation.resource.vendor&.logo_thumbnail),
      I18n.t('app.allocations.labels.institution_logo') => (image_tag(allocation.institution.logo_thumbnail) if allocation.institution.logo_thumbnail)
    }
  end

  def show_allocation_cell(allocation)
    if can? :read, allocation
      link_to allocation.id, allocation_path(allocation)
    else
      allocation.id
    end
  end

  def edit_allocation_button(allocation)
    link_to I18n.t('app.defaults.labels.edit'),
            edit_allocation_path(allocation),
            class: 'btn btn-outline-primary btn-sm edit-allocation-button'
  end
end

