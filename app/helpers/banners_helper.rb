module BannersHelper
  def general_banner_fields(banner)
    {
      I18n.t('activerecord.attributes.banner.banner') => normal_banner(banner),
      I18n.t('activerecord.attributes.banner.audience') => banner.audience,
      I18n.t('activerecord.attributes.banner.institution_id') => banner.institution_name,
      I18n.t('activerecord.attributes.banner.institution_group_id') => banner.institution_group&.code,
      I18n.t('activerecord.attributes.banner.user_view_ids') => format_array_field_as_badges(banner.user_view_codes),
      I18n.t('activerecord.attributes.banner.start_time') => date_time_humanize(banner.start_time),
      I18n.t('activerecord.attributes.banner.end_time') => date_time_humanize(banner.end_time),
      I18n.t('activerecord.attributes.banner.active') => banner.active?,
      I18n.t('activerecord.attributes.banner.created_by') => (banner.created_by ? link_to(banner.created_by.email, user_path(banner.created_by)) : ''),
      I18n.t('activerecord.attributes.banner.created_at') => date_time_humanize(banner.created_at),
      I18n.t('activerecord.attributes.banner.updated_at') => date_time_humanize(banner.updated_at)
    }
  end

  def normal_banner(banner)
    "<div class=\"alert alert-#{banner.style} show text-center\" role=\"alert\">
    #{banner.content}
    </div>".html_safe
  end

  def versions_banner_fields(banner)
    {
      I18n.t('activerecord.attributes.banner.versions') =>
        banner_versions_table(banner)
    }
  end
  def banner_versions_table(banner)
    render 'shared/versions_table', model: banner
  end

  def outage_banner(startDate, endDate, message, bs_color: 'danger')
    today = Date.current
    startDate = Date.parse(startDate)
    endDate = Date.parse(endDate)
    if today.between?(startDate,endDate)
      message = "<div class=\"alert alert-#{bs_color} mb-2\">
                <div class=\"container\">
                  <div class=\"row\">
                    <div class=\"col-sm-12 text-center\">
                      #{message}
                    </div>
                  </div>
                </div>
              </div>"
      message.html_safe
    end
  end
end
