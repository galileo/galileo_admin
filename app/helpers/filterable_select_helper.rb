# frozen_string_literal: true

module FilterableSelectHelper

  def filter_options(container_selector = '.form-group', **filter_hashes)
    data = {
      'filterable-select' => filter_hashes.to_json
    }
    data['filterable-select-container'] = container_selector unless container_selector.nil?
    { data: data }
  end

  # This applies a customizable suffix to items that meet the criteria of the rule
  def suffix_options(suffix, field, rule, soft_filter: false)
    data = {
      'suffixed-select' => {
        field => {
          suffix: suffix,
          rule: rule,
          softFilter: soft_filter # When true, items that don't meet this criteria will be skipped over when selecting
                                  # the default value for the select
        }
      }.to_json
    }
    { data: data }
  end

end
