# frozen_string_literal: true

# TemplateViewBento related helpers
module TemplateViewBentosHelper
  def general_template_view_bento_fields(template_view_bento)
    {
      I18n.t('activerecord.attributes.template_view_bento.template_name') => template_view_bento.template_name,
      I18n.t('activerecord.attributes.template_view_bento.user_view') => template_view_bento.user_view,
      I18n.t('activerecord.attributes.template_view_bento.predefined_bento_id') => link_to(
        template_view_bento.predefined_bento.disambiguated_name,
        template_view_bento.predefined_bento),
      I18n.t('activerecord.attributes.template_view_bento.default_order') => template_view_bento.default_order,
      I18n.t('activerecord.attributes.template_view_bento.shown_by_default') => template_view_bento.shown_by_default,
      I18n.t('activerecord.attributes.contact.created_at') => template_view_bento.created_at,
      I18n.t('activerecord.attributes.contact.updated_at') => template_view_bento.updated_at
    }
  end
end

