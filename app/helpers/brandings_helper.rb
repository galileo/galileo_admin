# frozen_string_literal: true

# view-related helper methods for Branding
module BrandingsHelper
  def general_branding_fields(branding)
    {
      I18n.t('activerecord.attributes.branding.institution') => branding.institution.name,
      I18n.t('activerecord.attributes.branding.name') => branding.name,
      I18n.t('activerecord.attributes.branding.text') => branding.text,
      I18n.t('activerecord.attributes.branding.image') =>
        (image_tag(branding.image_thumbnail) if branding.image.attached?)
    }
  end

  def allocations_branding_fields(branding)
    label_and_size = "#{I18n.t('activerecord.attributes.branding.allocations')} (#{branding.all_allocations.size})"
    {
      label_and_size => render('associated_allocations_table', allocations: branding.all_allocations)
    }
  end
end
