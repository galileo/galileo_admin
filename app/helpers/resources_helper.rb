# frozen_string_literal: true

# view helpers for Resources
module ResourcesHelper
  def general_resource_fields(resource)
    {
      I18n.t('activerecord.attributes.resource.code') => resource.code,
      I18n.t('activerecord.attributes.resource.name') => resource.name,
      I18n.t('activerecord.attributes.resource.express_link') => resource.express_link,
      I18n.t('activerecord.attributes.resource.logo') =>
        (image_tag(resource.logo_thumbnail) if resource.logo.attached?),
      I18n.t('activerecord.attributes.resource.institution_id') => resource.institution&.name,
      I18n.t('activerecord.attributes.resource.display') => resource.display,
      I18n.t('activerecord.attributes.resource.branding_id') => branding_info(resource),
      I18n.t('activerecord.attributes.resource.parent_id') => (resource.parent ? link_to(resource.parent.name, resource_path(resource.parent.id)) : ''),
      I18n.t('activerecord.attributes.resource.child_resources') => format_array_field(resource.children.collect(&:name)),
      I18n.t('activerecord.attributes.resource.active') => resource.active,
      I18n.t('activerecord.attributes.resource.bypass_galileo_authentication') => resource.bypass_galileo_authentication,
      I18n.t('activerecord.attributes.resource.format_ids') => format_array_field_as_badges(resource.format_names),
      I18n.t('activerecord.attributes.resource.subject_ids') => format_array_field_as_badges(resource.subject_names),
      I18n.t('activerecord.attributes.resource.user_view_ids') => format_array_field_as_badges(resource.user_view_names),
      I18n.t('activerecord.attributes.resource.created_at') => resource.created_at,
      I18n.t('activerecord.attributes.resource.updated_at') => resource.updated_at
    }
  end

  def general_inst_user_resource_fields(resource)
    {
      I18n.t('activerecord.attributes.resource.code') => resource.code,
      I18n.t('activerecord.attributes.resource.name') => resource.name,
      I18n.t('activerecord.attributes.resource.express_link') => resource.express_link,
      I18n.t('activerecord.attributes.resource.logo') =>
        (image_tag(resource.logo_thumbnail) if resource.logo.attached?),
      I18n.t('activerecord.attributes.resource.institution_id') => resource.institution&.name,
      I18n.t('activerecord.attributes.resource.display') => resource.display,
      I18n.t('activerecord.attributes.resource.branding_id') => branding_info(resource),
      I18n.t('activerecord.attributes.resource.active') => resource.active,
      I18n.t('activerecord.attributes.resource.bypass_galileo_authentication') => resource.bypass_galileo_authentication,
      I18n.t('activerecord.attributes.resource.format_ids') => format_array_field_as_badges(resource.format_names),
      I18n.t('activerecord.attributes.resource.subject_ids') => format_array_field_as_badges(resource.subject_names),
      I18n.t('activerecord.attributes.resource.user_view_ids') => format_array_field_as_badges(resource.user_view_names),
      I18n.t('activerecord.attributes.resource.created_at') => resource.created_at,
      I18n.t('activerecord.attributes.resource.updated_at') => resource.updated_at
    }
  end

  def authentication_resource_fields(resource)
    {
      I18n.t('activerecord.attributes.resource.ip_access_url') => resource.ip_access_url,
      I18n.t('activerecord.attributes.resource.remote_access_url') => resource.remote_access_url,
      I18n.t('activerecord.attributes.resource.open_athens_url') => resource.open_athens_url,
      I18n.t('activerecord.attributes.resource.open_athens') => resource.open_athens,
      I18n.t('activerecord.attributes.resource.without_open_athens_redirector') => resource.without_open_athens_redirector,
      I18n.t('activerecord.attributes.resource.oa_proxy') => resource.oa_proxy,
      I18n.t('activerecord.attributes.resource.proxy_remote') => resource.proxy_remote,
      I18n.t('activerecord.attributes.resource.access_note') => resource.access_note,
      I18n.t('activerecord.attributes.resource.credential_display') =>
        resource.credential_display

    }
  end

  def authentication_inst_user_resource_fields(resource)
    {
      I18n.t('activerecord.attributes.resource.ip_access_url') => resource.ip_access_url,
      I18n.t('activerecord.attributes.resource.remote_access_url') => resource.remote_access_url,
      I18n.t('activerecord.attributes.resource.open_athens_url') => resource.open_athens_url,
      I18n.t('activerecord.attributes.resource.open_athens') => resource.open_athens,
      I18n.t('activerecord.attributes.resource.without_open_athens_redirector') => resource.without_open_athens_redirector,
      I18n.t('activerecord.attributes.resource.oa_proxy') => resource.oa_proxy,
      I18n.t('activerecord.attributes.resource.access_note') => resource.access_note,
      I18n.t('activerecord.attributes.resource.credential_display') =>
        resource.credential_display
    }
  end

  def vendor_resource_fields(resource)
    {
      I18n.t('activerecord.attributes.resource.vendor_id') => (resource.vendor ? link_to(resource.vendor&.name_with_code, vendor_path(resource.vendor.id)) : ''),
      I18n.t('activerecord.attributes.resource.vendor_name') => resource.vendor_name,
      I18n.t('activerecord.attributes.resource.product_suite') => resource.product_suite,
      I18n.t('activerecord.attributes.resource.title_list_url') => resource.title_list_url,
      I18n.t('activerecord.attributes.resource.user_id') => resource.user_id,
      I18n.t('activerecord.attributes.resource.password') => resource.password
    }
  end

  def vendor_redacted_resource_fields(resource)
    {
      I18n.t('activerecord.attributes.resource.vendor_name') => resource.vendor_name,
      I18n.t('activerecord.attributes.resource.product_suite') => resource.product_suite,
      I18n.t('activerecord.attributes.resource.title_list_url') => resource.title_list_url
    }
  end

  def institution_resource_fields(resource)
    {
      I18n.t('activerecord.attributes.resource.institutions') =>
        resource_allocations_table(resource)
    }
  end

  def scope_resource_fields(resource)
    {
      I18n.t('activerecord.attributes.resource.short_description') => resource.short_description,
      I18n.t('activerecord.attributes.resource.long_description') => resource.long_description,
      I18n.t('activerecord.attributes.resource.coverage_dates') => resource.coverage_dates,
      I18n.t('activerecord.attributes.resource.update_frequency') => resource.update_frequency,
      I18n.t('activerecord.attributes.resource.coverage_dates') => resource.coverage_dates,
      I18n.t('activerecord.attributes.resource.audience') => resource.audience,
      I18n.t('activerecord.attributes.resource.language') => resource.language,
      I18n.t('activerecord.attributes.resource.keywords') => format_array_field_as_badges(resource.keywords)
    }
  end

  def statistics_resource_fields(resource)
    {
      I18n.t('activerecord.attributes.resource.include_in_stats') => resource.include_in_stats,
      I18n.t('activerecord.attributes.resource.resource_category') => resource.resource_category,
      I18n.t('activerecord.attributes.resource.serial_count') => resource.serial_count
    }
  end

  def notes_resource_fields(resource)
    {
      I18n.t('activerecord.attributes.resource.special_information') => simple_format(resource.special_information),
      I18n.t('activerecord.attributes.resource.note') => simple_format(resource.note)
    }
  end

  def versions_resource_fields(resource)
    {
      I18n.t('activerecord.attributes.institution.versions') =>
      resource_versions_table(resource)
    }
  end

  def branding_info(resource)
    return unless resource.institution && resource.branding

    link_to(resource.branding.name, institution_branding_path(resource.institution, resource.branding))
  end

  def resource_allocations_table(resource)
    render 'resources/allocations_table', resource: resource
  end

  def resource_versions_table(resource)
    render 'shared/versions_table', model: resource
  end
end
