module NotifyGroupsHelper
  def general_notify_group_fields(notify_group)
    {
      I18n.t('activerecord.attributes.notify_group.code') => notify_group.code,
      I18n.t('activerecord.attributes.notify_group.name') => notify_group.name,
      I18n.t('activerecord.attributes.notify_group.notify_dates') =>
        format_array_field(notify_group.notify_dates),
      I18n.t('activerecord.attributes.notify_group.change_dates') =>
        format_array_field(notify_group.change_dates),
      I18n.t('activerecord.attributes.notify_group.created_at') =>
        notify_group.created_at,
      I18n.t('activerecord.attributes.notify_group.updated_at') =>
        notify_group.updated_at
    }
  end

  def institutions_notify_group_fields(notify_group)
    {
      I18n.t('activerecord.attributes.notify_group.institutions') =>
        linked_item_group(notify_group.institutions, ['name'])
    }
  end

  def notes_notify_group_fields(notify_group)
    {
      I18n.t('activerecord.attributes.notify_group.note') => notify_group.note,
      I18n.t('activerecord.attributes.notify_group.special_information') =>
        notify_group.special_information
    }
  end
end
