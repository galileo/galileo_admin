# frozen_string_literal: true

# view-related helper methods for Institutions
module InstitutionsHelper

  def glri_types
    {
      'none' => 'Not a participant',
      'galileo' => 'GALILEO-managed',
      'locally' => 'Institution-managed'
    }
  end

  def overview_institution_fields(institution)
    fields = {
      I18n.t('activerecord.attributes.institution.code') => institution.code,
      I18n.t('activerecord.attributes.institution.eds_customer_id') => institution.eds_customer_id,
      I18n.t('activerecord.attributes.institution.name') => institution.name,
      I18n.t('activerecord.attributes.institution.eds_profile_default') => institution.eds_profile_default,
      I18n.t('activerecord.attributes.institution.institution_group') =>
        (link_to(institution.institution_group.code, institution_group_path(institution.institution_group)) if institution.institution_group),
      I18n.t('activerecord.attributes.institution.active') => institution.active,
      I18n.t('activerecord.attributes.institution.glri_participant') => institution.glri_participant
      }
    if FeatureFlags.enabled?(:view_switching) && ViewService.institution_display_views?(@institution.inst_type)
      fields[I18n.t('activerecord.attributes.institution.view_switching')] = institution.view_switching
      end
    fields.merge({
                   I18n.t('activerecord.attributes.institution.open_athens') => institution.open_athens,
                   I18n.t('activerecord.attributes.institution.oa_proxy') => institution.oa_proxy,
                   I18n.t('activerecord.attributes.institution.oa_proxy_org') => institution.oa_proxy_org,
                   I18n.t('activerecord.attributes.institution.current_password') => display_password(institution.current_password),
                   I18n.t('activerecord.attributes.institution.galileo_ez_proxy_ip') => institution.galileo_ez_proxy_ip,
                   I18n.t('activerecord.attributes.institution.open_athens_proxy_ip') => institution.open_athens_proxy_ip,
                   I18n.t('activerecord.attributes.institution.ip_addresses') => format_array_field(institution.ip_addresses)
                 })
  end

  def general_institution_fields(institution)
    fields = {
      I18n.t('activerecord.attributes.institution.name') => institution.name,
      I18n.t('activerecord.attributes.institution.public_code') => institution.public_code,
      I18n.t('activerecord.attributes.institution.code') => institution.code,
      I18n.t('activerecord.attributes.institution.institution_group') => (link_to(institution.institution_group.code, institution_group_path(institution.institution_group)) if institution.institution_group)
    }
    if FeatureFlags.enabled?(:flexible_bento_templates)
      fields[I18n.t('activerecord.attributes.institution.bento_template')] = (link_to(institution.bento_template, template_view_bentos_path(
        filterrific: {with_template_name: institution.bento_template, with_user_view: ''}
      )) if institution.bento_template.present?)
    end
    fields.merge({
      I18n.t('activerecord.attributes.institution.active') => institution.active,
      I18n.t('activerecord.attributes.institution.special') => institution.special,
      I18n.t('activerecord.attributes.institution.test_site') => institution.test_site,
      I18n.t('activerecord.attributes.institution.address') => institution.address,
      I18n.t('activerecord.attributes.institution.zip_codes') => format_array_field(institution.zip_codes),
      I18n.t('activerecord.attributes.institution.counties') => format_array_field(institution.counties),
      I18n.t('activerecord.attributes.institution.glri_participant') => institution.glri_participant,
      I18n.t('activerecord.attributes.institution.created_at') => institution.created_at,
      I18n.t('activerecord.attributes.institution.updated_at') => institution.updated_at

    })
  end

  def authentication_institution_fields(institution)
    {
      I18n.t('activerecord.attributes.institution.current_password') =>
        current_password_panel_content(institution).html_safe,
      I18n.t('activerecord.attributes.institution.new_password') =>
        new_password_panel_content(institution).html_safe,
      I18n.t('activerecord.attributes.institution.prev_password') => institution.prev_password,
      I18n.t('activerecord.attributes.institution.password_last_notified') => institution.password_last_notified,
      I18n.t('activerecord.attributes.institution.password_last_changed') => institution.password_last_changed,
      I18n.t('activerecord.attributes.institution.notify_group_id') =>
        if institution.notify_group
          link_to(institution.notify_group.name, notify_group_path(institution.notify_group))
        else
          I18n.t('app.defaults.none')
        end,
      I18n.t('activerecord.attributes.institution.notify_dates') => format_array_field(institution.notify_dates),
      I18n.t('activerecord.attributes.institution.change_dates') => format_array_field(institution.change_dates),
      I18n.t('activerecord.attributes.institution.open_athens') => institution.open_athens,
      I18n.t('activerecord.attributes.institution.oa_proxy') => institution.oa_proxy,
      I18n.t('activerecord.attributes.institution.oa_proxy_org') => institution.oa_proxy_org,
      I18n.t('activerecord.attributes.institution.open_athens_scope') => institution.open_athens_scope,
      I18n.t('activerecord.attributes.institution.open_athens_api_name') => institution.open_athens_api_name,
      I18n.t('activerecord.attributes.institution.open_athens_subscope') => format_array_field(institution.open_athens_subscope_array),
      I18n.t('activerecord.attributes.institution.open_athens_org_id') => institution.open_athens_org_id,
      I18n.t('activerecord.attributes.institution.open_athens_entity_id') => institution.open_athens_entity_id,
      I18n.t('activerecord.attributes.institution.open_athens_proxy_ip') => institution.open_athens_proxy_ip,
      I18n.t('activerecord.attributes.institution.pines_codes') => format_array_field(institution.pines_codes),
      I18n.t('activerecord.attributes.institution.use_local_ez_proxy') => institution.use_local_ez_proxy,
      I18n.t('activerecord.attributes.institution.galileo_ez_proxy_ip') => institution.galileo_ez_proxy_ip,
      I18n.t('activerecord.attributes.institution.galileo_ez_proxy_url') => institution.galileo_ez_proxy_url,
      I18n.t('activerecord.attributes.institution.local_ez_proxy_ip') => institution.local_ez_proxy_ip,
      I18n.t('activerecord.attributes.institution.local_ez_proxy_url') => institution.local_ez_proxy_url,
      I18n.t('activerecord.attributes.institution.ip_addresses') => format_array_field(institution.ip_addresses),
      I18n.t('activerecord.attributes.institution.ip_authentication') => institution.ip_authentication,
      I18n.t('activerecord.attributes.institution.lti_consumer_key') => institution.lti_consumer_key,
      I18n.t('activerecord.attributes.institution.lti_custom_inst_id') => institution.lti_custom_inst_id,
      I18n.t('activerecord.attributes.institution.lti_secret') => institution.lti_secret
    }
  end

  def resource_institution_fields(institution)
    {
      I18n.t('activerecord.attributes.institution.resources') =>
        institution_allocations_table(institution)
    }
  end

  def contacts_institution_fields(institution)
    label_and_size = "#{I18n.t('activerecord.attributes.institution.contacts')} (#{institution.contacts.size})"
    {
      label_and_size => render('shared/associated_contacts_table', model: institution)
    }
  end

  def reports_institution_fields(institution)
    {
      I18n.t('activerecord.attributes.institution.reports') =>
      institution_reports_table(institution)
    }
  end

  def notes_institution_fields(institution)
    {
      I18n.t('activerecord.attributes.institution.note') =>
      simple_format(institution.note),
      I18n.t('activerecord.attributes.institution.special_information') =>
      simple_format(institution.special_information)
    }
  end

  def versions_institution_fields(institution)
    {
      I18n.t('activerecord.attributes.institution.versions') =>
      institution_versions_table(institution)
    }
  end

  def eds_institution_fields(institution)
    {
      I18n.t('activerecord.attributes.institution.eds_customer_id') =>
        institution.eds_customer_id,
      I18n.t('activerecord.attributes.institution.eds_profile_default') =>
        institution.eds_profile_default,
      I18n.t('activerecord.attributes.institution.eds_profile_kids') =>
        institution.eds_profile_kids,
      I18n.t('activerecord.attributes.institution.eds_profile_teen') =>
        institution.eds_profile_teen,
      I18n.t('activerecord.attributes.institution.eds_profile_high_school') =>
        institution.eds_profile_high_school,
      I18n.t('activerecord.attributes.institution.eds_managed_by') =>
        institution.eds_managed_by
    }
  end

  def portal_institution_fields(institution)
    {
      I18n.t('activerecord.attributes.institution.logo') =>
        (if institution.logo.attached?
           image_tag(institution.logo_thumbnail)
         end),
      I18n.t('activerecord.attributes.institution.wayfinder_keywords') =>
        format_array_field(institution.wayfinder_keywords),
      I18n.t('activerecord.attributes.institution.zotero_bib') =>
        institution.zotero_bib?,
      I18n.t('activerecord.attributes.institution.journals_link') =>
        institution.journals_link,
      I18n.t('activerecord.attributes.institution.institution_url_1') =>
        institution.institution_url_1,
      I18n.t('activerecord.attributes.institution.institution_url_label_1') =>
        institution.institution_url_label_1,
      I18n.t('activerecord.attributes.institution.institution_url_2') =>
        institution.institution_url_2,
      I18n.t('activerecord.attributes.institution.institution_url_label_2') =>
        institution.institution_url_label_2,
      I18n.t('activerecord.attributes.institution.institution_url_3') =>
        institution.institution_url_3,
      I18n.t('activerecord.attributes.institution.institution_url_label_3') =>
        institution.institution_url_label_3,
      I18n.t('activerecord.attributes.institution.institution_url_4') =>
        institution.institution_url_4,
      I18n.t('activerecord.attributes.institution.institution_url_label_4') =>
        institution.institution_url_label_4,
      I18n.t('activerecord.attributes.institution.institution_url_5') =>
        institution.institution_url_5,
      I18n.t('activerecord.attributes.institution.institution_url_label_5') =>
        institution.institution_url_label_5
    }
  end

  def institution_allocations_table(institution)
    render 'institutions/allocations_table', institution: institution
  end

  def institution_versions_table(institution)
    render 'shared/versions_table', model: institution
  end

  def institution_reports_table(institution)
    render 'institutions/reports_table', institution: institution
  end

  def new_password_panel_content(institution)
    "#{display_password(institution.new_password)} #{new_new_password_button(institution)} #{notify_new_password_button(institution)}"
  end

  def current_password_panel_content(institution)
    "#{display_password(institution.current_password)} #{update_asap_password_button(institution)} #{notify_current_password_button(institution)}"
  end

  def new_new_password_button(institution)
    return unless can? :manage, Institution

    link_to(
      I18n.t('app.institution.labels.new_new_password'),
      new_new_password_institution_path(institution),
      data: {
        confirm: I18n.t('app.institution.confirmation.new_new_password')
      },
      class: 'btn btn-primary btn-sm float-right'
    )
  end

  def update_asap_password_button(institution)
    return unless can? :manage, Institution

    link_to(
      I18n.t('app.institution.labels.update_asap_password'),
      update_password_asap_institution_path(institution),
      data: {
        confirm: I18n.t('app.institution.confirmation.update_asap_password')
      },
      class: 'btn btn-primary btn-sm float-right'
    )
  end

  def notify_new_password_button(institution)
    return unless can? :manage, Institution

    link_to(
      I18n.t('app.institution.labels.send_notifications'),
      notify_new_password_institution_path(institution),
      data: {
        confirm: I18n.t('app.institution.confirmation.notify_new_password')
      },
      class: 'btn btn-primary btn-sm float-right'
    )
  end

  def notify_current_password_button(institution)
    return unless can? :manage, Institution

    link_to(
      I18n.t('app.institution.labels.send_notifications'),
      notify_current_password_institution_path(institution),
      data: {
        confirm: I18n.t('app.institution.confirmation.notify_current_password')
      },
      class: 'btn btn-primary btn-sm float-right'
    )
  end

  def display_password(password)
    return password if Rails.configuration.show_passwords_to_institutional_users

    current_user.institutional? ? '' : password
  end
end


