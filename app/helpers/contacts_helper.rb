# frozen_string_literal: true

# Contact related helpers
module ContactsHelper
  def general_contact_fields(contact)
    fields = {}
    fields[I18n.t('activerecord.attributes.contact.first_name')] = contact.first_name
    fields[I18n.t('activerecord.attributes.contact.last_name')] = contact.last_name
    fields[I18n.t('activerecord.attributes.contact.email')] = contact.email
    fields[I18n.t('activerecord.attributes.contact.phone')] = contact.phone
    fields[I18n.t('activerecord.attributes.contact.institution_id')] = link_to(contact.institution.name, institution_path(contact.institution)) if contact.institution_id
    fields[I18n.t('activerecord.attributes.contact.vendor_id')] = link_to(contact.vendor.name, vendor_path(contact.vendor)) if contact.vendor_id
    fields[I18n.t('activerecord.attributes.contact.types')] = contact_types_badges(contact.types)
    fields[I18n.t('activerecord.attributes.contact.notes')] = contact.notes
    fields[I18n.t('activerecord.attributes.contact.user')] = (link_to(contact.user.email, user_path(contact.user)) if contact.user) if current_user.admin?
    fields[I18n.t('activerecord.attributes.contact.created_at')] = contact.created_at
    fields[I18n.t('activerecord.attributes.contact.updated_at')] = contact.updated_at
    fields
  end

  def versions_contact_fields(contact)
    {
      I18n.t('activerecord.attributes.contact.versions') =>
      contact_versions_table(contact)
    }
  end

  def contact_versions_table(contact)
    render 'shared/versions_table', model: contact
  end

  def contact_types_badges(contact_types)
    contact_types.map do |type|
      tag.span(class: 'badge badge-primary') do
        type
      end
    end.join('').html_safe
  end

  def notify_current_password_contact_button(contact)
    return unless (can? :manage, Institution) || (current_user.helpdesk?)

    link_to(
      I18n.t('app.contact.labels.send_password_notifications'),
      notify_current_password_contact_path(contact),
      data: {
        confirm: I18n.t('app.contact.confirmation.notify_current_password')
      },
      class: 'btn btn-primary'
    )
  end

end

