# frozen_string_literal: true
# Vendor related helpers
module VersionsHelper

  def general_version_fields(version)
    fields = {}
    fields[I18n.t('app.versions.columns.id')] = version.id
    fields[I18n.t('app.versions.columns.item_type')] = version.item_type
    fields[I18n.t('app.versions.columns.item_id')] = version.item_id
    fields[I18n.t('app.versions.columns.event')] = version.event
    fields[I18n.t('app.versions.columns.object')] = pretty_json(version.object)
    fields[I18n.t('app.versions.columns.object_changes')] = pretty_json(version.object_changes)
    fields[I18n.t('app.versions.columns.created_at')] = version.created_at
    fields
  end
  def retrieve_user_email_link(user_id)
    return 'System' unless user_id

    user = User.find(user_id)

    if user
      link_to user.email, user_path(user), title: user.name
    else
      'a deleted user'
    end
  end

  def retrieve_user_role(user_id)
    return '' unless user_id

    user = User.find(user_id)

    if user
      user.role
    else
      ''
    end
  end

  # Returns a formatted list of attributes that appear in a version's changelist
  # @param [PaperTrail::Version] version
  # @return [String]
  def parse_changeset(version)
    # version may be an ActiveRecord object and needs to be converted to a PaperTrail::Version
    version = PaperTrail::Version.find(version.id) unless version.is_a?(PaperTrail::Version)
    str = ''
    return str if version.nil? || %w[create destroy].include?(version.event)

    change_set = version.changeset
    return str unless change_set

    change_set.each do |field, _values|
      next if %w[created_at updated_at].include?(field)

      str += "#{field}<br>"
    end
    default_message = if %w[Contact Institution Resource].include? version.item_type
                        'Logo Added?'
                      else
                        ''
                      end
    str.blank? ? default_message : str.html_safe
  end

  # Returns a Diffy formatted diff between version1 and version2 of some attribute
  # @param [String] version1
  # @param [String] version2
  # @return [String]
  def diff(version1, version2)
    changes = Diffy::Diff.new(version1, version2,
                              include_plus_and_minus_in_html: true,
                              include_diff_info: true)
    changes.to_s.present? ? changes.to_s(:html).html_safe : false
  end

  # @param [PaperTrail::Version] version
  # @param [String] classes additional HTML classes to append
  def changes_btn_link(version, classes = '')
    btn_primary_link('changes',
                     version_path(version),
                     classes)
  end

  # @param [PaperTrail::Version] version
  # @param [String] classes additional HTML classes to append
  def undelete_version_btn_link(version, classes = '')
    link_to 'undelete',
            undelete_version_path(version),
            method: :post, class: "btn btn-primary #{classes}"
  end

  def undelete_bento_version_btn_link(version, classes = '')
    link_to 'undelete',
            undelete_bento_version_path(version),
            method: :post, class: "btn btn-primary #{classes}"
  end

  def item_name_link(version, name = version.item_name)
    klass = version.item_type.constantize
    model = klass.find_by_id version.item_id
    if model.nil? # model is in destroyed state so show link to version
      link_to name, version_path(version)
    else
      link_to name, polymorphic_path(model)
    end
  end

  def item_id_link(version)
    klass = version.item_type.constantize
    model = klass.find_by_id version.item_id
    if model.nil? # model is in destroyed state so show link to version
      link_to version.id, version_path(version)
    else
      link_to version.id, polymorphic_path(model)
    end
  end


  def item_changes(version)
    return unless version.object_changes

    JSON.parse version.object_changes
  end
end
