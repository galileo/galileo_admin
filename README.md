# GALILEO Admin

### Developer Information

#### Requirements
+ Ruby 3.2.2
+ Bundler 2.x
+ Rails 7.0 (optional, will be installed with bundler)
+ Node 12.8
+ Yarn 1.0
+ Docker & Docker Compose
+ 7z (for legacy data ingestion)
+ `google-chrome-stable` for headless testing
+ ImageMagick

#### Setup Using Docker-Compose
Instructions for an Ubuntu/Mint Environment  
1. Clone this repo  
2. Install ruby 3.2.2  
3. Install bundler with `sudo gem install bundler` (may not be necessary with RVM install)  
4. Install `7z` support with `sudo apt-get install p7zip-full`  
5. Install `vips` support with `sudo apt install libvips42`  or `brew install vips`
6. Install `google-chrome-stable` for RSpec feature specs  
7. Install Docker ([instructions available here](https://docs.docker.com/engine/install/ubuntu/)) and ensure you can run `docker ps` without sudo by adding yourself to the docker group (`sudo usermod -aG docker your_username`).  
8. Install Docker Compose ([Instructions here](https://docs.docker.com/compose/install/), be sure to get a relatively recent version.  Package repos might be a few versions behind.  
9. `bundle install` to install gem dependencies
10. add master.key to the config/ directory (get this from one of the developers)
11. From the application root directory, run: `./provision/setup_dev_env.sh`and wait for everything to build.    
12. Install `AWS CLI` with `sudo apt-get install awscli`
13. `aws configure` to set up the aws cli with credentials (get this from one of the developers)
14. `bundle exec rails server` to start development web server
15. Go to [http://localhost:3000](localhost:3000)
16. Sign in with the test user (email: super@uga.edu, password: password)

#### Docker Commands Cheatsheet
+ List containers(running and stopped) - `docker-compose ps`
+ Remove running containers - `docker-compose down`
+ Stop running containers - `docker-compose stop` (I think this is a thing)
+ Stop a single running container - `docker-compose stop solr`
+ Start containers - `docker-compose up -d`
+ Start a single container - `docker compose start solr`
+ View container logs - `docker-compose logs`
+ View logs for specific container - `docker-compose logs postgres`
+ Get a terminal in a running container - `docker-compose exec postgres /bin/bash`
+ Restart all containers - `docker-compose restart`
+ Restart a single container - `docker-compose restart solr`
+ Rebuild containers - `docker-compose build`

#### Test Suite
1. `RAILS_ENV=test bundle exec rspec` to run all specs

#### Development/Test environment tips
1.  You can pre- or post-fix your rake commands with the desired environment: `RAILS_ENV=development`
2.  You can export RAILS_ENV to the environment with `export RAILS_ENV=development` and only override it when you want to run the test suite
3.  To make this a persistent environment variable (for all non-login shells after a reboot), add `RAILS_ENV=development` to the end of `/home/<your_username>/.bashrc` or to the system environment `PATH` variable.
4.  When running both GALILEO Admin and GALILEO Search in the rails development server, you will need to specify an alternate port for the second one you start: `bundle exec rails server -p 3001`

### License
© 2024 Board of Regents of the University System of Georgia
