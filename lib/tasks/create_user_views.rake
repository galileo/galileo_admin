# frozen_string_literal: true

desc 'Create user views from subjects'
task create_user_views: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.start "create_user_views job in `#{Rails.env}`"

  count = 0
  Resource.all.includes(:subjects).find_in_batches do |batch|
    batch.each do |resource|
      subject_names = resource.subjects.pluck(:name)
      user_views = UserView.where name: subject_names
      resource.user_views = user_views
    end
    count += batch.size
    notifier.notify "Creating resource user_views: #{count}"
  end

  count = 0
  Allocation.all.includes(:subjects).find_in_batches(batch_size: 10_000) do |batch|
    batch.each do |allocation|
      subject_names = allocation.subjects.pluck(:name)
      user_views = UserView.where name: subject_names
      allocation.user_views = user_views
    end
    count += batch.size
    notifier.notify "Creating allocation user_views: #{count}"
  end

  notifier.finish "create_user_views job in `#{Rails.env}`"
end

