# frozen_string_literal: true

require 'open-uri'

desc 'report inst recs'
task(report_inst_recs: :environment) do |_, args|
  $stdout.sync = true
  process = '`report_inst_recs`'

  notifier = NotificationService.new
  notifier.start process

  begin

  inst_list = URI.open('https://stats.galileo.usg.edu/snapshots/institutions', "r:UTF-8") {|f| f.read }
  inst_list.split(/\n/).each do |line|
    ( code, name ) = line.split(" ",2)
    unless Institution.find_by_code(code)
      notifier.notify "#{process} [#{code}] [#{name}]"
    end
  end

  rescue StandardError => e
    notifier.notify "#{process} failed. exception: #{e}"
  end

  notifier.finish process
end

desc 'add resource recs'
task(add_resource_recs: :environment) do |_, args|
  $stdout.sync = true
  process = '`add_resource_recs`'

  # matches `zupn`, `zupn:wall_street_journal`, and `zooy-pie1`
  # does not match 'datatimes'
  RESOURCE_CODE_REGX = %r{\A[a-z0-9]{4}(?::[a-z0-9_\-]+|-[a-z0-9]{4})?\z}

  # matches `zooy-pie1` and captures `pie1` as institution code
  GLRI_CODE_REGX = %r{\A[a-z0-9]{4}-([a-z0-9]{4})\z}

  notifier = NotificationService.new
  notifier.start process

  found = 0
  added = 0
  skipped = 0
  failed = 0

  begin

  resource_list = URI.open('https://stats.galileo.usg.edu/snapshots/databases', "r:UTF-8") {|f| f.read }
  resource_list.split(/\n/).each do |line|
    ( code, name ) = line.split(" ",2)
    unless code =~ RESOURCE_CODE_REGX
      notifier.notify "#{process} SKIP (bad code): [#{code}] [#{name}]"
      skipped += 1
      next
    end
    if (resource = Resource.find_by_code(code))
      found += 1
    else
      resource = Resource.new(code: code, name: name)
      if code =~ GLRI_CODE_REGX
        institution = Institution.find_by_code( $1 )
        resource.institution_id = institution.id
      end
      if resource.save
        puts "#{process} Added: [#{code}] [#{name}]"  # puts because notify takes too long
        added += 1
      else
        notifier.notify "#{process} FAILED: [#{code}] [#{name}]"
        failed += 1
      end
    end
  end

  rescue StandardError => e
    notifier.notify "#{process} failed. exception: #{e}"
  end

  notifier.notify "#{process} found: #{found}"
  notifier.notify "#{process} added: #{added}"
  notifier.notify "#{process} skipped: #{skipped}"
  notifier.notify "#{process} failed: #{failed}"
  notifier.finish process
end

desc 'Update sites with DOE school codes'
task update_sites_with_doe_school_codes: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notify_msg = 'Update sites with DOE school codes'
  notifier.notify notify_msg

  updated       = 0
  created       = 0
  skipped       = 0
  site_ids      = []
  count         = 0

  csv_file_path = "#{Rails.root}/tmp/merged_sites.csv"
  # headers: institution_code,institution_name,code,new_code,name,site_type,charter,change
  CSV.foreach(csv_file_path, headers: true) do |row|
    count += 1

    action = row['change']
    if action == '3-none'
      skipped += 1
      next
    end

    site_code = row['code']
    new_code  = row['new_code']

    if action == '2-update'
      if (site = Site.find_by_code( site_code ))
        Site.where(id: site.id).update_all(code: new_code)
        updated += 1
        site_ids << site.id
      else
        raise Exceptions::CsvImporterError.new, "#{notify_msg}: Site not found: `#{site_code}`" 
      end
    elsif action == '1-add'
      inst_code = row['institution_code']
      if (inst = Institution.find_by_code( inst_code ))
        hash = { code: new_code, name: row['name'], site_type: row['site_type'], charter: row['charter'], institution_id: inst.id }
        Site.create! hash
        created += 1
      else
        raise Exceptions::CsvImporterError.new, "#{notify_msg}: Institution not found: #{inst_code}"
      end
    end

  end

  msg = "created: #{created}, updated: #{updated}, skipped: #{skipped}, total: #{count}"
  notifier.notify msg
  notifier.notify "Finished: #{notify_msg}"

  site_count = site_ids.size
  sites_indexed = 0
  site_ids.each_slice(500) do |batch|
    notifier.notify "Reindexing sites #{sites_indexed + 1} - #{sites_indexed + batch.size} of #{site_count}"
    Sunspot.index! Site.where(id: batch).for_indexing
    sites_indexed += batch.size
  end
  notifier.notify "Finished: Reindexing #{site_count} sites"

end

desc 'Update sites with PINES branch codes'
task update_sites_with_pines_branch_codes: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notify_msg = 'Update sites with PINES branch codes'
  notifier.notify notify_msg

  updated       = 0
  created       = 0
  skipped       = 0
  site_ids      = []
  count         = 0

  csv_file_path = "#{Rails.root}/tmp/merged_pines.csv"
  # headers: institution_code,institution_name,site_code,new_code,site_name,new_name,site_type,charter,change
  CSV.foreach(csv_file_path, headers: true) do |row|
    count += 1

    action = row['change']
    if action == '3-none'
      skipped += 1
      next
    end

    site_code = row['site_code']
    new_code  = row['new_code']
    new_name  = row['new_name']

    if action == '2-update'
      if (site = Site.find_by_code( site_code ))
        Site.where(id: site.id).update_all(code: new_code, name: new_name)
        updated += 1
        site_ids << site.id
      else
        raise Exceptions::CsvImporterError.new, "#{notify_msg}: Site not found: `#{site_code}`" 
      end
    elsif action == '1-add'
      inst_code = row['institution_code']
      if (inst = Institution.find_by_code( inst_code ))
        hash = { code: new_code, name: row['new_name'], site_type: row['site_type'], charter: row['charter'], institution_id: inst.id }
        Site.create! hash
        created += 1
      else
        raise Exceptions::CsvImporterError.new, "#{notify_msg}: Institution not found: #{inst_code}"
      end
    end

  end

  msg = "created: #{created}, updated: #{updated}, skipped: #{skipped}, total: #{count}"
  notifier.notify msg
  notifier.notify "Finished: #{notify_msg}"

  site_count = site_ids.size
  sites_indexed = 0
  site_ids.each_slice(500) do |batch|
    notifier.notify "Reindexing sites #{sites_indexed + 1} - #{sites_indexed + batch.size} of #{site_count}"
    Sunspot.index! Site.where(id: batch).for_indexing
    sites_indexed += batch.size
  end
  notifier.notify "Finished: Reindexing #{site_count} sites"

end
