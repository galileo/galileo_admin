# frozen_string_literal: true

desc 'Retrieve and load data dump'
task import_data: :environment do |_, _args|
  $stdout.sync = true

  notifier = NotificationService.new
  if Rails.env.production?
    message = 'Not allowed to run import_data in production.'
    notifier.notify(message)
    return
  end

  notifier.start 'import_data'

  begin
    DataImportService.import_all(notifier: notifier)

    notifier.start 'Solr reindex'
    SolrService.delete_all
    SolrService.reindex_all
    notifier.finish 'Solr reindex'

    notifier.start 'Rebuilding IP Cache'
    IpService.reset_cache
    notifier.finish 'Rebuilding IP Cache', additional_message: "#{Redis.current.dbsize} entries in cache."

  rescue StandardError => e
    notifier.notify "import_data failed. exception: #{e}"
  end

  notifier.finish 'import_data'
end

