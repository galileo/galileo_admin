# frozen_string_literal: true

desc 'Empty the image bucket for this environment'
task s3_empty_image_bucket: :environment do |_, _args|
  $stdout.sync = true

  notifier = NotificationService.new
  notifier.start 's3_empty_image_bucket'
  S3Service.empty_image_bucket notifier: notifier
  notifier.finished 's3_empty_image_bucket'
end

desc 'Sync the images in the production bucket with the current environment'
task s3_sync_production_images: :environment do |_, _args|
  $stdout.sync = true

  notifier = NotificationService.new
  notifier.start 's3_empty_image_bucket'
  S3Service.sync_image_bucket notifier: notifier
  notifier.finish 's3_empty_image_bucket'
end

desc 'Make sure all logo and feature thumbnails are uploaded to AWS'
task process_all_thumbnails: :environment do |_, _args|
  $stdout.sync = true

  notifier = NotificationService.new
  notifier.start 'process_all_thumbnails'
  S3Service.process_all_thumbnails notifier: notifier
  notifier.finish 'process_all_thumbnails'
end

desc 'Re-process every thumbnail and upload to AWS if necessary'
task force_process_all_thumbnails: :environment do |_, _args|
  $stdout.sync = true

  notifier = NotificationService.new
  notifier.start 'process_all_thumbnails'
  S3Service.process_all_thumbnails delete_variant_cache: true, notifier: notifier
  notifier.finish 'process_all_thumbnails'
end