# frozen_string_literal: true

desc 'unassign passwords'
task(unassign_passwords: :environment) do |_, args|
  $stdout.sync = true
  process = '`unassign_passwords`'

  notifier = NotificationService.new
  notifier.start process

  begin
    current_passwords = Institution.active.pluck('current_password')
    new_passwords     = Institution.active.pluck('new_password')
    all_passwords     = (current_passwords + new_passwords).uniq.compact

    one_year_ago = 1.year.ago.to_date

    passwords_to_check = Password.where(exclude: false).where.not(commit_date: nil)

    skipped = 0
    updated = 0
    passwords_to_check.each do |pw_rec|
      password = pw_rec.password
      commit_date = pw_rec.commit_date.to_date
      if all_passwords.include? password
        skipped += 1
      elsif
        commit_date > one_year_ago
        skipped += 1
      else
        pw_rec.update( commit_date: nil )
        updated += 1
      end
    end

    notifier.notify "Updated: #{updated} Skipped: #{skipped}"

  rescue StandardError => e
    notifier.notify "#{process} failed. exception: #{e}"
  end

  notifier.finish process
end

task(unassign_passwords_reconciliation: :environment) do |_, args|
  $stdout.sync = true
  process = '`unassign_passwords`'

  notifier = NotificationService.new
  notifier.start process

  begin
    current_passwords = Institution.active.pluck('current_password').compact
    new_passwords     = Institution.active.pluck('new_password').compact
    all_passwords     = (current_passwords + new_passwords).uniq.compact

    one_year_ago = 1.year.ago.to_date

    passwords_to_check = Password.where(exclude: false).where.not(commit_date: nil)

    to_skip   = []  # passwords in use--we don't want to update them
    to_update = []  # passwords not in use--we want to remove the commit_date

    passwords_to_check.each do |pw_rec|
      password = pw_rec.password
      commit_date = pw_rec.commit_date.to_date
      if all_passwords.include? password
        to_skip << password
      elsif
        commit_date > one_year_ago
        to_skip << password
      else
        to_update << password
      end
    end

    # to_skip should equal all_passwords, but it doesn't, this is why:
    # 1. some passwords in all_passwords are excluded (yes it can be true)
    # 2. some passwords in all_passwords are not in the passwords table

    excluded_passwords = Password.where(exclude: true).pluck('password').compact

    excluded_in_use = []  # these are the passwords in all_passwords that are excluded
    excluded_passwords.each do |pw|
      if all_passwords.include? pw
        excluded_in_use << pw
      end
    end

    # these are the passwords in all_passwords that are not in the passwords table
    oddities = all_passwords - to_skip - excluded_in_use

    notifier.notify "all_passwords #{all_passwords.count} excluded_in_use #{excluded_in_use.count} oddities #{oddities.count} passwords_to_check #{passwords_to_check.count} to_skip #{to_skip.count} to_update #{to_update.count}"

    # puts oddities.inspect  # don't notify!


  rescue StandardError => e
    notifier.notify "#{process} failed. exception: #{e}"
  end

  notifier.finish process
end
