# frozen_string_literal: true

desc 'Notify, generate cycle and change passwords for Institutions'
task password_tasks: :environment do |_, _args|
  notifier = NotificationService.new
  notifier.start "password job in `#{Rails.env}`"
  service = PasswordService.new
  begin
    service.run notifier
  rescue StandardError => e
    notifier.notify "Password task failed: `#{e.message}`"
    next
  end
  results = service.report
  notifier.notify "New Passwords Established: `#{results[:updated].length}`"
  notifier.notify "Current Passwords Changed: `#{results[:changed].length}`"
  notifier.notify "Errors: `#{results[:errors].length}`"
  notifier.start "password job in `#{Rails.env}`"
end
