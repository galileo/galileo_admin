# frozen_string_literal: true

desc 'Clear IP Cache'
task ip_cache_clear: :environment do |_, _args|
  notifier = NotificationService.new
  IpService.clear_cache
  notifier.notify 'IP cache cleared'
end

desc 'Clear and rebuild IP cache'
task ip_cache_rebuild: :environment do |_, _args|
  notifier = NotificationService.new
  notifier.start 'ip_cache_rebuild'
  IpService.reset_cache
  notifier.finish 'ip_cache_rebuild', additional_message: "#{Redis.current.dbsize} entries in cache."
end

desc 'Display how many entries in the IP cache'
task ip_cache_size: :environment do |_, _args|
  $stdout.sync = true

  notifier = NotificationService.new
  notifier.notify "There are #{Redis.current.dbsize} entries in the ip cache."
end

desc 'Clear Nonce Cache'
task nonce_cache_clear: :environment do |_, _args|
  $stdout.sync = true

  notifier.start 'nonce_cache_clear'
  notifier = NotificationService.new
  redis = Redis.new(url: Rails.application.credentials[:redis][:url][Rails.env.to_sym],
                    db: Rails.application.credentials[:redis][:nonce_db][Rails.env.to_sym],
                    driver: :hiredis)
  redis.flushdb
  notifier.finish 'nonce_cache_clear'
end

desc 'Display how many entries in the Nonce cache'
task nonce_cache_size: :environment do |_, _args|
  $stdout.sync = true

  notifier = NotificationService.new
  redis = Redis.new(url: Rails.application.credentials[:redis][:url][Rails.env.to_sym],
                    db: Rails.application.credentials[:redis][:nonce_db][Rails.env.to_sym],
                    driver: :hiredis)
  notifier.notify "There are #{redis.dbsize} entries in the nonce cache."
end