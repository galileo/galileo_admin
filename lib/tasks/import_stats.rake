# frozen_string_literal: true

desc 'Retrieve and load raw stats'
task(:import_all_stats, [:yyyymmdd] => [:environment]) do |_, args|
  $stdout.sync = true
  process = '`import_all_stats`'
  notifier = NotificationService.new
  notifier.start process
  begin
    StatsImportService.import_raw_stats('login', args[:yyyymmdd], notifier: notifier)
    StatsImportService.import_raw_stats('link', args[:yyyymmdd], notifier: notifier)
    StatsImportService.import_raw_stats('search', args[:yyyymmdd], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: #{e}"
  end
  notifier.finish process
end

desc 'Retrieve and load raw login stats'
task(:import_login_stats, [:yyyymmdd] => [:environment]) do |_, args|
  $stdout.sync = true
  process = '`import_login_stats`'
  notifier = NotificationService.new
  begin
    StatsImportService.import_raw_stats('login', args[:yyyymmdd], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: #{e}"
  end
end

desc 'Retrieve and load raw link stats'
task(:import_link_stats, [:yyyymmdd] => [:environment]) do |_, args|
  $stdout.sync = true
  process = '`import_link_stats`'
  notifier = NotificationService.new
  begin
    StatsImportService.import_raw_stats('link', args[:yyyymmdd], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: #{e}"
  end
end

desc 'Retrieve and load raw search stats'
task(:import_search_stats, [:yyyymmdd] => [:environment]) do |_, args|
  $stdout.sync = true
  process = '`import_search_stats`'
  notifier = NotificationService.new
  begin
    StatsImportService.import_raw_stats('search', args[:yyyymmdd], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: #{e}"
  end
end

desc 'Retrieve and load (one time only) monthly legacy stats'
task import_monthly_legacy_stats: :environment do |_, args|
  $stdout.sync = true
  process = '`import_monthly_legacy_stats`'
  notifier = NotificationService.new
  begin
    StatsImportService.load_monthly_stats(notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: #{e}"
  end
end

desc 'Retrieve and load (one time only) legacy snapshots'
task import_legacy_snapshots: :environment do |_, args|
  $stdout.sync = true
  process = '`import_legacy_snapshots`'
  notifier = NotificationService.new
  begin
    StatsImportService.load_legacy_snapshots(notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: #{e}"
  end
end

