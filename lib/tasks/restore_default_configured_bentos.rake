# frozen_string_literal: true

desc 'restore default configured bentos'
task restore_all_configured_bentos: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.start "restore_all_configured_bentos job in `#{Rails.env}`"

  count = 0
  Institution.active.find_in_batches(batch_size: 100) do |batch|
    batch.each do |institution|
      institution.restore_defaults_configured_bentos!(preserve_institution_created_bentos: false)
    end
    count += batch.size
    notifier.notify "Restoring configured bentos: #{count}"
  end

  notifier.finish "restore_all_configured_bentos job in `#{Rails.env}`"
end

