# frozen_string_literal: true

desc 'Extract Allocations from Solr in Development'

task dev_solr_extract: :environment do |_, _args|
  return unless Rails.env.development?

  results = []
  continue = true
  fields = %w[id blacklight_id_ss class_ss display_bs active_bs
              for_institution_ss for_resource_ss resource_name_ss format_sms
              resource_name_texts resource_short_description_texts
              resource_long_description_texts resource_keywords_texts]

  solr = RSolr.connect url: 'http://localhost:9983/'

  while continue
    response = solr.get 'solr/galileo_admin-development/select',
                        params: { fl: fields, sort: 'id asc', indent: 'on',
                                  wt: 'json', start: results.length, rows: 250 }
    results += response['response']['docs']
    continue = false if response['response']['docs'].empty?
  end

  File.write('allocations.json', results.to_json)
end
