# frozen_string_literal: true

desc 'Delete all solr data'
task solr_delete_all: :environment do |_, _args|
  $stdout.sync = true
  SolrService.delete_all
end

desc 'Delete the solr data for a single table'
task :solr_delete_table, [:table_name] => :environment do |_, args|
  $stdout.sync = true
  SolrService.delete_table args[:table_name]
end

desc 'Delete institutions solr data'
task solr_delete_institutions: :environment do |_, _args|
  $stdout.sync = true
  SolrService.delete_table 'Institution'
end

desc 'Delete allocations solr data'
task solr_delete_allocations: :environment do |_, _args|
  $stdout.sync = true
  SolrService.delete_table 'Allocation'
end

desc 'Delete sites solr data'
task solr_delete_sites: :environment do |_, _args|
  $stdout.sync = true
  SolrService.delete_table 'Site'
end

desc 'Reindex all solr data'
task reindex_all: :environment do |_, _args|
  $stdout.sync = true
  SolrService.reindex_all
end

desc 'Reindex the solr data for a single table'
task :solr_reindex_table, [:table_name, :starting_id, :batch_size] => :environment do |_, args|
  $stdout.sync = true
  SolrService.reindex args[:table_name], starting_id: args[:starting_id].to_i, batch_size: args[:batch_size].to_i
end

desc 'Reindex Institutions solr data'
task reindex_institutions: :environment do |_, _args|
  $stdout.sync = true
  SolrService.reindex 'Institution'
end

desc 'Reindex Allocations solr data'
task reindex_allocations: :environment do |_, _args|
  $stdout.sync = true
  SolrService.reindex 'Allocation'
end

desc 'Reindex Sites solr data'
task reindex_sites: :environment do |_, _args|
  $stdout.sync = true
  SolrService.reindex 'Site'
end

desc 'Reindex BentoConfigs solr data'
task reindex_bento_configs: :environment do |_, _args|
  $stdout.sync = true

  SolrService.reindex 'BentoConfig'
end

desc 'Reindex ConfiguredBento solr data'
task reindex_configured_bentos: :environment do |_, _args|
  $stdout.sync = true
  SolrService.reindex 'ConfiguredBento'
end

desc 'Destructive reindex ConfiguredBento solr data'
task destructive_reindex_configured_bentos: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.start 'Solr wipe and reindex ConfiguredBento'
  SolrService.delete_table 'ConfiguredBento', notifier: notifier
  SolrService.reindex 'ConfiguredBento', notifier: notifier
  notifier.finish 'Solr wipe and reindex ConfiguredBento'
end

desc 'Destructive reindex of  solr data for a given table'
task :destructive_reindex_table, [:table_name] => :environment do |_, args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.start "Solr wipe and reindex #{args[:table_name]}"
  SolrService.delete_table args[:table_name], notifier: notifier
  SolrService.reindex args[:table_name], notifier: notifier
  notifier.finish "Solr wipe and reindex #{args[:table_name]}"
end

