# frozen_string_literal: true

desc 'Accumulate raw stats to daily stats'
task(:accumulate_daily_all_stats, [:yyyymmdd] => [:environment]) do |_, args|
  $stdout.sync = true
  process = "`accumulate_daily_all_stats`: #{args[:yyyymmdd]}"
  notifier = NotificationService.new
  notifier.start process
  begin
    StatsAccumulationService.accumulate_daily_stats('login', args[:yyyymmdd], notifier: notifier)
    StatsAccumulationService.accumulate_daily_stats('link', args[:yyyymmdd], notifier: notifier)
    StatsAccumulationService.accumulate_daily_stats('search', args[:yyyymmdd], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: `#{e}`"
  end
  notifier.finish process
end

desc 'Accumulate raw login stats to daily stats'
task(:accumulate_daily_login_stats, [:yyyymmdd] => [:environment]) do |_, args|
  $stdout.sync = true
  notifier = NotificationService.new
  begin
    StatsAccumulationService.accumulate_daily_stats('login', args[:yyyymmdd], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: `#{e}`"
  end
end

desc 'Accumulate raw link stats to daily stats'
task(:accumulate_daily_link_stats, [:yyyymmdd] => [:environment]) do |_, args|
  $stdout.sync = true
  notifier = NotificationService.new
  begin
    StatsAccumulationService.accumulate_daily_stats('link', args[:yyyymmdd], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: `#{e}`"
  end
end

desc 'Accumulate raw search stats to daily stats'
task(:accumulate_daily_search_stats, [:yyyymmdd] => [:environment]) do |_, args|
  $stdout.sync = true
  notifier = NotificationService.new
  begin
    StatsAccumulationService.accumulate_daily_stats('search', args[:yyyymmdd], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: `#{e}`"
  end
end

desc 'Accumulate daily stats to monthly stats'
task(:accumulate_monthly_all_stats, [:yyyymm] => [:environment]) do |_, args|
  $stdout.sync = true
  process = "`accumulate_monthly_all_stats`: #{args[:yyyymm]}"
  notifier = NotificationService.new
  notifier.start process
  begin
    StatsAccumulationService.accumulate_monthly_stats('login', args[:yyyymm], notifier: notifier)
    StatsAccumulationService.accumulate_monthly_stats('link', args[:yyyymm], notifier: notifier)
    StatsAccumulationService.accumulate_monthly_stats('search', args[:yyyymm], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: `#{e}`"
  end
  notifier.finish process
end

desc 'Accumulate daily login stats to monthly stats'
task(:accumulate_monthly_login_stats, [:yyyymm] => [:environment]) do |_, args|
  $stdout.sync = true
  notifier = NotificationService.new
  begin
    StatsAccumulationService.accumulate_monthly_stats('login', args[:yyyymm], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: `#{e}`"
  end
end

desc 'Accumulate daily link stats to monthly stats'
task(:accumulate_monthly_link_stats, [:yyyymm] => [:environment]) do |_, args|
  $stdout.sync = true
  notifier = NotificationService.new
  begin
    StatsAccumulationService.accumulate_monthly_stats('link', args[:yyyymm], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: `#{e}`"
  end
end

desc 'Accumulate daily search stats to monthly stats'
task(:accumulate_monthly_search_stats, [:yyyymm] => [:environment]) do |_, args|
  $stdout.sync = true
  notifier = NotificationService.new
  begin
    StatsAccumulationService.accumulate_monthly_stats('search', args[:yyyymm], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: `#{e}`"
  end
end

desc 'Update Stats Snapshots'
task(:update_stats_snapshots, [:yyyymm] => [:environment]) do |_, args|
  $stdout.sync = true
  notifier = NotificationService.new
  begin
    StatsAccumulationService.update_stats_snapshots(args[:yyyymm], notifier: notifier)
  rescue StandardError => e
    notifier.notify "#{process} failed. exception: `#{e}`"
  end
end

