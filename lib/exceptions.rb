# frozen_string_literal: true

module Exceptions
  module Link
    class UnsupportedVariableError < StandardError; end
    class EZproxyURLError < StandardError; end
  end
  class CsvImporterError < StandardError; end
  class DataImportError < StandardError; end
  class PasswordChangeError < StandardError; end
  class IpLookupFailedError < StandardError; end

  class StatImportError < StandardError; end
  class RawStatLoadError < StandardError; end
  class DailyStatAccumulateError < StandardError; end
  class MonthlyStatAccumulateError < StandardError; end

end