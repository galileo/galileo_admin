# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/password_mailer
# NOTE: when using the RANDOM institution feature, the emails headers and body
# will not match, use the inst_code param to properly check an institution email
class PasswordMailerPreview < ActionMailer::Preview

  def password_notification
    PasswordMailer
      .with(
        dates: { on: 'September 21, 2000', until: 'September 21, 2099' },
        inst: institution
      ).password_notification
  end

  def nightly_password_change
    PasswordMailer
      .with(
        dates: { on: 'September 21, 2000', until: 'September 21, 2099' },
        inst: institution
      ).nightly_password_change
  end

  def current_password
    PasswordMailer
      .with(
        dates: { on: 'September 21, 2000', until: 'September 21, 2099' },
        inst: institution
      ).current_password
  end

  def new_password
    PasswordMailer
      .with(
        dates: { on: 'September 21, 2000', until: 'September 21, 2099' },
        inst: institution
      ).new_password
  end

  private

  def institution
    Institution.find_by(code: params[:inst_code]) ||
      Institution.active.order('RANDOM()').first
  end
end
