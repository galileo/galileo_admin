# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PasswordMailer, type: :mailer do
  describe 'password notification' do
    let(:effective_dates) do
      { on: '0101', until: '0631' }
    end
    let(:inst) { Fabricate(:institution_with_password_contact) }
    let(:mail) do
      PasswordMailer.with(
        inst: inst,
        dates: effective_dates
      ).password_notification
    end
    it 'has proper headers' do
      expect(mail.subject).to eq I18n.t('app.password_mailer.password_notification.subject')
      expect(mail.to).to include inst.contacts.first.email
    end
    it 'contains the effective dates' do
      expect(mail.body.encoded).to include(
        effective_dates[:on], effective_dates[:until]
      )
    end
  end
  describe 'password change' do
    let(:effective_dates) do
      { on: '0101', until: '0631' }
    end
    let(:inst) { Fabricate(:institution_with_password_contact) }
    let(:mail) do
      PasswordMailer.with(
        inst: inst,
        dates: effective_dates
      ).nightly_password_change
    end
    it 'has proper headers' do
      expect(mail.subject).to eq I18n.t('app.password_mailer.nightly_password_change.subject')
      expect(mail.to).to include inst.contacts.first.email
    end
    it 'contains the effective dates' do
      expect(mail.body.encoded).to include(
        effective_dates[:on], effective_dates[:until]
      )
    end
  end
end
