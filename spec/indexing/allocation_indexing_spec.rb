# frozen_string_literal: true

require 'rails_helper'

describe 'Allocation indexing', type: :model do
  before do
    Sunspot.remove_all! Allocation
    Sunspot.index! allocation
  end
  context 'basic attributes' do
    let(:allocation) { Fabricate :allocation }
    it 'is indexed' do
      hit = SearchService.alloc_hit_for allocation.blacklight_id
      expect(hit.stored(:for_institution)).to eq allocation.institution.code
      expect(hit.stored(:for_resource)).to eq allocation.resource.code
    end
  end
  context 'with branding' do
    let(:allocation) { Fabricate(:glri_resource_with_branding).allocations.first }
    it 'has branding image' do
      branding = allocation.resource.branding
      hit = SearchService.alloc_hit_for allocation.blacklight_id
      expect(hit.stored(:branding_image)).to eq branding.image_thumbnail.key
      expect(hit.stored(:branding_text)).to eq branding.text
    end
    # it 'updates indexed values if branding is removed' do
    #   resource = allocation.resource
    #   resource.branding = nil
    #   resource.save
    #   hit = SearchService.alloc_hit_for allocation.blacklight_id
    #   expect(hit.stored(:branding_image)).to be_nil
    #   expect(hit.stored(:branding_text)).to be_nil
    # end
    it 'updates indexed values if branding is destroyed' do
      branding = allocation.resource.branding
      branding.destroy
      hit = SearchService.alloc_hit_for allocation.blacklight_id
      expect(hit.stored(:branding_image)).to be_nil
      expect(hit.stored(:branding_text)).to be_nil
    end
    # it 'updates indexed values if branding is changed' do
    #   new_branding = Fabricate.build :branding_with_image, institution: nil
    #   resource = allocation.resource
    #   resource.branding = new_branding
    #   resource.save
    #   hit = SearchService.alloc_hit_for allocation.blacklight_id
    #   expect(hit.stored(:branding_image)).to eq new_branding.image_thumbnail.key
    #   expect(hit.stored(:branding_text)).to eq new_branding.text
    # end
  end
  context 'thumbnails' do
    let(:allocation) do
      institution = Fabricate :institution_with_logo
      Fabricate :resource_with_logo, institution: institution
      institution.allocations.first
    end
    it 'has the resource thumbnail' do
      hit = SearchService.alloc_hit_for allocation.blacklight_id
      expect(hit.stored(:thumbnail)).to eq allocation.resource.logo_thumbnail_key
    end
    it 'has the new resource thumbnail if a new one is provided' do
      resource = allocation.resource
      old_key = resource.logo_thumbnail_key
      resource.logo.attach(
        io: File.open(Rails.root.join('spec', 'fabricators', 'images', 'test_logo.png')),
        filename: 'test_logo.png',
        content_type: 'image/png'
      )
      resource.save
      hit = SearchService.alloc_hit_for allocation.blacklight_id
      expect(hit.stored(:thumbnail)).not_to eq old_key
      expect(hit.stored(:thumbnail)).to eq allocation.resource.logo_thumbnail_key
    end
    # it 'has the institution thumbnail if the resource thumbnail is removed' do
    #   resource = allocation.resource
    #   resource.remove_logo = true
    #   resource.save
    #   hit = SearchService.alloc_hit_for allocation.blacklight_id
    #   expect(hit.stored(:thumbnail)).to eq allocation.institution.logo_thumbnail_key
    # end
    # it 'has a new image key if the institution image is updated' do
    #   institution = Fabricate :institution_with_logo
    #   Fabricate :resource, institution: institution
    #   allocation = institution.allocations.first
    #   old_key = institution.logo_thumbnail_key
    #   institution.logo.attach(
    #     io: File.open(Rails.root.join('spec', 'fabricators', 'images', 'test_logo.png')),
    #     filename: 'test_logo.png',
    #     content_type: 'image/png'
    #   )
    #   institution.save
    #   hit = SearchService.alloc_hit_for allocation.blacklight_id
    #   expect(hit.stored(:thumbnail)).not_to eq old_key
    #   expect(hit.stored(:thumbnail)).to eq allocation.institution.logo_thumbnail_key
    # end
  end
end
