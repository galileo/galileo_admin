# frozen_string_literal: true

require 'rails_helper'

describe 'Institution indexing', type: :model do
  before do
    Sunspot.remove_all! Institution
    Sunspot.index! institution
  end
  context 'basic attributes' do
    let(:institution) { Fabricate :institution }
    it 'is indexed' do
      hit = SearchService.inst_hit_for institution.code
      expect(hit.stored(:name)).to eq institution.name
      expect(hit.stored(:code)).to eq institution.code
      expect(hit.stored(:open_athens)).to eq institution.open_athens
    end
    context 'related objects' do
      context 'logo' do
        let(:institution) { Fabricate :institution_with_logo }
        it 'has logo thumbnail key' do
          hit = SearchService.inst_hit_for institution.code
          expect(hit.stored(:thumbnail)).to eq institution.logo_thumbnail.key
        end
        it 'removes the logo key if image is removed' do
          institution.remove_logo = true
          institution.save
          Sunspot.commit
          hit = SearchService.inst_hit_for institution.code
          expect(hit.stored(:thumbnail)).to be_nil
        end
      end
    end
  end
end
