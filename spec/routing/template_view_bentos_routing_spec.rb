require "rails_helper"

RSpec.describe TemplateViewBentosController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/template_view_bentos").to route_to("template_view_bentos#index")
    end

    it "routes to #new" do
      expect(:get => "/template_view_bentos/new").to route_to("template_view_bentos#new")
    end

    it "routes to #show" do
      expect(:get => "/template_view_bentos/1").to route_to("template_view_bentos#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/template_view_bentos/1/edit").to route_to("template_view_bentos#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/template_view_bentos").to route_to("template_view_bentos#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/template_view_bentos/1").to route_to("template_view_bentos#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/template_view_bentos/1").to route_to("template_view_bentos#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/template_view_bentos/1").to route_to("template_view_bentos#destroy", :id => "1")
    end
  end
end
