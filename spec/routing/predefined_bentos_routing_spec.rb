require "rails_helper"

RSpec.describe PredefinedBentosController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/predefined_bentos").to route_to("predefined_bentos#index")
    end

    it "routes to #new" do
      expect(:get => "/predefined_bentos/new").to route_to("predefined_bentos#new")
    end

    it "routes to #show" do
      expect(:get => "/predefined_bentos/1").to route_to("predefined_bentos#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/predefined_bentos/1/edit").to route_to("predefined_bentos#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/predefined_bentos").to route_to("predefined_bentos#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/predefined_bentos/1").to route_to("predefined_bentos#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/predefined_bentos/1").to route_to("predefined_bentos#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/predefined_bentos/1").to route_to("predefined_bentos#destroy", :id => "1")
    end
  end
end
