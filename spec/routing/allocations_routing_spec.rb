# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AllocationsController, type: :routing do

  it 'routes to #edit' do
    expect(get: '/allocations/1/edit').to route_to(controller: 'allocations',
                                                   action: 'edit',
                                                   id: '1')
  end

  it 'routes to #index' do
    expect(get: '/allocations').to route_to(controller: 'allocations',
                                            action: 'index')
  end

  it 'routes to #update via PUT' do
    expect(put: '/allocations/1').to route_to(controller: 'allocations',
                                              action: 'update',
                                              id: '1')
  end

  it 'routes to #update via PATCH' do
    expect(patch: '/allocations/1').to route_to(controller: 'allocations',
                                                action: 'update',
                                                id: '1')
  end
end
