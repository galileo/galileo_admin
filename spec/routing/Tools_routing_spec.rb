require 'rails_helper'

RSpec.describe ToolsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/tools').to route_to('tools#index')
    end
    it 'routes to #csv_import_tool' do
      expect(get: '/tools/csv_import').to route_to('tools#csv_import_tool')
    end
    it 'routes to #csv_import' do
      expect(put: '/tools/csv_import').to route_to('tools#csv_import')
    end
    it 'routes to #ip_cache_status_tool' do
      expect(get: '/tools/ip_cache_status').to route_to('tools#ip_cache_status_tool')
    end
    it 'routes to #ip_cache_status' do
      expect(put: '/tools/ip_cache_status').to route_to('tools#ip_cache_status')
    end
  end
end
