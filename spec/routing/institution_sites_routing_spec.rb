# frozen_string_literal: true

require 'rails_helper'

RSpec.describe InstitutionSitesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: 'institutions/1/sites').to route_to('institution_sites#index', institution_id: '1')
    end

    it 'routes to #show' do
      expect(get: 'institutions/1/sites/1').to route_to('institution_sites#show', id: '1', institution_id: '1')
    end

    it 'routes to #edit' do
      expect(get: 'institutions/1/sites/1/edit').to route_to('institution_sites#edit', id: '1', institution_id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: 'institutions/1/sites/1').to route_to('institution_sites#update', id: '1', institution_id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: 'institutions/1/sites/1').to route_to('institution_sites#update', id: '1', institution_id: '1')
    end
  end
end
