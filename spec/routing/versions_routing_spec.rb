# frozen_string_literal: true

require 'rails_helper'

RSpec.describe VersionsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/versions').to route_to('versions#index')
    end

    it 'routes to #show' do
      expect(get: '/versions/1').to route_to('versions#show', id: '1')
    end

    it 'routes to #undelete' do
      expect(post: '/versions/1/undelete').to route_to('versions#undelete', id: '1')
    end

    it 'routes to #deleted contacts' do
      expect(get: '/versions/contact/deleted').to route_to('versions#deleted', item_type: "contact")
    end

    it 'routes to #deleted institutions' do
      expect(get: '/versions/institution/deleted').to route_to('versions#deleted', item_type: "institution")
    end

    it 'routes to #deleted resources' do
      expect(get: '/versions/resource/deleted').to route_to('versions#deleted', item_type: "resource")
    end
  end
end
