# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BrandingsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: 'institutions/1/brandings').to route_to('brandings#index', institution_id: '1')
    end

    it 'routes to #new' do
      expect(get: 'institutions/1/brandings/new').to route_to('brandings#new', institution_id: '1')
    end

    it 'routes to #show' do
      expect(get: 'institutions/1/brandings/1').to route_to('brandings#show', id: '1', institution_id: '1')
    end

    it 'routes to #edit' do
      expect(get: 'institutions/1/brandings/1/edit').to route_to('brandings#edit', id: '1', institution_id: '1')
    end

    it 'routes to #create' do
      expect(post: 'institutions/1/brandings').to route_to('brandings#create', institution_id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: 'institutions/1/brandings/1').to route_to('brandings#update', id: '1', institution_id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: 'institutions/1/brandings/1').to route_to('brandings#update', id: '1', institution_id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: 'institutions/1/brandings/1').to route_to('brandings#destroy', id: '1', institution_id: '1')
    end
  end
end
