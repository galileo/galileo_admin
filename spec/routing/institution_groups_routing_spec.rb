require "rails_helper"

RSpec.describe InstitutionGroupsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/institution_groups").to route_to("institution_groups#index")
    end

    it "routes to #new" do
      expect(:get => "/institution_groups/new").to route_to("institution_groups#new")
    end

    it "routes to #show" do
      expect(:get => "/institution_groups/1").to route_to("institution_groups#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/institution_groups/1/edit").to route_to("institution_groups#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/institution_groups").to route_to("institution_groups#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/institution_groups/1").to route_to("institution_groups#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/institution_groups/1").to route_to("institution_groups#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/institution_groups/1").to route_to("institution_groups#destroy", :id => "1")
    end
  end
end
