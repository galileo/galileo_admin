require 'rails_helper'

RSpec.describe BentoConfigsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: 'institutions/1/bento_configs').to route_to('bento_configs#index', institution_id: '1')
    end

    it 'routes to #new' do
      expect(get: 'institutions/1/bento_configs/new').to route_to('bento_configs#new', institution_id: '1')
    end

    it 'routes to #show' do
      expect(get: 'institutions/1/bento_configs/1').to route_to('bento_configs#show', id: '1', institution_id: '1')
    end

    it 'routes to #edit' do
      expect(get: 'institutions/1/bento_configs/1/edit').to route_to('bento_configs#edit', id: '1', institution_id: '1')
    end

    it 'routes to #create' do
      expect(post: 'institutions/1/bento_configs').to route_to('bento_configs#create', institution_id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: 'institutions/1/bento_configs/1').to route_to('bento_configs#update', id: '1', institution_id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: 'institutions/1/bento_configs/1').to route_to('bento_configs#update', id: '1', institution_id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: 'institutions/1/bento_configs/1').to route_to('bento_configs#destroy', id: '1', institution_id: '1')
    end
  end
end
