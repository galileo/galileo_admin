require "rails_helper"

RSpec.describe ConfiguredBentosController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: 'institutions/1/brandings').to route_to('brandings#index', institution_id: '1')
      expect(:get => "/institutions/1/configured_bentos").to route_to("configured_bentos#index", institution_id: '1')
    end

    it "routes to #new" do
      expect(:get => "/institutions/1/configured_bentos/new").to route_to("configured_bentos#new", institution_id: '1')
    end

    it "routes to #show" do
      expect(:get => "/institutions/1/configured_bentos/1").to route_to("configured_bentos#show", :id => "1", institution_id: '1')
    end

    it "routes to #edit" do
      expect(:get => "/institutions/1/configured_bentos/1/edit").to route_to("configured_bentos#edit", :id => "1", institution_id: '1')
    end


    it "routes to #create" do
      expect(:post => "/institutions/1/configured_bentos").to route_to("configured_bentos#create", institution_id: '1')
    end

    it "routes to #update via PUT" do
      expect(:put => "/institutions/1/configured_bentos/1").to route_to("configured_bentos#update", :id => "1", institution_id: '1')
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/institutions/1/configured_bentos/1").to route_to("configured_bentos#update", :id => "1", institution_id: '1')
    end

    it "routes to #destroy" do
      expect(:delete => "/institutions/1/configured_bentos/1").to route_to("configured_bentos#destroy", :id => "1", institution_id: '1')
    end
  end
end

