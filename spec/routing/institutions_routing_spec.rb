# frozen_string_literal: true

require 'rails_helper'

RSpec.describe InstitutionsController, type: :routing do

  it 'routes to #index' do
    expect(get: '/institutions').to route_to('institutions#index')
  end

  it 'routes to #new' do
    expect(get: '/institutions/new').to route_to('institutions#new')
  end

  it 'routes to #show' do
    expect(get: '/institutions/1').to route_to(controller: 'institutions',
                                               action: 'show',
                                               id: '1')
  end

  it 'routes to #edit' do
    expect(get: '/institutions/1/edit').to route_to(controller: 'institutions',
                                                    action: 'edit',
                                                    id: '1')
  end

  it 'routes to #create' do
    expect(post: '/institutions').to route_to('institutions#create')
  end

  it 'routes to #update via PUT' do
    expect(put: '/institutions/1').to route_to(controller: 'institutions',
                                               action: 'update',
                                               id: '1')
  end

  it 'routes to #update via PATCH' do
    expect(patch: '/institutions/1').to route_to(controller: 'institutions',
                                                 action: 'update',
                                                 id: '1')
  end

  it 'routes to #destroy' do
    expect(delete: '/institutions/1').to route_to(controller: 'institutions',
                                                  action: 'destroy',
                                                  id: '1')
  end

  context 'email routes' do
    it 'routes to #new_new_password' do
      expect(get: '/institutions/1/new_new_password').to(
        route_to(controller: 'institutions',
                 action: 'new_new_password',
                 id: '1')
      )
    end
    it 'routes to #update_password_asap' do
      expect(get: '/institutions/1/update_password_asap').to(
        route_to(controller: 'institutions',
                 action: 'update_password_asap',
                 id: '1')
      )
    end
    it 'routes to #notify_current_password' do
      expect(get: '/institutions/1/notify_current_password').to(
        route_to(controller: 'institutions',
                 action: 'notify_current_password',
                 id: '1')
      )
    end
    it 'routes to #notify_new_password' do
      expect(get: '/institutions/1/notify_new_password').to(
        route_to(controller: 'institutions',
                 action: 'notify_new_password',
                 id: '1')
      )
    end
  end
  context 'versioning routes' do
    it 'routes to #diff' do
      expect(get: '/institutions/1/diff').to route_to('institutions#diff', id: '1')
    end

    it 'routes to #rollback' do
      expect(get: '/institutions/1/rollback').to route_to('institutions#rollback', id: '1')
    end

    it 'routes to #deleted' do
      expect(get: '/institutions/deleted').to route_to('versions#deleted', item_type: 'institution')
    end
  end
  context 'resource listing routes' do
    it 'routes to #index' do
      expect(get: '/institutions/1/resources').to(
        route_to(controller: 'institution_resources',
                 action: 'index',
                 institution_id: '1')
      )
    end
  end
end
