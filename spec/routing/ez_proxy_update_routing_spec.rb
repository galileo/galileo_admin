require "rails_helper"

RSpec.describe EzProxyUpdateController, type: :routing do
  describe "routing" do
    it "routes to #report" do
      expect(get: "/ez_proxy_update").to route_to("ez_proxy_update#report")
    end

    it "routes to #report 2" do
      expect(get: "/ez_proxy_update/inst_code").to route_to("ez_proxy_update#report", inst_code: 'inst_code')
    end
  end
end
