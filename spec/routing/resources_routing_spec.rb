# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ResourcesController, type: :routing do
  it 'routes to #index' do
    expect(get: '/resources').to route_to('resources#index')
  end

  it 'routes to #new' do
    expect(get: '/resources/new').to route_to('resources#new')
  end

  it 'routes to #show' do
    expect(get: '/resources/1').to route_to(controller: 'resources',
                                            action: 'show',
                                            id: '1')
  end

  it 'routes to #edit' do
    expect(get: '/resources/1/edit').to route_to(controller: 'resources',
                                                 action: 'edit',
                                                 id: '1')
  end

  it 'routes to #create' do
    expect(post: '/resources').to route_to('resources#create')
  end

  it 'routes to #update via PUT' do
    expect(put: '/resources/1').to route_to(controller: 'resources',
                                            action: 'update',
                                            id: '1')
  end

  it 'routes to #update via PATCH' do
    expect(patch: '/resources/1').to route_to(controller: 'resources',
                                              action: 'update',
                                              id: '1')
  end

  it 'routes to #destroy' do
    expect(delete: '/resources/1').to route_to(controller: 'resources',
                                               action: 'destroy',
                                               id: '1')
  end

  it 'routes to #copy' do
    expect(get: '/resources/1/copy').to route_to(controller: 'resources',
                                                 action: 'copy',
                                                 id: '1')
  end

  context 'versioning routes' do
    it 'routes to #diff' do
      expect(get: '/resources/1/diff').to route_to('resources#diff', id: '1')
    end

    it 'routes to #rollback' do
      expect(get: '/resources/1/rollback').to route_to('resources#rollback', id: '1')
    end

    it 'routes to #deleted' do
      expect(get: '/resources/deleted').to route_to('versions#deleted', item_type: 'resource')
    end
  end
end
