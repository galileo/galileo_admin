# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::BaseController do
  describe 'routing' do
    it 'routes to #ok' do
      expect(get: 'api/v1/ok').to route_to(controller: 'api/v1/base',
                                           action: 'ok')
    end
  end
end

RSpec.describe Api::V1::AuthSupportController do
  describe 'routing' do
    it 'routes to #inst' do
      expect(get: 'api/v1/inst').to route_to(controller: 'api/v1/auth_support',
                                             action: 'inst')
    end
  end
end
