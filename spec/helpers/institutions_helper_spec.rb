# frozen_string_literal: true

require 'rails_helper'

describe InstitutionsHelper do
  include ApplicationHelper
  let(:inst) { Fabricate :institution }
  describe 'general_institution_fields' do
    it 'returns respective institution general tab information' do
      expect(general_institution_fields(inst).values).to include(inst.name)
    end
  end
end
