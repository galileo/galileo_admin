# frozen_string_literal: true

require 'rails_helper'

describe ApplicationHelper do
  let(:label) { Faker::Lorem.word }
  let(:value) { Faker::Lorem.sentence }
  let(:inst) { Fabricate :institution }
  describe 'attribute' do
    it 'returns shared attribute tags of a label and value' do
      expect(attribute(label, value)).to include(
        'card',
        label,
        value
      )
    end
  end

  describe 'tab_fields_for' do
    it 'returns a div containing fields for the relation' do
      # expect(tab_fields_for(inst, general_institution_fields)).to include('trash')
    end
  end

  describe 'format_array_field' do
    let(:inst) { Fabricate :institution, ip_addresses: %w[1.1.1.1 1.2.3.4] }
    it 'returns a set of tags separated by <br> tags' do
      expect(format_array_field(inst.ip_addresses)).to include '<br>'
    end

    it 'does not crash the site even if the array is nil' do
      expect(format_array_field(nil)).to eq nil
    end
  end
end
