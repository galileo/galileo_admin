# frozen_string_literal: true

require 'rails_helper'

describe LegacyDatesHelper do
  describe '#valid_yearless_date?' do
    it 'returns false for invalid month day combos' do
      expect(
        valid_yearless_date?('0000')
      ).to be_falsey
    end
    it 'returns false for invalid date format' do
      expect(
        valid_yearless_date?('01010')
      ).to be_falsey
    end
    it 'returns true for valid month day combos' do
      expect(
        valid_yearless_date?('0101')
      ).to be_truthy
    end
  end
  describe '#includes_today?' do
    it 'returns true if array contains today\'s MMDD' do
      expect(
        includes_today?(['0101', DateTime.now.strftime('%m%d')])
      ).to be_truthy
    end
    it 'returns false if array does not contain today\'s MMDD' do
      expect(
        includes_today?(['0101'])
      ).to be_falsey
    end
    it 'returns false if array is empty' do
      expect(
        includes_today?([])
      ).to be_falsey
    end
  end
end
