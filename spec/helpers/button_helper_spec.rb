# frozen_string_literal: true

require 'rails_helper'

describe ButtonHelper do
  let(:model) { Fabricate :institution }
  let(:model_name) { model.class.name.underscore.pluralize }

  describe '#btn_primary_link' do
    it 'renders a link with bootstrap classes' do
      expect(
        btn_primary_link(
          'Test Link',
          'http://test.com'
        )
      ).to eq(
        '<a class="btn btn-primary " href="http://test.com">Test Link</a>'
      )
    end
    it 'appends passed in classes' do
      expect(
        btn_primary_link(
          'Test Link',
          'http://test.com',
          'btn-lg'
        )
      ).to eq(
        '<a class="btn btn-primary btn-lg" href="http://test.com">Test Link</a>'
      )
    end
  end

  describe '#new_btn_link' do
    let(:institution_relation) { double('institution_relation', klass: Institution) }
    it 'makes a model out of passed in activerecord relation' do
      expect(new_btn_link(institution_relation)).to include(
        '/institutions/new',
        'btn btn-primary'
      )
    end
  end

  # the following dont seem to need testing because
  # they return a btn_primary_link , which is tested above
  describe '#edit_btn_link' do
    it 'returns an edit button' do
      expect(edit_btn_link(model)).to include(
        "/#{model_name}/#{model.id}/edit",
        'btn btn-primary'
      )
    end
  end
  describe '#delete_btn_link' do
    it 'returns a delete button' do
      expect(delete_btn_link(model)).to include(
        "/#{model_name}/#{model.id}",
        'btn btn-primary',
        'data-method="delete"'
      )
    end
  end

  describe '#index_page_buttons' do
    let(:institution_relation) { double('institution_relation', klass: Institution) }
    it 'returns a div with buttons' do
      expect(index_page_buttons(institution_relation)).to include(
        'btn-group',
        'btn btn-primary',
        'Institution Actions'
      )
    end
  end

  describe '#show_page_buttons' do
    it 'appends edit and delete buttons' do
      expect(show_page_buttons(model)).to include(
        "/#{model_name}/#{model.id}/edit",
        "/#{model_name}/#{model.id}"
      )
    end
  end
  describe '#action_buttons' do
    it 'returns a div with small buttons' do
      expect(action_buttons(model)).to include(
        'btn-group',
        'btn-sm',
        'Institution Actions'
      )
    end
  end
end
