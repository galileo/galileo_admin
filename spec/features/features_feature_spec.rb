# frozen_string_literal: true

require 'rails_helper'

feature 'Feature management' do
  include Devise::Test::IntegrationHelpers
  context 'For an institutional  portal subrole user' do
    let(:user) { Fabricate :institutional_portal_subrole_user_with_features }
    before :each do
      sign_in user
      Fabricate.times 2, :resource
      @institution = user.institutions.last
      Fabricate :resource, institution: @institution
    end
    context 'the feature button' do
      it 'is displayed on the Institution show page' do
        visit institution_path @institution
        expect(page).to have_link 'Features'
      end
    end
    context 'the index page' do
      it 'shows one local feature and the default features from the institution
        group' do
        visit institution_features_path @institution
        expect(page).to have_text @institution.name
        expect(page).to have_text @institution.features.first.link_label
        @institution.institution_group.features.each do |ig_feature|
          expect(page).to have_text ig_feature.link_label
        end
      end
    end
    context 'the new page' do
      it 'allows the creation of new features' do
        visit new_institution_feature_path @institution
        expect(page).to have_field(
                          I18n.t('activerecord.attributes.feature.view_type'),
                          )
        expect(page).to have_field(
                          I18n.t('activerecord.attributes.feature.position'),
                          )
        expect(page).to have_field(
                          I18n.t('activerecord.attributes.feature.link_label'),
                          )
        expect(page).to have_field(
                          I18n.t('activerecord.attributes.feature.link_url'),
                          )
        expect(page).to have_field(
                          I18n.t('activerecord.attributes.feature.link_description'),
                          )
      end
    end
    context 'the show page' do
      it 'shows all data from a feature record and an edit button' do
        feature = @institution.features.first
        visit institution_feature_path @institution, feature
        expect(page).to have_text feature.view_type
        expect(page).to have_text feature.position
        expect(page).to have_text feature.link_label
        expect(page).to have_text feature.link_url
        expect(page).to have_text feature.link_description
        expect(page).to have_link I18n.t('app.defaults.labels.edit')
      end
    end
    context 'the edit page' do
      it 'shows fields to edit all expected feature attributes' do
        feature = @institution.features.first
        visit edit_institution_feature_path @institution, feature
        expect(page).to have_field(
                          I18n.t('activerecord.attributes.feature.view_type'),
                          with: feature.view_type
                        )
        expect(page).to have_field(
                          I18n.t('activerecord.attributes.feature.position'),
                          with: feature.position
                        )
        expect(page).to have_field(
                          I18n.t('activerecord.attributes.feature.link_label'),
                          with: feature.link_label
                        )
        expect(page).to have_field(
                          I18n.t('activerecord.attributes.feature.link_url'),
                          with: feature.link_url
                        )
        expect(page).to have_field(
                          I18n.t('activerecord.attributes.feature.link_description'),
                          with: feature.link_description
                        )
        expect(page).to have_field I18n.t('activerecord.attributes.feature.image')
      end
    end
    context 'they cannot alter features for institutions to which they are not assigned' do
      let(:message) { 'You are not authorized to access this page' }
      it 'and a message will display' do
        unassigned_institution = Fabricate :institution do
          features count: 1
        end
        visit institution_features_path(unassigned_institution)
        expect(page).to have_text message
        visit(institution_feature_path(unassigned_institution,
                                       unassigned_institution.features.first))
        expect(page).to have_text message
        visit(edit_institution_feature_path(unassigned_institution,
                                            unassigned_institution
                                              .features.first))
        expect(page).to have_text message
      end
      it 'but will allow them to view the defaults for their institution
        type' do
        group = @institution.institution_group
        visit institution_group_features_path(group)
        expect(page).not_to have_text message
      end
    end
  end
end
