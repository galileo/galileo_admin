# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Contacts management' do
  include Devise::Test::IntegrationHelpers
  context 'For an admin user' do
    before :each do
      user = Fabricate :admin_user
      sign_in user
      Fabricate.times 2, :contact
      Fabricate :contact, institution_id: Fabricate(:institution)
    end
    #test
    context 'Basic CRUD', js: true do
      it 'can create a new contact' do
        visit new_contact_path

        fill_in 'First Name', with: 'Test First'
        fill_in 'Last Name', with: 'Test Last'
        fill_in 'Email', with: 'test@email.com'

        click_button 'Create Contact'

        expect(page).to have_text 'Test First'
      end

      it 'can request an existing contact' do
        contact = Contact.last

        visit contact_path contact

        expect(page).to have_text contact.name
      end

      it 'can update an existing contact' do
        contact = Contact.last

        visit edit_contact_path contact

        fill_in 'First Name', with: 'Test Contact'

        click_button 'Update Contact'

        expect(page).to have_text 'Test Contact'
      end

      it 'can delete an existing contact' do
        contact = Contact.last

        visit contact_path contact

        accept_alert do
          click_link 'Delete'
        end

        expect(page).to have_text contact.name
      end
    end
    context 'on the deleted contacts page' do
      scenario 'can undelete a contact' do
        deleted_contact = Fabricate :contact, first_name: 'Test Contact'
        deleted_contact.destroy
        visit contacts_path
        expect(page).not_to have_text deleted_contact.first_name

        visit deleted_contacts_path
        click_link deleted_contact.name
        click_link 'undelete'

        expect(page).to have_text 'Record undeleted!'
      end
    end

  end
  context 'For an institutional user' do
    let(:user) { Fabricate :institutional_user_with_institutions }
    before :each do
      sign_in user
      Fabricate.times 2, :resource
      Fabricate :contact, institution: user.institutions.last
    end
    context 'Basic CRUD', js: true do
      it 'can not request an existing contact' do
        institution = user.institutions.last
        contact = institution.contacts.first
        other_contact = Fabricate :contact

        visit contact_path contact
        expect(page).to have_text 'You are not authorized to access this page.'
        visit contact_path other_contact
        expect(page).to have_text 'You are not authorized to access this page.'
      end
    end
  end

  context 'for a help desk user' do
    before :each do
      user = Fabricate :helpdesk_user
      sign_in user
      institution = Fabricate :institution
      Fabricate :contact, institution: institution
    end
    context 'Basic CRUD', js: true do
      it 'can create a new contact' do
        visit new_contact_path

        fill_in 'First Name', with: 'Test First'
        fill_in 'Last Name', with: 'Test Last'
        fill_in 'Email', with: 'test@email.com'

        click_button 'Create Contact'

        expect(page).to have_text 'Test First'
      end

      it 'can request an existing contact' do
        contact = Contact.last

        visit contact_path contact

        expect(page).to have_text contact.name
      end

      it 'can update an existing contact' do
        contact = Contact.last

        visit edit_contact_path contact

        fill_in 'First Name', with: 'Test Contact'

        click_button 'Update Contact'

        expect(page).to have_text 'Test Contact'
      end

      it 'can delete an existing contact' do
        contact = Contact.last

        visit contact_path contact

        accept_alert do
          click_link 'Delete'
        end

        expect(page).to have_text contact.name
      end
    end

    context 'on the contacts list' do
      scenario 'can see contacts and an edit button' do
        visit contacts_path
        expect(page).to have_text Contact.last.email
        expect(page).to have_link I18n.t('app.defaults.labels.edit')
      end
    end
    context 'on the contact show page' do
      scenario 'can see contact show page' do
        contact = Contact.last
        visit contact_path contact
        expect(page).to have_text contact.email
        expect(page).to have_link I18n.t('app.defaults.labels.edit')
        expect(page).to have_link I18n.t('app.defaults.labels.delete')
      end
    end
  end
end