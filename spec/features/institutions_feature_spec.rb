# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Institution management' do
  include Devise::Test::IntegrationHelpers
  context 'for an admin user' do
    before :each do
      user = Fabricate :admin_user
      sign_in user
      Fabricate.times 2, :institution
    end
    context 'Basic CRUD', js: true do
      it 'can create a new institution' do
        visit new_institution_path

        fill_in 'Name', with: 'Test Institution'
        fill_in 'Code', with: 'test'

        click_button 'Create Institution'

        expect(page).to have_text 'Test Institution'
      end

      it 'can request an existing institution' do
        institution = Institution.last

        visit institution_path institution

        expect(page).to have_text institution.name
      end

      it 'can update an existing institution' do
        institution = Institution.last

        visit edit_institution_path institution

        fill_in 'Name', with: 'Test Institution'

        click_button 'Update Institution'

        expect(page).to have_text 'Test Institution'
      end

      it 'can delete an existing institution' do
        institution = Institution.last

        visit institution_path institution

        accept_alert do
          click_link 'Delete'
        end

        expect(page).to have_text institution.name
      end
    end
    context 'on the institution list' do
      scenario 'user lists all Institutions' do
        visit institutions_path
        expect(page).to have_text Institution.last.name
        expect(page).to have_text Institution.last.code
      end
      scenario 'can visit and see deleted institutions' do
        deleted_institution = Fabricate :institution, name: 'Test Institution'
        deleted_institution.destroy
        visit institutions_path
        click_link 'Show Deleted'

        expect(page).to have_text deleted_institution.name
      end
      scenario 'can visit and see sites' do
        deleted_institution = Fabricate :institution, name: 'Test Institution'
        deleted_institution.destroy
        visit institutions_path
        click_link 'Show Deleted'

        expect(page).to have_text deleted_institution.name
      end
    end
    context 'on an institution page' do
      scenario 'can see create new contact button' do
        institution = Institution.last
        visit institution_path institution
        expect(page).to have_link I18n.t('app.contacts.labels.new_contact')
      end
      scenario 'can see the sites button' do
        institution = Institution.last
        visit institution_path institution
        expect(page).to have_link I18n.t('activerecord.attributes.institution.sites')
      end
      scenario 'can see the features button' do
        institution = Institution.last
        visit institution_path institution
        expect(page).to have_link I18n.t('activerecord.attributes.institution.features')
      end
      scenario 'can see the brandings button' do
        institution = Institution.last
        visit institution_path institution
        expect(page).to have_link I18n.t('activerecord.attributes.institution.brandings')
      end
    end
    context 'on the deleted institutions page' do
      scenario 'can undelete an institution' do
        deleted_institution = Fabricate :institution, name: 'Test Institution'
        deleted_institution.destroy
        visit institutions_path
        expect(page).not_to have_text deleted_institution.name

        visit deleted_institutions_path
        click_link deleted_institution.name
        click_link 'undelete'

        expect(page).to have_text 'Record undeleted!'
      end
    end
    context 'Brandings' do
      context 'on the Brandings Page' do
        scenario 'can see the new branding button' do
          institution = Fabricate :institution_with_branding
          visit institution_brandings_path institution
          expect(page).to have_link I18n.t('app.brandings.label.new_branding')
        end
        scenario 'can see a list of the institutions branding' do
          institution = Fabricate :institution_with_branding
          visit institution_brandings_path institution
          expect(page).to have_text institution.brandings.first.name
        end
        scenario 'can see edit buttons next to a branding' do
          institution = Fabricate :institution_with_branding
          institution.brandings << Fabricate(:branding)
          visit institution_brandings_path institution
          expect(page).to have_link I18n.t('app.defaults.labels.edit')
        end
      end
      context 'on an individual Branding Page' do
        scenario 'can see the edit button' do
          institution = Fabricate :institution_with_branding
          visit institution_branding_path(institution, institution.brandings.first)
          expect(page).to have_link I18n.t('app.defaults.labels.edit')
        end
        scenario 'can see the delete button' do
          institution = Fabricate :institution_with_branding
          visit institution_branding_path(institution, institution.brandings.first)
          expect(page).to have_link I18n.t('app.defaults.labels.delete')
        end
      end
    end
    context 'Sites' do
      context 'on the Sites Page' do
        scenario 'can see the new site button' do
          institution = Fabricate :institution_with_sites
          visit institution_sites_path institution
          expect(page).to have_link I18n.t('app.sites.label.new_site')
        end
        scenario "can see a list of the institution's sites" do
          institution = Fabricate :institution_with_sites
          visit institution_sites_path institution
          expect(page).to have_text institution.sites.first.name
          expect(page).to have_text institution.sites.last.name
        end
        scenario 'can see the edit buttons' do
          institution = Fabricate :institution_with_sites
          visit institution_sites_path institution
          expect(page).to have_link I18n.t('app.defaults.labels.edit')
        end
      end
      context 'on an individual Site Page' do
        scenario 'can see the edit button' do
          institution = Fabricate :institution_with_sites
          visit institution_site_path(institution, institution.sites.first)
          expect(page).to have_link I18n.t('app.defaults.labels.edit')
        end
        scenario 'can see the delete button' do
          institution = Fabricate :institution_with_sites
          visit institution_site_path(institution, institution.sites.first)
          expect(page).to have_link I18n.t('app.defaults.labels.delete')
        end
      end
    end
  end

  context 'for a general institutional user' do
    let(:user) { Fabricate :institutional_user_with_sites }
    before :each do
      sign_in user
      Fabricate.times 1, :institution
    end
    context 'Basic CRUD', js: true do
      it 'can not create a new institution' do
        visit new_institution_path
        expect(page).to have_text 'You are not authorized to access this page.'
      end
      it 'can request a users institution' do
        institution = user.institutions.last

        visit institution_path institution

        expect(page).to have_text institution.name
      end
      it 'cannot request an institution that is not a users institution' do
        institution = Fabricate :institution

        visit institution_path institution

        expect(page).to have_text 'You are not authorized to access this page.'
      end
      it 'can not update a users existing institution or other institutions' do
        institution = user.institutions.last
        other_institution = Fabricate :institution

        visit edit_institution_path institution

        expect(page).to have_text 'You are not authorized to access this page.'

        visit edit_institution_path other_institution

        expect(page).to have_text 'You are not authorized to access this page.'
      end
      it 'can not delete an existing institution' do
        institution = user.institutions.last

        visit institution_path institution

        expect(page).not_to have_link I18n.t('app.defaults.labels.delete')
      end
    end
    context 'on the Institution list' do
      before(:each) { visit institutions_path }
      scenario 'only assigned institutions are shown' do
        expect(page).to have_text user.institutions.first.code
        expect(page).to have_text user.institutions.last.code
        expect(page).not_to have_text Institution.last.code
      end
      scenario 'no edit buttons are shown' do
        within '.table' do
          expect(page).not_to have_link I18n.t('app.defaults.labels.edit')
        end
      end
    end
    context 'on the Institution page' do
      before(:each) { visit institution_path(user.institutions.last) }
      scenario "buttons that shouldn't be shown" do
        expect(page).not_to have_link I18n.t('app.institution.labels.new_new_password')
        expect(page).not_to have_link I18n.t('app.institution.labels.send_notifications')
        expect(page).not_to have_link I18n.t('app.institution.labels.update_asap_password')
        expect(page).not_to have_link I18n.t('app.contacts.labels.new_contact')
        expect(page).not_to have_link I18n.t('app.defaults.labels.edit')
      end
      scenario 'buttons that should be shown' do
        expect(page).to have_link I18n.t('activerecord.attributes.institution.sites')
        expect(page).to have_link I18n.t('activerecord.attributes.institution.features')
        expect(page).to have_link I18n.t('activerecord.attributes.institution.brandings')
      end
      scenario 'Can visit Brandings' do
        click_link 'Brandings'
        expect(page).to have_text "Brandings: #{user.institutions.last.name}"
      end
    end
    context 'Branding' do
      context 'on the Brandings Page' do
        context 'a general institutional user' do
          scenario 'can see a list of the institutions brandings' do
            institution = user.institutions.last
            institution.brandings << Fabricate(:branding)
            visit institution_brandings_path institution
            # can see
            expect(page).to have_text institution.brandings.first.name

            #can't see
            expect(page).not_to have_link I18n.t('app.brandings.label.new_branding')
            expect(page).not_to have_link I18n.t('app.defaults.labels.edit')
          end
          scenario 'can visit an individual Branding Page for their institution' do
            institution = user.institutions.last
            institution.brandings << Fabricate(:branding)
            visit institution_branding_path(institution, institution.brandings.first)
            #can see
            expect(page).to have_text institution.brandings.first.name

            #can't see
            expect(page).not_to have_link I18n.t('app.defaults.labels.edit')
            expect(page).not_to have_link I18n.t('app.defaults.labels.delete')
          end
          scenario 'can\'t see other institutions branding pages' do
            institution = Fabricate :institution_with_branding
            visit institution_branding_path(institution, institution.brandings.first)
            expect(page).to have_text 'You are not authorized to access this page.'
          end

        end

      end

    end
  end

  context 'for a help desk user' do
    before :each do
      user = Fabricate :helpdesk_user
      sign_in user
      Fabricate.times 2, :institution
    end
    context 'Basic CRUD', js: true do
      it 'can request an institution' do
        institution = Institution.last

        visit institution_path institution

        expect(page).to have_text institution.name
      end
    end
    context 'on the Institution list' do
      scenario 'can see institutions and their passwords but no edit buttons' do
        visit institutions_path
        expect(page).to have_text Institution.first.current_password
        expect(page).to have_text Institution.last.current_password
        expect(page).not_to have_link I18n.t('app.defaults.labels.edit')
      end
    end
    context 'on the institution show page' do
      scenario 'can see institutions show page' do
        institution = Institution.last
        visit institution_path institution
        expect(page).to have_text institution.current_password
        expect(page).not_to have_link edit_institution_path institution
        expect(page).not_to have_link 'delete'
      end
      scenario 'do not see the password action buttons' do
        institution = Institution.last
        visit institution_path institution
        expect(page).not_to have_link I18n.t('app.institution.labels.new_new_password')
        expect(page).not_to have_link I18n.t('app.institution.labels.send_notifications')
        expect(page).not_to have_link I18n.t('app.institution.labels.update_asap_password')
      end
      scenario 'can see create new contact button' do
        institution = Institution.last
        visit institution_path institution
        expect(page).to have_link I18n.t('app.contacts.labels.new_contact')
      end
      scenario 'can not see the sites button' do
        expect(page).not_to have_link I18n.t('activerecord.attributes.institution.sites')
      end
      scenario 'can not see the features button' do
        expect(page).not_to have_link I18n.t('activerecord.attributes.institution.features')
      end
      scenario 'can see the brandings button' do
        expect(page).not_to have_link I18n.t('activerecord.attributes.institution.brandings')
      end
    end
  end
end