# frozen_string_literal: true

require 'rails_helper'

feature 'Resource management' do
  include ActionView::Helpers::SanitizeHelper
  include Devise::Test::IntegrationHelpers
  context 'For an admin user' do
    before :each do
      user = Fabricate :admin_user
      sign_in user
      Fabricate.times 2, :resource
      Fabricate :resource, institution_id: Fabricate(:institution)
    end
    scenario 'the table shows all resources with edit and copy buttons' do
      visit resources_path
      within '#resources-table' do
        expect(find_all('tr').count).to eq 4
        expect(find_all('.resource-edit-button').count).to eq 3
        expect(find_all('.resource-copy-button').count).to eq 3
      end
    end
    context 'Basic CRUD', js: true do
      it 'can create a new resource' do
        visit new_resource_path

        fill_in 'Name', with: 'Test Resource'
        fill_in 'Code', with: 'test'

        click_button 'Create Resource'

        expect(page).to have_text 'Test Resource'
      end

      it 'can request an existing resource' do
        resource = Resource.last

        visit resource_path resource

        expect(page).to have_text resource.name
      end

      it 'can update an existing resource' do
        resource = Resource.last

        visit edit_resource_path resource

        fill_in 'Name', with: 'Test Resource'

        click_button 'Update Resource'

        expect(page).to have_text 'Test Resource'
      end

      it 'can delete an existing resource' do
        resource = Resource.last

        visit resource_path resource

        accept_alert do
          click_link 'Delete'
        end

        expect(page).to have_text resource.name
      end
    end
    context 'on the deleted resources page' do
      scenario 'can undelete resource' do
        deleted_resource = Fabricate :resource, name: 'Test Resource'
        deleted_resource.destroy
        visit resources_path
        expect(page).not_to have_text deleted_resource.name

        visit deleted_resources_path
        click_link deleted_resource.name
        click_link 'undelete'

        expect(page).to have_text 'Record undeleted!'
      end
    end
    context 'when copying resources' do
      before :each do
        Fabricate :institution, name: 'Test Institution', code: 'test'
        Fabricate :resource
        visit copy_resource_path Resource.last
        fill_in I18n.t('activerecord.attributes.resource.name'), with: 'Cheese'
        fill_in I18n.t('activerecord.attributes.resource.code'), with: 'chez'
        fill_in I18n.t('activerecord.attributes.resource.ip_access_url'), with: 'https://www.example.com'
      end
      scenario 'the values copied into the name, code, and ip URL fields are displayed' do
        click_on 'Create Resource'
        expect(page).to have_current_path resource_path Resource.last
        expect(page).to have_text 'chez'
        expect(page).to have_text 'Cheese'
        expect(page).to have_text 'https://www.example.com'
      end
      scenario 'a newly generated glri code is displayed when a glri institution is selected' do
        page.select 'Test Institution', from: I18n.t('activerecord.attributes.resource.institution_id')
        click_on 'Create Resource'
        expect(page).to have_current_path resource_path Resource.last
        expect(page).to have_text 'chee-test'
      end
      scenario 'an EDS reminder message is displayed' do
        click_on 'Create Resource'
        expect(page).to have_text strip_tags(
                                    I18n.t('app.resources.messages.copy_info_admin',
                                           contact_href: I18n.t('app.links.contact'))
                                  )
      end
    end
  end
  context 'For an institutional advanced subrole user' do
    let(:user) { Fabricate :institutional_advanced_subrole_user_with_institutions }
    before :each do
      sign_in user
      Fabricate.times 2, :resource
      Fabricate :resource, institution: user.institutions.last, active: false
      Fabricate :resource, institution: user.institutions.last
    end
    context 'Basic CRUD', js: true do
      it 'can create a new glri resource' do
        visit new_resource_path

        fill_in 'Name', with: 'Test Resource'

        click_button 'Create Resource'

        expect(page).to have_text 'Test Resource'
        expect(Resource.last.glri?).to be true
      end

      it 'can request a users resource' do
        institution = user.institutions.last
        resource = institution.resources.first

        visit resource_path resource

        expect(page).to have_text resource.name
      end

      it 'can request a resource that is not a users resource' do
        resource = Fabricate :resource

        visit resource_path resource

        expect(page).to have_text resource.name
      end

      it 'can update a users existing resource' do
        institution = user.institutions.last
        resource = institution.resources.first

        visit edit_resource_path resource

        fill_in 'Name', with: 'Test Resource'

        click_button 'Update Resource'

        expect(page).to have_text 'Test Resource'
      end
    end
    context 'on the resources page', js: true do
      scenario 'the table shows the  glri resources for the users institution
                with two editable resources and one active copyable resource' do
        pending
        # visit resources_path
        # select 'Either', from: 'Active'
        # select 'Either', from: 'GLRI State'
        # within '#resources-table' do
        #   expect(find_all('.resource-row').count).to eq 2
        #   expect(find_all('.resource-edit-button').count).to eq 2
        #   expect(find_all('.resource-copy-button').count).to eq 1
        # end
        fail
      end
      scenario 'the user can click copy and see a new resource form
                pre-populated with some of the copied resource data' do
        visit resources_path
        find_all('.resource-copy-button').first.click
        copied_resource = Resource.last
        expect(page).to have_current_path copy_resource_path copied_resource
        expect(page).to have_text copied_resource.name
      end
    end
    context 'when copying resources from the user\'s institution' do
      before :each do
        Fabricate :resource, code: 'chez-test', name: 'Cheese',
                  ip_access_url: 'https://www.example.com', institution: user.institutions.first
        visit copy_resource_path Resource.last
      end
      scenario 'the values copied into the name and ip URL fields are displayed' do
        click_on 'Create Resource'
        expect(page).to have_current_path resource_path Resource.last
        expect(page).to have_text 'Cheese'
        expect(page).to have_text 'https://www.example.com'
      end
      scenario 'an EDS reminder message is displayed' do
        click_on 'Create Resource'
        expect(page).to have_text strip_tags(
                                    I18n.t('app.resources.messages.copy_info_admin',
                                           contact_href: I18n.t('app.links.contact'))
                                  )
      end
    end
    context 'when copying resources from a foreign institution' do
      before :each do
        Fabricate :institution, name: 'Random Institution', code: 'rand'
        Fabricate :glri_resource, code: 'chez-rand', name: 'Cheese',
                  ip_access_url: 'https://www.example.com'
        visit copy_resource_path Resource.last
      end
      scenario 'the values copied into the name and ip URL fields are displayed' do
        click_on 'Create Resource'
        expect(page).to have_current_path resource_path Resource.last
        expect(page).to have_text 'Cheese'
        expect(page).to_not have_text 'https://www.example.com'
      end
    end
    context 'on a single resource page' do
      scenario 'the user can see certain tabs and fields if created for their
                resource' do
        visit resource_path Resource.last
        within '#resource-show-tabs' do
          expect(page).to have_text(
                            I18n.t('app.resources.tabs.authentication')
                          )
          expect(page).to have_text(
                            I18n.t('app.resources.tabs.notes')
                          )
        end
        expect(page).to have_text(
                          I18n.t('activerecord.attributes.resource.user_id')
                        )
        expect(page).to have_text(
                          I18n.t('activerecord.attributes.resource.password')
                        )
      end
      scenario "the user can't see some tabs if created by another resource" do
        visit resource_path Resource.first
        within '#resource-show-tabs' do
          expect(page).to_not have_text(I18n.t('app.resources.tabs.urls'))
          expect(page).to_not have_text(I18n.t('app.resources.tabs.notes'))
          expect(page).to_not have_text(I18n.t('app.resources.tabs.institutions'))
          expect(page).to_not have_text(I18n.t('app.resources.tabs.versions'))
          expect(page).to_not have_text(I18n.t('app.resources.tabs.legacy'))
          expect(page).to_not have_text(I18n.t('app.resources.tabs.statistics'))
        end
      end
      scenario "the user can't see some fields if created by another resource" do
        visit resource_path Resource.first
        within '#resource-show-tabs' do
          expect(page).to_not have_text(
                                I18n.t('activerecord.attributes.resource.user_id')
                              )
          expect(page).to_not have_text(
                                I18n.t('activerecord.attributes.resource.password')
                              )
          expect(page).to_not have_text(
                                I18n.t('activerecord.attributes.resource.institution_id')
                              )
          expect(page).to_not have_text(
                                I18n.t('activerecord.attributes.resource.parent_id')
                              )
          expect(page).to_not have_text(
                                I18n.t('activerecord.attributes.resource.child_resources')
                              )
        end
      end
    end
    context 'when creating resources' do
      before :each do
        visit new_resource_path
        fill_in I18n.t('activerecord.attributes.resource.name'), with: 'Cheese'
        fill_in I18n.t('activerecord.attributes.resource.ip_access_url'), with: 'https://www.example.com'
        click_on 'Create Resource'
      end
      scenario 'a newly generated code is displayed' do
        expect(page).to have_current_path resource_path Resource.last
        expect(page).to have_text Resource.last.code
      end
      scenario 'an EDS reminder message is displayed' do
        expect(page).to have_text strip_tags(
                                    I18n.t('app.resources.messages.copy_info_inst',
                                           contact_href: I18n.t('app.links.contact'))
                                  )
        expect(page).to have_link 'submit a ticket to GALILEO',
                                  href: I18n.t('app.links.contact')
      end
    end
  end
  context 'for a help desk user' do
    before :each do
      user = Fabricate :helpdesk_user
      sign_in user
      Fabricate.times 2, :resource
    end
    context 'Basic CRUD', js: true do
      it 'can not request an existing resource' do
        resource = Resource.last

        visit resource_path resource

        expect(page).to have_text 'You are not authorized to access this page.'
      end
    end
  end
end
