# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Sidebar management' do
  include Devise::Test::IntegrationHelpers
  context 'For an admin user' do
    before :each do
      user = Fabricate :admin_user
      sign_in user
    end
    #test
    context 'Sidebar Menu items' do
      context 'Institutions' do
        before :each do
          Fabricate :institution
        end
        it 'Highlights Institutions tab when visiting institutions page' do

          visit institutions_path

          expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.institutions'))
        end
        it 'Highlights Institutions tab when visiting institution\'s resources page' do

          visit institution_resources_path Institution.last

          expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.institutions'))
        end
        it 'Highlights Institutions tab when visiting institution\'s sites page' do

          visit institution_sites_path Institution.last

          expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.institutions'))
        end
        it 'Highlights Institutions tab when visiting institution\'s brandings page' do

          visit institution_brandings_path Institution.last

          expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.institutions'))
        end
        it 'Highlights Institutions tab when visiting institution\'s features page' do

          visit institution_features_path Institution.last

          expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.institutions'))
        end
      end
      it 'Highlights Resources tab when visiting resources page' do

        visit resources_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.resources'))
      end
      it 'Highlights Contacts tab when visiting contacts page' do

        visit contacts_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.contacts'))
      end
      it 'Highlights Users tab when visiting users page' do

        visit users_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.users'))
      end
      it 'Highlights Subjects tab when visiting subjects page' do

        visit subjects_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.subjects'))
      end
      it 'Highlights Formats tab when visiting formats page' do

        visit formats_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.formats'))
      end
      it 'Highlights Passwords tab when visiting passwords page' do

        visit passwords_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.passwords'))
      end
      it 'Highlights Vendors tab when visiting vendors page' do

        visit vendors_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.vendors'))
      end
      it 'Highlights Institution Groups tab when visiting institution_groups page' do

        visit institution_groups_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.institution_groups'))
      end
      it 'Highlights Notify Groups tab when visiting notify_groups page' do

        visit notify_groups_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.notify_groups'))
      end
      it 'Highlights Reports tab when visiting reports page' do

        visit reports_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.reports'))
      end
      it 'Highlights History tab when visiting versions page' do

        visit versions_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.versions'))
      end
    end
  end
  context 'For an institutional user' do
    let(:user) { Fabricate :institutional_user_with_institutions }
    before :each do
      sign_in user
    end
    context 'Sidebar Menu items' do
      context 'Institutions' do
        it 'Highlights Institutions tab when visiting institutions page' do

          visit institutions_path

          expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.institutions'))
        end
        it 'Highlights Institutions tab when visiting institution\'s resources page' do

          visit institution_resources_path Institution.last

          expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.institutions'))
        end
        it 'Highlights Institutions tab when visiting institution\'s brandings page' do

          visit institution_brandings_path Institution.last

          expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.institutions'))
        end
      end
      it 'Highlights Resources tab when visiting resources page' do

        visit resources_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.resources'))
      end
    end
  end
  context 'For an helpdesk user' do
    before :each do
      user = Fabricate :helpdesk_user
      sign_in user
      institution = Fabricate :institution
    end
    context 'Sidebar Menu items' do
      context 'Institutions' do
        it 'Highlights Institutions tab when visiting institutions page' do

          visit institutions_path

          expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.institutions'))
        end
        it 'Highlights Institutions tab when visiting institution\'s resources page' do

          visit institution_resources_path Institution.last

          expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.institutions'))
        end
      end
      it 'Highlights Contacts tab when visiting contacts page' do

        visit contacts_path

        expect(page).to have_css('#sidebar a.active', text: I18n.t('app.menu.contacts'))
      end
    end
  end
end
