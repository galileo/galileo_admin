# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PasswordService, type: :model do
  include LegacyDatesHelper
  let(:service) { PasswordService.new }

  describe '#first_future_date' do
    it 'returns the first future date' do
      expect(
        service.first_future_date(%w[0130 0605 0912], '0701')
      ).to eq '0912'
      expect(
        service.first_future_date(%w[0130 0605 0912], '0605')
      ).to eq '0912'
      expect(
        service.first_future_date(%w[0130 0605 0912], '1001')
      ).to be_nil
    end
  end

  describe '#new_password_effective_dates' do

    it 'returns the effective range from the Institution configured dates' do
      inst = Fabricate(:institution,
                       notify_dates: %w[0107 0521 0830],
                       change_dates: %w[0130 0605 0912])
      comp = '0521'
      dates = service.new_password_effective_dates(inst, comp)
      expect(dates[:on]).to eq '0605'
      expect(dates[:until]).to eq '0912'
    end

    it 'returns the effective range from the NotifyGroup configured dates' do
      inst = Fabricate(:institution,
                       notify_group:
                         Fabricate(:notify_group,
                                   notify_dates: %w[0107 0521 0830],
                                   change_dates: %w[0130 0605 0912]))

      comp = '0521'
      dates = service.new_password_effective_dates(inst, comp)
      expect(dates[:on]).to eq '0605'
      expect(dates[:until]).to eq '0912'
    end

    it 'returns the effective range from the Institution configured dates (dates not in order)' do
      inst = Fabricate(:institution,
                       notify_dates: %w[1210 0409 0701],
                       change_dates: %w[0108 0511 0817])
      comp = '1210'
      next_year = Time.new.year + 1
      dates = service.new_password_effective_dates(inst, comp, for_display: true)
      expect(dates[:on]).to eq "January 8, #{next_year}"
      expect(dates[:until]).to eq "May 11, #{next_year}"
    end

    it 'returns the effective range from the NotifyGroup configured dates (dates not in order)' do
      inst = Fabricate(:institution,
                       notify_group:
                         Fabricate(:notify_group,
                                   notify_dates: %w[1210 0409 0701],
                                   change_dates: %w[0108 0511 0817]))

      comp = '1210'
      next_year = Time.new.year + 1
      dates = service.new_password_effective_dates(inst, comp, for_display: true)
      expect(dates[:on]).to eq "January 8, #{next_year}"
      expect(dates[:until]).to eq "May 11, #{next_year}"
    end

    it 'returns the effective range from Institution using special date not in
        notify dates' do
      inst = Fabricate(:institution,
                       notify_group:
                         Fabricate(:notify_group,
                                   notify_dates: %w[0107 0521 0830],
                                   change_dates: %w[0130 0605 0912]))

      comp = '1008'
      dates = service.new_password_effective_dates(inst, comp, asap: true)
      expect(dates[:on]).to eq comp
      expect(dates[:until]).to eq '0130'
    end

    it 'returns the effective range from NotifyGroup using special date not in
        notify dates' do
      inst = Fabricate(:institution,
                       notify_dates: %w[0107 0521 0830],
                       change_dates: %w[0130 0605 0912])

      comp = '1008'
      dates = service.new_password_effective_dates(inst, comp, asap: true)
      expect(dates[:on]).to eq comp
      expect(dates[:until]).to eq '0130'
    end

  end

  describe '#change_asap' do
    before(:each) do
      Fabricate.times 5, :password
    end
    it 'sets a new_password on the institution if not set' do
      inst = Fabricate :institution, new_password: ''
      service.change_asap inst
      expect(inst.new_password).not_to be_blank
    end

    it 'uses the existing new_password on the institution if not equal to current password' do
      new_password = Password.first
      inst = Fabricate :institution, new_password: new_password.password
      service.change_asap inst
      expect(inst.new_password).to eq new_password.password
    end

    it 'sets a new_password on the institution if its equal to the current password' do
      inst = Fabricate :institution, current_password: 'password', new_password: 'password'
      service.change_asap inst
      expect(inst.new_password).to_not eql 'password'
    end
  end

  # TODO (done?) filter bad dates (on institution/notify_group save), e.g., 0229 0431 0631 0931 1131 2222, etc.
  # NOTE these tests assume exclusive matches, i.e.,
  #      that if the given date matches exactly, we return the next date

  # given any date (including year), return next change date (including year)
  describe '#next_date' do

    it 'single-date change dates' do
      expect(PasswordService.next_date(%w[0101], '20210101')).to eq '20220101'
      expect(PasswordService.next_date(%w[0101], '20210102')).to eq '20220101'
      expect(PasswordService.next_date(%w[0101], '20211231')).to eq '20220101'

      expect(PasswordService.next_date(%w[1231], '20211231')).to eq '20221231'
      expect(PasswordService.next_date(%w[1231], '20211230')).to eq '20211231'
      expect(PasswordService.next_date(%w[1231], '20210101')).to eq '20211231'

      expect(PasswordService.next_date(%w[0615], '20210615')).to eq '20220615'
      expect(PasswordService.next_date(%w[0615], '20210614')).to eq '20210615'
      expect(PasswordService.next_date(%w[0615], '20210616')).to eq '20220615'
    end

    it 'multi-date change dates, same day matches' do
      expect(PasswordService.next_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20210101'
        )).to eq '20210201'
      expect(PasswordService.next_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20210601'
        )).to eq '20210701'
      expect(PasswordService.next_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20211201'
        )).to eq '20220101'
    end

    it 'multi-date change dates, next day cases' do
      expect(PasswordService.next_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20210102'
        )).to eq '20210201'
      expect(PasswordService.next_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20210602'
        )).to eq '20210701'
      expect(PasswordService.next_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20211202'
        )).to eq '20220101'
    end

    it 'other multi-date change dates' do
      expect(PasswordService.next_date(%w[0101 0401 0701 1001], '20210101')).to eq '20210401'
      expect(PasswordService.next_date(%w[0101 0401 0701 1001], '20210701')).to eq '20211001'
      expect(PasswordService.next_date(%w[0101 0401 0701 1001], '20210102')).to eq '20210401'
      expect(PasswordService.next_date(%w[0101 0401 0701 1001], '20211002')).to eq '20220101'

      expect(PasswordService.next_date(%w[0331 0630 0930 1231], '20210930')).to eq '20211231'
      expect(PasswordService.next_date(%w[0331 0630 0930 1231], '20211231')).to eq '20220331'
      expect(PasswordService.next_date(%w[0331 0630 0930 1231], '20210401')).to eq '20210630'
      expect(PasswordService.next_date(%w[0331 0630 0930 1231], '20220101')).to eq '20220331'

      # maybe unexpected but valid (same result as previous test)
      expect(PasswordService.next_date(%w[0930 1231 0331 0630], '20220101')).to eq '20220331'
    end
  end

  # given any date (including year), return 'effective until' date (including year)
  # this should be the same as: next_date( next_date(any_date) )
  # this is sometimes called the "next next change date"
  describe '#effective_until_date' do

    it 'single-date change dates' do
      expect(PasswordService.effective_until_date(%w[0101], '20210101')).to eq '20230101'  # next: 20220101, next-next: 20230101
      expect(PasswordService.effective_until_date(%w[0101], '20210102')).to eq '20230101'  # next: 20220101, next-next: 20230101
      expect(PasswordService.effective_until_date(%w[0101], '20211231')).to eq '20230101'  # next: 20220101, next-next: 20230101

      expect(PasswordService.effective_until_date(%w[1231], '20211231')).to eq '20231231'  # next: 20221231, next-next: 20231231
      expect(PasswordService.effective_until_date(%w[1231], '20211230')).to eq '20221231'  # next: 20211231, next-next: 20221231
      expect(PasswordService.effective_until_date(%w[1231], '20220101')).to eq '20231231'  # next: 20221231, next-next: 20231231

      expect(PasswordService.effective_until_date(%w[0615], '20210615')).to eq '20230615'  # next: 20220615, next-next: 20230615
      expect(PasswordService.effective_until_date(%w[0615], '20210614')).to eq '20220615'  # next: 20210615, next-next: 20220615
      expect(PasswordService.effective_until_date(%w[0615], '20210616')).to eq '20230615'  # next: 20220615, next-next: 20230615
    end

    it 'multi-date change dates, same day matches' do
      expect(PasswordService.effective_until_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20210101'
        )).to eq '20210301'  # next: 20210201, next-next: 20210301
      expect(PasswordService.effective_until_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20210601'
        )).to eq '20210801'  # next: 20210701, next-next: 20210801
      expect(PasswordService.effective_until_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20211201'
        )).to eq '20220201'  # next: 20220101, next-next: 20220201
    end

    it 'multi-date change dates, next day cases' do
      expect(PasswordService.effective_until_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20210102'
        )).to eq '20210301'  # next: 20210201, next-next: 20210301
      expect(PasswordService.effective_until_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20210602'
        )).to eq '20210801'  # next: 20210701, next-next: 20210801
      expect(PasswordService.effective_until_date(
        %w[0101 0201 0301 0401 0501 0601 0701 0801 0901 1001 1101 1201], '20211202'
        )).to eq '20220201'  # next: 20220101, next-next: 20220201
    end

    it 'other multi-date change dates' do
      expect(PasswordService.effective_until_date(%w[0101 0401 0701 1001], '20210101')).to eq '20210701'  # next: 20210401, next-next: 20210701
      expect(PasswordService.effective_until_date(%w[0101 0401 0701 1001], '20210701')).to eq '20220101'  # next: 20211001, next-next: 20220101
      expect(PasswordService.effective_until_date(%w[0101 0401 0701 1001], '20210102')).to eq '20210701'  # next: 20210401, next-next: 20210701
      expect(PasswordService.effective_until_date(%w[0101 0401 0701 1001], '20211002')).to eq '20220401'  # next: 20220101, next-next: 20220401

      expect(PasswordService.effective_until_date(%w[0331 0630 0930 1231], '20210930')).to eq '20220331'  # next: 20211231, next-next: 20220331
      expect(PasswordService.effective_until_date(%w[0331 0630 0930 1231], '20211231')).to eq '20220630'  # next: 20220331, next-next: 20220630
      expect(PasswordService.effective_until_date(%w[0331 0630 0930 1231], '20210401')).to eq '20210930'  # next: 20210630, next-next: 20210930
      expect(PasswordService.effective_until_date(%w[0331 0630 0930 1231], '20220101')).to eq '20220630'  # next: 20220331, next-next: 20220630

      # maybe unexpected but valid (same result as previous test)
      expect(PasswordService.effective_until_date(%w[0930 1231 0331 0630], '20220101')).to eq '20220630'
    end
  end

  describe '#dateline_dates' do
    it 'returns number line of dates' do
      expect(PasswordService.dateline_dates(%w[0101 0701], '2021')).to eq %w[20210101 20210701 20220101 20220701 20230101 20230701]
      expect(PasswordService.dateline_dates(%w[0101], '2021')).to eq %w[20210101 20220101 20230101]
    end
  end

  describe '#now_yyyymmdd' do
    it 'returns current time' do
        expect(PasswordService.now_yyyymmdd).to eq DateTime.now.strftime('%Y%m%d')
    end
  end
end
