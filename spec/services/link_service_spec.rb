# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LinkService, type: :model do
  subject(:service) { LinkService.new allocation }

  context 'URLs from the Resource' do
    let(:allocation) { Fabricate :allocation }
    it 'returns links from the Resource' do

      expect(service.ip.link). to eq allocation.resource.ip_access_url
      expect(service.remote.link). to eq allocation.resource.remote_access_url
    end
    it 'encodes and generates an OA redirector link' do
      expect(service.open_athens.link).to eq(
        'https://go.openathens.net/redirector/institution.edu?url=https%3A%2F%2Fopenathens.example.org'
      )
    end
  end

  context 'URLs from the Allocation' do
    let(:allocation) { Fabricate :allocation_with_overrides }
    it 'returns links from the Allocation' do
      expect(service.ip.link). to eq allocation.ip_access_url
      expect(service.remote.link). to eq allocation.remote_access_url
    end
    it 'returns links from the Resource if an Allocation value is removed' do
      allocation.ip_access_url = ''
      allocation.save
      expect(service.ip.link). to eq allocation.resource.ip_access_url
      expect(service.remote.link). to eq allocation.remote_access_url
    end
  end

  context 'URLs from the Allocation with proxy_remote' do
    let(:allocation) { Fabricate :allocation_with_proxy_remote }
    it 'returns proxied remote link from the Resource' do
      ezproxy_uri = Addressable::URI.heuristic_parse allocation.institution.galileo_ez_proxy_url
      ezproxy_uri.scheme = 'https' if ezproxy_uri.scheme.nil? || ezproxy_uri.scheme == 'http'
      ezproxy_uri.path = 'login'
      remote_url   = allocation.resource.remote_access_url
      expect(service.remote.link). to eq "#{ezproxy_uri}?url=#{remote_url}"
    end
  end

  context 'URLs from the Allocation with proxy_remote and override' do
    let(:allocation) { Fabricate :allocation_with_proxy_remote_and_override }
    it 'returns NON-proxied remote link from the Allocation' do
      expect(service.remote.link). to eq allocation.remote_access_url
    end
  end
end
