# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ResourceCodeService, type: :model do

  # helpers
  def add_resource(name, inst = nil)
    resource = Resource.new(name: name)
    if inst
        inst_id = inst.id
        resource.institution_id = inst_id
    end
    code = ResourceCodeService.new_code_for(resource)
    resource.code = code
    resource.save
    resource  # returned
  end

  describe '#exclude_list' do
    it 'contains 4-character codes from existing records' do

      resource = add_resource('Zoological Reports, Volume 1')
      expect(resource.code).to eq 'zool'

      resource = add_resource('Zoological Reports, Volume 2')
      expect(resource.code).to eq 'zooo'

      resource = add_resource('Zoological Reports, Volume 3')
      expect(resource.code).to eq 'zoog'

      inst = Fabricate(:institution, code: 'abr1', name: 'Abraham Baldwin')
      resource = add_resource('Zoological Reports, Volume 4', inst)
      expect(resource.code).to eq 'zooi-abr1'

      resource = add_resource("PI's Symbols")
      expect(resource.code).to eq 'pisy'

      exclude = ResourceCodeService.exclude_list
      expect(exclude).to include('pisy', 'zool', 'zooo', 'zoog', 'zooi')  # i.e., zooi without -abr1

    end
  end

  describe '#generate_new_code' do
    it 'returns a unique 4-character code' do
      seed = ResourceCodeService.generate_seed('Zoological Reports')
      expect(
        ResourceCodeService.generate_new_code(seed)
      ).to eq 'zool'
    end
  end

  describe '#new_code_for' do
    it 'returns a new code for resource' do
      resource = Resource.new
      resource.name = 'Zoological Reports'
      expect(
        ResourceCodeService.new_code_for(resource)
      ).to eq 'zool'
    end
  end

  describe '#profanity_list' do
    it 'contains a profane word' do
      profanity = ResourceCodeService.profanity_list
      expect(profanity).to include('piss', 'nazi')
    end
  end

end
