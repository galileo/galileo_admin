# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SearchService, type: :model do
  subject(:service) { SearchService }

  context 'for Allocations' do
    before(:each) do
      Sunspot.remove_all Allocation
      @allocation = Fabricate :allocation
      Fabricate :allocation, resource: @allocation.resource
      Fabricate :allocation, institution: @allocation.institution
      Sunspot.index! Allocation.all
    end

    describe '#allocations_by_bl_id' do
      it 'returns an Allocation using an id' do
        results = service.allocation_find @allocation.blacklight_id
        expect(results.length).to eq 1
        expect(results.first).to eq @allocation
      end
    end

    describe '#allocations_for_resource' do
      it 'returns an Allocation using a Resource' do
        results = service.allocations_for_resource @allocation.resource
        expect(results.length).to eq 2
        expect(results).to include @allocation
      end
    end

    describe '#allocations_for_institutions' do
      it 'returns an Allocation using an Institution' do
        results = service.allocations_for_institution @allocation.institution
        expect(results.length).to eq 2
        expect(results).to include @allocation
      end
    end
  end

  context 'for Institutions' do
    let(:institution) { Fabricate :institution, open_athens: false }
    before(:each) do
      Sunspot.remove_all Institution
      Sunspot.index! institution
    end

    describe '#institution_find' do
      it 'returns an Institution by code' do
        results = service.institution_find(institution.code)
        expect(results.length).to eq 1
        expect(results.first).to eq institution
      end
    end

    describe '#institutions_by' do
      let!(:oa_institution) { Fabricate :open_athens_institution }
      before(:each) { Sunspot.commit }
      it 'returns Institutions based on a param' do
        results = service.institutions_by(open_athens: true)
        expect(results.length).to eq 1
        expect(results.first).to eq oa_institution
      end
    end
  end

  context 'retuning Solr hits' do
    context '#inst_hit_for' do
      let(:institution) { Fabricate :institution }
      before(:each) do
        Sunspot.remove_all Institution
        Sunspot.index! institution
      end
      it 'returns a hit' do
        result = service.inst_hit_for(institution.code)
        expect(result).to be_a Sunspot::Search::Hit
      end
    end
    context '#alloc_hit_for' do
      before(:each) do
        Sunspot.remove_all Allocation
        @allocation = Fabricate :allocation
        Fabricate :allocation, resource: @allocation.resource
        Fabricate :allocation, institution: @allocation.institution
        Sunspot.index! Allocation.all
      end
      it 'returns a hit' do
        result = service.alloc_hit_for(@allocation.blacklight_id)
        expect(result).to be_a Sunspot::Search::Hit
      end
    end
  end
end
