# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IpService, type: :model do

  describe '#key_from' do
    it 'returns a formatted cache key from an integer address' do
      test = 4
      expect(IpService.key_from(test)).to eq 'ip_4'
    end

    it 'returns a formatted cache key from a string address' do
      test = '0.0.0.4'
      expect(IpService.key_from(test)).to eq 'ip_4'
    end
  end

  describe '#search' do
    before do
      IpService.add_ip '0.0.0.1', 'tes1'
      IpService.add_ip '0.0.0.2', 'tes2'
      IpService.add_ip '0.0.0.3', 'tes3'
    end
    it 'returns the correct institution code when found' do
      expect(IpService.search('0.0.0.2')).to eq 'tes2'
    end
    it 'returns the nil when key not found' do
      expect(IpService.search('0.0.0.4')).to be_nil
    end
  end

  describe '#search_range' do
    before do
      IpService.add_ip '0.0.0.1', 'tes1'
      IpService.add_ip '0.0.0.2', 'tes2'
      IpService.add_ip '0.0.0.3', 'tes3'
    end
    it 'returns the correct institution code when a single ip address is given' do
      expect(IpService.search_range('0.0.0.2')).to contain_exactly(['0.0.0.2', 'tes2'])
    end
    it 'returns nil when a single ip address is given that doesn\'t match' do
      expect(IpService.search_range('0.0.0.4')).to contain_exactly(['0.0.0.4', nil])
    end
    it 'returns the correct institution codes when a range is given' do
      expect(IpService.search_range('0.0.0.1-3')).to contain_exactly(['0.0.0.1', 'tes1'], ['0.0.0.2', 'tes2'], ['0.0.0.3', 'tes3'])
    end
    it 'returns the correct institution codes and nils when a kleene range is given' do
      expect(IpService.search_range('0.0.0.*')).to include(['0.0.0.1', 'tes1'], ['0.0.0.2', 'tes2'], ['0.0.0.4', nil])
    end
    it 'returns the empty array when nothing is passed' do
      expect(IpService.search_range('')).to eql []
    end
  end

  describe '#clear_cache' do
    before do
      IpService.add_ip '0.0.0.1', 'tes1'
      IpService.add_ip '0.0.0.2', 'tes2'
      IpService.add_ip '0.0.0.3', 'tes3'
    end
    it 'clears all entries from cache' do
      IpService.clear_cache
      expect(IpService.search('0.0.0.1')).to be_nil
      expect(IpService.search('0.0.0.2')).to be_nil
      expect(IpService.search('0.0.0.3')).to be_nil
    end
  end

  describe '#reset_cache' do
    before do
      IpService.add_ip '0.0.0.1', 'tes1'
      Fabricate(:institution, id: 5,  code: 'tes1', ip_addresses: %w[5.0.0.0], active: true)
      Fabricate(:institution, id: 15, code: 'tes2', ip_addresses: %w[15.0.0.0], active: true)
    end
    it 'clears previous ip entries from cache' do
      IpService.reset_cache
      expect(IpService.search('0.0.0.1')).to be_nil
    end
  end

  describe '#add_ip_ranges' do
    it 'adds the correct ips to the cache given the ip range notation and inst id' do
      IpService.add_ip_ranges(%w[1.2.3.4 1.0.1-2.*], 'tes1')
      expect(IpService.search('1.2.3.4')).to eq 'tes1'
      expect(IpService.search('1.0.1.0')).to eq 'tes1'
      expect(IpService.search('1.0.2.255')).to eq 'tes1'
      expect(IpService.search('1.0.3.0')).to be_nil
    end
  end

  describe '#add_ip' do
    it 'adds the correct ip to the cache given the ip int notation and inst id' do
      IpService.add_ip(1, 'tes1')
      expect(IpService.search('0.0.0.1')).to eq 'tes1'
      expect(IpService.search('1.0.3.0')).to be_nil
    end
  end
  describe '#delete_ip_ranges' do
    before do
      IpService.add_ip_ranges(%w[1.2.3.4 1.0.1-2.*], 'tes1')
    end
    it 'removes the correct ips from the cache' do
      IpService.delete_ip_ranges(%w[1.0.1.*])
      expect(IpService.search('1.0.1.0')).to be_nil
      expect(IpService.search('1.0.1.255')).to be_nil
      expect(IpService.search('1.0.2.0')).to eq 'tes1'
      expect(IpService.search('1.0.2.255')).to eq 'tes1'
    end
  end

  describe '#ips_in_range' do
    it 'returns the correct integer ip for single notation' do
      test = '0.0.0.1'
      expect(IpService.ips_in_range(test).first).to eq 1
    end
    it 'returns the correct integer ip for star notation' do
      test = '0.0.0.*'
      expect(IpService.ips_in_range(test)).to eq Array(0..255)
    end

    it 'returns the correct integer ip for hyphen notation' do
      test = '0.0.0.5-10'
      expect(IpService.ips_in_range(test)).to eq Array(5..10)
    end
  end

  describe '#string_representation' do
    it 'returns ip notation of integer' do
      expect(IpService.string_representation(1)).to eq '0.0.0.1'
    end
  end
  describe do
  end

  describe '#convert' do
    it 'returns the same ip integers for single notation' do
      expect(IpService.convert('0.0.0.1')).to eq %w[0.0.0.1 0.0.0.1]
    end
    it 'returns the correct ips for ranges in 4th octet' do
      expect(IpService.convert('0.0.0.0-64')).to match_array %w[0.0.0.0
                                                                0.0.0.64]
    end
    it 'returns the correct ips for ranges followed by asterisk' do
      expect(IpService.convert('0.0.0-64.*')).to match_array %w[0.0.0.0
                                                                0.0.64.255]
    end
    it 'returns the correct ips for asterisk' do
      expect(IpService.convert('0.0.0.*')).to match_array %w[0.0.0.0
                                                             0.0.0.255]
    end
    it 'returns the correct ips for double asterisks' do
      expect(IpService.convert('0.0.*.*')).to match_array %w[0.0.0.0
                                                             0.0.255.255]
    end
    it 'raises error if asterisk is in first place' do
      expect do
        IpService.convert('*.0.0.0')
      end.to raise_error(/'\*' only allowed in last 2 places/)
    end
    it 'raises error if asterisk is in second place' do
      expect do
        IpService.convert('0.*.0.0')
      end.to raise_error(/'\*' only allowed in last 2 places/)
    end
    it 'raises error if range is in first place' do
      expect do
        IpService.convert('0-25.0.0.0')
      end.to raise_error(/'-' only allowed in last 2 places/)
    end
    it 'raises error if range is in second place' do
      expect do
        IpService.convert('0.0-25.0.0')
      end.to raise_error(/'-' only allowed in last 2 places/)
    end
    it 'raises error if range has too many numbers' do
      expect do
        IpService.convert('0.0.0-25-75.*')
      end.to raise_error(/Not a valid range:/)
    end
    it "raises error if the range's first number is invalid" do
      expect do
        IpService.convert('0.0.Wrong-25.*')
      end.to raise_error(/Not a valid number:/)
    end
    it "raises error if the range's second number is invalid" do
      expect do
        IpService.convert('0.0.25-700.*')
      end.to raise_error(/Not a valid number:/)
    end
    it "raises error if the range's first number is more than second" do
      expect do
        IpService.convert('0.0.25-20.*')
      end.to raise_error(/Not a valid range:/)
    end
    it 'raises error if invalid octet' do
      expect do
        IpService.convert('N.O.P.E')
      end.to raise_error(/Can't convert into octet:/)
    end
    it 'raises error if octet number not in valid ip range' do
      expect do
        IpService.convert('256.1.1.1')
      end.to raise_error(/Can't convert into octet:/)
    end
  end

  describe '#valid_octet?' do
    it 'returns true when argument is between 0 and 255' do
      expect(IpService.valid_octet?(0)).to eq true
      expect(IpService.valid_octet?(128)).to eq true
      expect(IpService.valid_octet?(255)).to eq true
    end
    it 'returns false when argument is not between 0 and 255' do
      expect(IpService.valid_octet?(-1)).to eq false
      expect(IpService.valid_octet?(256)).to eq false
      expect(IpService.valid_octet?('11Wrong!11')).to eq false
    end
  end

  describe '#valid_ip_range?' do
    it 'returns true when range is valid' do
      expect(IpService.valid_ip_range?('1.0.0.0')).to eq true
      expect(IpService.valid_ip_range?('1.0.5.*')).to eq true
      expect(IpService.valid_ip_range?('255.0.0-128.*')).to eq true
    end
    it 'returns false when range is invalid' do
      expect(IpService.valid_ip_range?('wrong')).to eq false
      expect(IpService.valid_ip_range?('0.0.0.255')).to eq false
      expect(IpService.valid_ip_range?('1.*.255.255')).to eq false
      expect(IpService.valid_ip_range?('125-129.0.0.0')).to eq false
      expect(IpService.valid_ip_range?('255.0-128.0.*')).to eq false
      expect(IpService.valid_ip_range?('*.255.255.1')).to eq false
    end
    it 'returns true when range is private when allow_private is set true' do
      expect(IpService.valid_ip_range?('10.255.255.255', allow_private: true)).to eq true
      expect(IpService.valid_ip_range?('172.16.0.*', allow_private: true)).to eq true
      expect(IpService.valid_ip_range?('*.255.255.1', allow_private: true)).to eq false
      expect(IpService.valid_ip_range?('wrong', allow_private: true)).to eq false
    end
  end

  describe '#ranges_intersect?' do
    it "returns false when the ranges don't intersect" do
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.0'), IpService.range_bookends('1.0.0.1'))).to eq false
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.1.*'), IpService.range_bookends('1.0.0.*'))).to eq false
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.1.*'), IpService.range_bookends('1.0.0.*'))).to eq false
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.1.0-5'), IpService.range_bookends('1.0.1.6-10'))).to eq false
    end
    it 'returns true when ranges intersect' do
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.0'), IpService.range_bookends('1.0.0.0'))).to eq true
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.*'), IpService.range_bookends('1.0.0.50'))).to eq true
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.50'), IpService.range_bookends('1.0.0.*'))).to eq true
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.10-50'), IpService.range_bookends('1.0.0.*'))).to eq true
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.10-50'), IpService.range_bookends('1.0.0.25'))).to eq true
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.10-50'), IpService.range_bookends('1.0.0.10'))).to eq true
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.10-50'), IpService.range_bookends('1.0.0.50'))).to eq true

      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.*'), IpService.range_bookends('1.0.0.0-255'))).to eq true
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.10-50'), IpService.range_bookends('1.0.0.10-50'))).to eq true
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.10-50'), IpService.range_bookends('1.0.0.25-50'))).to eq true
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.25-50'), IpService.range_bookends('1.0.0.10-50'))).to eq true
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.50-100'), IpService.range_bookends('1.0.0.10-50'))).to eq true
      expect(IpService.ranges_intersect?(IpService.range_bookends('1.0.0.50-100'), IpService.range_bookends('1.0.0.100-150'))).to eq true
    end
  end

  describe '#valid_ip_int?' do
    it 'returns true when argument is valid ip' do
      expect(IpService.valid_ip_int?(IPAddr.new('1.0.0.0').to_i)).to eq true
      expect(IpService.valid_ip_int?(IPAddr.new('9.255.255.255').to_i)).to eq true
      expect(IpService.valid_ip_int?(IPAddr.new('11.0.0.0').to_i)).to eq true
      expect(IpService.valid_ip_int?(IPAddr.new('126.255.255.255').to_i)).to eq true
      expect(IpService.valid_ip_int?(IPAddr.new('128.0.0.0').to_i)).to eq true
      expect(IpService.valid_ip_int?(IPAddr.new('169.253.255.255').to_i)).to eq true
      expect(IpService.valid_ip_int?(IPAddr.new('169.255.0.0').to_i)).to eq true
      expect(IpService.valid_ip_int?(IPAddr.new('172.15.255.255').to_i)).to eq true
      expect(IpService.valid_ip_int?(IPAddr.new('172.32.0.0').to_i)).to eq true
      expect(IpService.valid_ip_int?(IPAddr.new('192.167.255.255').to_i)).to eq true
      expect(IpService.valid_ip_int?(IPAddr.new('192.169.0.0').to_i)).to eq true
      expect(IpService.valid_ip_int?(IPAddr.new('255.255.255.255').to_i)).to eq true
    end
    it 'returns false when argument is invalid ip' do
      expect(IpService.valid_ip_int?(-1)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('0.0.0.0').to_i)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('0.255.255.255').to_i)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('10.0.0.0').to_i)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('10.255.255.255').to_i)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('127.0.0.0').to_i)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('127.255.255.255').to_i)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('169.254.0.0').to_i)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('169.254.255.255').to_i)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('172.16.0.0').to_i)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('172.31.255.255').to_i)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('192.168.0.0').to_i)).to eq false
      expect(IpService.valid_ip_int?(IPAddr.new('192.168.255.255').to_i)).to eq false
      expect(IpService.valid_ip_int?(4_294_967_296)).to eq false # 255.255.255.256
    end

    it 'returns true when given a private IP range if allow_private ser to true' do
      expect(IpService.valid_ip_int?(
               IPAddr.new('10.0.0.0').to_i,
               allow_private: true
             )).to eq true
      expect(IpService.valid_ip_int?(
               IPAddr.new('172.16.0.0').to_i,
               allow_private: true
             )).to eq true
      expect(IpService.valid_ip_int?(
               IPAddr.new('192.168.0.0').to_i,
               allow_private: true
             )).to eq true
      expect(IpService.valid_ip_int?(
               IPAddr.new('0.0.0.0').to_i,
               allow_private: true
             )).to eq false
      expect(IpService.valid_ip_int?(
               IPAddr.new('169.254.0.0').to_i,
               allow_private: true
             )).to eq false
      expect(IpService.valid_ip_int?(
               4_294_967_296,
               allow_private: true
             )).to eq false
    end
  end
end
