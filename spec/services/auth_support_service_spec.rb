# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AuthSupportService, type: :model do
  subject(:service) { AuthSupportService }

  # enable cache
  let(:memory_store) { ActiveSupport::Cache.lookup_store(:memory_store) }
  let(:cache) { Rails.cache }

  before(:each) do
    allow(Rails).to receive(:cache).and_return(memory_store)
    Rails.cache.clear
  end

  def params(hash)
    ActionController::Parameters.new(hash)
  end

  describe '#institution_lookup' do
    context 'Passphrase lookup' do
      it 'returns a successful response if a corresponding Institution exists' do
        institution = Fabricate :institution
        p = params passphrase: institution.current_password
        result = service.institution_lookup p
        expect(result[:inst][:code]).to eq institution.code
      end
      it 'returns a message if a corresponding Institution does not exist' do
        p = params passphrase: 'blah'
        result = service.institution_lookup p
        expect(result[:message]).to eq 'Institution not found'
      end
    end
    context 'IP lookup' do
      it 'returns a successful response if a corresponding Institution
          exists' do
        institution = Fabricate :institution, ip_addresses: ['123.45.67.8']
        p = params ip: institution.ip_number.first
        result = service.institution_lookup p
        expect(result[:inst][:code]).to eq institution.code
      end
      it 'returns a message if a corresponding Institution does not exist' do
        p = params ip: '123.12.12.1'
        result = service.institution_lookup(p)
        expect(result[:message]).to eq 'No inst for 123.12.12.1'
      end
    end
    context 'OpenAthens ID lookup' do
      context 'using SubOrg ID' do
        let :institution do
          Fabricate :institution,
                    ip_addresses: ['123.12.12.1'],
                    open_athens_subscope: '1234567.test.edu'
        end
        it 'returns a successful response if a corresponding Institution with
            a SubOrg ID values exists and the IP is also corresponding' do
          p = params oa_id: institution.open_athens_subscope,
                     ip: '123.12.12.1'
          result = service.institution_lookup p
          expect(result[:inst][:code]).to eq institution.code
          expect(result[:context][:remote]).to be_falsey
        end
        it 'returns a successful response if a corresponding Institution with
            a SubOrg ID values exists and the IP not corresponding' do
          p = params oa_id: institution.open_athens_subscope,
                     ip: '123.1.2.3'
          result = service.institution_lookup p
          expect(result[:inst][:code]).to eq institution.code
          expect(result[:context][:remote]).to be_truthy
        end
        it 'returns an unsuccessful response if a corresponding Institution with
            a Sub-Scope values does not exist' do
          p = params oa_id: 'bad.org.id',
                     ip: '123.12.12.1'
          result = service.institution_lookup p
          expect(result[:message]).to eq 'Institution not found'
        end
      end
      context 'using OrgID' do
        let :institution do
          Fabricate :institution,
                    ip_addresses: ['123.45.67.8'],
                    open_athens_scope: 'test.edu'
        end
        it 'returns a successful response if a corresponding Institution with
            a Scope value exists and the IP is also corresponding' do
          p = params oa_id: institution.open_athens_scope,
                     ip: '123.45.67.8'
          result = service.institution_lookup p
          expect(result[:inst][:code]).to eq institution.code
          expect(result[:context][:remote]).to be_falsey
        end
        it 'returns an unsuccessful response if a corresponding Institution with
            a Scope value does not exist' do
          p = params oa_id: 'bad.org',
                     ip: '123.12.12.1'
          result = service.institution_lookup p
          expect(result[:message]).to eq 'Institution not found'
        end
      end
    end
    context 'Geo lookup' do
      it 'returns Institution information if a given Zip Code is associated with
          an existing Institution' do
        institution = Fabricate :institution, zip_codes: %w[30606 30607 30608]
        p = params zip: '30607'
        result = service.institution_lookup p
        expect(result[:inst][:code]).to eq institution.code
        expect(result[:context][:remote]).to be_truthy
      end
      it 'returns a successful request with not found message if Zip Code is not
          found associated with any Institution' do
        p = params zip: '12345'
        result = service.institution_lookup p
        expect(result[:message]).to eq 'Institution not found'
      end
    end
    context 'PINES lookup' do
      it 'returns Institution information based on a PINES system code' do
        institution = Fabricate :institution, pines_codes: ['pines']
        p = params pines: 'pines'
        result = service.institution_lookup p
        expect(result[:inst][:code]).to eq institution.code
        expect(result[:context][:remote]).to be_truthy
      end
      it 'returns Institution information based on a PINES library code' do
        institution = Fabricate :institution, pines_codes: ['pines']
        p = params pines: 'pines-athens'
        result = service.institution_lookup p
        expect(result[:inst][:code]).to eq institution.code
        expect(result[:context][:remote]).to be_truthy
      end
      it 'returns a successful request with not found message if the PINES
          authentication service responds unsuccessfully' do
        p = params pines: 'pines'
        result = service.institution_lookup p
        expect(result[:message]).to eq 'Institution not found'
      end
    end
  end
end
