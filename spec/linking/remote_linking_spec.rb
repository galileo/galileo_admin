# frozen_string_literal: true

require 'rails_helper'

describe 'Remote access link generation via LinkService', type: :model do
  include LinkServiceHelpers

  subject { LinkService }

  context 'the URL source' do
    context 'is the resource' do
      let(:allocation) { Fabricate :allocation }
      it 'uses the proper URL from the Resource' do
        expect(subject.new(allocation).remote.link).to eq(
          'https://remote.example.org'
        )
      end
    end
    context 'is the allocation' do
      let(:allocation) { Fabricate :allocation_with_overrides }
      it 'uses the proper URL from the Allocation' do
        expect(subject.new(allocation).remote.link).to eq(
          'https://remote.override.org'
        )
      end
    end
    context 'if no remote_access_url is set' do
      let(:allocation) do
        allocation_for(
          resource: Fabricate(:resource, remote_access_url: nil)
        )
      end
      it 'uses the ip_access_url' do
        expect(subject.new(allocation).remote.link).to eq(
          allocation.resource.ip_access_url
        )
      end
    end
  end

  context 'ezproxy configurations' do
    context 'with proxy_remote FALSE' do
      let(:allocation) do
        allocation_for resource: Fabricate(:resource, proxy_remote: false)
      end
      it 'returns the bare remote_access_url' do
        expect(subject.new(allocation).remote.link).to eq(
          'https://remote.example.org'
        )
      end
    end
    context 'with proxy_remote TRUE' do
      context 'and use_local_ez_proxy TRUE' do
        let(:allocation) do
          allocation_for(
            institution: Fabricate(:institution, use_local_ez_proxy: true),
            resource: Fabricate(:resource, proxy_remote: true)
          )
        end
        it 'proxies the URL with the local ezproxy URL' do
          expect(subject.new(allocation).remote.link).to eq(
            'https://proxy.inst.edu/login?url=https://remote.example.org'
          )
        end
      end
      context 'and use_local_ez_proxy FALSE' do
        let(:allocation) do
          allocation_for(
            institution: Fabricate(:institution, use_local_ez_proxy: false),
            resource: Fabricate(:resource, proxy_remote: true)
          )
        end
        it 'proxies the URL with the galileo ezproxy URL' do
          expect(subject.new(allocation).remote.link).to eq(
            'https://proxy.galileo.edu/login?url=https://remote.example.org'
          )
        end
      end
    end
  end

  # oa_proxy is used only when inst and res oa_proxy is true
  # in those cases, oa_proxy overrides ezproxy (proxy_remote)
  #
  # 8 cases for oa_proxy and proxy_remote (ezproxy)
  # case | result      | inst:oaproxy | res:oaproxy | res:proxy_remote
  # -----|-------------|--------------|-------------|----------------
  # 000  | no proxying | false        | false       | false
  # 001  | use ezproxy | false        | false       | true
  # 010  | no proxying | false        | true        | false
  # 011  | use ezproxy | false        | true        | true
  # 100  | no proxying | true         | false       | false
  # 101  | use ezproxy | true         | false       | true
  # 110  | use oaproxy | true         | true        | false
  # 111  | use oaproxy | true         | true        | true
  context 'oa_proxy vs. ezproxy configurations' do

    # case 000 no proxying
    context 'with inst:oa_proxy FALSE, res:oa_proxy FALSE, res:proxy_remote FALSE' do
      let(:allocation) do
        allocation_for(
          institution: Fabricate(:institution, oa_proxy: false),
          resource: Fabricate(:resource, oa_proxy: false, proxy_remote: false)
        )
      end
      it 'returns the bare remote_access_url' do
        expect(subject.new(allocation).remote.link).to eq(
          'https://remote.example.org'
        )
      end
    end

    # case 001 use ezproxy
    context 'with inst:oa_proxy FALSE, res:oa_proxy FALSE, res:proxy_remote TRUE' do
      let(:allocation) do
        allocation_for(
          institution: Fabricate(:institution, oa_proxy: false),
          resource: Fabricate(:resource, oa_proxy: false, proxy_remote: true)
        )
      end
      it 'proxies the URL with the galileo ezproxy URL' do
        expect(subject.new(allocation).remote.link).to eq(
          'https://proxy.galileo.edu/login?url=https://remote.example.org'
        )
      end
    end

    # case 010 no proxying
    context 'with inst:oa_proxy FALSE, res:oa_proxy TRUE, res:proxy_remote FALSE' do
      let(:allocation) do
        allocation_for(
          institution: Fabricate(:institution, oa_proxy: false),
          resource: Fabricate(:resource, oa_proxy: true, proxy_remote: false)
        )
      end
      it 'returns the bare remote_access_url' do
        expect(subject.new(allocation).remote.link).to eq(
          'https://remote.example.org'
        )
      end
    end

    # case 011 use ezproxy
    context 'with inst:oa_proxy FALSE, res:oa_proxy TRUE, res:proxy_remote TRUE' do
      let(:allocation) do
        allocation_for(
          institution: Fabricate(:institution, oa_proxy: false),
          resource: Fabricate(:resource, oa_proxy: true, proxy_remote: true)
        )
      end
      it 'proxies the URL with the galileo ezproxy URL' do
        expect(subject.new(allocation).remote.link).to eq(
          'https://proxy.galileo.edu/login?url=https://remote.example.org'
        )
      end
    end

    # case 100 no proxying
    context 'with inst:oa_proxy TRUE, res:oa_proxy FALSE, res:proxy_remote FALSE' do
      let(:allocation) do
        allocation_for(
          institution: Fabricate(:institution, oa_proxy: true),
          resource: Fabricate(:resource, oa_proxy: false, proxy_remote: false)
        )
      end
      it 'returns the bare remote_access_url' do
        expect(subject.new(allocation).remote.link).to eq(
          'https://remote.example.org'
        )
      end
    end

    # case 101 use ezproxy
    context 'with inst:oa_proxy TRUE, res:oa_proxy FALSE, res:proxy_remote TRUE' do
      let(:allocation) do
        allocation_for(
          institution: Fabricate(:institution, oa_proxy: true),
          resource: Fabricate(:resource, oa_proxy: false, proxy_remote: true)
        )
      end
      it 'proxies the URL with the galileo ezproxy URL' do
        expect(subject.new(allocation).remote.link).to eq(
          'https://proxy.galileo.edu/login?url=https://remote.example.org'
        )
      end
    end

    # case 110 use oaproxy
    context 'with inst:oa_proxy TRUE, res:oa_proxy TRUE, res:proxy_remote FALSE' do
      let(:allocation) do
        allocation_for(
          institution: Fabricate(:institution, oa_proxy: true),
          resource: Fabricate(:resource, oa_proxy: true, proxy_remote: false)
        )
      end
      it 'proxies the URL with the oaproxy URL' do
        expect(subject.new(allocation).remote.link).to eq(
          "https://proxy.openathens.net/login?entityID=https%3A%2F%2Fidp.wa.galileo.usg.edu%2Fentity&qurl=https%3A%2F%2Fremote.example.org"
        )
      end
    end

    # case 111 use oaproxy
    context 'with inst:oa_proxy TRUE, res:oa_proxy TRUE, res:proxy_remote TRUE' do
      let(:allocation) do
        allocation_for(
          institution: Fabricate(:institution, oa_proxy: true),
          resource: Fabricate(:resource, oa_proxy: true, proxy_remote: true)
        )
      end
      it 'proxies the URL with the oaproxy URL' do
        expect(subject.new(allocation).remote.link).to eq(
          "https://proxy.openathens.net/login?entityID=https%3A%2F%2Fidp.wa.galileo.usg.edu%2Fentity&qurl=https%3A%2F%2Fremote.example.org"
        )
      end
    end

  end
end
