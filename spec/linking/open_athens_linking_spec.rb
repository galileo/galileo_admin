# frozen_string_literal: true

require 'rails_helper'

describe 'OpenAthens access link generation via LinkService', type: :model do
  include LinkServiceHelpers

  subject { LinkService }
  let(:institution) { Fabricate :open_athens_institution }

  context 'the URL source' do
    context 'is the resource' do
      let(:allocation) { allocation_for institution: institution }
      it 'uses the proper URL from the Resource' do
        expect(subject.new(allocation).open_athens.link).to eq(
          'https://go.openathens.net/redirector/institution.edu?url=https%3A%2F%2Fopenathens.example.org'
        )
      end
    end
    context 'is the allocation' do
      let(:allocation) do
        Fabricate :allocation_with_overrides, institution: institution
      end
      it 'uses the proper URL from the Allocation' do
        expect(subject.new(allocation).open_athens.link).to eq(
          'https://go.openathens.net/redirector/institution.edu?url=https%3A%2F%2Fopenathens.override.org'
        )
      end
    end
    context 'if no open_athens_url is set' do
      let(:allocation) do
        allocation_for(
          institution: institution,
          resource: Fabricate(:resource, open_athens_url: nil)
        )
      end
      it 'uses the ip_access_url' do
        expect(subject.new(allocation).open_athens.link).to eq(
          'https://go.openathens.net/redirector/institution.edu?url=https%3A%2F%2Fwww.example.org'
        )
      end
    end
  end
  context 'without OpenAthens support' do
    context 'on the Institution' do
      let(:institution) { Fabricate :institution, open_athens_scope: nil }
      let(:allocation) { allocation_for institution: institution }
      it 'returns no open_athens.link' do
        expect(subject.new(allocation).open_athens.link).to be_nil
      end
    end
    context 'on the Resource' do
      let(:resource) { Fabricate :resource, open_athens: false }
      let(:allocation) { allocation_for resource: resource }
      it 'returns no open_athens.link' do
        expect(subject.new(allocation).open_athens.link).to be_nil
      end
    end
  end
  context 'the Redirector prefix' do
    let(:allocation) do
      allocation_for institution: institution,
                     resource: Fabricate(:resource, open_athens: true)
    end
    let(:link) { subject.new(allocation).open_athens.link }
    it 'includes the Institution scope' do
      expect(link).to include institution.open_athens_scope
    end
  end
  context 'the URL encoding' do
    let(:resource) do
      Fabricate :resource, open_athens_url: 'https://url.params.are/?=fun#not'
    end
    let(:allocation) do
      allocation_for resource: resource,
                     institution: institution
    end
    it 'contains only valid param characters' do
      link = subject.new(allocation).open_athens.link
      expect { URI.parse link }.not_to raise_error
      uri = URI.parse link
      params = CGI.parse(uri.query)
      expect(params).to have_key 'url'
      expect(params['url'][0]).to eq resource.open_athens_url
    end
  end
end
