# frozen_string_literal: true

require 'rails_helper'

describe 'IP access link generation via LinkService', type: :model do
  include LinkServiceHelpers

  subject { LinkService }

  context 'the URL source' do
    context 'is the resource' do
      let(:allocation) { Fabricate :allocation }
      it 'uses the proper URL from the Resource' do
        expect(subject.new(allocation).ip.link).to eq 'https://www.example.org'
      end
    end
    context 'is the allocation' do
      let(:allocation) { Fabricate :allocation_with_overrides }
      it 'uses the proper URL from the Allocation' do
        expect(subject.new(allocation).ip.link).to eq 'https://www.override.org'
      end
    end
  end
  context 'interpolation' do
    context 'where the variable value source is the allocation' do
      let(:allocation) do
        Fabricate :allocation_with_credentials do
          resource do
            Fabricate :resource, ip_access_url:
              'https://www.test.com/{VAR:url_dbsuid}?pw={VAR:url_dbspwd}'
          end
        end
      end
      it 'interpolates the credentials' do
        expect(subject.new(allocation).ip.link).to eq(
          'https://www.test.com/institution?pw=password'
        )
      end
    end
  end
end