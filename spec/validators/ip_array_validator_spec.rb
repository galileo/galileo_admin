# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IpArrayValidator do
  context 'when supported values are supplied' do
    it 'allows arrays with valid ip_addresses' do
      inst = Fabricate :institution, ip_addresses: %w[1.0.0.0
                                                   75.1.5.*
                                                   255.5.34-67.*]
      expect(inst).to be_valid
    end

    it 'allows empty arrays to be valid' do
      inst = Fabricate :institution, ip_addresses: []
      expect(inst).to be_valid
    end
  end

  context 'when unsupported values are supplied' do
    subject :inst do
      Fabricate :institution
    end
    it 'does not allow invalid ips' do
      subject.ip_addresses = %w[1.0.0.0.6]
      subject.valid?
      expect(subject.errors[:ip_addresses]).to include(/invalid ip range/)
    end

    it 'does not allow ips in invalid ranges' do
      subject.ip_addresses = %w[0.0.0.0]
      subject.valid?
      expect(subject.errors[:ip_addresses]).to include(/invalid ip range/)
    end

    it 'does not allow ips with * in first 2 octets' do
      subject.ip_addresses = %w[*.*.1.1]
      subject.valid?
      expect(subject.errors[:ip_addresses]).to include(/invalid ip range/)
    end

    it 'does not allow ips with - in first 2 octets' do
      subject.ip_addresses = %w[32-54.67.1.1]
      subject.valid?
      expect(subject.errors[:ip_addresses]).to include(/invalid ip range/)
    end

    it 'does not allow ips with * followed by range' do
      subject.ip_addresses = %w[1.2.*.0-128]
      subject.valid?
      expect(subject.errors[:ip_addresses]).to include(/invalid ip range/)
    end

    it 'does not allow ips with * followed by normal octet' do
      subject.ip_addresses = %w[1.2.*.128]
      subject.valid?
      expect(subject.errors[:ip_addresses]).to include(/invalid ip range/)
    end

    it 'does not allow ips with a range followed by normal octet' do
      subject.ip_addresses = %w[1.2.0-128.64]
      subject.valid?
      expect(subject.errors[:ip_addresses]).to include(/invalid ip range/)
    end

  end
end
