# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DateArrayValidator do
  context 'when supported values are supplied' do
    it 'allows arrays with supported dates' do
      inst = Fabricate :institution, change_dates: %w[0907 1007]
      expect(inst).to be_valid
    end

    it 'allows empty arrays to be valid' do
      inst = Fabricate :institution, notify_dates: []
      expect(inst).to be_valid
    end
  end

  context 'when unsupported values are supplied' do
    subject :inst do
      Fabricate :institution
    end
    it 'does not allow dates with invalid months' do
      subject.change_dates = %w[0101 9901 1031]
      subject.valid?
      expect(subject.errors[:change_dates]).to include('has invalid values')
    end

    it 'does not allow dates with more than 4 characters' do
      subject.notify_dates = %w[01011]
      subject.valid?
      expect(subject.errors[:notify_dates]).to include('has invalid values')
    end

    it 'does not allow dates with less than 4 characters' do
      subject.notify_dates = %w[01 0109]
      subject.valid?
      expect(subject.errors[:notify_dates]).to include('has invalid values')
    end
  end
end
