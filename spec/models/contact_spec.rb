require 'rails_helper'

RSpec.describe Contact, type: :model do

  context 'basic attributes' do
    subject { Fabricate :contact }
    it 'has a first name' do
      expect(subject.first_name).not_to be_empty
    end

    it 'has a last name' do
      expect(subject.last_name).not_to be_empty
    end

    it 'has an email' do
      expect(subject.email).not_to be_empty
    end

    it 'is invalid without a unique email' do
      another_contact = Fabricate.build(:contact,
                                        email: subject.email
                                       )
      another_contact.valid?
      expect(another_contact.errors[:email]).to include(
                                                   I18n.t('errors.messages.taken')
                                                 )
    end

    it 'is invalid without a unique case insensitive email' do
      another_contact = Fabricate.build(:contact,
                                        email: subject.email.upcase
      )
      another_contact.valid?
      expect(another_contact.errors[:email]).to include(
                                                I18n.t('errors.messages.taken')
                                                )
    end
  end

  context 'contact types' do
    subject { Fabricate :contact, types: %w[pass prim] }
    it 'are returned with #types' do
      expect(subject.types).to be_an Array
      expect(subject.types).to include 'pass', 'prim'
      expect(subject.types.length).to eq 2
    end
    context 'validations' do
      it 'are invalid if not in permitted list' do
        contact = Fabricate.build :contact, types: ['nope']
        contact.valid?
        expect(contact.errors).to have_key :types
      end
    end
  end

  context 'associated records' do
    let(:institution) { Fabricate :institution }
    let(:vendor) { Fabricate :vendor }

    it 'can be related to an institution' do
      contact = Fabricate.build(:contact, institution: institution)
      expect(contact).to be_valid
    end

    it 'can be related to a vendor' do
      contact = Fabricate.build(:contact, vendor: vendor)
      expect(contact).to be_valid
    end

    it 'does not need to be related to a vendor or an institution' do
      contact = Fabricate.build(:contact, institution: nil, vendor: nil)
      expect(contact).to be_valid
    end

    it 'is invalid if it is related to a vendor and an institution' do
      contact = Fabricate.build(:contact, institution: institution, vendor: vendor)
      contact.valid?
      expect(contact.errors.attribute_names).to include(:institution_id, :vendor_id)
    end

    it 'is invalid if it is has the same email and institution_id as another contact' do
      existing_contact = Fabricate(:contact, email: 'test@example.com', institution: institution)
      contact = Fabricate.build(:contact, email: existing_contact.email, institution: institution)
      contact.valid?
      expect(contact.errors.attribute_names).to include(:email)
    end

    it 'is invalid if it is has the same email and vendor_id as another contact' do
      existing_contact = Fabricate(:contact, email: 'test@example.com', vendor: vendor)
      contact = Fabricate.build(:contact, email: existing_contact.email, vendor: vendor)
      contact.valid?
      expect(contact.errors.attribute_names).to include(:email)
    end

    it 'is valid if it is has the same email and different institution_id as another contact' do
      existing_contact = Fabricate(:contact, email: 'test@example.com', institution: institution)
      contact = Fabricate(:contact, email: existing_contact.email, institution: Fabricate(:institution))
      expect(contact).to be_valid
    end

    it 'is valid if it is has the same email and different vendor_id as another contact' do
      existing_contact = Fabricate(:contact, email: 'test@example.com', vendor: vendor)
      contact = Fabricate.build(:contact, email: existing_contact.email, vendor: Fabricate(:vendor))
      expect(contact).to be_valid
    end

    it 'is valid if it is associated with an institution and has the same email as a vendor contact' do
      existing_contact = Fabricate(:contact, email: 'test@example.com', vendor: vendor)
      contact = Fabricate.build(:contact, email: existing_contact.email, institution: institution)
      expect(contact).to be_valid
    end
  end
end
