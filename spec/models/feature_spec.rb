# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Feature, type: :model do
  context 'basic attributes' do
    let(:feature) { Fabricate :feature }
    it 'has basic accessors' do
      expect(feature).to respond_to :view_type
      expect(feature).to respond_to :position
      expect(feature).to respond_to :link_url
      expect(feature).to respond_to :link_label
      expect(feature).to respond_to :link_description
      expect(feature).to respond_to :featuring
    end
    it 'can has a polymorphic relation to Institution or InstitutionGroup' do
      expect(feature.featuring).to be_an_instance_of Institution
      ig_feature = Fabricate(:feature, featuring: Fabricate(:institution_group))
      expect(ig_feature.featuring).to be_an_instance_of InstitutionGroup
    end
  end
  context 'attached image' do
    let(:feature) { Fabricate :feature_with_image }
    it 'can have an attached image' do
      expect(feature.image).to be_an_instance_of ActiveStorage::Attached::One
    end
    it 'has a thumbnail variant' do
      expect(feature.thumbnail).to be_an_instance_of(ActiveStorage::Variant).or(
                                   be_an_instance_of ActiveStorage::VariantWithRecord)
    end
  end
  context 'validations' do
    context 'view attributes' do
      it 'has an acceptable view_type value, if present' do
        hs_feature = Fabricate.build :feature
        expect(hs_feature).to be_valid
        wacky_feature = Fabricate.build(:feature, view_type: 'wacky')
        expect(wacky_feature.save).to be_falsey
        expect(wacky_feature.errors).to have_key :view_type
      end
      it 'must have a position' do
        bad_feature = Fabricate.build(:feature, position: nil)
        expect(bad_feature.save).to be_falsey
        expect(bad_feature.errors).to have_key :position
      end
      it 'has a position value between 1 and 6' do
        good_feature = Fabricate.build :feature
        expect(good_feature).to be_valid
        bad_feature = Fabricate.build :feature, position: 7
        expect(bad_feature.save).to be_falsey
        expect(bad_feature.errors).to have_key :position
      end
      it 'must have a unique position value under the Featuring thing' do
        feature1 = Fabricate :feature
        feature2 = Fabricate.build(:feature, position: feature1.position,
                                             featuring: feature1.featuring)
        expect(feature2.save).to be_falsey
        ig_feature = Fabricate.build :feature,
                                     position: feature1.position,
                                     featuring: Fabricate(:institution_group)
        expect(ig_feature).to be_valid
      end

      it 'strips link_url of white space' do
        feature = Fabricate :feature, link_url:"                http://www.example.com              "

        expect(feature.link_url).to eq 'http://www.example.com'
      end
    end
    context 'link attributes' do
      it 'must have a link_description field of proper length' do
        feature = Fabricate.build(:feature, link_description: nil)
        expect(feature).to be_valid
        bad_feature = Fabricate.build(:feature,
                                      link_description: 'X' * 351)
        expect(bad_feature).to be_invalid
        bad_feature.valid?
        expect(bad_feature.errors).to have_key :link_description
      end
    end
  end
end
