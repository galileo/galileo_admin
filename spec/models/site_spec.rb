# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Site, type: :model do
  context 'attributes' do
    subject :institution do
      Fabricate(:site, code: 'telm', site_type: 'elem', name: 'Test Elementary School')
    end

    it 'is valid with an institution code and a name' do
      expect(subject).to be_valid
    end

    it 'is invalid with no site_type' do
      site = Fabricate.build(:site, site_type: nil)
      expect(site.valid?).to be_falsey
    end

    it 'is invalid with name blank' do
      subject.name = ''
      subject.valid?
      expect(subject.errors[:name])
        .to include(I18n.t('errors.messages.blank'))
    end

    it 'is invalid without a unique code' do
      site = Fabricate :site
      another_site = Fabricate.build(
        :site,
        code: site.code
      )
      another_site.valid?
      expect(another_site.errors[:code]).to include(
        I18n.t('errors.messages.taken')
      )
    end

    it 'is invalid without a unique name' do
      site = Fabricate :site
      another_site = Fabricate.build(
      :site,
      name: site.name
      )
      another_site.valid?
      expect(another_site.errors[:name]).to include(
        I18n.t('errors.messages.taken')
      )
    end

    it 'is invalid without a invalid site_type' do
      site = Fabricate.build(:site, site_type: 'Invalid Site Type')
      site.valid?
      expect(site.errors[:site_type]).to include('not a permitted site type')
    end

    it 'removes extra spaces from name when saved' do
      site = Fabricate.build(:site, name: '     Site      Test        Name   ')
      site.save
      expect(site.name).to eq('Site Test Name')
    end
  end
end

