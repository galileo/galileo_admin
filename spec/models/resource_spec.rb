# frozen_string_literal: true

require 'rails_helper'

describe Resource, type: :model do
  context 'attributes' do
    subject :resource do
      Resource.new(
        code: 'abcd',
        name: 'Test Resource',
        legacy_data: '{}'
      )
    end
    it 'has basic attributes' do
      expect(subject.code).to eq 'abcd'
      expect(subject.name).to eq 'Test Resource'
    end
    it 'can have related Formats' do
      subject.formats << Fabricate.times(2, :format)
      expect(subject).to respond_to :formats
      expect(subject.formats.length).to eq 2
      expect(subject.formats.first).to be_a Format
    end
    it 'can have related Subjects' do
      subject.subjects << Fabricate.times(2, :subject)
      expect(subject).to respond_to :subjects
      expect(subject.subjects.length).to eq 2
      expect(subject.subjects.first).to be_a Subject
    end
    it 'can have related Branding' do
      subject.branding = Fabricate(:branding)
      expect(subject.branding).to be_a Branding
    end
    context 'sanitization' do
      it 'removes invalid HTML from description fields' do
        resource = Fabricate(:resource,
          short_description: '<p><b>HTML</b></p>',
          long_description: '<a href="#"><i>Link</i></a>')
        expect(resource.short_description).to eq '<p>HTML</p>'
        expect(resource.long_description).to eq '<a href="#">Link</a>'
      end
    end
    it {is_expected.to strip_attributes(
                         :name, :short_description, :long_description, :vendor_name, :ip_access_url,
                         :remote_access_url, :open_athens_url, :access_note, :audience, :language,
                         :title_list_url, :product_suite, :note, :special_information) }
    context 'validations' do
      it 'is valid with a resource id and a name' do
        expect(subject).to be_valid
      end
      it 'is invalid without a resource id' do
        resource = Resource.new(code: nil)
        resource.valid?
        expect(resource.errors[:code])
          .to include(I18n.t('errors.messages.blank'))
      end
      it 'is invalid without a name' do
        resource = Resource.new(name: nil)
        resource.valid?
        expect(resource.errors[:name]).to include(I18n.t('errors.messages.blank'))
      end
      it 'is invalid without a unique code' do
        resource = Fabricate :resource
        another_resource = Fabricate.build(
          :resource,
          code: resource.code
        )
        another_resource.valid?
        expect(another_resource.errors[:code]).to include(
          I18n.t('errors.messages.taken')
        )
      end
      it 'is valid with a correctly formatted code' do
        resource = Fabricate.build(:resource, code:'abcd')
        expect(resource).to be_valid
        resource = Fabricate.build(:resource, code:'abcd-abc1')
        expect(resource).to be_valid
        resource = Fabricate.build(:resource, code:'abcd:testing-for_something')
        expect(resource).to be_valid
      end
      it 'is invalid with a incorrectly formatted code' do
        resource = Fabricate.build(:resource, code:'abc')
        resource.valid?
        expect(resource.errors[:code]).to include(I18n.t('app.resources.messages.validate_code'))
        resource = Fabricate.build(:resource, code:'abcd-ab1')
        resource.valid?
        expect(resource.errors[:code]).to include(I18n.t('app.resources.messages.validate_code'))
        resource = Fabricate.build(:resource, code:'abcd:tes$ting-for_something')
        resource.valid?
        expect(resource.errors[:code]).to include(I18n.t('app.resources.messages.validate_code'))
      end

      it 'is valid when ip_access_url and remote_access_url both contain a valid url' do
        resource = Fabricate(:resource,
          ip_access_url: 'https://www.example.org',
          remote_access_url: 'https://remote.example.org')
        expect(resource).to be_valid
      end
      it 'is invalid when ip_access_url does not start with http or https' do
        resource = Fabricate.build(:resource,
          ip_access_url: 'www.example.org')
        resource.valid?
        expect(resource.errors[:ip_access_url]).to include("should start with http:// or https://")
      end
      it 'is invalid when remote_access_url does not start with http or https' do
        resource = Fabricate.build(:resource,
          remote_access_url: 'www.example.org')
        resource.valid?
        expect(resource.errors[:remote_access_url]).to include("should start with http:// or https://")
      end
      it "'is valid with an open_athens_url that doesn't contain the OA redirector" do
        subject.open_athens_url = 'http://dl.acm.org'
        expect(subject).to be_valid
      end

      it 'is invalid with an open_athens_url that contains the OA redirector' do
        subject.open_athens_url = "#{LinkService::OA_REDIRECTOR_URL}/test"
        subject.valid?
        expect(subject.errors[:open_athens_url])
          .to include('OpenAthens URL should not contain the OpenAthens redirector prefix. The URL is automatically encoded when "OpenAthens Enabled?" is selected')
      end
      it 'is valid when open_athens and without_open_athens_redirector are both true' do
        resource = Fabricate(:resource,
          open_athens: true,
          without_open_athens_redirector: true)
        expect(resource).to be_valid
      end
      it 'is not valid when open_athens is false and without_open_athens_redirector is true' do
        resource = Fabricate(:resource,
          open_athens: false)
        resource.without_open_athens_redirector = true
        expect(resource.valid?).to be_falsey
        expect(resource.errors[:without_open_athens_redirector]).to include(I18n.t("app.resources.messages.validate_without_open_athens_redirector"))
      end

      it 'is invalid if credential_display is true and user_id and password are both blank' do
        glri_resource = Fabricate(:glri_resource, user_id: '', password: '')
        glri_resource.credential_display = 'always'
        glri_resource.valid?
        expect(glri_resource.errors[:credential_display]).to include("always can't be selected when Access User ID and Access Password are both blank")
      end

      it 'is valid if credential_display is true and password is blank' do
        glri_resource = Fabricate(:glri_resource, user_id: 'test', password: '')
        glri_resource.credential_display = 'always'
        expect(glri_resource).to be_valid
      end

      it 'is valid if credential_display is true and user_id is blank' do
        glri_resource = Fabricate(:glri_resource, user_id: '', password: 'test')
        glri_resource.credential_display = 'always'
        expect(glri_resource).to be_valid
      end
    end
  end
  context '#logo' do
    subject { Fabricate :resource_with_logo }
    it 'can have a logo attached' do
      expect(subject.logo).to be_an_instance_of(ActiveStorage::Attached::One)
    end

    it 'can have a logo_thumbnail if logo attached' do
      expect(subject.logo_thumbnail).to be_an_instance_of(ActiveStorage::Variant).or(
                                        be_an_instance_of ActiveStorage::VariantWithRecord)
    end
  end
  context 'default values' do
    subject(:inst) { Resource.new(name: 'test', code: 'test') }
    it 'has the correct default values' do
      expect(subject.active).to eq false
      expect(subject.open_athens).to eq false
      expect(subject.without_open_athens_redirector).to eq false
      expect(subject.display).to eq true
      expect(subject.show_institutional_branding).to eq false
      expect(subject.bypass_galileo_authentication).to eq false
    end
  end

  context 'ip_access_url required for active resource' do
    let(:resource) do
      Fabricate.build(:resource, active: true, ip_access_url: nil)
    end
    it 'is invalid without an ip_access_url if active' do
      resource.valid?
      expect(resource.errors[:ip_access_url]).to include(
        I18n.t('errors.messages.blank')
      )
    end
  end
  context 'parent and child relations' do
    let(:parent) { Fabricate :resource }
    let(:child) { Fabricate(:resource, parent_id: parent.id) }
    it 'child can have a parent' do
      expect(child.parent).to eq parent
    end
    it 'parent can have children' do
      expect(parent.children).to include child
    end
    it 'share being marked inactive' do
      expect(parent.active).to be_truthy
      expect(child.active).to be_truthy
      parent.active = false
      parent.save
      expect(parent.active).to be_falsey
      expect(child.reload.active).to be_falsey
    end
  end
  context 'branding' do
    subject { Fabricate :glri_resource_with_branding }
    it 'is valid when it belongs to the glri institution' do
      glri_inst = subject.institution
      subject.branding = glri_inst.brandings.first
      expect(subject).to be_valid
    end

    it 'is invalid when the glri institution is not set' do
      subject.institution = nil
      subject.branding = Fabricate :branding
      subject.valid?
      expect(subject.errors[:branding_id]).to include('GLRI institution must be selected')
    end

    it 'is invalid when the branding does not belong to the glri institution' do
      branding = Fabricate :branding
      subject.branding = branding
      subject.valid?
      expect(subject.errors[:branding_id]).to include("#{branding.name} is not a valid branding for institution #{subject.institution.name}")
    end
  end

  context 'allocations' do
    describe 'glri resource' do
      it 'has allocations when active set to true' do
        resource = Fabricate(:glri_resource, active: false)
        expect(resource.allocations.size).to eq 0
        resource.update(active: true)
        expect(resource.allocations.size).to eq 1
      end

      it 'does not have allocations when active is set to false' do
        resource = Fabricate(:glri_resource, active: true)
        expect(resource.allocations.size).to eq 1
        resource.update(active: false)
        expect(resource.allocations.size).to eq 0
      end

      it 'has allocations when active is set back to true' do
        resource = Fabricate(:glri_resource, active: true)
        expect(resource.allocations.size).to eq 1
        resource.update(active: false)
        expect(resource.allocations.size).to eq 0
        resource.update(active: true)
        expect(resource.allocations.size).to eq 1
      end

      it 'does not has allocations when active is set back to false' do
        resource = Fabricate(:glri_resource, active: false)
        expect(resource.allocations.size).to eq 0
        resource.update(active: true)
        expect(resource.allocations.size).to eq 1
        resource.update(active: false)
        expect(resource.allocations.size).to eq 0
      end
    end
    describe 'core resource' do
      it 'has allocations when an institution is added and active is true' do
        institution = Fabricate :institution
        resource = Fabricate(:core_resource, institution_ids: [], active: true)
        expect(resource.allocations.size).to eq 0
        resource.update(institution_ids: [institution.id])
        expect(resource.allocations.size).to eq 1
      end

      it 'does not have allocations when an institution is removed and active is true' do
        institution = Fabricate :institution
        resource = Fabricate(:core_resource, institution_ids: [institution.id], active: true)
        expect(resource.allocations.size).to eq 1
        resource.update(institution_ids: [])
        expect(resource.allocations.size).to eq 0
      end

      it 'does not have allocations when there is an associated institution and active is changed to false' do
        institution = Fabricate :institution
        resource = Fabricate(:core_resource, institution_ids: [institution.id], active: true)
        expect(resource.allocations.size).to eq 1
        resource.update(active: false)
        expect(resource.allocations.size).to eq 0
      end

      it 'does not have allocations when active is set back to true' do
        institution = Fabricate :institution
        resource = Fabricate(:core_resource, institution_ids: [institution.id], active: true)
        expect(resource.allocations.size).to eq 1
        resource.update(active: false)
        expect(resource.allocations.size).to eq 0
        resource.update(active: true)
        expect(resource.allocations.size).to eq 0
      end
    end
  end

end
