# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Vendor, type: :model do
  context 'attributes' do
    describe '#logo' do
      subject { Fabricate :vendor_with_logo }
      it 'can have a logo attached' do
        expect(subject.logo).to be_an_instance_of(ActiveStorage::Attached::One)
      end

      it 'can have a logo_thumbnail if logo attached' do
        expect(subject.logo_thumbnail).to be_an_instance_of(ActiveStorage::Variant).or(
                                          be_an_instance_of ActiveStorage::VariantWithRecord)
      end
    end
  end
end
