require 'rails_helper'

RSpec.describe TemplateViewBento, type: :model do
  context 'attributes' do
    context 'validations' do
      subject :template_view_bento do
        Fabricate(:template_view_bento)
      end

      it 'is valid with an template_name, a predefined_bento, and a user_view' do
        expect(subject).to be_valid
      end

      it 'is invalid without an template_name' do
        subject.template_name = ''
        subject.valid?
        expect(subject.errors[:template_name]).to include(I18n.t('errors.messages.blank'))
      end

      it 'is invalid without a predefined_bento' do
        subject.predefined_bento = nil
        subject.valid?
        expect(subject.errors[:predefined_bento]).to include("must exist")
      end

      it 'is invalid without a user_view' do
        subject.user_view = ''
        subject.valid?
        expect(subject.errors[:user_view]).to include(I18n.t('errors.messages.blank'))
      end

      it 'is invalid with an unsupported user_view if template_name is an inst_type' do
        subject.template_name = 'k12'
        subject.user_view = 'unsupported'
        subject.valid?
        expect(subject.errors[:user_view]).to include("'unsupported' is not a supported user view. Allowed user views: elementary, middle, highschool, educator")
      end

    end
  end
end
