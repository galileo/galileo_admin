# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Allocation, type: :model do
  let(:allocation) { Fabricate :allocation }
  before(:each) do
    Sunspot.remove_all! Allocation
  end
  context 'basic attributes' do
    it 'has relations' do
      expect(allocation.institution).to be_an Institution
      expect(allocation.resource).to be_an Resource
    end

    it 'can override branding with one from institution' do
      branding = Fabricate :branding
      allocation.institution.brandings << branding

      allocation.branding = branding
      expect(allocation).to be_valid
    end

    it 'must have a valid branding from institution' do
      bad_branding = Fabricate :branding
      allocation.branding = bad_branding
      allocation.valid?

      expect(allocation.errors[:branding_id]).to include(I18n.t('app.allocations.messages.valid_branding'))
    end
  end
  context 'indexing behaviors' do
    context 'when resource is modified' do
      it 'is reindexed with updated data' do
        resource = allocation.resource
        resource.name = 'Test'
        resource.save
        Sunspot.commit
        results = SearchService.allocations_by resource_name: allocation.resource.name
        expect(results.first).to eq allocation
      end
    end
    context 'when resource relations are modified' do
      it 'is reindexed with updated relation' do
        resource = allocation.resource
        resource.formats << Fabricate(:format)
        resource.save
        Sunspot.commit
        results = SearchService.allocations_by format: Format.last.name
        expect(results.first).to eq allocation
      end
    end
    context 'when an institution is removed' do
      it 'removes the corresponding allocation' do
        resource = allocation.resource
        institution = Fabricate :institution
        resource.institutions << institution
        Sunspot.commit
        expect(
          SearchService.allocations_for_institution(institution).first
        ).to eq institution.allocations.first
        resource.institutions = []
        Sunspot.commit
        expect(
          SearchService.allocations_for_institution(institution)
        ).to be_empty
      end
      it 'removes the corresponding allocation for a child resource' do
        parent_resource = allocation.resource
        institution = allocation.institution
        child_resource = Fabricate :resource, parent: parent_resource
        child_resource.institutions << institution
        Sunspot.commit
        results = SearchService.allocations_for_institution(institution)
        expect(results.length).to eq 2
        expect(results).to include institution.allocations.first,
                                   child_resource.allocations.first
        parent_resource.institutions.delete institution
        Sunspot.commit
        results = SearchService.allocations_for_institution(institution)
        expect(results.length).to eq 0
      end
    end
    context 'when resource is marked inactive' do
      it 'removes all allocations' do
        resource = allocation.resource
        Sunspot.commit
        results = SearchService.allocations_for_resource resource
        expect(results.length).to eq 1
        resource.active = false
        resource.save
        Sunspot.commit
        results = SearchService.allocations_for_resource resource
        expect(results.length).to eq 0
      end
      it 'removes all allocations for child resources' do
        resource = allocation.resource
        institution = allocation.institution
        child_resource = Fabricate :resource, parent: resource,
                                              institutions: [institution]
        Sunspot.commit
        results = SearchService.allocations_for_resource child_resource
        expect(results.length).to eq 1
        resource.active = false
        resource.save
        Sunspot.commit
        results = SearchService.allocations_for_resource child_resource
        expect(results.length).to eq 0
      end
    end
  end
  context '#branding_image' do
    it 'returns a key when a branding image is attached' do
      allocation = Fabricate :allocation_with_branding_image
      expect(allocation.branding_image).to_not be_blank
    end

    it 'returns nil when a branding image is not attached' do
      allocation = Fabricate :allocation_with_branding_image
      allocation.resource.branding.image.purge
      expect(allocation.branding_image).to be_nil
    end

    it 'returns nil when the resource has no branding' do
      allocation = Fabricate :allocation_with_branding_image
      allocation.resource.branding = nil
      expect(allocation.branding_image).to be_nil
    end
  end
  context 'subscription_type' do
    context 'default values' do
      it 'is set to none by default on central allocations' do
        allocation = Fabricate( :central_allocation, subscription_type: '')
        expect(allocation.subscription_type).to eq 'none'
      end

      it 'is set to public_core by default on central public allocations' do
        allocation = Fabricate( :central_public_allocation, subscription_type: '')
        expect(allocation.subscription_type).to eq 'public_core'
      end

      it 'is set to glri by default on glri allocations' do
        resource = Fabricate( :glri_resource)
        allocation = resource.allocations.first
        expect(allocation.subscription_type).to eq 'glri'
      end

      it 'is set to public_glri by default on public glri allocations' do
        resource = Fabricate( :glri_public_resource)
        allocation = resource.allocations.first
        expect(allocation.subscription_type).to eq 'public_glri'
      end
    end
    context 'validations' do
      it 'is valid with a valid subscription_type' do
        allocation = Fabricate.build(:central_allocation, subscription_type: 'core')
        expect(allocation).to be_valid
      end
      it 'is invalid with an invalid subscription_type' do
        allocation = Fabricate.build(:allocation_with_branding_image, subscription_type: 'not_a_valid_type')
        allocation.valid?
        expect(allocation.errors.attribute_names).to include(:subscription_type)
      end

      it 'is invalid with "glri" if it has a core resource' do
        resource = Fabricate :resource
        allocation = Fabricate.build( :allocation, resource_id: resource.id, subscription_type: 'glri')
        allocation.valid?
        expect(allocation.errors.attribute_names).to include(:subscription_type)
      end

      it 'is automatically set with "glri" if it has a glri resource' do
        glri_resource = Fabricate :glri_resource
        allocation = glri_resource.allocations.first
        expect(allocation.subscription_type).to eq 'glri'
      end
    end
  end
end
