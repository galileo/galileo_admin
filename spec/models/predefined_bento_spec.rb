require 'rails_helper'

RSpec.describe PredefinedBento, type: :model do

  context 'methods' do
    subject :predefined_bento do
      Fabricate(:predefined_bento)
    end

    describe '#service_field_prerequisites' do
      it 'has service eds_api' do
        subject.service = 'eds_api'
        subject.definition = { 'Limit to Custom Catalog' => 1 }
        expect(subject.service_field_prerequisites).to include("Custom Catalog Number")
      end
      it 'has service other' do
        subject.service = 'galileo'
        expect(subject.service_field_prerequisites).to eq([])
      end
    end

    describe '#disambiguated_name' do
      it 'shows display_name and code' do
        subject.display_name = 'Test Predefined Bento'
        subject.code = 'test'
        expect(subject.disambiguated_name).to eq("Test Predefined Bento (test)")
      end
    end

    describe '#menu_values' do
      it 'gives an array of names/codes' do
        subject.display_name = 'Test Predefined Bento'
        subject.code = 'test'
        expect(PredefinedBento.menu_values).to include(["Test Predefined Bento (test)", "test"])
      end
    end

    describe '#sort_options' do
      it 'gives an array of sort options' do
        expect(PredefinedBento.sort_options).to include(['Recently Created', 'created_at_desc'])
      end
    end

    describe '#supported_params_by_service' do
      it 'gives params for eds_api' do
        expect(PredefinedBento.supported_params_by_service('eds_api').first).to include(
          'Additional Limiters',
          'Has Parent Publication',
          'Equivalent Limiter Clause',
          'Limit to Custom Catalog'
        )
        expect(PredefinedBento.supported_params_by_service('eds_api').second.keys).to include(
          :'Source Type Facet',
          :'Content Provider Facet'
        )
      end
      it 'gives params for eds_publications' do
        expect(PredefinedBento.supported_params_by_service('eds_publications')).to include('Profile Name')
      end
      it 'gives (empty) params for other' do
        expect(PredefinedBento.supported_params_by_service('xyz')).to eq([])
      end
    end

    describe '(class) #definition_display_order' do
      it 'gives display order for eds_api' do
        expect(PredefinedBento.definition_display_order('eds_api')).to be_present
      end
      it 'gives nil for other' do
        expect(PredefinedBento.definition_display_order('xyz')).to be_nil
      end
    end

    describe '(instance) #definition_display_order' do
      it 'gives display order for eds_api' do
        subject.service = 'eds_api'
        expect(subject.definition_display_order).to be_present
      end
      it 'gives nil for other' do
        subject.service = 'xyz'
        expect(subject.definition_display_order).to be_nil
      end
    end

    describe '#sorted_definition' do
      it 'sorts definition per eds_api' do
        subject.service = 'eds_api'
        subject.definition = {
          'Limit to Custom Catalog' => true,
          'Source Type Facet' => true,
          'bogus 1' => true,
          'bogus 2' => true
        }
        expect(subject.sorted_definition.to_a).to eq({
          'Source Type Facet' => true,
          'Limit to Custom Catalog' => true,
          'bogus 1' => true,
          'bogus 2' => true
        }.to_a)
      end
      it 'doesn\'t sort definition when other' do
        subject.service = 'xyz'
        subject.definition = {
          'Limit to Custom Catalog' => true,
          'Source Type Facet' => true,
          'bogus 1' => true,
          'bogus 2' => true
        }
        expect(subject.sorted_definition.to_a).to eq({
          'Limit to Custom Catalog' => true,
          'Source Type Facet' => true,
          'bogus 1' => true,
          'bogus 2' => true
        }.to_a)
      end
    end

    describe '#service_name' do
      it 'gives display name for eds_api' do
        subject.service = 'eds_api'
        expect(subject.service_name).to eq("EDS API")
      end
      it 'gives display name for galileo' do
        subject.service = 'galileo'
        expect(subject.service_name).to eq("GALILEO")
      end
      it 'gives display name for eds_publications' do
        subject.service = 'eds_publications'
        expect(subject.service_name).to eq("EBSCO Publication Finder")
      end
      it 'gives display name for primo' do
        subject.service = 'primo'
        expect(subject.service_name).to eq("Primo API")
      end
    end

    describe '#has_definition' do
      it 'has a definition' do
        subject.definition = { "boo" => "hoo" }
        expect(subject.has_definition).to eq true
      end
      it 'doesn\'t have a definition' do
        subject.definition = nil
        expect(subject.has_definition).to eq false
      end
    end

    describe '#destroy_configured_bentos' do
      it 'calls bulk_destroy' do
        allow(ConfiguredBento).to receive(:bulk_destroy).and_return("destroyed all configured_bentos")
        expect(subject.destroy_configured_bentos).to eq "destroyed all configured_bentos"
      end
    end

    describe '#protected?' do
      it 'databases is protected' do
        subject.code = "databases"
        expect(subject.protected?).to eq true
      end
      it 'eds_custom is protected' do
        subject.code = "eds_custom"
        expect(subject.protected?).to eq true
      end
      it 'eds_resource is protected' do
        subject.code = "eds_resource"
        expect(subject.protected?).to eq true
      end
      it 'other codes are not protected' do
        subject.code = "other"
        expect(subject.protected?).to eq false
      end
    end
  end

  context 'attributes' do
    context 'validations' do
      subject :predefined_bento do
        Fabricate(:predefined_bento)
      end

      it 'is valid with a service, a code, and a display_name' do
        expect(subject).to be_valid
      end

      it 'is invalid without a service' do
        subject.service = ''
        subject.valid?
        expect(subject.errors[:service]).to include(I18n.t('errors.messages.blank'))
      end

      it 'is invalid without a code' do
        subject.code = ''
        subject.valid?
        expect(subject.errors[:code]).to include(I18n.t('errors.messages.blank'))
      end

      it 'is invalid without a display_name' do
        subject.display_name = ''
        subject.valid?
        expect(subject.errors[:display_name]).to include(I18n.t('errors.messages.blank'))
      end

      it 'is invalid without a valid service' do
        subject.service = 'invalid'
        subject.valid?
        expect(subject.errors[:service].first).to include("'invalid' is not a supported type")
      end

    end
  end
end
