require 'rails_helper'

RSpec.describe InstitutionGroup, type: :model do
  context 'attributes' do
    let(:code) { 'test' }
    subject :institution_group do
      Fabricate :institution_group, code: code
    end
    it 'has basic attributes' do
      expect(subject.code).to eq code
    end

    it 'is valid with a code and an inst_type' do
      expect(subject).to be_valid
    end

    it 'is invalid without a code' do
      group = Fabricate.build(:institution_group, code: nil)
      group.valid?
      expect(group.errors[:code]).to include(I18n.t('errors.messages.blank'))
    end

    it 'is invalid with an invalid inst_type' do
      subject.inst_type = 'wrong'
      subject.valid?
      expect(subject.errors[:inst_type])
        .to include('not a permitted inst_type')
    end

    it 'is invalid when code has been used' do
      bad_group = Fabricate.build( :institution_group, code: subject.code)
      bad_group.valid?
      expect(bad_group.errors[:code]).to include(I18n.t('errors.messages.taken'))
    end

    it 'can have Legacy Group Institutions' do
      Fabricate :institution
      Institution.last.legacy_institution_groups << subject
      expect(subject).to respond_to 'legacy_group_institutions'
      expect(subject.legacy_group_institutions.first).to be_an Institution
    end
  end
end
