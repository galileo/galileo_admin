# frozen_string_literal: true

require 'rails_helper'

describe Institution, type: :model do
  include LegacyDatesHelper

  context 'attributes' do
    subject :institution do
      Fabricate(:institution, code: 'abcd', public_code: 'abcdp',
                              name: 'Test Institution',
                              legacy_data: '{}'
      )
    end
    it 'has basic attributes' do
      expect(subject.code).to eq 'abcd'
      expect(subject.public_code).to eq 'abcdp'
      expect(subject.name).to eq 'Test Institution'
    end

    it 'is valid with an institution code, public code, and a name' do
      expect(subject).to be_valid
    end

    it 'is valid with use_local_ez_proxy set to true and local_ez_proxy_url set' do
      subject.use_local_ez_proxy = true
      subject.local_ez_proxy_url = 'example.com'
      expect(subject).to be_valid
    end

    it 'is invalid with use_local_ez_proxy set to true and local_ez_proxy_url not set' do
      subject.use_local_ez_proxy = true
      subject.local_ez_proxy_url = nil
      subject.valid?
      expect(subject.errors[:local_ez_proxy_url])
        .to include(I18n.t('errors.messages.blank'))
    end

    it 'is invalid without an institution code' do
      inst = Institution.new(code: nil)
      inst.valid?
      expect(inst.errors[:code])
        .to include(I18n.t('errors.messages.blank'))
    end

    it 'is invalid without a name' do
      inst = Institution.new(name: nil)
      inst.valid?
      expect(inst.errors[:name]).to include(I18n.t('errors.messages.blank'))
    end

    it 'is invalid without a unique institution code' do
      inst = Fabricate :institution
      another_institution = Fabricate.build(
        :institution,
        code: inst.code
      )
      another_institution.valid?
      expect(another_institution.errors[:code]).to include(
        I18n.t('errors.messages.taken')
      )
    end

    it 'is invalid without a unique institution name' do
      inst = Fabricate :institution
      another_institution = Fabricate.build(
      :institution,
      name: inst.name
      )
      another_institution.valid?
      expect(another_institution.errors[:name]).to include(
        I18n.t('errors.messages.taken')
      )
    end

    it 'can have an associated InstitutionGroup' do
      subject.institution_group = Fabricate(:institution_group)
      expect(subject).to respond_to 'institution_group'
      expect(subject.institution_group).to be_a InstitutionGroup
    end

    it 'can have associated NotifyGroups' do
      institution = Fabricate :institution_with_notify_group
      expect(institution).to respond_to :notify_group
      expect(institution.notify_group).to be_a NotifyGroup
      expect(institution.notify_group).to eq NotifyGroup.last
    end

    it 'can have associated Brandings' do
      institution = Fabricate :institution
      institution.brandings = Fabricate.times(2, :branding)
      expect(institution).to respond_to :brandings
      expect(institution.brandings.length).to eq 2
      expect(institution.brandings.first).to be_a Branding
    end

    it 'is valid when change_dates has valid dates' do
      inst = Fabricate :institution, change_dates: %w[0907 1007]
      expect(inst).to be_valid
    end

    it 'is valid when notify_dates has an empty array' do
      inst = Fabricate :institution, notify_dates: []
      expect(inst).to be_valid
    end

    it 'is valid when the ip_addresses array is empty' do
      inst = Fabricate :institution, ip_addresses: []
      expect(inst).to be_valid
    end

    it 'is not valid when change_dates has invalid months' do
      inst = Fabricate :institution
      inst.change_dates = %w[0101 9901 1031]
      inst.valid?
      expect(inst.errors[:change_dates]).to include('has invalid values')
    end

    it 'is not valid when notify_dates has dates with more than 4 characters' do
      inst = Fabricate :institution
      inst.notify_dates = %w[0205 01011]
      inst.valid?
      expect(inst.errors[:notify_dates]).to include('has invalid values')
    end

    it 'is not valid when notify_dates has dates with less than 4 characters' do
      inst = Fabricate :institution
      inst.notify_dates = %w[01 0109]
      inst.valid?
      expect(inst.errors[:notify_dates]).to include('has invalid values')
    end
    it 'is valid with a correctly formatted code' do
      institution = Fabricate.build(:institution, code:'abcd')
      expect(institution).to be_valid
      institution = Fabricate.build(:institution, code:'1234')
      expect(institution).to be_valid
      institution = Fabricate.build(:institution, code:'ab12')
      expect(institution).to be_valid
    end
    it 'is invalid with a incorrectly formatted code' do
      institution = Fabricate.build(:institution, code:'abc')
      institution.valid?
      expect(institution.errors[:code]).to include(I18n.t('app.institution.messages.validate_code'))
      institution = Fabricate.build(:institution, code:'123')
      institution.valid?
      expect(institution.errors[:code]).to include(I18n.t('app.institution.messages.validate_code'))
    end
  end
  context 'default values' do
    subject(:inst) { Institution.new(name: 'test', code: 'test') }
    it 'has the correct default values' do
      expect(subject.active).to eq false
      expect(subject.open_athens).to eq false
      expect(subject.ip_authentication).to eq true
      expect(subject.glri_participant).to eq 'none'
    end
  end
  context 'contacts' do
    subject(:inst) { Fabricate :institution }
    before(:each) do
      Fabricate.times(2, :contact,
                      types: ['pass'],
                      institution: inst)
      Fabricate(:contact,
                types: ['glri'],
                institution: inst)
    end
    it 'can return only password contacts' do
      expect(inst.password_contacts.length).to eq 2
      inst.password_contacts.each do |c|
        expect(c.password?).to be_truthy
      end
    end
  end
  context 'deactivate glri resources' do
    subject(:inst) { Fabricate :institution, active: true }
    let!(:glri_resource1) do
      Fabricate :resource, code: "zzo1-#{inst.code}", institution_id: inst.id,
                           active: true
    end
    let!(:glri_resource2) do
      Fabricate :resource, code: "zzo2-#{inst.code}", institution_id: inst.id,
                           active: true
    end
    it 'is an active institution with active glri resources' do
      expect(inst.glri_resources.count).to eq 2
      inst.glri_resources.each do |r|
        expect(r.active).to be_truthy
      end
    end
    it 'deactivates its glri resources when institution is no longer active' do
      expect(inst.glri_resources.count).to eq 2
      inst.active = false
      inst.save
      inst.glri_resources.each do |r|
        expect(r.active).to be_falsey
      end
    end
  end
  context 'password uniqueness validation' do
    let(:institution) { Fabricate :institution }
    context 'for current password' do
      it 'is not valid when a record has the same current_password as
          another' do
        inst = Fabricate.build(:institution,
                               current_password: institution.current_password)
        expect(inst.valid?).to be_falsey
        expect(inst.errors[:current_password].first).to include institution.code
      end
      it 'is not valid when a record has the same current_password as another
          institutions new_password' do
        inst = Fabricate.build(:institution,
                               current_password: institution.new_password)
        expect(inst.valid?).to be_falsey
        expect(inst.errors[:current_password].first).to include institution.code
      end
      it 'is valid with a globally unique current_password' do
        institution # init
        inst = Fabricate.build(:institution,
                               current_password: 'password')
        expect(inst.valid?).to be_truthy
      end
    end
    context 'for new password' do
      it 'is not valid when a record has the same new_password as another' do
        inst = Fabricate.build(:institution,
                               new_password: institution.new_password)
        expect(inst.valid?).to be_falsey
        expect(inst.errors[:new_password].first).to include institution.code
      end
      it 'is not valid when a record has the same new_password as another
          institutions current_password' do
        inst = Fabricate.build(:institution,
                               new_password: institution.current_password)
        expect(inst.valid?).to be_falsey
        expect(inst.errors[:new_password].first).to include institution.code
      end
      it 'is valid with a globally unique new_password' do
        institution # init
        inst = Fabricate.build(:institution,
                               new_password: 'password')
        expect(inst.valid?).to be_truthy
      end
    end
  end
  context 'ip_addresses uniqueness validation' do
    it 'is not valid when a record has ip ranges that have ip addresses registered to another institution' do
      Fabricate :institution, code: 'ins1', ip_addresses: %w[1.1.1.50-100]
      Fabricate :institution, code: 'ins2', ip_addresses: %w[2.2.2.0-50]
      inst = Fabricate.build(:institution,
                             code: 'test',
                             ip_addresses: %w[3.3.3.* 2.2.2.25-75 1.1.1.75-125])
      expect(inst.valid?).to eq false
      expect(inst.errors[:ip_addresses]).to include('2.2.2.25-75 contains an ip address in 2.2.2.0-50 that is registered to ins2',
                                                    '1.1.1.75-125 contains an ip address in 1.1.1.50-100 that is registered to ins1')
    end
    it 'is valid when the institution is not active and has ip ranges that have ip addresses registered to another institution' do
      Fabricate :institution, code: 'ins1', ip_addresses: %w[1.1.1.50-100]
      Fabricate :institution, code: 'ins2', ip_addresses: %w[2.2.2.0-50]
      inst = Fabricate.build(:institution,
                             code: 'test',
                             active: false,
                             ip_addresses: %w[3.3.3.* 2.2.2.25-75 1.1.1.75-125])
      expect(inst.valid?).to eq true
    end
    it 'is valid valid when the institution is active and has ip ranges that have ip addresses registered to inactive institutions' do
      Fabricate :institution, code: 'ins1', active: false, ip_addresses: %w[1.1.1.50-100]
      Fabricate :institution, code: 'ins2', active: false, ip_addresses: %w[2.2.2.0-50]
      inst = Fabricate.build(:institution,
                             code: 'test',
                             ip_addresses: %w[3.3.3.* 2.2.2.25-75 1.1.1.75-125])
      expect(inst.valid?).to eq true
    end
    it 'is not valid when a record is set to active with ip ranges that have ip addresses registered to another institution' do
      inst = Fabricate.build(:institution,
                             code: 'test',
                             active: false,
                             ip_addresses: %w[3.3.3.* 2.2.2.25-75 1.1.1.75-125])
      Fabricate :institution, code: 'ins1', ip_addresses: %w[1.1.1.50-100]
      Fabricate :institution, code: 'ins2', ip_addresses: %w[2.2.2.0-50]
      expect(inst.valid?).to eq true
      inst.active = true
      expect(inst.valid?).to eq false
      expect(inst.errors[:ip_addresses]).to include('2.2.2.25-75 contains an ip address in 2.2.2.0-50 that is registered to ins2',
                                                    '1.1.1.75-125 contains an ip address in 1.1.1.50-100 that is registered to ins1')
    end
    it 'is valid with a unique ip_address ranges' do
      inst = Fabricate.build(:institution,
                             code: 'test',
                             ip_addresses: %w[1.1.1.0-49 2.2.2.51-101 3.3.3.*])
      expect(inst.valid?).to eq true
    end
  end
  context 'notify dates logic' do
    describe '#next_change_date' do
      it 'provides the next change date' do
        inst = Fabricate(:institution, change_dates: %w[0101 0601 1201])
        expect(inst.next_change_date('0603')).to eq '1201'
        expect(inst.next_change_date('1201')).to eq '0101'
      end
      it 'provides the next change date from the NotifyGroup' do
        inst = Fabricate(:institution,
                         notify_group:
                           Fabricate(:notify_group,
                                     change_dates: %w[0130 0605 0912]))
        expect(inst.next_change_date('0603')).to eq '0605'
      end
      it 'provides the next change date if today is a change date' do
        inst = Fabricate(:institution, change_dates: %w[0130 0605 0912])
        expect(inst.next_change_date('0605')).to eq '0912'
      end
    end
    describe '#next_next_change_date' do
      it 'provides the date after the next change date' do
        inst = Fabricate(:institution, change_dates: %w[0101 0601 1201])
        expect(inst.next_next_change_date('0603')).to eq '0101'
        expect(inst.next_next_change_date('1201')).to eq '0601'
      end
    end
  end
  context 'OpenAthens configuration validation' do
    context 'with OpenAthens true but no other details' do
      let(:institution) do
        Fabricate.build :institution, open_athens: true
      end
      it 'is invalid' do
        expect(institution).not_to be_valid
        expect(institution.errors[:open_athens_scope]).to include "can't be blank"
        expect(institution.errors[:open_athens_org_id]).to include "can't be blank"
        expect(institution.errors[:open_athens_entity_id]).to include "can't be blank"
      end
    end
    context 'with OpenAthens true with a subscope value but not a scope' do
      let(:institution) do
        Fabricate.build :institution,
                        open_athens: true,
                        open_athens_subscope: 'subscope',
                        open_athens_org_id: 'org_id',
                        open_athens_entity_id: 'entity_id'
      end
      it 'is invalid' do
        expect(institution).not_to be_valid
        expect(institution.errors[:open_athens_scope]).to include "can't be blank"
      end
    end
    context 'with OpenAthens true and duplicate subscope values' do
      let(:institution) do
        Fabricate :institution,
                  open_athens: true,
                  open_athens_scope: 'scope',
                  open_athens_subscope: 'subscope',
                  open_athens_org_id: 'org_id',
                  open_athens_entity_id: 'entity_id'
      end
      it 'is invalid' do
        institution2 =
          Fabricate.build :institution,
                          open_athens: true,
                          open_athens_scope: 'scope',
                          open_athens_subscope: institution.open_athens_subscope,
                          open_athens_org_id: 'org_id2',
                          open_athens_entity_id: 'entity_id'
        expect(institution2).not_to be_valid
        expect(institution2.errors[:open_athens_subscope]).to include 'has already been taken'
      end
      context 'unique composite OA config' do
        it 'is valid if the OA config is unique under the Scope, with an empty
            Sub-Scope' do
          scope = institution.open_athens_scope
          institution2 =
            Fabricate.build :institution,
                            open_athens: true,
                            open_athens_scope: scope,
                            open_athens_subscope: '',
                            open_athens_org_id: 'org_id2',
                            open_athens_entity_id: 'entity_id'
          expect(institution2).to be_valid
        end
        it 'is invalid if the OA config not unique under the Scope' do
          scope = institution.open_athens_scope
          institution2 =
            Fabricate.build :institution,
                            open_athens: true,
                            open_athens_scope: scope,
                            open_athens_subscope: institution.open_athens_subscope,
                            open_athens_org_id: 'org_id2',
                            open_athens_entity_id: 'entity_id'
          expect(institution2).not_to be_valid
          expect(institution2.errors[:open_athens_subscope]).to include 'has already been taken'
        end
      end
    end
  end
  context 'IP cache' do
    let(:memory_store) { ActiveSupport::Cache.lookup_store(:memory_store) }
    let(:cache) { Rails.cache }

    before do
      allow(Rails).to receive(:cache).and_return(memory_store)
      Rails.cache.clear
    end
    describe 'On Create' do
      it 'has all its ip addresses in the cache' do
        inst = Fabricate :institution, ip_addresses: %w[1.0.0.0 1.0.0.1]
        expect(IpService.search('1.0.0.0')).to eq inst.code
        expect(IpService.search('1.0.0.1')).to eq inst.code
        expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.1]
      end
    end
    describe 'On Destroy' do
      it 'has all its ip addresses removed from cache the cache' do
        inst = Fabricate :institution, ip_addresses: %w[1.0.0.0 1.0.0.1]
        expect(IpService.search('1.0.0.0')).to eq inst.code
        expect(IpService.search('1.0.0.1')).to eq inst.code
        expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.1]

        Institution.destroy(inst.id)
        expect(IpService.search('1.0.0.0')).to eq nil
        expect(IpService.search('1.0.0.1')).to eq nil

      end
    end

    describe 'On Update' do
      let(:inst) do
        Fabricate :institution, id: 1, ip_addresses: %w[1.0.0.0 1.0.0.1]
      end
      it "has it's updated ip addresses reflected in the cache" do
        inst.update(ip_addresses: %w[1.0.0.0 1.0.0.2])
        expect(IpService.search('1.0.0.0')).to eq inst.code
        expect(IpService.search('1.0.0.1')).to eq nil
        expect(IpService.search('1.0.0.2')).to eq inst.code
        expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.2]
      end
    end
    describe 'On Active' do
      let(:inst) do
        Fabricate :institution, id: 1, active: false, ip_addresses: %w[1.0.0.0 1.0.0.1]
      end
      it "has it's ip addresses added to the cache" do
        expect(IpService.search('1.0.0.0')).to eq nil
        expect(IpService.search('1.0.0.1')).to eq nil
        inst.update(active: true)
        expect(IpService.search('1.0.0.0')).to eq inst.code
        expect(IpService.search('1.0.0.1')).to eq inst.code
        expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.1]
      end
      it 'has the correct ips in the cache when ips are also updated' do
        expect(IpService.search('1.0.0.0')).to eq nil
        expect(IpService.search('1.0.0.1')).to eq nil
        expect(IpService.search('1.0.0.2')).to eq nil

        inst.update(active: true, ip_addresses: %w[1.0.0.0 1.0.0.2])
        expect(IpService.search('1.0.0.0')).to eq inst.code
        expect(IpService.search('1.0.0.1')).to eq nil
        expect(IpService.search('1.0.0.2')).to eq inst.code
        expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.2]
      end
    end
    describe 'On Inactive' do
      it "has it's ip addresses removed from the cache" do
        inst = Fabricate :institution, active: true, ip_addresses: %w[1.0.0.0 1.0.0.1]
        expect(IpService.search('1.0.0.0')).to eq inst.code
        expect(IpService.search('1.0.0.1')).to eq inst.code

        inst.update(active: false)
        expect(IpService.search('1.0.0.0')).to eq nil
        expect(IpService.search('1.0.0.1')).to eq nil
        expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.1]
      end
      it 'has all ip addresses removed from the cache when ips updated at same time' do
        inst = Fabricate :institution, active: true, ip_addresses: %w[1.0.0.0 1.0.0.1]
        expect(IpService.search('1.0.0.0')).to eq inst.code
        expect(IpService.search('1.0.0.1')).to eq inst.code
        expect(IpService.search('1.0.0.2')).to eq nil

        inst.update(active: false, ip_addresses: %w[1.0.0.0 1.0.0.2])
        expect(IpService.search('1.0.0.0')).to eq nil
        expect(IpService.search('1.0.0.1')).to eq nil
        expect(IpService.search('1.0.0.2')).to eq nil
        expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.2]
      end
    end
    describe 'On ip_authentication true' do
      describe 'Institution is active' do
        it "has it's ip addresses added to the cache" do
          inst = Fabricate :institution, id: 1, active: true, ip_authentication: false, ip_addresses: %w[1.0.0.0 1.0.0.1]
          expect(IpService.search('1.0.0.0')).to eq nil
          expect(IpService.search('1.0.0.1')).to eq nil
          inst.update(ip_authentication: true)
          expect(IpService.search('1.0.0.0')).to eq inst.code
          expect(IpService.search('1.0.0.1')).to eq inst.code
          expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.1]
        end
        it 'has the correct ips in the cache when ips are also updated' do
          inst = Fabricate :institution, id: 1, active: true, ip_authentication: false, ip_addresses: %w[1.0.0.0 1.0.0.1]
          expect(IpService.search('1.0.0.0')).to eq nil
          expect(IpService.search('1.0.0.1')).to eq nil
          expect(IpService.search('1.0.0.2')).to eq nil

          inst.update(ip_authentication: true, ip_addresses: %w[1.0.0.0 1.0.0.2])
          expect(IpService.search('1.0.0.0')).to eq inst.code
          expect(IpService.search('1.0.0.1')).to eq nil
          expect(IpService.search('1.0.0.2')).to eq inst.code
          expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.2]
        end
      end
      describe 'Institution is inactive' do
        it "doesn't have ip addresses added to the cache" do
          inst = Fabricate :institution, id: 1, active: false, ip_authentication: false, ip_addresses: %w[1.0.0.0 1.0.0.1]
          expect(IpService.search('1.0.0.0')).to eq nil
          expect(IpService.search('1.0.0.1')).to eq nil
          inst.update(ip_authentication: true)
          expect(IpService.search('1.0.0.0')).to eq nil
          expect(IpService.search('1.0.0.1')).to eq nil
          expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.1]
        end
      end
    end
    describe 'On ip_authentication false' do
      describe 'Institution is active' do
        it "has it's ip addresses removed from the cache" do
          inst = Fabricate :institution, id: 1, active: true, ip_authentication: true, ip_addresses: %w[1.0.0.0 1.0.0.1]
          expect(IpService.search('1.0.0.0')).to eq inst.code
          expect(IpService.search('1.0.0.1')).to eq inst.code
          inst.update(ip_authentication: false)
          expect(IpService.search('1.0.0.0')).to eq nil
          expect(IpService.search('1.0.0.1')).to eq nil
          expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.1]
        end
        it 'has all ip addresses removed from the cache when ips updated at same time' do
          inst = Fabricate :institution, active: true, ip_authentication: true, ip_addresses: %w[1.0.0.0 1.0.0.1]
          expect(IpService.search('1.0.0.0')).to eq inst.code
          expect(IpService.search('1.0.0.1')).to eq inst.code
          expect(IpService.search('1.0.0.2')).to eq nil

          inst.update(ip_authentication: false, ip_addresses: %w[1.0.0.0 1.0.0.2])
          expect(IpService.search('1.0.0.0')).to eq nil
          expect(IpService.search('1.0.0.1')).to eq nil
          expect(IpService.search('1.0.0.2')).to eq nil
          expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.2]
        end
        it "doesn't have ip addresses added to the cache when ips updated" do
          inst = Fabricate :institution, active: true, ip_authentication: false, ip_addresses: %w[1.0.0.0]
          expect(IpService.search('1.0.0.0')).to eq nil

          inst.update(ip_addresses: %w[1.0.0.1 1.0.0.2])
          expect(IpService.search('1.0.0.1')).to eq nil
          expect(IpService.search('1.0.0.2')).to eq nil
          expect(inst.ip_addresses).to eq %w[1.0.0.1 1.0.0.2]
        end
      end
      describe 'Institution is inactive' do
        it "doesn't have ip addresses added to the cache" do
          inst = Fabricate :institution, id: 1, active: false, ip_authentication: false, ip_addresses: %w[1.0.0.0 1.0.0.1]
          expect(IpService.search('1.0.0.0')).to eq nil
          expect(IpService.search('1.0.0.1')).to eq nil
          inst.update(ip_authentication: true)
          expect(IpService.search('1.0.0.0')).to eq nil
          expect(IpService.search('1.0.0.1')).to eq nil
          expect(inst.ip_addresses).to eq %w[1.0.0.0 1.0.0.1]
        end
      end
    end

  end
  context 'logo' do
    subject { Fabricate :institution_with_logo }
    it 'can have a logo attached' do
      expect(subject.logo).to be_an_instance_of(ActiveStorage::Attached::One)
    end

    it 'can have a logo_thumbnail if logo attached' do
      expect(subject.logo_thumbnail).to be_an_instance_of(ActiveStorage::Variant).or(
                                        be_an_instance_of ActiveStorage::VariantWithRecord)
    end
  end
  context '#vendor_names' do
    it 'returns all vendor names of it\'s resources' do
      inst = Fabricate :institution
      Fabricate(:resource, vendor_name: 'test1', institution_id: inst.id)
      Fabricate(:resource, vendor_name: 'test2', institution_id: inst.id)
      expect(inst.vendor_names).to contain_exactly('test1', 'test2')
    end
  end
  context 'display features' do
    let(:institution_group_with_all_features) do
      Fabricate(:institution_group_with_all_features)
    end
    context 'with no local features' do
      let(:institution) do
        Fabricate :institution,
                  institution_group: institution_group_with_all_features
      end
      it 'has all features from the InstitutionGroup' do
        expect(institution.features).to be_empty
        expect(institution.institution_group).to be_an_instance_of InstitutionGroup
        expect(institution.display_features['default']).to eq(
          institution.institution_group.features.map(&:to_a)
        )
      end
    end
    context 'with some local features' do
      let(:institution) do
        Fabricate(:institution,
                  features: Fabricate.times(3, :feature) do
                    position { sequence(nil, 1) }
                  end,
                  institution_group: institution_group_with_all_features)
      end
      it 'has the first 3 features from itself, and the rest from the
          InstitutionGroup' do
        (1..3).each do |i|
          display_feature = institution.display_features['default'][i - 1]
          group_feature = institution.institution_group.features
                                     .find_by(position: i, view_type: 'default')
          expect(display_feature).not_to eq group_feature.to_a
        end
        (4..Feature::FEATURE_COUNT).each do |i|
          display_feature = institution.display_features['default'][i - 1]
          group_feature = institution.institution_group.features
                                     .find_by(position: i, view_type: 'default')
          expect(display_feature).to eq group_feature.to_a
        end
      end
    end
    context 'with all local features' do
      let(:institution) do
        Fabricate(:institution, institution_group: institution_group_with_all_features) do
          features do
            (1..Feature::FEATURE_COUNT).map { |i| Fabricate :feature, position: i }
          end
        end
      end
      it 'has all features from the Institution' do
        (1..Feature::FEATURE_COUNT).each do |i|
          display_feature = institution.display_features[i - 1]
          group_feature = institution.institution_group.features
                                     .find_by(position: i)
          expect(display_feature).not_to eq group_feature
        end
      end
      it 'removes extra spaces from name when saved' do
        institution = Fabricate.build(:institution, name: '     Institution      Test        Name   ')
        institution.save
        expect(institution.name).to eq('Institution Test Name')
      end
    end
  end
end
