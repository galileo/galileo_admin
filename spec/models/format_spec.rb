# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Format, type: :model do
  context 'attributes' do
    subject(:format) { Fabricate :format }
    context 'basic attributes' do
      it 'has a name' do
        expect(format).to respond_to :name
      end
      it 'can return a set of Resources' do
        format.resources << Fabricate.times(2, :resource)
        expect(format).to respond_to :resources
        expect(format.resources.count).to eq 2
        expect(format.resources.first).to be_a Resource
        expect(format.resources.last).to eq Resource.last
      end
    end
  end
  context 'validations' do
    it 'is invalid without a name' do
      format = Format.new(name: nil)
      format.valid?
      expect(format.errors[:name]).to include(I18n.t('errors.messages.blank'))
    end
    it 'is invalid without a unique name' do
      format = Fabricate :format
      another_format = Fabricate.build(:format, name: format.name)
      another_format.valid?
      expect(another_format.errors[:name]).to include(
        I18n.t('errors.messages.taken')
      )
    end
  end
end
