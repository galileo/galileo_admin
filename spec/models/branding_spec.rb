# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Branding, type: :model do
  context 'attributes' do
    subject do
      Fabricate(:branding, name: 'Test branding')
    end

    it 'can have related resources' do
      subject.resources << Fabricate(:resource, institution: subject.institution)
      expect(subject.resources.length).to eq 1
      expect(subject.resources.first).to be_a Resource
    end

    it 'is valid with an institution code and a name' do
      expect(subject).to be_valid
    end

    it 'is invalid with name blank' do
      subject.name = ''
      subject.valid?
      expect(subject.errors[:name])
        .to include(I18n.t('errors.messages.blank'))
    end
  end

  context '#image' do
    subject { Fabricate :branding_with_image }
    it 'can have a image attached' do
      expect(subject.image).to be_an_instance_of(ActiveStorage::Attached::One)
    end
  end

end

