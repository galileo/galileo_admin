require 'rails_helper'

def all_keys_from_param_permit_args(args)
  # This takes arguments of the form supplied to params.permit() and collapses them into an array of field names.
  # args may be an Array of strings, a Hash with string keys, or both an Array containing one of each of the above.
  all_keys = []
  args = [args] if args.is_a? Hash
  args.each do |item|
    if item.is_a? Array
      all_keys += item
    elsif item.is_a? Hash
      all_keys += item.keys
    else
      all_keys << item
    end
  end
  all_keys
end

RSpec.describe ConfiguredBento, type: :model do

  context 'class methods' do

    describe '#sort_options' do
      it 'contains Recently Created' do
        expect(ConfiguredBento.sort_options).to include(['Recently Created', 'created_at_desc'])
      end
      it 'contains Recently Updated' do
        expect(ConfiguredBento.sort_options).to include(['Recently Updated', 'updated_at_desc'])
      end
    end

    describe '#shown_active_options' do
      it 'contains True' do
        expect(ConfiguredBento.shown_active_options).to include(%w[True true])
      end
      it 'contains False' do
        expect(ConfiguredBento.shown_active_options).to include(%w[False false])
      end
    end

    describe '#shown_default_options' do
      it 'contains True' do
        expect(ConfiguredBento.shown_default_options).to include(%w[True true])
      end
      it 'contains False' do
        expect(ConfiguredBento.shown_default_options).to include(%w[False false])
      end
    end

    context 'with configured bentos' do
      subject :configured_bentos do
        Fabricate.times(3, :configured_bento)
      end

      describe '#bulk_insert' do
        it 'returns nil with no configured bentos' do
          expect(ConfiguredBento.bulk_insert([])).to be_nil
        end
        it 'returns results but does not index them' do
          expect(ConfiguredBento.bulk_insert(configured_bentos, send_to_solr: false).class).to eq(ActiveRecord::Result)
        end
        it 'returns results and indexes them' do
          expect(ConfiguredBento.bulk_insert(configured_bentos).class).to eq(ActiveRecord::Result)
        end
      end

      describe '#bulk_destroy' do
        it 'destroys configured bentos' do
          expect(ConfiguredBento.bulk_destroy(configured_bentos)).to eq(3)
        end
      end
    end

    describe '#send_bulk_insert_to_solr' do
      it 'calls Sunspot.index!' do
        expect(Sunspot).to receive(:index!).and_return("Configured bentos indexed")
        expect(ConfiguredBento.send_bulk_insert_to_solr([])).to eq("Configured bentos indexed")
      end
      it 'calls Sunspot.index! and raises exception' do
        expect(Sunspot).to receive(:index!).exactly(ConfiguredBento::BULK_SOLR_OPERATION_RETRY_COUNT).times.and_raise(RSolr::Error::ConnectionRefused)
        expect(ConfiguredBento.send_bulk_insert_to_solr([])).to be_nil
      end
    end

    describe '#send_bulk_delete_to_solr' do
      it 'calls Sunspot.remove_by_id!' do
        expect(Sunspot).to receive(:remove_by_id!).and_return("Configured bentos deleted")
        expect(ConfiguredBento.send_bulk_delete_to_solr([])).to eq("Configured bentos deleted")
      end
      it 'calls Sunspot.remove_by_id! and raises exception' do
        expect(Sunspot).to receive(:remove_by_id!).exactly(ConfiguredBento::BULK_SOLR_OPERATION_RETRY_COUNT).times.and_raise(RSolr::Error::ConnectionRefused)
        expect(ConfiguredBento.send_bulk_delete_to_solr([])).to be_nil
      end
    end

    describe '#supported_customizations' do
      it 'returns eds_custom customizations' do
        customizations = ConfiguredBento.supported_customizations('eds_custom')
        expect(all_keys_from_param_permit_args(customizations).any?).to be_truthy
      end
      it 'returns eds_resource customizations' do
        customizations = ConfiguredBento.supported_customizations('eds_resource')
        expect(all_keys_from_param_permit_args(customizations).any?).to be_truthy
      end
      it 'returns no customizations otherwise' do
        customizations = ConfiguredBento.supported_customizations('nonexistent_bento')
        expect(all_keys_from_param_permit_args(customizations).any?).to be_falsey
      end
    end

    describe '#created_by_states' do
      it 'returns created by states' do
        expect(ConfiguredBento.created_by_states).to include(%w[GALILEO galileo])
      end
    end

    describe '#slug_cache_for_view' do
      it 'return slug cache' do
        configured_bentos = Fabricate.times(3, :configured_bento)
        allow(ConfiguredBento).to receive(:where).and_return(configured_bentos)
        expect(ConfiguredBento.slug_cache_for_view(1,'')).to be_a Hash
      end
    end
  end

  context 'instance methods' do
    subject :configured_bento do
      Fabricate(:configured_bento)
    end

    describe '#solr_hit' do
      it 'returnes a solr hit' do
        search = Fabricate(:search)
        search.total = 1
        search.hits = ["Test Search Hit 1"]
        allow(ConfiguredBento).to receive(:search).and_return(search)
        expect(subject.solr_hit).to eq("Test Search Hit 1")
      end
    end

    describe '#solr_stored_hash' do
      it 'returns data from the hit stored values' do
        hit = Fabricate(:hit)
        hit.stored_values = "test hit"
        expect(subject.solr_stored_hash(hit)).to eq("test hit")
      end
    end

    describe '#merge_customizations' do
      it 'returns empty hash(.to_json) if no customizations' do
        expect(subject.merge_customizations).to eq("{}")
      end
      it 'returns merged customizations' do
        subject.predefined_bento = Fabricate(:predefined_bento_with_definition)
        subject.customizations = {boy: :girl}
        expect(subject.merge_customizations).to eq("{\"Source Type Facet\":[\"Academic Journals\",\"Conference Materials\"],\"Additional Limiters\":\"NOT PT eBook\",\"boy\":\"girl\"}")
      end
    end

    describe '#computed_display_name' do
      it 'returns this bento\'s display_name' do
        expect(subject.computed_display_name).to eq('Test Configured Bento')
      end
      it 'returns default display_name' do
        subject.display_name = nil
        expect(subject.computed_display_name).to eq('Test Predefined Bento')
      end
    end

    describe '#computed_description' do
      it 'returns default description' do
        expect(subject.computed_description).to eq('This is a test configured bento')
      end
      it 'returns this bento\'s description' do
        subject.description = nil
        expect(subject.computed_description).to eq('This is a test predefined bento')
      end
    end

    describe '#display_user_view' do
      it 'returns the name of the user view' do
        subject.user_view = 'educator'
        expect(subject.display_user_view).to eq('Educator')
      end
    end

    describe '#default_slug' do
      it 'returns the configured bento (generated) slug' do
        subject.display_name = "Test Configured Bento"
        expect(subject.default_slug).to eq("test-configured-bento")
      end
      it 'returns the predefined bento code' do
        subject.display_name = nil
        subject.predefined_bento.code = "test"
        expect(subject.default_slug).to eq("test")
      end
    end

    describe '#slug_unique?' do
      # caution: triple negatives ahead
      it '(is unique) find configured bento with that test_slug and that id' do
        subject.slug_cache = nil
        allow(ConfiguredBento).to receive_message_chain( :where, :where, :not, :none? ) { true }
        expect(subject.slug_unique?('bad_slug')).to be_truthy
      end
      it '(is not unique) find configured bento with that test_slug but not that id' do
        subject.slug_cache = nil
        allow(ConfiguredBento).to receive_message_chain( :where, :where, :not, :none? ) { false }
        expect(subject.slug_unique?('good_slug')).to be_falsey
      end
      it '(is unique) finds test_slug in slug_cache with this bento\'s id' do
        subject.slug_cache = { subject.slug => subject.id }
        expect(subject.slug_unique?(subject.slug)).to be_truthy
      end
      it '(is unique) does not find test_slug in slug_cache' do
        subject.slug_cache = { elvis: :presley }
        expect(subject.slug_unique?(subject.slug)).to be_truthy
      end
      it '(is not unique) finds test_slug in slug_cache with different id' do
        subject.slug_cache = { subject.slug => subject.id+1 }
        expect(subject.slug_unique?(subject.slug)).to be_falsey
      end
    end

    describe '#slug_might_change?' do
      it 'might change if slug is nil' do
        subject.slug = nil
        expect(subject.slug_might_change?).to be_truthy
      end
      it 'might change if display name was changed' do
        subject.slug = 'not nil'
        allow(subject).to receive(:display_name_changed?).and_return(true)
        expect(subject.slug_might_change?).to be_truthy
      end
      it 'might change if display name is nil and predefined bento id was changed' do
        subject.slug = 'not nil'
        allow(subject).to receive(:display_name_changed?).and_return(false)
        subject.display_name = nil
        allow(subject).to receive(:predefined_bento_id_changed?).and_return(true)
        expect(subject.slug_might_change?).to be_truthy
      end
      it 'might not change if display name is not nil' do
        subject.slug = 'not nil'
        allow(subject).to receive(:display_name_changed?).and_return(false)
        subject.display_name = 'not nil'
        allow(subject).to receive(:predefined_bento_id_changed?).and_return(true)
        expect(subject.slug_might_change?).to be_falsey
      end
      it 'might not change if predefined bento id was not changed' do
        subject.slug = 'not nil'
        allow(subject).to receive(:display_name_changed?).and_return(false)
        subject.display_name = nil
        allow(subject).to receive(:predefined_bento_id_changed?).and_return(false)
        expect(subject.slug_might_change?).to be_falsey
      end
    end

    describe '#update_slug' do
      it 'returns default_slug, because it is unique' do
        subject.display_name = "Default Slug"
        subject.slug_cache = { subject.default_slug => subject.id } # so slug_unique?:true
        expect(subject.update_slug).to eq('default-slug')
      end
      it 'returns different slug in order to make it unique' do
        subject.display_name = "Default Slug"
        subject.slug_cache = { subject.default_slug => subject.id+1 } # so slug_unique?:false initially
        expect(subject.update_slug).to eq('default-slug-1')
      end
    end

    describe '#created_by_galileo?' do
      it 'is created by galileo if there is a template view bento' do
        allow(subject).to receive(:template_view_bento).and_return(true)
        expect(subject.created_by_galileo?).to be_truthy
      end
      it 'otherwise, it is not created by galileo' do
        allow(subject).to receive(:template_view_bento).and_return(false)
        expect(subject.created_by_galileo?).to be_falsey
      end
    end

    describe '#created_by_institution?' do
      it 'is created by institution if there is NOT a template view bento' do
        allow(subject).to receive(:template_view_bento).and_return(false)
        expect(subject.created_by_institution?).to be_truthy
      end
      it 'otherwise, it is not created by institution' do
        allow(subject).to receive(:template_view_bento).and_return(true)
        expect(subject.created_by_institution?).to be_falsey
      end
    end

    describe '#created_by' do
      it 'is created by galileo if there is a template view bento' do
        allow(subject).to receive(:template_view_bento).and_return(true)
        expect(subject.created_by).to eq('GALILEO')
      end
      it 'otherwise, it is created by institution' do
        allow(subject).to receive(:template_view_bento).and_return(false)
        expect(subject.created_by).to eq('Institution')
      end
    end

    describe '#predefined_bento_code' do
      it 'returns the predefined bento code' do
        subject.predefined_bento.code = "test"
        expect(subject.predefined_bento_code).to eq('test')
      end
    end

    describe '#predefined_bento_code=' do
      it 'sets the predefined bento based on code' do
        allow(PredefinedBento).to receive_message_chain(:find_by, :id).and_return(99)
        subject.predefined_bento_code = "test"
        expect(subject.predefined_bento_id).to eq(99)
      end
    end

    describe '#service_matches? and #validate_service_matches' do
      it 'matches if no predefined_bento' do
        subject.predefined_bento = nil
        expect(subject.service_and_prereqs_match?).to be_truthy
      end
      it 'matches if bento_config is nil and service does not require a config' do
        pending "resolution of ArgumentError: wrong number of arguments (given 0, expected 2..3)"
        subject.bento_config = nil
        service = Fabricate(:bento_service)
        service.config_fields = nil
        allow(BentoService).to receive(:get) { service }
        expect(subject.service_and_prereqs_match?).to be_truthy
      end
    end

  end

  context 'attributes' do
    context 'validations' do
      subject :configured_bento do
        Fabricate(:configured_bento)
      end

      it 'is valid with an institution, a predefined_bento, a user_view, a display_name, and a slug' do
        expect(subject).to be_valid
      end

      it 'is invalid without a institution' do
        subject.institution = nil
        subject.valid?
        expect(subject.errors[:institution]).to include("must exist")
      end

      it 'is invalid without a predefined_bento' do
        subject.predefined_bento = nil
        subject.valid?
        expect(subject.errors[:predefined_bento]).to include("must exist")
      end

      it 'is invalid without a user_view' do
        subject.user_view = ''
        subject.valid?
        expect(subject.errors[:user_view]).to include(I18n.t('errors.messages.blank'))
      end

      it 'is invalid without a slug' do
        subject.slug = ''
        subject.valid?
        expect(subject.errors[:slug]).to include(I18n.t('errors.messages.blank'))
      end

      it 'is invalid with an unsupported user_view' do
        subject.user_view = 'unsupported'
        subject.valid?
        expect(subject.errors[:user_view]).to include("'unsupported' is not a supported user view. Allowed user views: elementary, middle, highschool, educator")
      end

      it 'is invalid if predefined bento and bento config services do not match' do
        subject.user_view = 'elementary'
        subject.predefined_bento.service = 'galileo'
        subject.bento_config = Fabricate(:bento_config, service: 'eds_api', view_types: ['elementary'])
        subject.valid?
        expect(subject.errors).to have_key(:predefined_bento)
      end

      it 'is invalid if the predefined bento requires a bento config by a specific name and the names do not match' do
        subject.user_view = 'elementary'
        subject.predefined_bento.service = 'eds_api'
        subject.predefined_bento.service_credentials_name = 'eds_api_foo'
        subject.bento_config = Fabricate(:bento_config, service: 'eds_api', view_types: ['elementary'])
        expect(subject).to be_invalid
      end

      it 'is valid if the predefined bento requires a bento config by a specific name and the bento config has that name' do
        subject.predefined_bento.service = 'eds_api'
        subject.predefined_bento.service_credentials_name = 'eds_api_foo'
        subject.bento_config = Fabricate(:bento_config, service: 'eds_api', view_types: ['elementary'], name: 'eds_api_foo')
        expect(subject).to be_valid
      end

    end
  end
end
