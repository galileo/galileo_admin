# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Password, type: :model do
  context 'attributes' do
    subject :institution do
      Password.new(
        password: 'abcd'
      )
    end
    it 'has basic attributes' do
      expect(subject.password).to eq 'abcd'
    end

    it 'is valid with a password' do
      expect(subject).to be_valid
    end

    it 'is invalid without a password' do
      inst = Password.new(password: nil)
      inst.valid?
      expect(inst.errors[:password])
        .to include(I18n.t('errors.messages.blank'))
    end

    it 'is invalid without a unique password' do
      password = Fabricate :password
      another_password = Fabricate.build(
        :password,
        password: password.password
      )
      another_password.valid?
      expect(another_password.errors[:password]).to include(
        I18n.t('errors.messages.taken')
      )
    end

    describe '#commit!' do
      it 'updates the commit_date field and saves' do
        password = Fabricate :password
        old_commit_date = password.commit_date
        password.commit!
        expect(password.commit_date).not_to eq old_commit_date
      end
    end

    describe '#next_available' do
      it 'returns the next available password provided it has no commit date
          and is not excluded' do
        Fabricate(:password, commit_date: nil, exclude: true)
        Fabricate(:password, commit_date: DateTime.now - 10.days)
        Fabricate(:password, commit_date: DateTime.now - 5.days)
        expected = Fabricate(:password, commit_date: nil, exclude: false)
        expect(Password.next_available).to eq expected
      end
      it 'returns the next available password based on the oldest commit
          date' do
        expected = Fabricate(:password, commit_date: DateTime.now - 10.days, exclude: false)
        Fabricate(:password, commit_date: DateTime.now - 5.days, exclude: false)
        expect(Password.next_available).to eq expected
      end
    end
  end
  context 'default values' do
    subject(:inst) { Password.new(password: 'test') }
    it 'has the correct default values' do
      expect(subject.exclude).to eq false
    end
  end

end
