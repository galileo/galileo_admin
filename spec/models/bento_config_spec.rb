require 'rails_helper'

RSpec.describe BentoConfig, type: :model do

  context 'methods' do
    subject :bento_config do
      Fabricate(:bento_config)
    end

    describe '#solr_hit' do
      it 'returnes a solr hit' do
        search = Fabricate(:search)
        search.total = 1
        search.hits = ["Test Search Hit 1"]
        allow(BentoConfig).to receive(:search).and_return(search)
        expect(subject.solr_hit).to eq("Test Search Hit 1")
      end
    end

    describe '#solr_stored_hash' do
      it 'returns data from the hit stored values' do
        hit = Fabricate(:hit)
        hit.stored_values = "test hit"
        expect(subject.solr_stored_hash(hit)).to eq("test hit")
      end
    end

    describe '#service_types_for_menu' do
      it 'includes, for example, the eds_api type' do
        expect(BentoConfig.service_types_for_menu).to include(["EDS API", "eds_api"])
      end
    end
  
    describe '#display_name' do
      it 'is eds_api' do
        subject.service = 'eds_api'
        subject.credentials = {'API Profile': 'api_profile'}
        expect(subject.display_name).to eq("EDS API (api_profile)")
      end
      it 'is primo' do
        subject.service = 'primo'
        subject.credentials = {'VID': 'vid'}
        expect(subject.display_name).to eq("Primo API (vid)")
      end
      it 'is other' do
        subject.service = 'galileo'
        subject.view_types = ['middle', 'elementary']
        expect(subject.display_name).to eq("GALILEO (middle, elementary)")
      end
    end
  end

  context 'attributes' do
    context 'validations' do
      subject :bento_config do
        Fabricate(:bento_config, service: 'eds_api', view_types: ['middle'])
      end

      it 'is valid with an institution_id, service, a name, and at least one view_type' do
        expect(subject).to be_valid
      end

      it 'is invalid without a institution id' do
        subject.institution_id = nil
        subject.valid?
        expect(subject.errors[:institution_id]).to include(I18n.t('errors.messages.blank'))
      end

      it 'is invalid without a service' do
        subject.service = ''
        subject.valid?
        expect(subject.errors[:service]).to include(I18n.t('errors.messages.blank'))
      end

      it 'is invalid without a valid service' do
        subject.service = 'invalid'
        subject.valid?
        expect(subject.errors[:service].first).to include("'invalid' is not a supported type")
      end

      it 'is valid even with no view_types' do
        subject.view_types = []
        expect(subject).to be_valid
      end

      it 'is invalid if its view_types overlap with another\'s' do
        other = Fabricate.build(:bento_config, service: 'eds_api', view_types: ['middle'])
        expect(other).to_not be_valid
      end

      it 'is invalid with an unsupported  view_type' do
        subject.view_types = ['unsupported']
        subject.valid?
        expect(subject.errors[:view_types]).to include("'unsupported' is not a supported view_type. Allowed types: elementary, middle, highschool, educator")
      end
    end
  end
end
