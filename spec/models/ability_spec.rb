# frozen_string_literal: true

require 'rails_helper'
require 'cancan/matchers'

RSpec.describe Ability, type: :model do
  context 'for an admin user' do
    let(:user) { Fabricate :admin_user }
    subject { Ability.new user }
    it 'can do it all' do
      is_expected.to be_able_to :manage, :all
    end
  end
  context 'for an institutional user' do
    let(:user) { Fabricate :institutional_user_with_institutions }
    let(:test_institution) { user.institutions.first }
    subject { Ability.new user }
    context 'concerning Institutions' do
      it 'can read only those Institutions assigned' do
        is_expected.to be_able_to :index, Institution
        is_expected.to be_able_to :show, test_institution
        is_expected.not_to be_able_to :update, test_institution
        is_expected.not_to be_able_to :delete, test_institution
      end
    end
    context 'concerning Resources' do
      let(:glri_resource) do
        Fabricate :resource, institution: user.institutions.first
      end
      let(:central_resource) { Fabricate :resource }
      it 'can read only GLRI those affiliated with their Institutions' do
        user.institutions.first.glri_resources << glri_resource
        is_expected.to be_able_to :index, Resource
        is_expected.to be_able_to :show, glri_resource
        is_expected.not_to be_able_to :copy, glri_resource
        is_expected.not_to be_able_to :update, glri_resource
        is_expected.not_to be_able_to :destroy, glri_resource
      end
      it 'can view but not copy and modify central Resources' do
        user.institutions.first.resources << central_resource
        is_expected.to be_able_to :index, Resource
        is_expected.to be_able_to :show, central_resource
        is_expected.not_to be_able_to :copy, central_resource
        is_expected.not_to be_able_to :update, central_resource
        is_expected.not_to be_able_to :destroy, central_resource
      end
      it 'can not create any Resource' do
        is_expected.not_to be_able_to :create, glri_resource
        is_expected.not_to be_able_to :create, central_resource
      end
    end
    context 'concerning Allocations' do
      context 'with GLRI resource' do
        let(:glri_resource) do
          Fabricate :resource, institution: user.institutions.first, active: true
        end
        it 'can not be managed' do
          allocation = glri_resource.allocations.first
          is_expected.not_to be_able_to :manage, allocation
        end
      end
      context 'with central resource' do
        let(:central_resource) do
          resource = Fabricate :resource
          resource.institutions = user.institutions
          resource
        end
        it 'can not be managed' do
          allocation = central_resource.allocations.first
          is_expected.not_to be_able_to :manage, allocation
        end
      end
      context 'with somebody else\'s resource' do
        let(:others_resource) do
          resource = Fabricate :resource
          resource.institutions = Fabricate.times 1, :institution
          resource
        end
        it 'cannot be managed' do
          allocation = others_resource.allocations.first
          is_expected.not_to be_able_to :manage, allocation
        end
      end
    end
    context 'concerning Features' do
      context 'institution group features' do
        it 'can be read if related to an assigned institution' do
          feature = Fabricate :feature
          test_institution.institution_group.features << feature
          is_expected.to be_able_to :read, feature
          is_expected.not_to be_able_to :modify, feature
        end
        it 'cannot be read if for an unrelated institution group' do
          unassigned_institution = Fabricate :institution
          feature = Fabricate :feature
          unassigned_institution.institution_group.features << feature
          is_expected.not_to be_able_to :read, feature
        end
      end
      context 'institution features' do
        it 'cannot be modified even if related to an assigned institution' do
          feature = Fabricate :feature
          test_institution.features << feature
          is_expected.not_to be_able_to :manage, feature
        end
        it 'cannot be modified if related to an unassigned institution' do
          unassigned_institution = Fabricate :institution
          feature = Fabricate :feature
          unassigned_institution.features << feature
          is_expected.not_to be_able_to :read, feature
          is_expected.not_to be_able_to :modify, feature
        end
      end
    end
  end
  context 'for an institutional advanced subrole user' do
    let(:user) { Fabricate :institutional_advanced_subrole_user_with_institutions }
    let(:test_institution) { user.institutions.first }
    subject { Ability.new user }
    context 'concerning Resources' do
      let(:glri_resource) do
        Fabricate :resource, institution: user.institutions.first
      end
      let(:central_resource) { Fabricate :resource }
      it 'can work with only GLRI those affiliated with their Institutions' do
        user.institutions.first.glri_resources << glri_resource
        is_expected.to be_able_to :index, Resource
        is_expected.to be_able_to :show, glri_resource
        is_expected.to be_able_to :copy, glri_resource
        is_expected.to be_able_to :update, glri_resource
        is_expected.not_to be_able_to :destroy, glri_resource
      end
      it 'can view and copy but not modify central Resources' do
        user.institutions.first.resources << central_resource
        is_expected.to be_able_to :index, Resource
        is_expected.to be_able_to :show, central_resource
        is_expected.to be_able_to :copy, central_resource
        is_expected.not_to be_able_to :update, central_resource
        is_expected.not_to be_able_to :destroy, central_resource
      end
      it 'can create only a GLRI Resource' do
        is_expected.to be_able_to :create, glri_resource
        is_expected.not_to be_able_to :create, central_resource
      end
    end
    context 'concerning Allocations' do
      context 'with GLRI resource' do
        let(:glri_resource) do
          Fabricate :resource, institution: user.institutions.first, active: true
        end
        it 'can be managed' do
          allocation = glri_resource.allocations.first
          is_expected.to be_able_to :manage, allocation
        end
      end
      context 'with central resource' do
        let(:central_resource) do
          resource = Fabricate :resource
          resource.institutions = user.institutions
          resource
        end
        it 'can be managed' do
          allocation = central_resource.allocations.first
          is_expected.to be_able_to :manage, allocation
        end
      end
      context 'with somebody else\'s resource' do
        let(:others_resource) do
          resource = Fabricate :resource
          resource.institutions = Fabricate.times 1, :institution
          resource
        end
        it 'cannot be managed' do
          allocation = others_resource.allocations.first
          is_expected.not_to be_able_to :manage, allocation
        end
      end
    end
  end
  context 'for an portal institutional user' do
    let(:user) { Fabricate :institutional_portal_subrole_user_with_institutions }
    let(:test_institution) { user.institutions.first }
    subject { Ability.new user }
    context 'concerning Features' do
      context 'institution group features' do
        it 'can be read if related to an assigned institution' do
          feature = Fabricate :feature
          test_institution.institution_group.features << feature
          is_expected.to be_able_to :read, feature
          is_expected.not_to be_able_to :modify, feature
        end
        it 'cannot be read if for an unrelated institution group' do
          unassigned_institution = Fabricate :institution
          feature = Fabricate :feature
          unassigned_institution.institution_group.features << feature
          is_expected.not_to be_able_to :read, feature
        end
      end
      context 'institution features' do
        it 'can be modified if related to an assigned institution' do
          feature = Fabricate :feature
          test_institution.features << feature
          is_expected.to be_able_to :manage, feature
        end
        it 'cannot be modified if related to an unassigned institution' do
          unassigned_institution = Fabricate :institution
          feature = Fabricate :feature
          unassigned_institution.features << feature
          is_expected.not_to be_able_to :read, feature
          is_expected.not_to be_able_to :modify, feature
        end
      end
    end
  end
  context 'for a helpdesk user' do
    let(:user) { Fabricate :helpdesk_user }
    subject { Ability.new user }
    it 'can show, create and modify Contacts' do
      is_expected.to be_able_to :read, Contact
      is_expected.to be_able_to :create, Contact
      is_expected.to be_able_to :update, Contact
      is_expected.to be_able_to :destroy, Contact
    end
    it 'can diff but not rollback Contacts' do
      is_expected.to be_able_to :diff, Contact
      is_expected.not_to be_able_to :rollback, Contact
    end
    it 'can view Versions of Contacts but not undelete' do
      contact = Fabricate :contact
      contact.email = Faker::Internet.email
      contact.save
      version = contact.versions.first
      is_expected.to be_able_to :read, version, item_type: 'Contact'
      is_expected.not_to be_able_to :undelete, version
    end
    it 'can view Institutions' do
      is_expected.to be_able_to :read, Institution
    end
  end
end