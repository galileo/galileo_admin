# frozen_string_literal: true

require 'rails_helper'

RSpec.describe NotifyGroup, type: :model do
  context 'attributes' do
    subject :notify_group do
      NotifyGroup.new(
        code: 'abcd',
        name: 'Test NotifyGroup',
        legacy_data: '{}'
      )
    end
    it 'has basic attributes' do
      expect(subject.code).to eq 'abcd'
      expect(subject.name).to eq 'Test NotifyGroup'
    end

    it 'is valid with a code and a name' do
      expect(subject).to be_valid
    end

    it 'is invalid without a code' do
      notify = NotifyGroup.new(code: nil)
      notify.valid?
      expect(notify.errors[:code])
        .to include(I18n.t('errors.messages.blank'))
    end

    it 'is invalid without a name' do
      notify = NotifyGroup.new(name: nil)
      notify.valid?
      expect(notify.errors[:name]).to include(I18n.t('errors.messages.blank'))
    end

    it 'is invalid without a unique code' do
      notify = Fabricate :notify_group
      another_notify = Fabricate.build(
        :notify_group,
        code: notify.code
      )
      another_notify.valid?
      expect(another_notify.errors[:code]).to include(
        I18n.t('errors.messages.taken')
      )
    end

    it 'can return a set of Institutions' do
      notify = Fabricate :notify_group do
        institutions(count: 2)
      end
      expect(notify).to respond_to :institutions
      expect(notify.institutions.count).to eq 2
      expect(notify.institutions.first).to be_a Institution
      expect(notify.institutions.last).to eq Institution.last
    end

    it 'is valid when change_dates has valid dates' do
      ng = Fabricate :notify_group, change_dates: %w[0907 1007]
      expect(ng).to be_valid
    end

    it 'is valid when notify_dates has an empty array' do
      ng = Fabricate :notify_group, notify_dates: []
      expect(ng).to be_valid
    end

    it 'is not valid when change_dates has invalid months' do
      ng = Fabricate :notify_group
      ng.change_dates = %w[0101 9901 1031]
      ng.valid?
      expect(ng.errors[:change_dates]).to include('has invalid values')
    end

    it 'is not valid when notify_dates has dates with more than 4 characters' do
      ng = Fabricate :notify_group
      ng.notify_dates = %w[0205 01011]
      ng.valid?
      expect(ng.errors[:notify_dates]).to include('has invalid values')
    end

    it 'is not valid when notify_dates has dates with less than 4 characters' do
      ng = Fabricate :notify_group
      ng.notify_dates = %w[01 0109]
      ng.valid?
      expect(ng.errors[:notify_dates]).to include('has invalid values')
    end

  end
end
