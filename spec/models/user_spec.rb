# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  context 'attributes' do
    let(:user) { Fabricate :admin_user }
    it 'has standard attributes' do
      expect(user).to respond_to :email
      expect(user).to respond_to :first_name
      expect(user).to respond_to :last_name
      expect(user).to respond_to :password
      expect(user).to respond_to :role
    end
  end
  context 'related institutions' do
    let(:institutional_user) { Fabricate :institutional_user }
    it 'can have assigned Institutions' do
      institutional_user.institutions = Fabricate.times(2, :institution)
      expect(institutional_user.institutions.count).to eq 2
      expect(institutional_user.institutions.first).to be_a Institution
    end
  end
  context 'validations' do
    it 'is invalid without an email' do
      user = Fabricate.build(:user, email: nil)
      user.valid?
      expect(user.errors[:email]).to include(I18n.t('errors.messages.blank'))
    end
    it 'is invalid without a first name' do
      user = Fabricate.build(:user, first_name: nil)
      user.valid?
      expect(user.errors[:first_name]).to include(I18n.t('errors.messages.blank'))
    end
    it 'is invalid without a last name' do
      user = Fabricate.build(:user, last_name: nil)
      user.valid?
      expect(user.errors[:last_name]).to include(I18n.t('errors.messages.blank'))
    end
    it 'is invalid without a password' do
      user = Fabricate.build(:user, password: nil)
      user.valid?
      expect(user.errors[:password]).to include(I18n.t('errors.messages.blank'))
    end
    it 'is invalid without a role' do
      user = Fabricate.build(:user, role: nil)
      user.valid?
      expect(user.errors[:role]).to include(I18n.t('errors.messages.blank'))
    end
    it 'is invalid with unacceptable role' do
      user = Fabricate.build(:user, role: 'cheeser')
      user.valid?
      expect(user.errors[:role]).to include(I18n.t('errors.messages.inclusion'))
    end
  end
  context 'status' do
    it 'is active when the invitation has been accepted and it has not been deleted' do
      user = Fabricate.build(:user, invitation_accepted_at: Time.now, deleted_at: nil)
      expect(user.status).to eq 'Active'
    end
    it 'is deleted when deleted_at is set' do
      user = Fabricate.build(:user, deleted_at: Time.now)
      expect(user.status).to eq 'Deleted'
    end
    it 'is invited when the invitation has been sent but not invited' do
      user = Fabricate.build(:user, invitation_sent_at: Time.now, invitation_accepted_at: nil)
      expect(user.status).to eq 'Invited'
    end
  end
  context 'subroles' do
    let(:subject) { Fabricate :institutional_user }
    it 'is valid with a valid subrole' do
      subject.subroles = [User::VALID_SUBROLES[:institutional].first]
      expect(subject).to be_valid
    end
    it 'is valid with no subroles' do
      subject.subroles = []
      expect(subject).to be_valid
    end
    it 'is invalid with a bad subrole' do
      subject.subroles = ['bad subrole']
      subject.valid?
      expect(subject.errors.where(:subroles).first.type).to include('invalid subrole for this role')
    end
  end
end
