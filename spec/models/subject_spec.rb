# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Subject, type: :model do
  context 'attributes' do
    subject(:subject) { Fabricate :subject }
    context 'basic attributes' do
      it 'has a name' do
        expect(subject).to respond_to :name
      end
      it 'can return a set of Resources' do
        subject.resources << Fabricate.times(2, :resource)
        expect(subject).to respond_to :resources
        expect(subject.resources.count).to eq 2
        expect(subject.resources.first).to be_a Resource
        expect(subject.resources.last).to eq Resource.last
      end
    end
  end
  context 'validations' do
    it 'is invalid without a name' do
      subject = Format.new(name: nil)
      subject.valid?
      expect(subject.errors[:name]).to include(I18n.t('errors.messages.blank'))
    end
    it 'is invalid without a unique name' do
      subject = Fabricate :format
      another_subject = Fabricate.build(:format, name: subject.name)
      another_subject.valid?
      expect(another_subject.errors[:name]).to include(
        I18n.t('errors.messages.taken')
      )
    end
  end
end
