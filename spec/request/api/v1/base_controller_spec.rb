# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::BaseController, type: :request do
  it 'returns unauthorized if no token' do
    get '/api/v1/ok'
    expect(response.status).to eq 401
  end
  it 'returns ok with valid token' do
    headers = { 'X-User-Token' =>
                  Rails.application.credentials.auth_support_api_token }
    get '/api/v1/ok', params: {}, headers: headers
    expect(response.status).to eq 200
  end
end