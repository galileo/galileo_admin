# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::AuthSupportController, type: :request do
  headers = { 'X-User-Token' =>
                Rails.application.credentials.auth_support_api_token }
  context 'with invalid parameters' do
    it 'responds with a 400 if conflicting parameter values encountered' do
      get '/api/v1/inst', params: { passphrase: 'a', ip: '1' }, headers: headers
      expect(response.status).to eq 400
    end
  end
  context 'using Password' do
    it 'returns Institution information if password is in use' do
      institution = Fabricate :institution
      get '/api/v1/inst',
          params: { passphrase: institution.current_password }, headers: headers
      json = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(json['inst']['code']).to eq institution.code
      expect(json['context']['remote']).to be_truthy
      expect(json['context']['auth_type']).to eq 'passphrase'
    end
    it 'returns a successful request with not found message if Password is not
        in use' do
      get '/api/v1/inst', params: { passphrase: 'blah' }, headers: headers
      json = JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(json['message']).to eq 'Institution not found'
    end
  end
end