# frozen_string_literal: true

module LinkServiceHelpers
  def allocation_for(entities)
    resource = entities[:resource]
    institution = entities[:institution]
    if resource && institution
      resource.institutions << institution
    elsif resource
      resource.institutions << Fabricate(:institution)
    elsif institution
      resource = Fabricate(:resource, institutions: [institution])
    end
    resource.allocations.first
  end
end