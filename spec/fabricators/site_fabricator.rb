# frozen_string_literal: true

Fabricator(:site) do
  institution
  code { Faker::Lorem.unique.characters(number: 4) }
  name { Faker::Company.unique.name }
  site_type 'elem'
  charter false
end

Fabricator(:middle_school_site, from: :site) do
  site_type 'midd'
end

