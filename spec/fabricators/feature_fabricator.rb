# frozen_string_literal: true

Fabricator(:feature) do
  position 1
  view_type 'default'
  link_url { Faker::Internet.url }
  link_label { Faker::Lorem.word }
  link_description { Faker::Lorem.sentence }
  featuring(fabricator: :institution)
end

Fabricator(:feature_with_image, from: :feature) do
  after_build do |config|
    image = {
      io: File.open(Rails.root.join('spec', 'fabricators', 'images', 'test_logo.png')),
      filename: 'test_logo.png',
      content_type: 'image/png'
    }
    config.image.attach(image)
  end
end