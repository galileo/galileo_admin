Fabricator(:configured_bento) do
  institution      { Fabricate :institution_with_k12_group }
  predefined_bento { Fabricate :predefined_bento }
  user_view        "educator"
  slug             "test"
  display_name     "Test Configured Bento"
  description      "This is a test configured bento"
end
