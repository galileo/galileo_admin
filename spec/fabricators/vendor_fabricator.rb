# frozen_string_literal: true

Fabricator(:vendor) do
  name { Faker::Company.name }
  code { Faker::Lorem.unique.word }
end

Fabricator(:vendor_with_logo, from: :vendor) do
  after_build do |vendor|
    vendor.logo.attach(
      io: File.open(Rails.root.join('spec', 'fabricators', 'images', 'test_logo.png')),
      filename: 'test_logo.png', content_type: 'image/png'
    )
  end
end
