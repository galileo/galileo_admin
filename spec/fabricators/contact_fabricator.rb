# frozen_string_literal: true

Fabricator(:contact) do
  first_name { Faker::Name.first_name }
  last_name { Faker::Name.last_name }
  email { Faker::Internet.email }
  phone { Faker::PhoneNumber.phone_number }
  notes 'Here are some notes!'
end

Fabricator(:password_contact, from: :contact) do
  types ['pass']
end
