# frozen_string_literal: true

Fabricator(:bento_service) do
 code 'x'
 display_name 'X'
end
