# frozen_string_literal: true

Fabricator(:institution) do
  code { Faker::Lorem.unique.characters(number: 4) }
  public_code { Faker::Lorem.word }
  name { Faker::Company.unique.name }
  legacy_institution_groups do
    Fabricate.times 1, :institution_group
  end
  institution_group
  current_password { Faker::Lorem.unique.characters(number: 6) }
  new_password { Faker::Lorem.unique.characters(number: 6) }
  prev_password { Faker::Lorem.unique.characters(number: 6) }
  change_dates []
  notify_dates []
  active true
  zip_codes { %w[12345] }
  galileo_ez_proxy_url 'proxy.galileo.edu'
  local_ez_proxy_url 'proxy.inst.edu'
end

Fabricator(:institution_with_notify_group, from: :institution) do
  notify_group
end

Fabricator(:institution_with_password_contact, from: :institution) do
  contacts { [Fabricate(:password_contact)] }
end

Fabricator(:open_athens_institution, from: :institution) do
  open_athens true
  open_athens_scope 'institution.edu'
  open_athens_org_id { Faker::Number.unique.number(digits: 8) }
  open_athens_entity_id 'https://idp.institution.edu/openathens'
end

Fabricator(:institution_with_logo, from: :institution) do
  after_build do |institution|
    institution.logo.attach(
      io: File.open(Rails.root.join('spec', 'fabricators', 'images', 'test_logo.png')),
      filename: 'test_logo.png', content_type: 'image/png'
    )
  end
end

Fabricator(:institution_with_k12_group, from: :institution) do
  institution_group { InstitutionGroup.find_by_code 'k12' }
end

Fabricator(:institution_with_branding, from: :institution) do
  brandings { [Fabricate(:branding_with_image)] }
end

Fabricator(:institution_with_sites, from: :institution) do
  sites { Fabricate.times(2, :site) }
end
