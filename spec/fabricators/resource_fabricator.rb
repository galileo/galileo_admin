# frozen_string_literal: true

Fabricator(:resource) do
  code { Faker::Lorem.unique.characters(number: 4) }
  name { Faker::Lorem.unique.words(number: 4) }
  active true
  open_athens true
  ip_access_url 'https://www.example.org'
  remote_access_url 'https://remote.example.org'
  open_athens_url 'https://openathens.example.org'
end

Fabricator(:resource_with_logo, from: :resource) do
  after_build do |resource|
    resource.logo.attach(
      io: File.open(Rails.root.join('spec', 'fabricators', 'images', 'test_logo.png')),
      filename: 'test_logo.png', content_type: 'image/png'
    )
  end
end

Fabricator(:resource_with_proxy_remote, from: :resource) do
  proxy_remote { true }
end

Fabricator(:glri_resource_with_branding, from: :glri_resource) do
  institution { Fabricate :institution_with_branding }
  branding { |attrs| attrs[:institution].brandings.first }
end

Fabricator(:glri_resource, from: :resource) do
  institution { Fabricate :institution }
  code 'abcd-efgh'
end

Fabricator(:glri_public_resource, from: :glri_resource) do
  bypass_galileo_authentication true
end

Fabricator(:core_resource, from: :resource) do
  institution { nil }
end

Fabricator(:central_resource, from: :resource) do
  institution { nil }
end

Fabricator(:central_public_resource, from: :central_resource) do
  bypass_galileo_authentication true
end
