# frozen_string_literal: true

Fabricator(:bento_config) do
  institution { Fabricate :institution_with_k12_group }
  service 'eds_api'
  view_types ['middle']
  credentials do
    {"API Profile": 'api_profile', "User ID": 'user_id', "Password": 'password'}
  end
end
