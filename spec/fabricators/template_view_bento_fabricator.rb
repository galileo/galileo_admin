# frozen_string_literal: true

Fabricator(:template_view_bento) do
  template_name        "k12"
  predefined_bento { Fabricate :predefined_bento }
  user_view        "highschool"
end
