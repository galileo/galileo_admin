# frozen_string_literal: true

Fabricator(:branding) do
  institution
  name { Faker::Company.unique.name }
  text { Faker::TvShows::RickAndMorty.quote }
end

Fabricator(:branding_with_image, from: :branding) do
  after_build do |branding|
    branding.image.attach(
      io: File.open(Rails.root.join('spec', 'fabricators', 'images', 'test_logo.png')),
      filename: 'test_logo.png', content_type: 'image/png'
    )
  end
end