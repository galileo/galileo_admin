# frozen_string_literal: true

Fabricator(:institution_group) do
  code { Faker::Lorem.unique.characters(number: 6) }
  inst_type { 'default' }
end

Fabricator(:institution_group_with_all_features, from: :institution_group) do
  features do
    (1..Feature::FEATURE_COUNT).map { |i| Fabricate :feature, position: i }
  end
end

