# frozen_string_literal: true

class Search

attr_accessor :hits
attr_accessor :total

Fabricator(:search) do
  hits(count: 3) { |attrs, i| "Test Search Hit #{i}" }
  total 3
end

end

class Hit

attr_accessor :stored_values

Fabricator(:hit) do
  stored_values "test hit"
end

end
