# frozen_string_literal: true

Fabricator(:subject) do
  name { Faker::Lorem.unique.word }
end
