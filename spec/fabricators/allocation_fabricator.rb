# frozen_string_literal: true

Fabricator(:allocation) do
  institution { Fabricate :open_athens_institution }
  resource
  display true
end

Fabricator(:central_allocation, from: :allocation) do
  resource {Fabricate :central_resource}
end

Fabricator(:central_public_allocation, from: :allocation) do
  resource {Fabricate :central_public_resource}
end

Fabricator(:allocation_with_overrides, from: :allocation) do
  ip_access_url 'https://www.override.org'
  remote_access_url 'https://remote.override.org'
  open_athens_url 'https://openathens.override.org'
end

Fabricator(:allocation_with_proxy_remote, from: :allocation) do
  resource { Fabricate :resource_with_proxy_remote }
end

Fabricator(:allocation_with_proxy_remote_and_override, from: :allocation) do
  resource { Fabricate :resource_with_proxy_remote }
  remote_access_url 'https://remote.override.org'
end

Fabricator(:allocation_with_credentials, from: :allocation) do
  user_id 'institution'
  password 'password'
end

Fabricator(:allocation_with_branding_image, from: :allocation) do
  resource { Fabricate :glri_resource_with_branding }
end
