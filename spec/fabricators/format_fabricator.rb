# frozen_string_literal: true

Fabricator(:format) do
  name { Faker::Lorem.unique.word }
end
