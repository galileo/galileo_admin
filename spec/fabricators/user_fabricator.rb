# frozen_string_literal: true

Fabricator(:user) do
  email { Faker::Internet.email }
  first_name {Faker::Name.first_name}
  last_name {Faker::Name.last_name}
  password { Faker::Internet.password(min_length: 8) }
  role 'admin'
  subroles []
end

Fabricator :admin_user, from: :user do
  role 'admin'
end

Fabricator :institutional_user, from: :user do
  role 'institutional'
end

Fabricator :institutional_advanced_subrole_user, from: :institutional_user do
  subroles ['advanced']
end

Fabricator :institutional_portal_subrole_user, from: :institutional_user do
  subroles ['portal']
end

Fabricator :helpdesk_user, from: :user do
  role 'helpdesk'
end

Fabricator :institutional_user_with_institutions, from: :institutional_user do
  institutions(count: 2)
end

Fabricator :institutional_advanced_subrole_user_with_institutions, from: :institutional_advanced_subrole_user do
  institutions(count: 2)
end

Fabricator :institutional_portal_subrole_user_with_institutions, from: :institutional_portal_subrole_user do
  institutions(count: 2)
end

Fabricator :institutional_user_with_sites, from: :institutional_user do
  institutions do
    Fabricate.times(2, :institution, sites: Fabricate.times(2, :site))
  end
end

Fabricator :institutional_portal_subrole_user_with_features, from: :institutional_portal_subrole_user do
  institutions do
    Fabricate.times(1, :institution,
                    features: Fabricate.times(1, :feature),
                    institution_group: Fabricate(:institution_group_with_all_features))
  end
end