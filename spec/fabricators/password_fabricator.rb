# frozen_string_literal: true

Fabricator(:password) do
  password { Faker::Hipster.unique.word }
  commit_date { Faker::Date.backward days: 365 }
  user { Faker::FunnyName.name }
end
