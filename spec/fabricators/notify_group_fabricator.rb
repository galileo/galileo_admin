# frozen_string_literal: true

Fabricator(:notify_group) do
  code { Faker::Lorem.unique.characters(number: 6) }
  name { Faker::University.name }
end

