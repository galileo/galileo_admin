# frozen_string_literal: true

Fabricator(:predefined_bento) do
  service 'galileo'
  code 'test'
  display_name 'Test Predefined Bento'
  description 'This is a test predefined bento'
end

Fabricator(:predefined_bento_with_definition, from: :predefined_bento) do
  definition do
    {"Source Type Facet":["Academic Journals","Conference Materials"],"Additional Limiters":"NOT PT eBook"}
  end
end
