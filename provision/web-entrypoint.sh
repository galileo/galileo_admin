#!/bin/bash
groupadd -g 999 gitlab-runner && useradd -u 999 -g 999 gitlab-runner && usermod -d /code gitlab-runner
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | tee /etc/apt/sources.list.d/google-chrome.list
apt-get update -qq && apt-get -y install nodejs sudo google-chrome-stable libvips42
curl 'http://solr:8983/solr/admin/cores?action=CREATE&name=galileo-test&instanceDir=/var/solr&configSet=galileo'
rm -rf vendor && mv /vendor . 
HOME=/code gem install bundler
HOME=/code bundle install --deployment
cp /code/config/database.yml.ci /code/config/database.yml
cp /code/config/sunspot.yml.ci /code/config/sunspot.yml
HOME=/code bundle exec rake db:create RAILS_ENV=test
HOME=/code bundle exec rake db:schema:load RAILS_ENV=test
HOME=/code bundle exec rake db:schema:load:stats RAILS_ENV=test
HOME=/code bundle exec rake db:seed RAILS_ENV=test
chown -R gitlab-runner:gitlab-runner /code /code/.bundle
sudo -E -u gitlab-runner google-chrome-stable --disable-setuid-sandbox --headless --disable-gpu --no-sandbox --remote-debugging-port=9222 http://localhost &
HOME=/code sudo -E -u gitlab-runner bundle exec rspec --color --format documentation
