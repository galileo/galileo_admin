#!/bin/bash
docker-compose build
docker-compose up -d

SOLR_TIMEOUT_WAIT=30
while [[ $(docker-compose exec solr /opt/solr/bin/solr status) =~ "No Solr nodes are running" ]]
do
  if [ $SOLR_TIMEOUT_WAIT -le 0 ]; then
    echo 'Solr never started :('
    break
  fi
  echo "Waiting for Solr..."
  sleep 1
  ((SOLR_TIMEOUT_WAIT--))
done

docker-compose exec solr /opt/solr/bin/solr create_core -c galileo-development -d /opt/solr/server/solr/configsets/galileo
docker-compose exec solr /opt/solr/bin/solr create_core -c galileo-test -d /opt/solr/server/solr/configsets/galileo
bundle config jobs $(nproc 2>/dev/null || sysctl -n hw.ncpu 2>/dev/null || getconf _NPROCESSORS_ONLN 2>/dev/null)
RAILS_ENV=development bundle exec rake db:setup
