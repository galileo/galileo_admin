# redis
if ! lsof -i:6379 | grep LISTEN; then
  ./redis-*/src/redis-server --daemonize yes --protected-mode no
fi

#Solr
if ! lsof -i:8983 | grep LISTEN; then
  ./solr-*/bin/solr start -c -force
fi

