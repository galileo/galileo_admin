#!/usr/bin/env bash

wget http://download.redis.io/releases/redis-5.0.5.tar.gz
tar -xvf redis-5.0.5.tar.gz
cd redis-5.0.5 || exit
make -j3
