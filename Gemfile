# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.2.2'

gem 'addressable', '~>2.8.0'
gem 'aws-sdk-s3', require: false
gem 'bootsnap', require: false
gem 'bootstrap', '~> 4.3.1'
gem 'bootstrap_form', '~> 4.2'
gem 'breadcrumbs_on_rails'
gem 'cancancan'
gem 'chosen-rails'
gem 'coffee-rails', '~> 5.0.0'
gem 'config'
gem 'devise', '~> 4.7'
gem 'devise_invitable'
gem 'diffy'
gem 'ebsco-eds'
gem 'exception_notification'
gem 'faraday'
gem 'filterrific'
gem 'font-awesome-rails'
gem 'hotwire-rails'
gem 'hiredis'
gem 'htmlentities'
gem 'httparty'
gem 'image_processing'
gem 'importmap-rails'
gem 'jbuilder', '~> 2.9.1'
gem 'jquery-rails', '>= 4.4.0'
gem 'mock_redis'
gem 'net-ftp'
gem 'nokogiri', '>= 1.13.6'
gem 'pagy'
gem 'paper_trail'
gem 'pg'
gem 'pg_search'
gem 'puma'
gem 'rails', '~> 7.0.8'
gem 'redis'
gem 'sass-rails'
gem 'slack-notifier'
gem "sentry-rails"
gem 'sentry-ruby'
gem 'stimulus-rails'
gem 'strip_attributes', '~> 1.11'
gem 'sunspot_rails'
gem 'turbo-rails'
gem 'uglifier', '>= 1.3.0'
gem 'webrick'

group :development do
  gem 'letter_opener'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara'
  gem 'capybara-screenshot'
  gem 'fabrication'
  gem 'faker'
  gem 'selenium-webdriver'
  gem 'simplecov'
end

group :development, :test do
  gem 'bullet'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails', '~> 4.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
