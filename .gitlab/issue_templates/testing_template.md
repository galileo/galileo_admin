----------------------------------------------------------------------
## Admin

----------------------------------------------------------------------
### Institutions

- [ ] https://admin.galileo.usg.edu/institutions
- [ ] Test k-12 view Overrides (ex NoveList)

----------------------------------------------------------------------
### Resources

- [ ] https://admin.galileo.usg.edu/resources

----------------------------------------------------------------------
### Contacts

- [ ] https://admin.galileo.usg.edu/contacts

----------------------------------------------------------------------
### Users

- [ ] https://admin.galileo.usg.edu/users

----------------------------------------------------------------------
### Subjects

- [ ] https://admin.galileo.usg.edu/subjects

----------------------------------------------------------------------
### Formats

- [ ] https://admin.galileo.usg.edu/formats

----------------------------------------------------------------------
### Passwords

- [ ] https://admin.galileo.usg.edu/passwords

----------------------------------------------------------------------
### Vendors

- [ ] https://admin.galileo.usg.edu/vendors

----------------------------------------------------------------------
### Institution Groups

- [ ] https://admin.galileo.usg.edu/institution_groups

----------------------------------------------------------------------
### Notify Groups

- [ ] https://admin.galileo.usg.edu/notify_groups

----------------------------------------------------------------------
### Reports

- [ ] https://admin.galileo.usg.edu/reports

----------------------------------------------------------------------
### Tools

- [ ] https://admin.galileo.usg.edu/tools

----------------------------------------------------------------------
### History

- [ ] https://admin.galileo.usg.edu/versions

----------------------------------------------------------------------
## Institutional

----------------------------------------------------------------------
### Institutions

- [ ] https://admin.galileo.usg.edu/institutions

----------------------------------------------------------------------
### Resources

- [ ] https://admin.galileo.usg.edu/resources

----------------------------------------------------------------------
### Help

- [ ] https://about.galileo.usg.edu/support/category/galileo_admin

----------------------------------------------------------------------
## Helpdesk

----------------------------------------------------------------------
### Institutions

- [ ] https://admin.galileo.usg.edu/institutions
- [ ] Bentos page (configured_bentos)
- [ ] Bento Edit pages
  - [ ] Change Name/description of existing bento
  - [ ] Re-order Bentos
    - [ ] Using drag/drop UI
    - [ ] Using advanced page 
  - [ ] Hide bento that is currently visible
  - [ ] Unhide bento that is currently hidden
  - [ ] Restore defaults at the view level (Restore View Defaults)
  - [ ] Restore defaults at the institution level (Restore All Defaults)
  - [ ] Create new bento
  - [ ] Create bento from "EDS Custom Resource". (ex: Gale Literature Resource Center)
  - [ ] Delete the newly created bentos
- [ ] Default Bentos
  - [ ] Bento Types
    - [ ] Create new bento type
  - [ ] Change default order of existing bento
  - [ ] Create new default bento



----------------------------------------------------------------------
### Contacts

- [ ] https://admin.galileo.usg.edu/contacts

----------------------------------------------------------------------
